<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Какие есть способы доставки у компании, тарифы перевозчиков и подробная информация.");
$APPLICATION->SetPageProperty("title", "Способы доставки и тарифы на нее");
$APPLICATION->SetTitle("Доставка");
?>
<?
$APPLICATION->IncludeComponent(
	"HB:static.page", 
	".default", 
	array(
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "26",
	),
	false
);
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>