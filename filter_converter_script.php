<?
require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";

define("NO_KEEP_STATISTIC", true);
define("NO_AGENT_CHECK", true);
CModule::IncludeModule("iblock");

$bs = new CIBlockSection;
$el = new CIBlockElement;
$props_array = array("GENRE" => 5, "THEME" => 1, "STYLE" => 2, "MATERIAL" => 4); // символьные коды свойств-справочников и соответствующие им ID инфоблоков-справочников

echo "Выполнение скрипта заблокировано";
/*
foreach ($props_array as $propsKey => $propsValue) { // создание разделов с именем и символьным кодом элементов инфоблоков-справочников либо обновление названий (и английских названий) текущих
    $arFilter = array("IBLOCK_ID" => $propsValue);
    $res = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID", "IBLOCK_ID", "CODE", "NAME", "ACTIVE", "SORT", "PROPERTY_NAME_EN", "DETAIL_PICTURE"));
    while ($element = $res->Fetch()) {
        if (!empty($element['CODE'])) {
            $sectFilter = array('IBLOCK_ID' => $element['IBLOCK_ID'], '=CODE' => $element['CODE']);
            $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $sectFilter, false, false, array("ID", "IBLOCK_ID"));
            if ($sect = $rsSect->Fetch()) {
                $arFieldsNew = array(
                    "NAME" => $element['NAME'],
                    "ACTIVE" => $element['ACTIVE'],
                    "CODE" => $element['CODE'],
                    "SORT" => $element['SORT'],
                    "DETAIL_PICTURE" => CFile::MakeFileArray($element["DETAIL_PICTURE"]),
                    "UF_NAME_EN" => $element['PROPERTY_NAME_EN_VALUE'],
                );
                $updateID = $bs->Update($sect["ID"], $arFieldsNew);
                $updateRes = ($updateID > 0);
                if (!$updateRes) {
                    echo 'Ошибка при обновлении раздела "' . $element['NAME'] . '"' . $bs->LAST_ERROR;
                } else {
                    echo "Раздел '" . $element['NAME'] . "' существует, обновлены рус. и англ. названия и изображение <br>";
                }
            } else {
                $arFieldsNew = array(
                    "IBLOCK_ID" => $propsValue,
                    "ACTIVE" => $element['ACTIVE'],
                    "NAME" => $element['NAME'],
                    "CODE" => $element['CODE'],
                    "SORT" => $element['SORT'],
                    "DETAIL_PICTURE" => CFile::MakeFileArray($element["DETAIL_PICTURE"]),
                    "UF_NAME_EN" => $element['PROPERTY_NAME_EN_VALUE'],
                );
                $ID = $bs->Add($arFieldsNew);
                $addRes = ($ID > 0);
                if (!$addRes) {
                    echo 'Ошибка при добавлении раздела "' . $element['NAME'] . '"' . $bs->LAST_ERROR;
                } else {
                    echo "Добавлен раздел '" . $element['NAME'] . "' ID: " . $ID . "<br>";
                }
            }
            unset($arFieldsNew);
        }
    }
    unset($arSelect, $arFilter, $sectFilter, $res, $element, $ID, $updateID, $addRes, $updateRes);
}
echo "<br>";

$elementCountSuccess = 0;
$elementCount = 0;

$arFilter = array("IBLOCK_ID" => 7);
$res = CIBlockElement::GetList(array(), $arFilter, false, false, array()); // обновление свойств-справочников у элементов инфоблока "работы"
while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $arProps = $ob->GetProperties();
    $error = false;

    $PROPS_ID = array(); // ID свойств-справочников - элементы
    $PROPS_NEW_ID = array(); // ID свойств-справочников - разделы

    foreach ($props_array as $propsKey => $propsValue) {
        $PROPS_ID[$propsKey] = array();
        if(is_array($arProps[$propsKey]['VALUE'])) {
            foreach ($arProps[$propsKey]['VALUE'] as $value) {
                $PROPS_ID[$propsKey][] = $value;
            }
        } else {
            $PROPS_ID[$propsKey][] = $arProps[$propsKey]['VALUE'];
        }
        if (!empty($PROPS_ID[$propsKey])) {
            $propFilter = array("IBLOCK_ID" => $propsValue, "=ID" => $PROPS_ID[$propsKey]);
            $propRes = CIBlockElement::GetList(array(), $propFilter, false, false, array("ID", "NAME", "CODE"));
            while ($propFields = $propRes->Fetch()) {
                if (!empty($propFields['CODE'])) {
                    $sectFilter = array('IBLOCK_ID' => $propsValue, '=CODE' => $propFields['CODE']);
                    $sectRs = CIBlockSection::GetList(array('left_margin' => 'asc'), $sectFilter, false, false, array("ID"));
                    while ($sectAr = $sectRs->Fetch()) {
                        $PROPS_NEW_ID[$propsKey][] = $sectAr['ID'];
                    }
                }
            }
        }
        if (!empty($PROPS_NEW_ID[$propsKey])) {
            if (CIBlockElement::SetPropertyValueCode($arFields['ID'], $propsKey . "_PARENT", $PROPS_NEW_ID[$propsKey])) {
                echo $propsKey . " работы '" . $arFields['NAME'] . "' обновлен<br>";
            } else {
                $error = true;
                echo "При обновлении " . $propsKey . " работы '" . $arFields['NAME'] . "' произошла ошибка<br>";
            }
        }
    }
    if (!$error) {
        $elementCountSuccess = $elementCountSuccess + 1;
    }
    $elementCount = $elementCount + 1;
    echo "<br>";
}
echo "Успешно обновлено " . $elementCountSuccess . " работ из " . $elementCount;
unset($arSelect, $arFilter, $PROPS_ID, $propFilter, $propRes, $sectFilter, $error);

$elementCountSuccess = 0;
$elementCount = 0;

$arFilter = array("IBLOCK_ID" => 6);
$res = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID", "IBLOCK_ID", "NAME", "PROPERTY_STYLE")); // обновление свойства "основной стиль автора" у элементов инфоблока "авторы"
while ($arFields = $res->Fetch()) {
    $error = false;
    $PROPS_NEW_ID = array(); // ID свойств-справочников - разделы

    if (!empty($arFields['PROPERTY_STYLE_VALUE'])) {
        $propFilter = array("IBLOCK_ID" => 2, "=ID" => $arFields['PROPERTY_STYLE_VALUE']);
        $propRes = CIBlockElement::GetList(array(), $propFilter, false, false, array("ID", "IBLOCK_ID", "CODE"));
        while ($propFields = $propRes->Fetch()) {
            if (!empty($propFields['CODE'])) {
                $sectFilter = array('IBLOCK_ID' => 2, '=CODE' => $propFields['CODE']);
                $sectRs = CIBlockSection::GetList(array('left_margin' => 'asc'), $sectFilter, false, false, array("ID"));
                while ($sectAr = $sectRs->Fetch()) {
                    $PROPS_NEW_ID["STYLE"][] = $sectAr['ID'];
                }
            }
        }
    }

    if (!empty($PROPS_NEW_ID["STYLE"])) {
        if (CIBlockElement::SetPropertyValueCode($arFields['ID'], "STYLE_PARENT", $PROPS_NEW_ID["STYLE"])) {
            echo "STYLE автора '" . $arFields['NAME'] . "' обновлен<br>";
        } else {
            $error = true;
            echo "При обновлении STYLE автора '" . $arFields['NAME'] . "' произошла ошибка<br>";
        }
    }
    if (!$error) {
        $elementCountSuccess = $elementCountSuccess + 1;
    }
    $elementCount = $elementCount + 1;
}
echo "Успешно обновлено " . $elementCountSuccess . " авторов из " . $elementCount;
*/

?>