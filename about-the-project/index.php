<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Использование сайта художниками и покупателями и для чего нужна площадка SwamiArt.");
$APPLICATION->SetPageProperty("title", "Что такое SwamiArt и как пользоваться сайтом");
$APPLICATION->SetTitle("О проекте");
?>
<?
$APPLICATION->IncludeComponent(
	"HB:static.page", 
	".default", 
	array(
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "26",
	),
	false
);
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>