var debugTimes = false;

/**
 * Global Variables
 */
var currentScreen = {
  update: function() {
    this.mobile = ($(window).innerWidth() < 768) ? true : false;
    this.tablet = ($(window).innerWidth() >= 768 && $(window).innerWidth() < 1280) ? true : false;
    this.desktop = ($(window).innerWidth() >= 1280) ? true : false;
  }
};
var currentOrientation = {
  update: function() {
    this.portrait = ($(window).innerWidth() <= $(window).innerHeight());
    this.landscape = ($(window).innerWidth() > $(window).innerHeight());

    if(this.portrait) {
      $('html').addClass('orientation-portrait').removeClass('orientation-landscape');
    }
    if(this.landscape) {
      $('html').addClass('orientation-landscape').removeClass('orientation-portrait');
    }
  }
};

/**
 * Viewport
 */
// var $metaViewport = $('meta[name="viewport"]');
// if(!$metaViewport.length) {
//   $metaViewport = $('<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">').appendTo($('head'));
// }
// function viewport() {
//   $metaViewport.attr('content', 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no');

//   // Книжная ориентация
//   if(window.screen.width / window.screen.height <= 1) {

//     if($(window).width() < 768) {
//       $metaViewport.attr('content', 'width=320');
//     } else {
//       $metaViewport.attr('content', 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no');
//     }

//   // Альбомная ориентация
//   } else {

//     // Мобилка
//     // if($(window).width() < 768) {
//     //   $metaViewport.attr('content', 'width=560');
//     // } else {
//       $metaViewport.attr('content', 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no');
//     // }

//   }

// }
// viewport();
// $(window).on('resize orientationchange', viewport);

/**
 * Document Ready
 */
$(document).ready(function() {

  /**
   * Media & Orientation Update
   */
  currentScreen.update();
  currentOrientation.update();
  setTimeout(function() {
    currentScreen.update();
    currentOrientation.update();
  }, 100);

  $(window).on('resize', function() {
    currentOrientation.update();
    currentScreen.update();
  });

});

/**
 * Validation
 */
if(typeof BX !== 'undefined' && BX.message('SITE_ID') == "en"){
  var langMessage = {
    required: "This field is required.",
    remote: "Please fix this field.",
    email: "Please enter a valid email address.",
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Please enter a valid number.",
    digits: "Please enter only digits.",
    equalTo: "Please enter the same value again.",
    maxlength: $.validator.format( "Please enter no more than {0} characters." ),
    minlength: $.validator.format( "Please enter at least {0} characters." ),
    rangelength: $.validator.format( "Please enter a value between {0} and {1} characters long." ),
    range: $.validator.format( "Please enter a value between {0} and {1}." ),
    max: $.validator.format( "Please enter a value less than or equal to {0}." ),
    min: $.validator.format( "Please enter a value greater than or equal to {0}." ),
    step: $.validator.format( "Please enter a multiple of {0}." )
  };
  var langMessageCustom = {
    phone: 'Please enter a valid phone number',
    images: 'Please add at least one image'
  };
} else {
  var langMessage = {
      required: "Заполните поле",
      remote: "Пожалуйста, введите правильное значение",
      email: "Введите правильный email",
      url: "Пожалуйста, введите корректный URL",
      date: "Пожалуйста, введите корректную дату",
      dateISO: "Пожалуйста, введите корректную дату в формате ISO",
      number: "Пожалуйста, введите число",
      digits: "Пожалуйста, вводите только цифры",
      creditcard: "Пожалуйста, введите правильный номер кредитной карты",
      equalTo: "Пароли не совпадают",
      extension: "Пожалуйста, выберите файл с правильным расширением",
      maxlength: $.validator.format( "Пожалуйста, введите не больше {0} символов" ),
      minlength: $.validator.format( "Пожалуйста, введите не меньше {0} символов" ),
      rangelength: $.validator.format( "Пожалуйста, введите значение длиной от {0} до {1} символов" ),
      range: $.validator.format( "Пожалуйста, введите число от {0} до {1}" ),
      max: $.validator.format( "Пожалуйста, введите число, меньшее или равное {0}" ),
      min: $.validator.format( "Пожалуйста, введите число, большее или равное {0}" ),
  };
  var langMessageCustom = {
    phone: 'Пожалуйста, введите корректный номер телефона',
    images: 'Пожалуйста, добавьте хотя бы одно изображение'
  };
}

$.extend( $.validator.messages, langMessage);

/**
 * Pluins Default
 */
$.fancybox.defaults.touch = false;
$.fancybox.defaults.mobile.clickContent = 'close';
$.fancybox.defaults.mobile.clickSlide = 'close';


$('[data-fancybox-modal]').each(function() {
  $(this).fancybox();
});

/**
 * Components
 */

@@include('../components/header/header.js');
@@include('../components/l-message/l-message.js');
@@include('../components/footer/footer.js');
@@include('../components/main-carousel/main-carousel.js');
@@include('../components/note-video/note-video.js');
@@include('../components/gallery/gallery.js');
@@include('../components/gallery-card/gallery-card.js');
@@include('../components/review-carousel/review-carousel.js');
@@include('../components/news/news.js');
@@include('../components/l-select/l-select.js');
@@include('../components/page-header/page-header.js');
@@include('../components/l-rangeslider/l-rangeslider.js');
@@include('../components/filter/filter.js');
@@include('../components/popup/popup.js');
@@include('../components/l-autocomplete/l-autocomplete.js');
@@include('../components/author/author.js');
@@include('../components/popular-filter/popular-filter.js');
@@include('../components/search/search.js');
@@include('../components/blog-detail/blog-detail.js');
@@include('../components/similar-articles/similar-articles.js');
@@include('../components/picture/picture.js');
@@include('../components/similar-pictures/similar-pictures.js');
@@include('../components/form/form.js');
@@include('../components/lk/lk.js');
@@include('../components/cart/cart.js');
@@include('../components/popup-location/popup-location.js');
@@include('../components/popup-select/popup-select.js');
