/**
 * Component: LK
 */

$(document).ready(function() {

    // Avatar
    $(document).on("change", ".js-lk-user-img-input", function(){
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('.js-lk-user-img').css('background-image', 'url('+e.target.result+')');
            }
            
            reader.readAsDataURL(this.files[0]);
        }
    });

    $('.js-stick').stick_in_parent({
        offset_top: 100,
        inner_scrolling: true,
    });


});