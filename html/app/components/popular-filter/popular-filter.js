/**
 * Component: Popular Filter
 */
$(document).ready(function() {

    $('.js-popular-filter-carousel').each(function(index, elem) {
        var $tools = $(elem).parents('.js-popular-filter').find('.js-popular-filter-tools');
        $(elem).slick({
            arrows: true,
            dots: false,
            prevArrow: '<button type="button" class="slick-arrow slick-prev"><svg class="icon icon-arrow"><use xlink:href="#icon-arrow"></use></svg></button>',
            nextArrow: '<button type="button" class="slick-arrow slick-next"><svg class="icon icon-arrow"><use xlink:href="#icon-arrow"></use></svg></button>',
            appendArrows: $tools,
            slidesToShow: 5,
            // autoplay: true,
            autoplaySpeed: 5000,
            pauseOnFocus: false,
            pauseOnHover: false,
            pauseOnDotsHover: false,
            swipeToSlide: true,
            responsive: [
                {
                    breakpoint: 1259,
                    settings: {
                        slidesToShow: 4,
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        variableWidth: false,
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 540,
                    settings: {
                        slidesToShow: 2,
                    }
                }
            ]
        });
    });

});