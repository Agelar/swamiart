/**
 * Component: Similar Pictures
 */
$(document).ready(function() {

    $('.js-similar-pictures').each(function(index, elem) {
        function adaptiveHeight() {
            if($(window).width() < 768) {
                var hh = 0;
                $(elem).find('.slick-slide.slick-active').each(function() {
                    var h = $(this).outerHeight();
                    if(h > hh) hh = h;
                });
                $(elem).animate({
                    height: hh
                }, 100);
            } else {
                $(elem).css('height', '');
            }
        }
        $(elem).on('init', function() {
            setTimeout(adaptiveHeight, 500);
        });
        $(elem).on('reInit afterChange', adaptiveHeight);
        $(elem).slick({
            arrow: true,
            dots: false,
            prevArrow: '<button type="button" class="slick-arrow slick-prev"><svg class="icon icon-arrow"><use xlink:href="#icon-arrow"></use></svg></button>',
            nextArrow: '<button type="button" class="slick-arrow slick-next"><svg class="icon icon-arrow"><use xlink:href="#icon-arrow"></use></svg></button>',
            appendArrows: $(elem).parents('.js-section').find('.js-section-tools').eq(0),
            slidesToShow: 4,
            autoplay: false,
            autoplaySpeed: 5000,
            pauseOnFocus: false,
            pauseOnHover: false,
            pauseOnDotsHover: false,
            adaptiveHeight: true,
            swipeToSlide: true,
            responsive: [
                {
                    breakpoint: 1259,
                    settings: {
                        slidesToShow: 2,
                        centerMode: true,
                        
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        centerMode: false,
                    }
                }
            ]
        });
        $(window).on('resize', adaptiveHeight);
    });

});