/**
 * Component: L-message
 */
$(document).ready(function() {

    $(document).on('click', '.js-l-message-close', function() {
        $(this).parents('.js-l-message').slideUp(300, function() {
            $(this).remove();
        });
    });

});