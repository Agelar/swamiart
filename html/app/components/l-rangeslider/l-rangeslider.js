/**
 * Componetns: L-RangeSlider
 */
$(document).ready(function() {

    $('.js-l-rangeslider').each(function(index, elem) {
        var $inputFrom = $(this).find('.js-l-rangeslider-from'),
            $inputTo = $(this).find('.js-l-rangeslider-to');
        var isFromTo = $inputFrom.length && $inputTo.length;

        var $input = $(this).find('.js-l-rangeslider-main');
        $input.ionRangeSlider({
            type: 'double',
            grid: $input.is('[data-values]'),
            skin: 'none',
            hide_min_max: $input.is('[data-values]'),
            hide_from_to: true,
            prettify_enabled: false,
            grid_snap: true,
            onStart: function() {
                setTimeout(function() {
                    rangeUpdate();
                }, 0);
            },
            onChange: function() {
                rangeUpdate();
            },
            onFinish: function(){
                rangeUpdate();
            },
            onUpdate: function() {
                rangeUpdate();
            },
        });

        function rangeUpdate() {
            if(isFromTo) {
                var data = $input.data('ionRangeSlider').result;
                $inputFrom.val(data.from);
                $inputTo.val(data.to);
            }
        }

        $inputFrom.on('input change', function(event) {

            var value = $(this).val().replace(/[^0-9]/ig, '');
            $(this).val(value);

            $input.data('ionRangeSlider').update({
                from: value
            })
        });
        $inputTo.on('input change', function(event) {

            var value = $(this).val().replace(/[^0-9]/ig, '');
            $(this).val(value);

            $input.data('ionRangeSlider').update({
                to: value
            })
        });

    });

    $(document).on('click', '.js-l-rangeslider .irs-grid-text', function() {
        var index = $(this).index('.irs-grid-text');
        var ion = $(this).parents('.js-l-rangeslider').find('.js-l-rangeslider-main').data('ionRangeSlider');
        var data = ion.result;

        var deltaFrom = Math.abs(index - data.from),
            deltaTo = Math.abs(index - data.to);
        console.log(index, deltaFrom, deltaTo);
        
        if(!deltaFrom || !deltaTo) return false;

        if(deltaFrom <= deltaTo) {
            ion.update({
                from: index
            });
        } else {
            ion.update({
                to: index
            });
        }

    });

});