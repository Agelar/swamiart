/**
 * Component: L-Autocomplete
 */
$(document).ready(function() {

    $('.js-l-autocomplete').each(function() {
        var $select = $(this).find('.js-l-autocomplete-select'),
            $options = $(this).find('.js-l-autocomplete-options'),
            $templateOption = $('<span class="l-autocomplete__option js-l-autocomplete-option"></span>');
        
        // $select.on('select2:open', function() {
        //     $('.select2-container .select2-dropdown').css('z-index', 1070);
        // });
        $select.select2({
            multiple: true,
            dropdownCss: {
                'z-index': '1070'
            }
        });

        function refresh() {
            $options.html('');
            var values = $select.val();
            for(var i = 0; i < values.length; i++) {
                var $option = $templateOption.clone();
                $option.attr('data-value', values[i]);
                var text = $select.find('option[value="'+values[i]+'"]').text();
                $option.text(text);
                $options.append($option);
            }
        }
        refresh();
        $select.on('change', refresh);

    });

    $(document).on('click', '.js-l-autocomplete-option', function() {
        var $select = $(this).parents('.js-l-autocomplete').find('.js-l-autocomplete-select');
        var values = $select.val(),
            value = $(this).attr('data-value');
        var index = values.indexOf(value);
        if (index !== -1) values.splice(index, 1);
        $select.val(values).trigger('change');
    });

    

});