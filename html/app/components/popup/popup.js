$(document).on("click",".popup__close",function(){
	$(this).closest('.popup').fadeOut(300);

	var scrollTop = $('html').scrollTop() || $('body').scrollTop() || $(document).scrollTop();
	$('html,body').css({
		'overflow':'',
		'padding-right':''
	});
	$('html, body').scrollTop(scrollTop);
	return false;
});
$(document).on("click",".js-popup-auth",function(){
	$(document).trigger('showPopup', ['auth']);
	return false;
});
$(document).on("click",".js-popup-reg",function(){
	$(document).trigger('showPopup', ['reg']);
	return false;
});
$(document).on("click",".js-popup-restore",function(){
	$(document).trigger('showPopup', ['restore']);
	return false;
});
    
$(document).on('showPopup', function (e, el) {
    $('.popup__content').fadeOut(300);
	setTimeout(function () {
		$(".popup").show();
		$('.popup__content--' + el).fadeIn(300);

		$('body').css('padding-right', window.innerWidth - $(window).width() + 'px');
		var scrollTop = $('html').scrollTop() || $('body').scrollTop() || $(document).scrollTop();
		$('html, body').css('overflow', 'hidden');
		$('html, body').scrollTop(scrollTop);
	}, 300);
	$('label.error').hide();
	$('.input.error').removeClass('error');

});


// validation
$('.popup__names .input').on('input', function() {
	this.value = this.value.replace(/[^А-Яа-яЁёA-Za-z]/ig, '');
});

$(document).on('formInit', function () {
	$('.popup-form-reg, .popup-form-auth, .popup__content-wrapper--restore').each(function () {
		$(this).validate({
			errorPlacement: function(error, element) {
				error.appendTo(element.parent());
			},
			submitHandler: function(form) {
				$(form).trigger('formSubmit');
				// form.submit();
			}
		});
	});
});

$(document).trigger('formInit');

$('.popup__container').on('click', function (event) {
	if(!$(event.target).closest('.popup__window').length) {
		$(this).closest('.popup').fadeOut(300, function() {
			$('html, body').css({'overflow': '', 'padding-right': ''});
		});
	}
});

// $('.popup__window').on('click', function (event) {
// 	event.stopPropagation();
// });