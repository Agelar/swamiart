$('.author__bio-more').on('click', function () {
	var $this = $(this);
	$this.hide(0);
	$this.closest('.author__bio').find('.author__bio-shown').addClass('is-opened').siblings('.author__bio-hidden').fadeIn(300);
});

// function moveAuthorActions() {
// 	if ($('.author__picture-action-inner').length && $(window).innerWidth() < 1280) {
// 		$('.author__picture-action-inner').appendTo('.author .container');
// 	}
// }

$('.author__stats').each(function (i, el) {
	var stats = new PerfectScrollbar(el);
	$(window).on('resize', function () {
		stats.update();
	});
	$('.author__stats').on('ps-scroll-x', function () {
		if (stats.reach.x === 'end') {
			$('.author__stats-first').fadeIn(300);
			$('.author__stats-last').fadeOut(300);
		} else if (stats.reach.x === 'start') {
			$('.author__stats-first').fadeOut(300);
			$('.author__stats-last').fadeIn(300);
		} else {
			$('.author__stats-first').fadeIn(300);
			$('.author__stats-last').fadeIn(300);
		}
	});
	if (stats.scrollbarXActive) {
		$('.author__stats-last').show(0);
	}
});

// $(window).on('load', function () {
// 	moveAuthorActions();
// });
// $(window).on('resize', function () {
// 	moveAuthorActions();
// });