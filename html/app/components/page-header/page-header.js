/**
 * Component: Page Header
 */
$('[data-parallax-img]').each(function(){
    var img = $(this);
    var imgParent = $(this).parent();
    function parallaxImg () {

        var speed = img.data('speed');
        var winY = $(this).scrollTop();
        var winH = $(this).height();

        var percent = parseFloat(speed) ? winY / (winH / speed) * 100 : 0;
        

        img.css({
            top: '0%',
            transform: 'translate(-50%, -' + percent + '%)'
        });
    }
    $(document).on({
        scroll: function () {
        parallaxImg();
        }, ready: function () {
        parallaxImg();
        }
    });
});