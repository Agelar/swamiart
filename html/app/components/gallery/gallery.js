/**
 * Component: Gallery
 */
$(document).ready(function() {

    function galleryInit() {
        var $gallery = $('.js-gallery');
        $gallery.masonry({
            // columnWidth: 280,
            itemSelector: '.js-gallery-item',
            gutter: '.js-gallery-gutter-sizer',
            horizontalOrder: currentScreen.mobile ? false : true,
            stamp: '.js-gallery-stamp',
        });
        $gallery.imagesLoaded().progress(function() {
            $('.js-gallery').masonry('layout');
        });
    }

    galleryInit();

    $(document).on('gallery:reinit', '.js-gallery', function() {
        if($('.js-gallery').data('masonry')) {
            $('.js-gallery').masonry('destroy');
            galleryInit();
        }
    });

});