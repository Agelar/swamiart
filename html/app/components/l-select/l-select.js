/**
 * Componenet: L-Select
 */
$(document).ready(function() {

    $('.js-l-select-input').select2({
        minimumResultsForSearch: Infinity,
    });
    
});