/**
 * Component: Picture
 */

 $(document).ready(function() {

    $('.js-picture-carousel-main').slick({
        arrows: true,
        dots: false,
        autoplay: false,
        slidesToShow: 1,
        prevArrow: '<button type="button" class="slick-prev slick-arrow"><svg class="icon arrow_smart"><use xlink:href="#arrow_smart"></use></svg></button>',
        nextArrow: '<button type="button" class="slick-next slick-arrow"><svg class="icon arrow_smart"><use xlink:href="#arrow_smart"></use></svg></button>',
        adaptiveHeight: true,
    });

    $('.js-picture-carousel-main').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        $('.js-picture-carousel-aside').slick('slickGoTo', nextSlide);
    });
    $('.js-picture-carousel-aside').on('swipe', function(event, slick) {
        var index = parseInt($(this).find('.slick-slide.slick-current').attr('data-slick-index'));
        $('.js-picture-carousel-main').slick('slickGoTo', index);
    });

    $(document).on('click', '.js-picture-carousel-aside .js-picture-carousel-slide', function() {
        var index = parseInt($(this).parents('.slick-slide').attr('data-slick-index'));
        $('.js-picture-carousel-main').slick('slickGoTo', index);
    });

    $('.js-picture-carousel-aside').slick({
        arrows: false,
        dots: false,
        autoplay: false,
        slidesToShow: 5,
        vertical: true,
        swipeToSlide: true,
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 5,
                    vertical: false,
                    // centerMode: true,
                    focusOnSelect: true
                }
            }
        ]
    });


    function bgToHref(val) {
        var href = val.replace('url', '');
        href = href.replace(/[\(\)\'\"]/ig, '');
        return href;
    }
    $(document).on('click', '.js-picture-zoom', function() {

        var $slides = $('.js-picture-carousel-main').find('.slick-slide').not('.slick-cloned').find('.js-picture-carousel-slide img');
        var gallery = [];
        $slides.each(function() {
            var img = bgToHref($(this).attr('src'));
            gallery.push({
                src: img
            });
        });

        var index = parseInt($('.js-picture-carousel-main').find('.slick-slide.slick-active').attr('data-slick-index'));
        $.fancybox.open(gallery, {
            // toolbar: false,
            buttons: ['close'],
            onInit: function(instance, current) {
                instance.jumpTo(index, 0);
            }
        });
    });

    $(document).on('click', '.js-picture-accordeon-head', function() {
        if($(this).is('.is-active')) {
            $(this).removeClass('is-active').next('.js-picture-accordeon-main').slideUp();
        } else {
            $(this).addClass('is-active').next('.js-picture-accordeon-main').slideDown();
        }
    });


 });