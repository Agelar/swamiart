/**
 * Component: Cart
 */
$(document).ready(function() {
    $('.js-cart-list-head').on('click', function() {
        $(this).toggleClass('is-active');
        $(this).siblings('.js-cart-list-main').stop().slideToggle(300, function() {
            $(document.body).trigger("sticky_kit:recalc");
        });
    });

    $(document).on('click', '.js-cart-item-tools-head', function() {
        $(this).siblings('.js-cart-item-tools-main').fadeToggle();
    });

    $(document).on('click', function(event) {
        var $tools = $(event.target).closest('.js-cart-item-tools');
        if(!$tools.length) {
            $('.js-cart-item-tools-main').fadeOut();
        } else {
            $('.js-cart-item-tools').not($tools).find('.js-cart-item-tools-main').fadeOut();
        }
    });
});