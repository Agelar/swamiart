/**
 * Component: Main Carousel
 */
$(document).ready(function() {
    $('.js-main-carousel').slick({
        arrows: false,
        dots: true,
        autoplay: true,
        autoplaySpeed: 5000,
        pauseOnFocus: false,
        pauseOnHover: false,
        pauseOnDotsHover: false,
    });
});