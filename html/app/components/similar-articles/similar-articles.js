$(document).ready(function() {

    $('.js-similar-articles').each(function(index, elem) {
        $(elem).slick({
            prevArrow: '<button type="button" class="slick-arrow slick-prev"><svg class="icon icon-arrow"><use xlink:href="#icon-arrow"></use></svg></button>',
            nextArrow: '<button type="button" class="slick-arrow slick-next"><svg class="icon icon-arrow"><use xlink:href="#icon-arrow"></use></svg></button>',
            appendArrows: $(elem).parents('.js-section').find('.js-section-tools').eq(0),
            slidesToShow: 3,
            swipeToSlide: true,
            responsive: [
                {
                    breakpoint: 1259,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        });
    });

});