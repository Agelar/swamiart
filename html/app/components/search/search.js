$(document).on('click', '.search__switch-btn:not(.search__switch-btn--current)', function (e) {
	e.preventDefault();
	var $this = $(this),
		data = $this.data('tab');
	
	$('.search__switch-btn--current').removeClass('search__switch-btn--current');
	$this.addClass('search__switch-btn--current');
	
	$('.search-tab--visible').hide(0).removeClass('search-tab--visible');
	$('.search-tab').each(function () {
		if ($(this).data('tab') === data) {
			$(this).fadeIn(300);
			$(this).addClass('search-tab--visible');
			data === 'pictures' && $('.js-gallery').masonry();
		}
	});
})