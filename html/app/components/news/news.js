/**
 * Component: News
 */
$(document).ready(function() {

    $('.js-news').each(function(index, elem) {
        $(elem).slick({
            arrow: true,
            dots: false,
            prevArrow: '<button type="button" class="slick-arrow slick-prev"><svg class="icon icon-arrow"><use xlink:href="#icon-arrow"></use></svg></button>',
            nextArrow: '<button type="button" class="slick-arrow slick-next"><svg class="icon icon-arrow"><use xlink:href="#icon-arrow"></use></svg></button>',
            appendArrows: $(elem).parents('.js-section').find('.js-section-tools').eq(0),
            slidesToShow: 2,
            // autoplay: true,
            autoplaySpeed: 5000,
            pauseOnFocus: false,
            pauseOnHover: false,
            pauseOnDotsHover: false,
            swipeToSlide: true,
            responsive: [
                {
                    breakpoint: 1259,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        });
    });

});