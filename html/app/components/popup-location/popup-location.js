$(document).on("click",".js-popup-location",function(){
    $('.js-select').toggleClass('is-active');
    $('.js-city').removeClass('is-active');
    $('.js-menu-touch-burger').trigger('click');
    $('.js-select .ps').trigger('ps:update');
});

$(document).on("click",".js-city-close",function(){
    $('.js-city').removeClass('is-active');
});

$(document).on("click",".js-location-close-select",function(){
    $('.js-city').removeClass('is-active');
});

$(document).on('click', function(event) {
    if(!$(event.target).closest('.js-popup-location, .js-city, .js-l-message').length){
        $(".js-city").removeClass('is-active');
    }
});

$(document).on('locationOpen', function(){
    $('.js-city').addClass('is-active');
});

if($('html').hasClass('is-layout')){
    $(document).trigger('locationOpen');
};
