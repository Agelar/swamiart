/**
 * Component: Header
 */
$(document).ready(function() {

    $(document).on('click', '.js-menu-touch-burger', function() {
        var $tool = $(this).parent();
        $tool.toggleClass('is-open');
        $('.js-tool-search').removeClass('is-open');
        $('.js-tool-user').removeClass('is-open');
        return false;
    });
    $(document).on('click', '.js-tool-search-link', function() {
        var $tool = $(this).parent();
        $tool.toggleClass('is-open');
        $tool.find('input').focus();
        $('.js-menu-touch').removeClass('is-open');
        $('.js-tool-user').removeClass('is-open');
        return false;
    });
    $(document).on('click', '.js-tool-user-link', function() {
        $(this).parent().toggleClass('is-open');
        $('.js-tool-search').removeClass('is-open');
        $('.js-menu-touch').removeClass('is-open');
        return false;
    });

    $(document).on('click', function(event) {
        if(!$(event.target).closest('.js-tool-search').length) {
            $('.js-tool-search').removeClass('is-open');
        }
        if(!$(event.target).closest('.js-menu-touch').length) {
            $('.js-menu-touch').removeClass('is-open');
        }
        if(!$(event.target).closest('.js-tool-user').length) {
            $('.js-tool-user').removeClass('is-open');
        }
    });

});