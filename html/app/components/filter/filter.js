/**
 * Component: Filter
 */
$(document).ready(function() {

    $(document).on('click', '.js-filter-tabs-head-item', function() {
        var index = $(this).index('.js-filter-tabs-head-item');
        var $dropdown = $(this).parents('.js-filter-tabs').find('.js-filter-tabs-main-item').eq(index);

        $(this).siblings().removeClass('is-active');
        $(this).addClass('is-active');

        $dropdown.siblings().hide(0);
        if($(window).width() < 768) {
            $dropdown.css('display', 'block');
        } else {
            $dropdown.fadeIn();
        }

        return false;
    });

    var $headItems = $('.js-filter-tabs-head-item');
    if(!$headItems.filter('.is-active').length) {
        $headItems.eq(0).trigger('click');
    }

    $(document).on('click', '.js-filter-toggle', function () {
        var $tabs = $(this).parents('.js-filter').find('.js-filter-tabs');
        if($(this).is('.is-active')) {
            $tabs.slideUp(300);
            $('.js-filter-toggle').removeClass('is-active');
            $(this).parents('.js-filter-buttons').removeClass('is-active');
        } else {
            $tabs.slideDown(300);
            $('.js-filter-toggle').addClass('is-active');
            $(this).parents('.js-filter-buttons').addClass('is-active');
        }
    });

    $(document).on('click', '.js-filter-head', function() {
        if($(this).is('.is-active')) {
            $(this).siblings('.js-filter-main').slideUp(300);
            if($(this).parents('.js-filter-fixed.is-active').length) {
                $(this).parents('.js-filter-wrapper').animate({
                    'height': $(this).innerHeight(),
                }, 300);
            } else {
                $(this).parents('.js-filter-wrapper').css('height', '');
            }
            $(this).removeClass('is-active');
            if($(this).parents('.js-filter-fixed').length) {
                var scroll = $('html').scrollTop() || $('body').scrollTop() || $(document).scrollTop();
                $('html').css('overflow', '');
                $('html,body').scrollTop(scroll);
            }
        } else {
            $(this).siblings('.js-filter-main').slideDown(300);
            $(this).addClass('is-active');
            if($(this).parents('.js-filter-fixed.is-active').length) {
                var scroll = $('html').scrollTop() || $('body').scrollTop() || $(document).scrollTop();
                $('html').css('overflow', 'hidden');
                $('html,body').scrollTop(scroll);
            }
        }
    });


    $(document).on('scroll', function() {
        var $filterWrapper = $('.js-filter-wrapper'),
            $filter = $('.js-filter-fixed');

        if($filterWrapper.length && $filter.length) {
            var scrollTop = $('html').scrollTop() || $('body').scrollTop() || $(document).scrollTop();

            if(scrollTop + $('.header').innerHeight() > $filterWrapper.offset().top) {
                $filterWrapper.css('height', $filter.innerHeight());
                $filter.addClass('is-active');
                if($filter.find('.js-filter-head.is-active').length) {
                    $('html').css('overflow', 'hidden');
                    $('html,body').scrollTop(scrollTop);
                }
            } else {
                $filter.removeClass('is-active');
                $filterWrapper.css('height', '');
            }
        }
    });
    
    $(document).on('click', '.js-filter-hide', function () {
        var $filter = $(this).parents('.js-filter');

        var $toggle = $filter.find('.js-filter-toggle');
        var $tabs = $filter.find('.js-filter-tabs');
        $tabs.slideUp(300);
        $toggle.removeClass('is-active');
        $toggle.parents('.js-filter-buttons').removeClass('is-active');
        
        $filter.find('.js-filter-head.is-active').trigger('click');

        var $galleryStart = $('.js-gallery-start');
        if($galleryStart.length) {
            $('html, body').scrollTop($galleryStart.offset().top - 100);
        }
    });

});