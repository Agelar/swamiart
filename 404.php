<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>

<div class="missing-page">
	<div class="missing-page__wrap">
		<div class="missing-page__rec">
			<div class="missing-page__pic">
				<img class="missing-page__img" src="<?= SITE_TEMPLATE_PATH ?>/assets/images/dali-404.png" alt="" />
			</div>
			<div class="missing-page__desc">
				<div class="missing-page__title">Картина не найдена</div>
				<form class="missing-page__search" action="/search/">
					<input class="missing-page__search-input" type="text" name="q" placeholder="Найти" required="required" />
					<button class="missing-page__search-btn" type="submit">
						<svg class="icon icon-search-w">
							<use xlink:href="#icon-search"></use>
						</svg>
					</button>
				</form>
				<a class="missing-page__btn" href="/">На главную</a>
			</div>
		</div>
	</div>
</div>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>