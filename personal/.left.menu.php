<?
$aMenuLinks = Array(
	Array(
		"Мои данные", 
		"/personal/my-details/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Мои заказы", 
		"/personal/order-list/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Мои работы", 
		"/personal/work-list/", 
		Array(), 
		Array("ACCSESS_GROUP"=>"5"), 
		"" 
	),
	Array(
		"Оценить работы", 
		"/personal/work-expert-list/", 
		Array(), 
		Array("ACCSESS_GROUP"=>"10"), 
		"" 
	),
	Array(
		"Стать автором", 
		"#", 
		Array(), 
		Array("ACCSESS_GROUP"=>"6", "ITEM_CLASS"=>"js-set-author-group"), 
		"" 
	),
	Array(
		"Изменить пароль", 
		"/personal/change-password/", 
		Array(), 
		Array("GROUP"=>"ONE"), 
		"" 
	),
	Array(
		"Уведомления и подписки", 
		"/personal/notifications/", 
		Array(), 
		Array("GROUP"=>"ONE"), 
		"" 
	),
	Array(
		"Выход", 
		"?logout=yes", 
		Array(), 
		Array("GROUP"=>"TWO", "ICON"=>"#icon-logout", "CLASS"=>"icon-logout"), 
		"" 
	)
);
?>