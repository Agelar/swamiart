<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Добавление работы");
global $USER;
if(!$USER->IsAuthorized()) LocalRedirect("/");
$authorId = HB\helpers\AuthorHelpers::getAuthorId();
if(!$authorId) LocalRedirect("my-details");
?>
<div class="section section_gray section_lk">
    <div class="container">
        <div class="lk">
            <div class="lk__aside">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "lk",
                    Array(
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "left",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => array(""),
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "left",
                        "USE_EXT" => "Y"
                    )
                );?>
            </div>
            <div class="lk__main">
                <?$APPLICATION->IncludeComponent("HB:work.add", "", Array(
                    
                ),
                false
                );?>
            </div>
        </div>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>