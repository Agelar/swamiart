<?php
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
\Bitrix\Main\Loader::IncludeModule("iblock");
\Bitrix\Main\Loader::IncludeModule("hb.site");
use HB\helpers\Helpers;
global $USER;

function rus2translit($string)
{
    $converter = array(
        'а' => 'a',	'б' => 'b',	'в' => 'v',
        'г' => 'g',	'д' => 'd',	'е' => 'e',
        'ё' => 'e',	'ж' => 'zh',	'з' => 'z',
        'и' => 'i',	'й' => 'y',	'к' => 'k',
        'л' => 'l',	'м' => 'm',	'н' => 'n',
        'о' => 'o',	'п' => 'p',	'р' => 'r',
        'с' => 's',	'т' => 't',	'у' => 'u',
        'ф' => 'f',	'х' => 'h',	'ц' => 'c',
        'ч' => 'ch',	'ш' => 'sh',	'щ' => 'sch',
        'ь' => "_",	'ы' => 'y',	'ъ' => "_",
        'э' => 'e',	'ю' => 'yu',	'я' => 'ya',

        'А' => 'A',	'Б' => 'B',	'В' => 'V',
        'Г' => 'G',	'Д' => 'D',	'Е' => 'E',
        'Ё' => 'E',	'Ж' => 'Zh',	'З' => 'Z',
        'И' => 'I',	'Й' => 'Y',	'К' => 'K',
        'Л' => 'L',	'М' => 'M',	'Н' => 'N',
        'О' => 'O',	'П' => 'P',	'Р' => 'R',
        'С' => 'S',	'Т' => 'T',	'У' => 'U',
        'Ф' => 'F',	'Х' => 'H',	'Ц' => 'C',
        'Ч' => 'Ch',	'Ш' => 'Sh',	'Щ' => 'Sch',
        'Ь' => "_",	'Ы' => 'Y',	'Ъ' => "_",
        'Э' => 'E',	'Ю' => 'Yu',	'Я' => 'Ya',
    );
    return strtr($string, $converter);
}

if(!$USER->IsAuthorized()){
    exit();
}
else{
    $userId = $USER->GetID();
}



$workId = Helpers::decode($_GET['work_id'], $userId);
$fileId = Helpers::decode($_GET['file_id'], $userId);

if(isset($workId) && isset($fileId)){
    if(is_numeric($workId) && is_numeric($fileId)) {

        $rsDigitalCopy = CIBlockElement::GetList(array(), array("IBLOCK_ID"=> IBLOCK_DIGITAL_COPIES_USERS_ID, 'PROPERTY_USER' => $userId, 'PROPERTY_WORK' => $workId), false, false, array('ID'));
        if($arDigitalCopy = $rsDigitalCopy->GetNext()){

            $F_ID=$fileId;
            $rsFile = CFile::GetByID($F_ID);
            $arFile = $rsFile->Fetch();
            $fName = trim($arFile["ORIGINAL_NAME"]);

            // далее от греха подальше нужно провести транслитерацию русских символов на английские
            $fName=rus2translit($fName);
            $fName = str_replace(" ","_",$fName);
            //определяем тип файла
            $c_Type=$arFile["CONTENT_TYPE"];

            // опредеяем путь к файлу
            $file = CFile::GetPath($F_ID);
            $file=$_SERVER['DOCUMENT_ROOT'].$file;

            //проверяем, а есть ли вообще этот файл
            if(!file_exists($file))
                echo "Ошибка: файл не найден.";
            else
            {
                // Set headers
                header("Cache-Control: public");
                header("Content-Description: File Transfer");
                header("Content-Disposition: attachment; filename=".iconv('UTF-8', 'WINDOWS-1251', $fName));
                header("Content-Type: $c_Type");
                header("Content-Transfer-Encoding: binary");
                // Read the file from disk
                ob_clean();
                flush();
                echo file_get_contents($file);
            }
        }
        else{
            print 'Недостаточно прав';
            exit();
        }
        


        
    }
    else echo "Ошибка: файл не найден";
}
else echo "Ошибка: Не указан ID файла";