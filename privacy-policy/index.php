<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Условия политики конфиденциальности сайта");
$APPLICATION->SetPageProperty("title", "Политика конфиденциальности сайта");
$APPLICATION->SetTitle("Политика конфиденциальности");
?>
<?
$APPLICATION->IncludeComponent(
	"HB:static.page", 
	".default", 
	array(
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "26",
	),
	false
);
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>