<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("My details");
?>
<div class="section section_gray section_lk">
    <div class="container">
        <div class="lk">
            <div class="lk__aside">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "lk",
                    Array(
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "left",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => array(""),
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "left",
                        "USE_EXT" => "Y"
                    )
                );?>
            </div>
            <div class="lk__main">
            <?$APPLICATION->IncludeComponent(
				"HB:lk.user", 
				".default", 
				array(
					"COMPONENT_TEMPLATE" => ".default",
					"MAIN_INFO" => array(
						0 => "UF_NAME_EN",
						1 => "UF_LAST_NAME",
						4 => "NAME",
						5 => "LAST_NAME",
						2 => "PERSONAL_PHONE",
						3 => "EMAIL",
						6 => "UF_EXTRA_PHONE",
					),
					"EXTRA_INFO" => array(
						0 => "PERSONAL_BIRTHDAY",
						1 => "PERSONAL_GENDER",
						2 => "PERSONAL_COUNTRY",
						3 => "PERSONAL_CITY",
						7 => "UF_PERSONAL_CITY_ID",
						6 => "UF_STYLE",
						4 => "UF_ABOUT",
						5 => "UF_ABOUT_EN",
					),
					"ADDRESS_INFO" => array(
						0 => "UF_CITY",
						1 => "UF_STREET",
						2 => "UF_HOUSE",
						3 => "UF_APARTAMENT",
						4 => "UF_CITY_ID",
					)
				),
				false
			);?>
            </div>
        </div>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>