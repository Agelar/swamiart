<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Описание всех условий возврата товара надлежащего качества.");
$APPLICATION->SetPageProperty("title", "Условия возврата товара надлежащего качества");
$APPLICATION->SetTitle("Возвраты");
?>
<?
$APPLICATION->IncludeComponent(
	"HB:static.page", 
	".default", 
	array(
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "26",
	),
	false
);
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>