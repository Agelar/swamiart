<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Loader;
use \Bitrix\Main\Application;
use \Bitrix\Main\Context;
use Bitrix\Main\Localization\Loc;
use HB\userEntity\Confirm;

Loc::loadMessages(__FILE__);

class ConfirmEmailComponent extends CBitrixComponent{

    function onPrepareComponentParams($arParams) {
        return $this->arParams;
    }

    function executeComponent(){
        if($this->request["CONFIRM_HASH"] && $this->request["USER_ID"]){
            $this->arResult["CONFIRM"] = Confirm::checkHash($this->request["USER_ID"],$this->request["CONFIRM_HASH"]) ? "Y" : "N";
        }

        if($this->arResult["CONFIRM"]){
            Confirm::confirmEmail($this->request["USER_ID"]);
        }else{
            LocalRedirect("/");
        }
        
        $this->includeComponentTemplate();   
    }
}