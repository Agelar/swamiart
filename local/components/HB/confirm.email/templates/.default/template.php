<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arResult["CONFIRM"] == "Y"):?>
    <div class="container access-denied">
        <div class="blog-detail__subheading">
            <p><?=GetMessage("CONFIRM_MESSAGE")?></p>
        </div>
    </div>
<?endif;?>