<?php
$MESS['Доставка с проверкой картины'] = 'Delivery with picture check';
$MESS['Доставка осуществляется в Москву на проверку, после чего доставка осуществляется клиенту'] = 'Delivery is carried out to Moscow for inspection, after which delivery is carried out to the client';
$MESS['Стандартная доставка'] = 'Standard shipping';
$MESS['Доставка от автора к покупателю'] = 'Delivery from the author to the buyer';
$MESS['PayPal'] = 'PayPal';
$MESS['Сбербанк'] = 'Sberbank';
$MESS['Описание сбербанка'] = 'For payment (input of your card details) you will be redirected to the payment gateway of PJSC SBERBANK. Compound
with the payment gateway and the transfer of information is carried out in a secure mode using the protocol
SSL encryption. If your bank supports the technology of secure online payments Verified
By Visa or MasterCard SecureCode for making a payment you may also need to enter a special password.
This site supports 256-bit encryption. Confidentiality of personal information provided
provided by PJSC SBERBANK. The entered information will not be provided to third parties except
stipulated by the legislation of the Russian Federation. Bank card payments are carried out in strict
compliance with the requirements of payment systems MIR, Visa Int. and MasterCard Europe Sprl.';