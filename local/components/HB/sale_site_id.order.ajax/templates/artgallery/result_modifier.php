<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use HB\helpers\Helpers;
use HB\helpers\AuthorHelpers;
use HB\helpers\LangHelpers;
use Bitrix\Main\Localization\Loc;
/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */

$component = $this->__component;
$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);

global $USER;
if($USER->IsAuthorized()){
    $arSelect = array("UF_*");
    $arResult['USER'] = CUser::GetList(($by="id"), ($order="desc"), array("ID" => $USER->GetID()),array("SELECT" => $arSelect, "FIELDS" =>$arSelect))->fetch();
    $arResult['USER'] = LangHelpers::getTranslationUser($arResult['USER']);
}


//print_r($arResult['JS_DATA']);

foreach($arResult['JS_DATA']['GRID']['ROWS'] as $workId => &$arWork){
    $work = Helpers::getElementList($arWork['data']['PRODUCT_ID'])[$arWork['data']['PRODUCT_ID']];
    $arWork['data']['NAME'] = $work['NAME'];
    $author = AuthorHelpers::getAuthors($arWork['data']['PROPERTY_AUTHOR_VALUE'])[$arWork['data']['PROPERTY_AUTHOR_VALUE']];
    $arWork['columns']['PROPERTY_AUTHOR_VALUE']['0']['value_format'] = '<a href="'.$author['DETAIL_PAGE_URL'].'">'.$author['NAME'].'</a>';
}

foreach($arResult['JS_DATA']['DELIVERY'] as $deliveryId => &$arDelivery){
    $arDelivery['NAME'] = Loc::getMessage($arDelivery['NAME']);
    $arDelivery['DESCRIPTION'] = Loc::getMessage($arDelivery['DESCRIPTION']);
}

foreach($arResult['PAY_SYSTEM'] as $payId => &$arPay){
    $arPay['NAME'] = Loc::getMessage($arPay['NAME']);
    $arPay['DESCRIPTION'] = Loc::getMessage($arPay['DESCRIPTION']);
}