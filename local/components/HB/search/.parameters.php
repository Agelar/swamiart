<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

\Bitrix\Main\Loader::includeModule('iblock');

$arTypesEx = CIBlockParameters::GetIBlockTypes(array("-"=>" "));

$arIBlocksPain = array();
$arIBlocksBlog = array();
$db_iblock = CIBlock::GetList(array("SORT"=>"ASC"), array("TYPE" => ($arCurrentValues["IBLOCK_TYPE_PAIN"]!="-"?$arCurrentValues["IBLOCK_TYPE_PAIN"]:"")));
while($arRes = $db_iblock->Fetch()) {
	$arIBlocksPain[$arRes["ID"]] = "[".$arRes["ID"]."] ".$arRes["NAME"];
}

$db_iblock = CIBlock::GetList(array("SORT"=>"ASC"), array("TYPE" => ($arCurrentValues["IBLOCK_TYPE_BLOG"]!="-"?$arCurrentValues["IBLOCK_TYPE_BLOG"]:"")));
while($arRes = $db_iblock->Fetch()) {
	$arIBlocksBlog[$arRes["ID"]] = "[".$arRes["ID"]."] ".$arRes["NAME"];
}
$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"AJAX_MODE" => array(),
		"IBLOCK_TYPE_PAINT" => array(
			"PARENT" => "BASE",
			"NAME" => "Тип инфоблока для картин",
			"TYPE" => "LIST",
			"VALUES" => $arTypesEx,
			"DEFAULT" => "",
			"REFRESH" => "Y",
		),
		"IBLOCK_ID_PAINT" => array(
			"PARENT" => "BASE",
			"NAME" => "Инфоблок для картин",
			"TYPE" => "LIST",
			"VALUES" => $arIBlocksPain,
			"DEFAULT" => '',
			"ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "Y",
		),
		"IBLOCK_TYPE_BLOG" => array(
			"PARENT" => "BASE",
			"NAME" => "Тип инфоблока для блога",
			"TYPE" => "LIST",
			"VALUES" => $arTypesEx,
			"DEFAULT" => "",
			"REFRESH" => "Y",
		),
		"IBLOCK_ID_BLOG" => array(
			"PARENT" => "BASE",
			"NAME" => "Инфоблок для блога",
			"TYPE" => "LIST",
			"VALUES" => $arIBlocksBlog,
			"DEFAULT" => '',
			"ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "Y",
		),
		"COUNT_PAINT" => array(
			"PARENT" => "BASE",
			"NAME" => "Количество Картин на одной странице",
			"TYPE" => "STRING",
			"DEFAULT" => "10",
		),
		"COUNT_BLOG" => array(
			"PARENT" => "BASE",
			"NAME" => "Количество Статей на одной странице",
			"TYPE" => "STRING",
			"DEFAULT" => "12",
		),
	),
);
CIBlockParameters::AddPagerSettings(
	$arComponentParameters,
	"",
	true, //$bDescNumbering
	true, //$bShowAllParam
	true, //$bBaseLink
	false
);
?>