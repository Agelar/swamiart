<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Loader;
use \Bitrix\Main\Application;
use \Bitrix\Main\Context;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Engine\ActionFilter;
Loc::loadMessages(__FILE__);

class SerachComponent extends CBitrixComponent{
    private $paintSecton = "pictures";
    private $articlesSecton = "articles";

    function onPrepareComponentParams($arParams) {
        return $arParams;
    }
    private function search($query, $iblock, $pageCount, $section){
        global $APPLICATION;
        $result = array();
        $arFilter = array(
            "QUERY" => $query,
            "SITE_ID" => SITE_ID,
            "!ITEM_ID" => "S%", 
            "MODULE_ID" => "iblock",
            "PARAM2" => $iblock,
        );
        
        $obSearch = new CSearch();
        $obSearch->Search($arFilter,array("CUSTOM_RANK"=>"DESC", "RANK"=>"DESC",));
        if (!$obSearch->selectedRowsCount()) {
            $obSearch->Search($arFilter,array("CUSTOM_RANK"=>"DESC", "RANK"=>"DESC",),array('STEMMING' => false));
        }
        $obSearch->NavStart($pageCount,false);
        while($res = $obSearch->NavNext()){
            $result["LIST"][] = $res["ITEM_ID"];
        }
        
        $result["COUNT"] = $obSearch->NavRecordCount;
        $navComponentObject = null;
        if($obSearch->NavPageCount > 1){
            $result["NAV_STRING"] = $obSearch->GetPageNavStringEx($navComponentObject, "", $this->arParams["PAGER_TEMPLATE"], true, null, array("BASE_LINK"=>$APPLICATION->GetCurPage(false) . "?q=" . $this->request["q"] . "&section=" . $section));
        }
        return $result;
    }

    function executeComponent(){
        Loader::IncludeModule("search");
        Loader::IncludeModule("hb.site");
        if($this->request["q"]){
            $this->arResult["SEARCH_QUERY"] = $this->request["q"];
            $this->arResult["PAINT"] = $this->search($this->request["q"], $this->arParams["IBLOCK_ID_PAINT"], $this->arParams["COUNT_PAINT"],$this->paintSecton);
            $this->arResult["BLOG"] = $this->search($this->request["q"], $this->arParams["IBLOCK_ID_BLOG"], $this->arParams["COUNT_BLOG"],$this->articlesSecton);

            $this->arResult["TOTAL_COUNT"] = $this->arResult["PAINT"]["COUNT"] + $this->arResult["BLOG"]["COUNT"];

            if($this->request["section"]){
                switch($this->request["section"]){
                    case $this->paintSecton:
                        if($this->arResult["PAINT"]["COUNT"] > 0){
                            $this->arResult["SELECT_SECTION"] = $this->paintSecton;
                        }
                        break;
                    case $this->articlesSecton:
                        if($this->arResult["BLOG"]["COUNT"] > 0){
                            $this->arResult["SELECT_SECTION"] = $this->articlesSecton;
                        }
                        break;
                }
            }
            if(empty($this->arResult["SELECT_SECTION"])){
                $this->arResult["SELECT_SECTION"] = $this->arResult["PAINT"]["COUNT"] > 0 ? $this->paintSecton : $this->articlesSecton;
            }
        }
        $this->includeComponentTemplate();   
    }
}