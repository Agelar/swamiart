function insertParam(key, value) {
    key = escape(key); value = escape(value);

    var kvp = document.location.search.substr(1).split('&');
    if (kvp == '') {
        document.location.search = '?' + key + '=' + value;
    }
    else {

        var i = kvp.length; var x; while (i--) {
            x = kvp[i].split('=');

            if (x[0] == key) {
                x[1] = value;
                kvp[i] = x.join('=');
                break;
            }
        }

        if (i < 0) { kvp[kvp.length] = [key, value].join('='); }

        history.pushState(null, null, "?"+kvp.join('&'));
    }
}
$(document).on("click",".js-search-tab",function(){
    //insertParam("section",$(this).attr('data-tab'));
});

$(document).ready(function() {
    if($('body').find('.gallery__item').length == 0) {
        $('body').find('.container__access-denied__hidden').show();
        $('body').find('.search-tab--visible').hide();
        $('body').find('.search__switch').hide();
    }
});