<?
$MESS["SEARCH_QUERY"] = "Результаты поиска по запросу";
$MESS["SEARCH_PLACE_HOLDER"] = "Поиск...";
$MESS["SEARCH_BTN"] = "Найти";
$MESS["PAINT_TAB"] = "Картины";
$MESS["BLOG_TAB"] = "Статьи";
$MESS["NOT_FOUND"] = "По вашему запросу ничего не было найдено";
$MESS["FIRST_SEARCH"] = "Введите поисковый запрос";
?>