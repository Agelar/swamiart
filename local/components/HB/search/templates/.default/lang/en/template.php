<?
$MESS["SEARCH_QUERY"] = "Search results";
$MESS["SEARCH_PLACE_HOLDER"] = "Search...";
$MESS["SEARCH_BTN"] = "Find";
$MESS["PAINT_TAB"] = "Paintings";
$MESS["BLOG_TAB"] = "Articles";
$MESS["NOT_FOUND"] = "Nothing was found by your request.";
$MESS["FIRST_SEARCH"] = "Enter search query";
?>