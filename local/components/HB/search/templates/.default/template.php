<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use Bitrix\Main\Localization\Loc;
use HB\helpers\WorkHelpers;
?>
<div class="section search">
    <div class="container">
        <?if($arResult["SEARCH_QUERY"]):?>
            <div class="search__heading">
                <?=Loc::getMessage("SEARCH_QUERY")?><br />
                <span class="search__heading-query">&laquo;<?=$arResult["SEARCH_QUERY"]?>&raquo;</span>
            </div>
        <?else:?>
            <div class="search__heading">
                <?=Loc::getMessage("FIRST_SEARCH")?>
            </div>
        <?endif;?>
        
        <form class="search__form" action="">
            <input class="search__input" name="q" type="text" placeholder="<?=Loc::getMessage("SEARCH_PLACE_HOLDER")?>" value="<?=$arResult["SEARCH_QUERY"]?>">
            <button class="search__btn" type="submit" value="search">
                <svg class="icon icon-search-w">
                    <use xlink:href="#icon-search"></use>
                </svg>
            </button>
        </form>
        <?if(intval($arResult["TOTAL_COUNT"]) > 0):?>
            <div class="search__switch">
                <?if($arResult["PAINT"]["LIST"]):?>
                    <a class="js-search-tab search__switch-btn <?if($arResult["SELECT_SECTION"] == "pictures"):?>search__switch-btn--current<?endif;?>" href="#" data-tab="pictures"> 
                        <?=Loc::getMessage("PAINT_TAB")?> (<span class="search__switch-btn-value"><?=$arResult["PAINT"]["COUNT"]?></span>)
                    </a>
                <?endif;?>  
                <?if($arResult["BLOG"]["LIST"]):?>
                    <a class="js-search-tab search__switch-btn <?if($arResult["SELECT_SECTION"] == "articles"):?>search__switch-btn--current<?endif;?>" href="#" data-tab="articles">
                        <?=Loc::getMessage("BLOG_TAB")?> (<span class="search__switch-btn-value"><?=$arResult["BLOG"]["COUNT"]?></span>)
                    </a>
                <?endif;?>
            </div>
        <?endif;?>
    </div>
</div>
<?if(intval($arResult["TOTAL_COUNT"]) > 0):?>
    <?if($arResult["PAINT"]["LIST"]):?>
        <div class="section search-tab <?if($arResult["SELECT_SECTION"] == "pictures"):?>search-tab--visible<?endif;?>" data-tab="pictures">
            <?global $arrFilter;
            $arrFilter = array("=ID" => $arResult["PAINT"]["LIST"]);
            $arFavorites = WorkHelpers::getFavoritesUserWork();
		    $arCart = WorkHelpers::getCartWork();
            ?>
            <?$APPLICATION->IncludeComponent(
                "bitrix:catalog.section",
                "work_search",
                Array(
                    "ACTION_VARIABLE" => "action",
                    "ADD_PICT_PROP" => "-",
                    "ADD_PROPERTIES_TO_BASKET" => "Y",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "ADD_TO_BASKET_ACTION" => "ADD",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "BACKGROUND_IMAGE" => "-",
                    "BASKET_URL" => "/personal/basket.php",
                    "BROWSER_TITLE" => "-",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "COMPATIBLE_MODE" => "Y",
                    "CONVERT_CURRENCY" => "N",
                    "CUSTOM_FILTER" => "",
                    "DETAIL_URL" => "",
                    "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "DISPLAY_COMPARE" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "ELEMENT_SORT_FIELD" => "sort",
                    "ELEMENT_SORT_FIELD2" => "id",
                    "ELEMENT_SORT_ORDER" => "asc",
                    "ELEMENT_SORT_ORDER2" => "desc",
                    "ENLARGE_PRODUCT" => "STRICT",
                    "FILTER_NAME" => "arrFilter",
                    "HIDE_NOT_AVAILABLE" => "N",
                    "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                    "IBLOCK_ID" => $arParams["IBLOCK_ID_PAINT"],
                    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE_PAINT"],
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "LABEL_PROP" => array(),
                    "LAZY_LOAD" => "N",
                    "LINE_ELEMENT_COUNT" => "3",
                    "LOAD_ON_SCROLL" => "N",
                    "MESSAGE_404" => "",
                    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                    "MESS_BTN_BUY" => "Купить",
                    "MESS_BTN_DETAIL" => "Подробнее",
                    "MESS_BTN_SUBSCRIBE" => "Подписаться",
                    "MESS_NOT_AVAILABLE" => "Нет в наличии",
                    "META_DESCRIPTION" => "-",
                    "META_KEYWORDS" => "-",
                    "OFFERS_LIMIT" => "5",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Товары",
                    "PAGE_ELEMENT_COUNT" => "18",
                    "PARTIAL_PRODUCT_PROPERTIES" => "N",
                    "PRICE_CODE" => array(
                        0 => "BASE"
                    ),
                    "PRICE_VAT_INCLUDE" => "Y",
                    "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "PRODUCT_PROPS_VARIABLE" => "prop",
                    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                    "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
                    "PRODUCT_SUBSCRIPTION" => "Y",
                    "RCM_TYPE" => "personal",
                    "SECTION_CODE" => "",
                    "SECTION_URL" => "",
                    "SECTION_USER_FIELDS" => array("", ""),
                    "SEF_MODE" => "N",
                    "SET_BROWSER_TITLE" => "Y",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "Y",
                    "SET_META_KEYWORDS" => "Y",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "Y",
                    "SHOW_404" => "N",
                    "SHOW_ALL_WO_SECTION" => "Y",
                    "SHOW_CLOSE_POPUP" => "N",
                    "SHOW_DISCOUNT_PERCENT" => "N",
                    "SHOW_FROM_SECTION" => "N",
                    "SHOW_MAX_QUANTITY" => "N",
                    "SHOW_OLD_PRICE" => "N",
                    "SHOW_PRICE_COUNT" => "1",
                    "SHOW_SLIDER" => "Y",
                    "SLIDER_INTERVAL" => "3000",
                    "SLIDER_PROGRESS" => "N",
                    "TEMPLATE_THEME" => "blue",
                    "USE_ENHANCED_ECOMMERCE" => "N",
                    "USE_MAIN_ELEMENT_SECTION" => "N",
                    "USE_PRICE_COUNT" => "N",
                    "USE_PRODUCT_QUANTITY" => "N",
                    "SORT_ARRAY" => $arResult["PAINT"]["LIST"],
                    "FAVORITES" => $arFavorites,
                    "CART_WORK" => $arCart,
                    "SEARCH_QUERY" => $arResult["SEARCH_QUERY"],
                ),
                $component
            );?>
            <div class="pagination">
                <?=$arResult["PAINT"]["NAV_STRING"]?>
            </div>
        </div>

        <div class="container container__access-denied__hidden" style="display: none;">
            <div class="container access-denied">
                <div class="blog-detail__subheading">
                    <?=Loc::getMessage("NOT_FOUND")?>
                </div>
            </div>
        </div>
    <?endif;?>
    <?if($arResult["BLOG"]["LIST"]):?>
        <div class="section search-tab <?if($arResult["SELECT_SECTION"] == "articles"):?>search-tab--visible<?endif;?>" data-tab="articles">
            <?global $arrFilter;
            $arrFilter = array("=ID" => $arResult["BLOG"]["LIST"]);
            ?>
            <?$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "searchListBlog",
                Array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "N",
                    "DISPLAY_PICTURE" => "N",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array("",""),
                    "FILTER_NAME" => "arrFilter",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => "11",
                    "IBLOCK_TYPE" => "content",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "INCLUDE_SUBSECTIONS" => "N",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => "20",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Новости",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "PROPERTY_CODE" => array("NAME_EN",""),
                    "SET_BROWSER_TITLE" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "ID",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER1" => "ASC",
                    "SORT_ORDER2" => "ASC",
                    "STRICT_SECTION_CHECK" => "N",
                    "SORT_ARRAY" => $arResult["BLOG"]["LIST"],
                    "SEARCH_QUERY" => $arResult["SEARCH_QUERY"],
                ),
                $component
            );?>
            <div class="pagination">
                <?=$arResult["BLOG"]["NAV_STRING"]?>
            </div>
        </div>
    <?endif;?>
<?elseif($arResult["SEARCH_QUERY"]):?>
    <div class="container">
        <div class="container access-denied">
            <div class="blog-detail__subheading">
                <?=Loc::getMessage("NOT_FOUND")?>
            </div>
        </div>
    </div>
<?endif;?>