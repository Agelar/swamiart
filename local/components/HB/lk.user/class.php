<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Engine\ActionFilter;
use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Localization\Loc;
use HB\helpers\Helpers;
use HB\helpers\LangHelpers;
use HB\userEntity\IblockAuthor;
use HB\userEntity\Sms;
use \Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

class LKUserComponent extends CBitrixComponent implements Controllerable
{

    public function onPrepareComponentParams($arParams)
    {
        return $arParams;
    }
    protected function listKeysSignedParameters()
    {
        return [
            "MAIN_INFO",
            "EXTRA_INFO",
            "ADDRESS_INFO",
        ];
    }

    public function configureActions()
    {
        return [
            'changeMain' => [
                'prefilters' => [
                    new ActionFilter\Authentication(),
                ],
            ],
            'changeExtra' => [
                'prefilters' => [
                    new ActionFilter\Authentication(),
                ],
            ],
            'changeAddress' => [
                'prefilters' => [
                    new ActionFilter\Authentication(),
                ],
            ],
            'sendSmsPhone' => [
                'prefilters' => [
                    new ActionFilter\Authentication(),
                ],
            ],
            'checkEmailUser' => [
                'prefilters' => [
                    new ActionFilter\Authentication(),
                ],
            ],
            'confirmPhone' => [
                'prefilters' => [
                    new ActionFilter\Authentication(),
                ],
            ],
        ];
    }
    private function updateUser($data)
    {
        global $USER;
        $user = new CUser;

        if ($user->Update($USER->GetID(), $data)) {
            if (!empty($data['UF_OLD_VALUES_JSON'])) {
                //отправляем письмо об изменении информации о пользователе 
                $arEventFields = array(
                    "LINK" => "/bitrix/admin/user_edit.php?ID=" . $USER->GetID(),
                    "NAME" => $this->getCurrentValues()['NAME'],
                    "DATE" => date('d.m.Y H:i'),
                );
                $сEventResult = \CEvent::Send("UPDATE_USER", SITE_ID, $arEventFields);
            }
            return true;
        } else {
            return false;
        }
    }

    public function sendSmsPhoneAction($phone)
    {
        Loader::IncludeModule("hb.site");
        global $USER;

        $result = array(
            "STATUS" => false,
            "HTML" => "",
        );

        if ($phone) {
            $status = Sms::SendUserSms($USER->GetID(), $phone);
        }
        if ($status["STATUS"] == "Y") {
            ob_start();
            $this->includeComponentTemplate("phone_confirm");
            $template = ob_get_contents();
            ob_end_clean();

            $result["STATUS"] = true;
            $result["HTML"] = $template;
        }
        return $result;
    }

    public function confirmPhoneAction($phone, $code)
    {
        Loader::IncludeModule("hb.site");
        global $USER;

        $result = array(
            "STATUS" => false,
            "HTML" => "",
        );

        $status = Sms::confirmSms($USER->GetID(), $phone, $code);

        if ($status) {
            $result["STATUS"] = true;
        } else {
            $result["STATUS"] = false;
            $result["HTML"] = Loc::getMessage("FAIL_CODE");
        }
        return $result;
    }

    public function checkEmailUserAction($email)
    {
        global $USER;

        $result = array(
            "STATUS" => !$this->checkUserEmail($email),
        );

        return $result;
    }

    public function changeMainAction($data)
    {
        global $USER;
        Loader::IncludeModule("iblock");
        Loader::IncludeModule("hb.site");
        $arUpdate = array();
        $result = array();

        foreach ($this->arParams["MAIN_INFO"] as $param) {
            $skip = false;
            if (isset($data[$param])) {
                switch ($param) {
                    case "PERSONAL_PHONE":
                        if (!Sms::avalibleUpdatePhone($USER->GetID(), $data[$param])) {
                            $skip = true;
                            continue;
                        }
                        break;
                    case "EMAIL":
                        if ($USER->GetEmail() != $data[$param]) {
                            if ($this->checkUserEmail($data[$param])) {
                                $skip = true;
                                continue;
                            }
                            $arUpdate["LOGIN"] = $data[$param];
                            $arUpdate["UF_CONFIRM_EMAIL"] = 0;
                        }
                        break;
                    case "NAME":
                        if (empty($data[$param])) {
                            $data[$param] = $data["UF_NAME_EN"];
                        }
                        break;
                    case "LAST_NAME":
                        if (empty($data[$param])) {
                            $data[$param] = $data["UF_LAST_NAME"];
                        }
                    case "UF_NAME_EN":
                        if (empty($data[$param])) {
                            $data[$param] = ucfirst(CUtil::translit($data["NAME"], "ru", array("replace_space" => "-", "replace_other" => "-")));
                        }
                        break;
                    case "UF_LAST_NAME":
                        if (empty($data[$param])) {
                            $data[$param] = ucfirst(CUtil::translit($data["LAST_NAME"], "ru", array("replace_space" => "-", "replace_other" => "-")));
                        }
                        break;
                }
                if (!$skip) {
                    $arUpdate[$param] = $data[$param];
                }
            }
        }

        if (!empty($arUpdate)) {
            $arUpdate["UF_OLD_VALUES_JSON"] = json_encode($this->getCurrentValues());  // сохранение старых значений в строковое поле в формате JSON
            $arUpdate["UF_MODERATION"] = USER_PROP_MODERATION_PROCESS; // установка доп. свойства "Статус модерации" в значение "В процессе"
            $status = $this->updateUser($arUpdate);
        }

        $this->arResult["USER_CURRENT_VALUES"] = $this->getCurrentValues();
        $this->arResult["MAIN"] = $this->getProps("MAIN_INFO");
        $this->arResult["AJAX_MODE"] = "Y";
        $this->arResult["AJAX_STATUS"] = $status;

        ob_start();
        $this->includeComponentTemplate("main_info");
        $template = ob_get_contents();
        ob_end_clean();

        if ($status && IblockAuthor::checkIsset($USER->GetID())) {
            $this->updateAuthorInfo($arUpdate);
        }

        $result["STATUS"] = $status;
        $result["HTML"] = $template;

        return $result;
    }
    public function changeExtraAction($data)
    {
        global $USER;
        Loader::IncludeModule("iblock");
        Loader::IncludeModule("sale");
        Loader::IncludeModule("hb.site");
        $arUpdate = array();
        $result = array();

        foreach ($this->arParams["EXTRA_INFO"] as $param) {
            if (isset($data[$param])) {
                $arUpdate[$param] = $data[$param];
            }
        }

        if (!empty($arUpdate)) {
            $arUpdate["UF_OLD_VALUES_JSON"] = json_encode($this->getCurrentValues()); // сохранение старых значений в строковое поле в формате JSON
            $arUpdate["UF_MODERATION"] = USER_PROP_MODERATION_PROCESS; // установка доп. свойства "Статус модерации" в значение "В процессе"
            $status = $this->updateUser($arUpdate);
        }

        $this->arResult["USER_CURRENT_VALUES"] = $this->getCurrentValues();
        $this->arResult["EXTRA"] = $this->getProps("EXTRA_INFO");
        $this->arResult["AJAX_MODE"] = "Y";
        $this->arResult["AJAX_STATUS"] = $status;

        ob_start();
        $this->includeComponentTemplate("extra_info");
        $template = ob_get_contents();
        ob_end_clean();

        if ($status && IblockAuthor::checkIsset($USER->GetID())) {
            $this->updateAuthorInfo($arUpdate);
        }

        $result["STATUS"] = $status;
        $result["HTML"] = $template;

        return $result;
    }

    public function changeAddressAction($data)
    {
        global $USER;
        Loader::IncludeModule("iblock");
        Loader::IncludeModule("hb.site");
        $arUpdate = array();
        $result = array();

        foreach ($this->arParams["ADDRESS_INFO"] as $param) {
            if (isset($data[$param])) {
                $arUpdate[$param] = $data[$param];
            }
        }

        if (!empty($arUpdate)) {
            $arUpdate["UF_OLD_VALUES_JSON"] = json_encode($this->getCurrentValues()); // сохранение старых значений в строковое поле в формате JSON
            $arUpdate["UF_MODERATION"] = USER_PROP_MODERATION_PROCESS; // установка доп. свойства "Статус модерации" в значение "В процессе"
            $status = $this->updateUser($arUpdate);
        }

        $this->arResult["USER_CURRENT_VALUES"] = $this->getCurrentValues();
        $this->arResult["ADDRESS"] = $this->getProps("ADDRESS_INFO");
        $this->arResult["AJAX_MODE"] = "Y";
        $this->arResult["AJAX_STATUS"] = $status;

        ob_start();
        $this->includeComponentTemplate("address_info");
        $template = ob_get_contents();
        ob_end_clean();

        $result["STATUS"] = $status;
        $result["HTML"] = $template;

        return $result;
    }

    private function updateAuthorInfo($data)
    {
        global $USER;
        if (isset($data["NAME"])) {
            $arFields["NAME"] = $data["NAME"] . ($data["LAST_NAME"] ? " " . $data["LAST_NAME"] : "");
        }

        if (isset($data["UF_NAME_EN"])) {
            $arFields["PROPERTIES"]["NAME_EN"] = $data["UF_NAME_EN"] . ($data["UF_LAST_NAME"] ? " " . $data["UF_LAST_NAME"] : "");
        }

        if (isset($data["UF_ABOUT"])) {
            $arFields["DETAIL_TEXT"] = $data["UF_ABOUT"];
        }

        if (isset($data["UF_STYLE"])) {
            $arFields["PROPERTIES"]["STYLE"] = $data["UF_STYLE"];
        }

        if (isset($data["UF_ABOUT_EN"])) {
            $arFields["PROPERTIES"]["DETAIL_TEXT_EN"] = $data["UF_ABOUT_EN"];
            if (empty($data["UF_ABOUT"])) {
                $arFields["DETAIL_TEXT"] = $data["UF_ABOUT_EN"];
            }
        }
        if (isset($data["UF_PERSONAL_CITY_ID"])) {
            $city = CSaleLocation::GetList(array(), array("LID" => "ru", "CITY_ID" => $data["UF_PERSONAL_CITY_ID"]), false, false, array())->fetch();
            $arFields["PROPERTIES"]["TOWN"] = $city["CITY_NAME"];
            $arFields["PROPERTIES"]["TOWN_EN"] = $city["CITY_NAME_ORIG"];
        } elseif (isset($data["PERSONAL_CITY"])) {
            if (LangHelpers::isEnVersion()) {
                $arFields["PROPERTIES"]["TOWN"] = $arFields["PROPERTIES"]["TOWN_EN"] = $data["PERSONAL_CITY"];
            } else {
                $arFields["PROPERTIES"]["TOWN"] = $data["PERSONAL_CITY"];
                $arFields["PROPERTIES"]["TOWN_EN"] = ucfirst(CUtil::translit($data["PERSONAL_CITY"], "ru", array("replace_space" => "-", "replace_other" => "-")));
            }
        }
        $arFields["PROPERTIES"]["MODERATION"] = AUTHOR_PROP_MODERATION_PROCESS; // установка свойства "Статус модерации" в значение "В процессе"

        if ($arFields) {
            IblockAuthor::UpdateAuthor($USER->GetID(), $arFields); // отправка новых значений свойств на модерацию
        }
    }

    private function getTypeProp($prop)
    {
        $result = array(
            "TYPE" => "text",
        );
        if ($this->arResult["USER_CURRENT_VALUES"][$prop]) {
            $result["VALUE"] = $this->arResult["USER_CURRENT_VALUES"][$prop];
        }
        $result["CODE"] = $prop;
        switch ($prop) {
            case "NAME":
            case "LAST_NAME":
                if (!LangHelpers::isEnVersion()) {
                    $result["REQUIRED"] = "Y";
                }
                $result["IS_NAME"] = "Y";
                break;
            case "UF_LAST_NAME":
            case "UF_NAME_EN":
                if (LangHelpers::isEnVersion()) {
                    $result["REQUIRED"] = "Y";
                }
                $result["IS_NAME"] = "Y";
                break;
            case "PERSONAL_PHONE":
                $result["IS_PHONE"] = "Y";
                $result["CHECKOUT_PHONE"] = "Y";
                $result["REQUIRED"] = "Y";
                break;
            case "UF_EXTRA_PHONE":
                $result["IS_PHONE"] = "Y";
                break;
            case "EMAIL":
                $result["TYPE"] = "email";
                $result["REQUIRED"] = "Y";
                break;
            case "PERSONAL_BIRTHDAY":
                for ($i = 1; $i <= 31; $i++) {
                    $result["DAY"][$i] = $i;
                }
                $result["MONTH"] = Helpers::getArrayMonth();
                $now = date("Y");
                for ($i = $now; $i >= $now - 100; $i--) {
                    $result["YEAR"][$i] = $i;
                }
                $result["TYPE"] = "brithday";
                break;
            case "PERSONAL_GENDER":
                $result["TYPE"] = "radio";
                $result["OPTION"] = array(
                    "M" => "MALE",
                    "F" => "FEMAIL",
                );
                break;
            case "PERSONAL_COUNTRY":
                $arCountry = GetCountryArray();
                $result["TYPE"] = "select";
                foreach ($arCountry["reference_id"] as $key => $contryId) {
                    $result["OPTION"][$contryId] = $arCountry["reference"][$key];
                }
                break;
            case "UF_STYLE":
                $db = CIBlockElement::GetList(array(), array("ACTIVE" => "Y", "IBLOCK_ID" => IBLOCK_STYLES_ID), false, false, array("ID", "NAME", "PROPERTY_NAME_EN"));
                while ($res = $db->fetch()) {
                    $result["OPTION"][$res["ID"]] = $res[LangHelpers::isEnVersion() ? "PROPERTY_NAME_EN_VALUE" : "NAME"];
                }
                $result["TYPE"] = "select";
                $result["USER_GROUP"] = "5";
                break;
            case "UF_ABOUT":
            case "UF_ABOUT_EN":
                $result["TYPE"] = "textarea";
                break;
            case "UF_CITY":
                $result["IS_ADDERSS"] = "Y";
                $result["REQUIRED"] = "Y";
                break;
            case "UF_HOUSE":
                $result["REQUIRED"] = "Y";
                break;
            case "UF_STREET":
                $result["REQUIRED"] = "Y";
                break;
            case "UF_APARTAMENT":
                $result["REQUIRED"] = "Y";
                break;
            case "UF_CITY_ID":
                $result["TYPE"] = "hidden";
                $result["IS_CDEK"] = "Y";
                break;
            case "UF_PERSONAL_CITY_ID":
                $result["TYPE"] = "hidden";
                $result["IS_TOWN_ID"] = "Y";
                break;
            case "PERSONAL_CITY":
                $result["IS_TOWN"] = "Y";
                if ($this->arResult["USER_CURRENT_VALUES"]["UF_PERSONAL_CITY_ID"]) {
                    $city = CSaleLocation::GetList(array(), array("LID" => LANGUAGE_ID, "CITY_ID" => $this->arResult["USER_CURRENT_VALUES"]["UF_PERSONAL_CITY_ID"]), false, array("nTopCount" => 10), array())->fetch();
                    $result["VALUE"] = $city["CITY_NAME"];
                }
                break;

        }

        return $result;
    }
    private function getProps($type)
    {
        $result = array();
        foreach ($this->arParams[$type] as $prop) {
            $result[] = $this->getTypeProp($prop);
        }
        return $result;
    }

    private function checkUserEmail($email)
    {
        global $USER;
        $arUser = CUser::GetList(($by = "id"), ($order = "desc"), array("=EMAIL" => $email), array("SELECT" => "ID"))->fetch();
        return ($arUser["ID"] != $USER->GetID() && $arUser) ? true : false;
    }
    private function getCurrentValues()
    {
        global $USER;
        $arSelect = array_merge($this->arParams["MAIN_INFO"], $this->arParams["EXTRA_INFO"], $this->arParams["ADDRESS_INFO"]);
        $arUser = CUser::GetList(($by = "id"), ($order = "desc"), array("ID" => $USER->GetID()), array("SELECT" => $arSelect, "FIELDS" => $arSelect))->fetch();

        return $arUser;
    }

    public function executeComponent()
    {
        Loader::IncludeModule("hb.site");
        global $USER;
        global $APPLICATION;
        $this->arResult["USER_CURRENT_VALUES"] = $this->getCurrentValues();

        $this->arResult["MAIN"] = $this->getProps("MAIN_INFO");
        $this->arResult["EXTRA"] = $this->getProps("EXTRA_INFO");
        $this->arResult["ADDRESS"] = $this->getProps("ADDRESS_INFO");
        $this->includeComponentTemplate();
    }
}
