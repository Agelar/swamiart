<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arRes = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("USER", 0, LANGUAGE_ID);
$userProp = array(
	"NAME" =>"Имя",
	"LAST_NAME" => "Фамилия",
	"PERSONAL_PHONE" => "Телефон",
	"EMAIL" => "E-mail",
	"PERSONAL_BIRTHDAY" => "Дата рождения",
	"PERSONAL_GENDER" => "Пол",
	"PERSONAL_COUNTRY" => "Страна",
	"PERSONAL_CITY" => "Город",
	"PERSONAL_STREET" => "Улица",

);
if (!empty($arRes))
{
	foreach ($arRes as $key => $val)
		$userProp[$val["FIELD_NAME"]] = (strLen($val["EDIT_FORM_LABEL"]) > 0 ? $val["EDIT_FORM_LABEL"] : $val["FIELD_NAME"]);
}
$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"MAIN_INFO" => array(
			"PARENT" => "BASE",
			"NAME" => "Список свойств выводимых в Основных",
			"TYPE" => "LIST",
			"MULTIPLE" => "Y",
			"VALUES" => $userProp,
			"ADDITIONAL_VALUES" => "Y",
		),
		"EXTRA_INFO" => array(
			"PARENT" => "BASE",
			"NAME" => "Список свойств выводимых в Дополнительных",
			"TYPE" => "LIST",
			"MULTIPLE" => "Y",
			"VALUES" => $userProp,
			"ADDITIONAL_VALUES" => "Y",
		),
		"ADDRESS_INFO" => array(
			"PARENT" => "BASE",
			"NAME" => "Список свойств выводимых в Адресах",
			"TYPE" => "LIST",
			"MULTIPLE" => "Y",
			"VALUES" => $userProp,
			"ADDITIONAL_VALUES" => "Y",
		),
	),
);
?>