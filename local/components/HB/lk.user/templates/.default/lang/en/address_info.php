<?
    $MESS["LK_ADDRESS_HEADER"] = "Delivery address";
    $MESS["LK_ADDRESS_SUBHEADER"] = "The address will be automatically substituted at checkout.";
    $MESS["LK_SAVE_BTN"] = "Save changes";
    $MESS["LK_FIELD_UF_CITY"] = "Town/city";
    $MESS["LK_FIELD_UF_STREET"] = "Address";
    $MESS["LK_FIELD_UF_STREET_PLACEHOLDER"] = "Street";
    $MESS["LK_FIELD_UF_HOUSE_PLACEHOLDER"] = "Home/Block/Building";
    $MESS["LK_FIELD_UF_APARTAMENT_PLACEHOLDER"] = "Appartment/Office";
    $MESS["LK_ALERT_SUCCSESS"] = "Settings were saved successfully";
    $MESS["LK_ALERT_ERROR"] = "Error while saving, please try later";
    $MESS["LK_TIP_ADDRESS_HEADER"] = "Be sure to fill in the address";
?>