<?
    $MESS["LK_MAIN_HEADER"] = "Main information";
    $MESS["LK_SAVE_BTN"] = "Save changes";
    $MESS["LK_FIELD_NAME"] = "Name (Russian)";
    $MESS["LK_FIELD_LAST_NAME"] = "Surname (Russian)";  
    $MESS["LK_FIELD_UF_NAME_EN"] = "Name";  
    $MESS["LK_FIELD_UF_LAST_NAME"] = "Surname";
    $MESS["LK_FIELD_PERSONAL_PHONE"] = "Phone";
    $MESS["LK_FIELD_EMAIL"] = "E-mail";
    $MESS["LK_FIELD_UF_EXTRA_PHONE"] = "Additional phone";
    $MESS["LK_ALERT_SUCCSESS"] = "Settings were saved successfully";
    $MESS["LK_ALERT_ERROR"] = "Error while saving, please try later";
?>