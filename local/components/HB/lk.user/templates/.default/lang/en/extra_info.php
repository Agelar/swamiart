<?
    $MESS["LK_EXTRA_HEADER"] = "Additional information";
    $MESS["LK_SAVE_BTN"] = "Save changes";
    $MESS["LK_FIELD_PERSONAL_BIRTHDAY"] = "Date of birth";
    $MESS["LK_FIELD_PERSONAL_GENDER"] = "Gender";
    $MESS["LK_FIELD_PERSONAL_COUNTRY"] = "Country";
    $MESS["LK_FIELD_PERSONAL_COUNTRY_PLACEHOLDER"] = "Choose the country";
    $MESS["LK_FIELD_PERSONAL_CITY"] = "City";
    $MESS["LK_FIELD_UF_STYLE"] = "Main pencil";
    $MESS["LK_FIELD_UF_STYLE_PLACEHOLDER"] = "Choose the pencil";
    $MESS["LK_FIELD_UF_ABOUT"] = "About me";
    $MESS["LK_FIELD_UF_ABOUT_EN"] = "About me (English)";
    $MESS["LK_DAY"] = "Day";
    $MESS["LK_MONTH"] = "Month";
    $MESS["LK_YEAR"] = "Year";
    $MESS["LK_MALE"] = "Male";
    $MESS["LK_FEMAIL"] = "Female";
    $MESS["LK_ALERT_SUCCSESS"] = "Settings were saved successfully";
    $MESS["LK_ALERT_ERROR"] = "Error while saving, please try later";
    $MESS["LK_TIP_UF_ABOUT"] = "Fill in the information about yourself as detailed as possible";
?>