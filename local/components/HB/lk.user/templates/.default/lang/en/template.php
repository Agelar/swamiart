<?
$MESS["LK_HEADER"] = "Personal information";
$MESS["LK_SUBHEADER"] = "You can add any information about yourself.";
$MESS["CODE_SEND"] = "A code has been sent, try again in:";
$MESS["LIMIT_AGAIN"] = "Limit is reached, please try again later";
$MESS["NOT_FOGOT"] = "Haven't received the code. ";
$MESS["NOT_SEND_AGAIN"] = "Send again";
$MESS["HELLO"] = "Hello,";
$MESS["EMAIL_ERROR"] = "E-mail is busy";
?>