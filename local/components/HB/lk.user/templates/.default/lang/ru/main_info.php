<?
    $MESS["LK_MAIN_HEADER"] = "Основная информация";
    $MESS["LK_SAVE_BTN"] = "Сохранить изменения";
    $MESS["LK_FIELD_NAME"] = "Имя";
    $MESS["LK_FIELD_LAST_NAME"] = "Фамилия";  
    $MESS["LK_FIELD_UF_NAME_EN"] = "Имя (английский)";  
    $MESS["LK_FIELD_UF_LAST_NAME"] = "Фамилия (английский)";
    $MESS["LK_FIELD_PERSONAL_PHONE"] = "Телефон";
    $MESS["LK_FIELD_EMAIL"] = "E-mail";
    $MESS["LK_FIELD_UF_EXTRA_PHONE"] = "Доп. Телефон";
    $MESS["LK_ALERT_SUCCSESS"] = "Настройки успешно сохранены";
    $MESS["LK_ALERT_ERROR"] = "При сохранении возникла ошибка, попробуйте позже";
?>