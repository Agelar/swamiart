<?
    $MESS["LK_EXTRA_HEADER"] = "Дополнительная информация";
    $MESS["LK_SAVE_BTN"] = "Сохранить изменения";
    $MESS["LK_FIELD_PERSONAL_BIRTHDAY"] = "Дата рождения";
    $MESS["LK_FIELD_PERSONAL_GENDER"] = "Пол";
    $MESS["LK_FIELD_PERSONAL_COUNTRY"] = "Страна";
    $MESS["LK_FIELD_PERSONAL_COUNTRY_PLACEHOLDER"] = "Выберите страну";
    $MESS["LK_FIELD_PERSONAL_CITY"] = "Город";
    $MESS["LK_FIELD_UF_STYLE"] = "Основной стиль";
    $MESS["LK_FIELD_UF_STYLE_PLACEHOLDER"] = "Выберите стиль";
    $MESS["LK_FIELD_UF_ABOUT"] = "О себе";
    $MESS["LK_FIELD_UF_ABOUT_EN"] = "О себе (английский)";
    $MESS["LK_DAY"] = "День";
    $MESS["LK_MONTH"] = "Месяц";
    $MESS["LK_YEAR"] = "Год";
    $MESS["LK_MALE"] = "Мужской";
    $MESS["LK_FEMAIL"] = "Женский";
    $MESS["LK_ALERT_SUCCSESS"] = "Настройки успешно сохранены";
    $MESS["LK_ALERT_ERROR"] = "При сохранении возникла ошибка, попробуйте позже";
    $MESS["LK_TIP_UF_ABOUT"] = "Заполните информацию о себе максимально подробно";
?>