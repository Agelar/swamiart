<?
    $MESS["LK_ADDRESS_HEADER"] = "Адрес доставки";
    $MESS["LK_ADDRESS_SUBHEADER"] = "Адрес будет автоматически подставляться при оформлении заказа";
    $MESS["LK_SAVE_BTN"] = "Сохранить изменения";
    $MESS["LK_FIELD_UF_CITY"] = "Населенный пункт";
    $MESS["LK_FIELD_UF_STREET"] = "Адрес";
    $MESS["LK_FIELD_UF_STREET_PLACEHOLDER"] = "Улица";
    $MESS["LK_FIELD_UF_HOUSE_PLACEHOLDER"] = "Дом/Корпус/Строение";
    $MESS["LK_FIELD_UF_APARTAMENT_PLACEHOLDER"] = "Квартира/Офис";
    $MESS["LK_ALERT_SUCCSESS"] = "Настройки успешно сохранены";
    $MESS["LK_ALERT_ERROR"] = "При сохранении возникла ошибка, попробуйте позже";
    $MESS["LK_TIP_ADDRESS_HEADER"] = "Обязательно заполните адрес";
?>