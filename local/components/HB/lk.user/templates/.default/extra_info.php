<?
    use Bitrix\Main\Localization\Loc;
?>
<?if($arResult["AJAX_MODE"] == "Y"):?>
    <?if($arResult["AJAX_STATUS"]):?>
        <div class="alert alert__success"> <?=Loc::getMessage("LK_ALERT_SUCCSESS")?> </div>
    <?else:?>
        <div class="alert alert__error"><?=Loc::getMessage("LK_ALERT_ERROR")?></div>
    <?endif;?>
<?endif;?>
<div class="lk-block"><span class="lk-block__head"><?=Loc::getMessage("LK_EXTRA_HEADER")?></span>
    <div class="lk-block__main">
        <form class="form js-form-lk" data-type="changeExtra" data-validate="data-validate">
            <?foreach($arResult["EXTRA"] as $key => $prop):?>
                <?switch($prop["TYPE"]):
                    case "text":?>
                        <div class="form__row">
                            <span class="form__row-title"><?=Loc::getMessage("LK_FIELD_".$prop["CODE"])?>
                            <?if(Loc::getMessage('LK_TIP_'.$prop["CODE"])){?>
                                <span class="tip js-tip" title="<?=Loc::getMessage('LK_TIP_'.$prop["CODE"])?>"></span>
                            <?}?>
                            </span>
                            <div class="form__row-main">
                                <div class="form__field">
                                    <input class="form__input <?if($prop["IS_TOWN"] == "Y"):?>js-town<?endif;?>" value="<?=$prop["VALUE"]?>" type="text" name="<?=$prop["CODE"]?>" <?if($prop["REQUIRED"] == "Y"):?>required="required"<?endif;?> />
                                </div>
                            </div>
                        </div>
                        <?break;?>
                    <?case "select":?>
                        <?if($prop["USER_GROUP"] && array_search($prop["USER_GROUP"], CUser::GetUserGroup($USER->GetID())) === false) break;?>
                        <div class="form__row">
                            <span class="form__row-title"><?=Loc::getMessage("LK_FIELD_".$prop["CODE"])?></span>
                            <div class="form__row-main">
                                <div class="form__field">
                                    <div class="form__select">
                                        <select name="<?=$prop["CODE"]?>" class="js-select2" data-placeholder="<?=Loc::getMessage("LK_FIELD_".$prop["CODE"]."_PLACEHOLDER")?>">
                                            <option value=""> </option>
                                            <?foreach($prop["OPTION"] as $key => $option):?>
                                                <option <?if($prop["VALUE"] == $key):?>selected<?endif;?> value="<?=$key?>"><?=$option?></option>
                                            <?endforeach;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?break;?>
                    <?case "radio":?>
                            <div class="form__row">
                                <span class="form__row-title form__row-title_small"><?=Loc::getMessage("LK_FIELD_".$prop["CODE"])?></span>
                                <div class="form__row-main">
                                    <?foreach($prop["OPTION"] as $key => $option):?>
                                        <label class="form__radio">
                                            <input class="radio" type="radio" name="<?=$prop["CODE"]?>" value="<?=$key?>" <?if($prop["VALUE"] == $key):?>checked="checked"<?endif;?> /><span><?=Loc::getMessage("LK_".$option)?></span>
                                        </label>
                                    <?endforeach;?>
                                </div>
                            </div>
                        <?break;?>
                    <?case "textarea":?>
                        <div class="form__row">
                            <span class="form__row-title"><?=Loc::getMessage("LK_FIELD_".$prop["CODE"])?>
                            <?if(Loc::getMessage('LK_TIP_'.$prop["CODE"])){?>
                                <span class="tip js-tip" title="<?=Loc::getMessage('LK_TIP_'.$prop["CODE"])?>"></span>
                            <?}?>
                            </span>
                            <div class="form__row-main">
                                <div class="form__field">
                                    <textarea class="form__area js-form-area" <?if($prop["REQUIRED"] == "Y"):?>required="required"<?endif;?> name="<?=$prop["CODE"]?>"><?=$prop["VALUE"]?></textarea>
                                </div>
                            </div>
                        </div>
                        <?break;?>
                    <?case "brithday":?>
                            <?$date = explode(".",$prop["VALUE"])?>
                            <div class="form__row">
                                <span class="form__row-title"><?=Loc::getMessage("LK_FIELD_".$prop["CODE"])?></span>
                                <div class="form__row-main">
                                    <div class="form__select">
                                        <select class="js-select2 js-brithday-day" data-placeholder="<?=Loc::getMessage("LK_DAY")?>">
                                            <option value=""></option>
                                            <?foreach($prop["DAY"] as $day):?>
                                                <option <?if($date[0] == $day):?>selected<?endif;?> value="<?=$day?>"><?=$day?></option>
                                            <?endforeach;?>
                                        </select>
                                    </div>
                                    <div class="form__select">
                                        <select class="js-select2 js-brithday-month" data-placeholder="<?=Loc::getMessage("LK_MONTH")?>">
                                            <option value=""></option>
                                            <?foreach($prop["MONTH"] as $key => $month):?>
                                                <option <?if($date[1] == $key):?>selected<?endif;?> value="<?=$key?>"><?=$month?></option>
                                            <?endforeach;?>
                                        </select>
                                    </div>
                                    <div class="form__select">
                                        <select class="js-select2 js-brithday-year" data-placeholder="<?=Loc::getMessage("LK_YEAR")?>">
                                            <option value=""></option>
                                            <?foreach($prop["YEAR"] as $year):?>
                                                <option <?if($date[2] == $year):?>selected<?endif;?> value="<?=$year?>"><?=$year?></option>
                                            <?endforeach;?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <input class="js-brithday" type="hidden" name="<?=$prop["CODE"]?>" value="<?=$prop["VALUE"]?>">
                        <?break;?>
                    <?case "hidden":?>
                        <input class="<?if($prop["IS_TOWN_ID"] == "Y"):?>js-town-value<?endif;?>" type="hidden" name="<?=$prop["CODE"]?>" value="<?=$prop["VALUE"]?>">
                        <?break;?>
                <?endswitch;?>
            <?endforeach;?>
            <div class="form__row"><span class="form__row-title"></span>
                <div class="form__row-main">
                    <button class="btn btn_green btn_form" type="submit"><?=Loc::getMessage("LK_SAVE_BTN")?></button>
                </div>
            </div>
        </form>
    </div>
</div>