var CPersonalArea = function(data){
    this.signedParameters = data.signedParameters;
    this.confirmPhone = $(".js-main-phone").data("valphone") ? $(".js-main-phone").data("valphone") : false;
    this.sendSms = false;
    this.againcCountSms = 0;
    this.sendingSms = false;
    this.time = 30;
    this.curTime = false;
    this.interval = false;

    this.setEvent();
    this.setDeliveryComplete();
    this.setTownComplete();

}

CPersonalArea.prototype.setEvent = function(){
    $(document).on('formSubmit',".js-form-lk",$.proxy(this.formSubmit,this));
    $(document).on("change",".js-brithday-day, .js-brithday-month, .js-brithday-year",$.proxy(this.changeBrithday,this));
    $(document).on("keyup",".js-main-phone",$.proxy(this.checkSms,this))
    $(document).on("click",".js-confirm-sms",$.proxy(this.confirmSms,this))
    $(document).on("click",".js-again-sms",$.proxy(this.againSms,this))
    $(document).on("change","input[name='EMAIL']",$.proxy(this.checkEmail,this));
}

CPersonalArea.prototype.formSubmit = function(e){
    e.preventDefault();
    var self = $(e.target);
    var form = self.serializeArray();
    var data = {};
    form.forEach(function(item,key){
        data[item.name] = item.value;
    });
    var type = self.attr("data-type");
    if(type.length > 0){
        this.sendFormLk(data,type);
    }
}

CPersonalArea.prototype.changeBrithday = function(e){
    var day = $(".js-brithday-day").val();
    var month = $(".js-brithday-month").val();
    var year = $(".js-brithday-year").val();
    if(day != "" && month != "" && year != ""){
        $(".js-brithday").val(day + "." + month + "." + year);
    }
}

CPersonalArea.prototype.sendFormLk = function(data,type){
    BX.ajax.runComponentAction('HB:lk.user',
    type, { 
      mode: 'class',
      signedParameters: this.signedParameters,
      data:{
          "data" : data
      }
    })
    .then(function(result) {
        $(".js-" + type + "-wrap").html(result.data.HTML);
        $(document).trigger('formInit');
        if($(".js-" + type + "-wrap").find(".alert").length > 0){
            $(".js-" + type + "-wrap").find(".alert").fadeIn(300);
            setTimeout(function(){
                $(".js-" + type + "-wrap").find(".alert").fadeOut(300,function(){
                    $(".js-" + type + "-wrap").find(".alert").remove();
                });
            },2000);
        }
    });
}

CPersonalArea.prototype.checkSms = function(e){
    console.log("test");
    var self = $(e.target);
    if(self.inputmask("isComplete")){
        var that = this;
        var phone = self.val();
        if(phone != "" && phone != that.confirmPhone && phone != that.sendSms){
            
            BX.ajax.runComponentAction('HB:lk.user',
                "sendSmsPhone", { 
                mode: 'class',
                signedParameters: that.signedParameters,
                data:{
                    "phone" : phone,
                }
            })
            .then(function(result) {
                if(result.data.STATUS){
                    that.sendSms = phone;
                    if($(".js-confirm-phone-wrap").length > 0){
                        $(".js-confirm-phone-wrap").replaceWith(result.data.HTML);
                    }else{
                        $(self).parents(".form__field").after(result.data.HTML);
                    }
                    
                }
            });
        } else {
            if($(".js-confirm-phone-wrap").length > 0){
                $(".js-confirm-phone-wrap").remove();
            }
        }
    }
    
}
CPersonalArea.prototype.checkEmail = function(e){
    e.preventDefault();
    var self = $(e.target);
    if(self.val() == ""){
        return;
    }
    BX.ajax.runComponentAction('HB:lk.user',
        "checkEmailUser", { 
        mode: 'class',
        signedParameters: this.signedParameters,
        data:{
            "email" : self.val(),
        }
    })
    .then(function(result) {
        if(result.data.STATUS){
            if($(".js-email-not-exist").length > 0){
                self.removeClass("error");
                $(".js-email-not-exist").remove();
            }
        }else{
            self.addClass("error");
            if($(".js-email-not-exist").length == 0){
                self.after('<label id="EMAIL-error js-email-not-exist" class="error" for="EMAIL">'+BX.message("EMAIL_ERROR")+'</label>');
            }
        }
    });
}
CPersonalArea.prototype.confirmSms = function(e){
    e.preventDefault();
    var self = $(e.target);
    BX.ajax.runComponentAction('HB:lk.user',
        "confirmPhone", { 
        mode: 'class',
        signedParameters: this.signedParameters,
        data:{
            "phone" : this.sendSms,
            "code": $(".js-confirmsms-input").val(),
        }
    })
    .then(function(result) {
        if(result.data.STATUS){
            $(".js-confirm-phone-wrap").remove();
        }else{
            alert(result.data.HTML);
        }
    });
}
CPersonalArea.prototype.againSms = function(e){
    e.preventDefault();
    var self = $(e.target);
    var that = this;
    if(this.againcCountSms < 4){
        this.againcCountSms++;
        if(!this.sendingSms){
            that.sendingSms = true;
            BX.ajax.runComponentAction('HB:lk.user',
            "sendSmsPhone", { 
            mode: 'class',
            signedParameters: this.signedParameters,
            data:{
                "phone" : this.sendSms,
            }
        })
        .then(function(result) {
            $(".js-confirm-wrap").html("<span>" + BX.message("CODE_SEND") + "</span><span class='js-timer-counter'>0:" + that.time + "</span>");
            that.curTime = that.time;
            that.interval = setInterval($.proxy(that.timerAgain,that),1000);
            that.sendingSms = false;
        });
        }
        
    }else{
        alert(BX.message("LIMIT_AGAIN"));
    }
}
CPersonalArea.prototype.timerAgain = function(){
    this.curTime--;
    if(this.curTime <= 0){
        clearInterval(this.interval);
        $(".js-confirm-wrap").html('<span>' + BX.message("NOT_FOGOT") + '</span><a href="#" class="js-again-sms">' + BX.message("NOT_SEND_AGAIN") + '</a>');
    }else{
        $(".js-timer-counter").html("0:" + (this.curTime < 10 ? "0" + this.curTime : this.curTime));
    }
}

CPersonalArea.prototype.setDeliveryComplete = function(){
    $(document).ready(function(){
        $(document).on('input', ".js-cdek", function(){
            $('.js-cdek').parent('.form__field').addClass('is-loader');
        });
        $(".js-cdek").autocomplete({
            source : function(request, response) {
                $.ajax({
                    url : "https://api.cdek.ru/city/getListByTerm/jsonp.php?callback=?",
                    dataType : "json",
                    data : {
                        q : function() {
                            return $(".js-cdek").val()
                        },
                        name_startsWith : function() {
                            return $(".js-cdek").val()
                        }
                    },
                    success : function(data) {
                        $('.js-cdek').parent('.form__field').removeClass('is-loader');
                        response($.map(data.geonames, function(item) {
                            return {
                                label : item.name,
                                value : item.name,
                                id : item.id
                            }
                        }));
                    }
                });
            },
            minLength : 1,
            select : function(event, ui) {
                $(".js-cdek").val(ui.item.value);
                $(".js-cdek").attr('data-city_id', ui.item.id);
                $(".js-cdek-value").val(ui.item.id);
            }
        });
    });
}

CPersonalArea.prototype.setTownComplete = function(){
    $(document).ready(function(){
        $(".js-town").autocomplete({
            source : function(request, response) {
                $.ajax({
                    url : "/local/ajax/getTownBitrix.php",
                    dataType : "json",
                    data : {
                        q: $(".js-town").val(),
                        lang: BX.message("LANGUAGE_ID"),
                    },
                    success : function(data) {
                        response($.map(data.city, function(item) {
                            return {
                                label : item.NAME,
                                value : item.NAME,
                                id : item.ID
                            }
                        }));
                    },
                });
            },
            minLength : 1,
            select : function(event, ui) {
                $(".js-town").val(ui.item.value);
                $(".js-town").attr('data-city_id', ui.item.id);
                $(".js-town-value").val(ui.item.id);
            },
            change: function(event, ui){
                if($(".js-town").attr('data-city_id') == ""){
                    $(".js-town").val("");
                    $(".js-town-value").val("");
                }
            }
        });
        $(document).on("keyup",".js-town",function(){
            $(".js-town").attr('data-city_id',"");
        });
    });
}