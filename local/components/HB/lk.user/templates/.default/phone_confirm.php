<?
    use Bitrix\Main\Localization\Loc;
?>
    <div class="sms-confirm js-confirm-phone-wrap">
        <div class="sms-confirm__main">
            <div class="sms-confirm__main-title">
                <label class="form__label"><?=Loc::getMessage("LK_SMS")?></label>
            </div>
            <div class="sms-confirm__main-fields">
                <input class="form__input js-confirmsms-input" type="text" maxlength="6" name="sms1" />
                <a href="#" class="btn btn_dark btn_small btn_sms js-confirm-sms"><?=Loc::getMessage("LK_SMS_CONFIRM")?></a>
            </div>
        </div>
        <div class="sms-confirm__desc">
            <div class="sms-confirm__text"><a class="href href_tip" href="#popupDesc"
                    data-fancybox="data-fancybox"><?=Loc::getMessage("LK_SMS_QUESTION")?></a></div>
            <div class="sms-confirm__text js-confirm-wrap">
                <span><?=Loc::getMessage("LK_SMS_ERROR")?></span>
                <a href="#" class="js-again-sms"><?=Loc::getMessage("LK_SMS_AGAIN")?></a>
            </div>
        </div>
    </div>