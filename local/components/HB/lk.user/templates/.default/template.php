<?
    use Bitrix\Main\Localization\Loc;
    use HB\userEntity\IblockAuthor;
    use HB\helpers\Helpers;
?>
<?$photo = IblockAuthor::getPersonalPicture($USER->GetID(),array("width"=>"100", "height"=>"100", BX_RESIZE_IMAGE_PROPORTIONAL));?>
<div class="lk__main-inner">
    <span class="lk-head"><?=Loc::getMessage("LK_HEADER")?></span>
    <div class="lk-user hidden-tablet-up">
        <?if($photo):?>
			<div class="lk-user__img js-lk-user-img" style="background-image:url(<?=$photo?>)"><input class="js-lk-user-img-input" type="file"/></div>
		<?else:?>
			<div class="lk-user__img js-lk-user-img not-photo" style=""><input class="js-lk-user-img-input" type="file"/></div>
		<?endif;?>
        <span class="lk-user__hello"><?=Loc::getMessage("HELLO")?></span><span class="lk-user__name"><?=Helpers::getUserName()?></span>
    </div>
    <div class="lk-desc"><span><?=Loc::getMessage("LK_SUBHEADER")?> </span></div>
    <div class="js-changeMain-wrap">
        <?include("main_info.php")?>
    </div>
    <div class="js-changeExtra-wrap">
        <?include("extra_info.php")?>
    </div>
    <div class="js-changeAddress-wrap">
        <?include("address_info.php")?>
    </div>
</div>
<script>
BX.message({
	LIMIT_AGAIN: '<?=GetMessageJS('LIMIT_AGAIN')?>',
	CODE_SEND: '<?=GetMessageJS('CODE_SEND')?>',
	NOT_FOGOT: '<?=GetMessageJS('NOT_FOGOT')?>',
	NOT_SEND_AGAIN: '<?=GetMessageJS('NOT_SEND_AGAIN')?>',
	EMAIL_ERROR: '<?=GetMessageJS('EMAIL_ERROR')?>',
});
</script>
<script>
    var CPersonalArea = new CPersonalArea(<?=\Bitrix\Main\Web\Json::encode([
        'signedParameters' => $this->getComponent()->getSignedParameters()
    ]);?>);
</script>