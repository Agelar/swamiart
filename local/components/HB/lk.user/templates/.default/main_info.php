
<?
    use Bitrix\Main\Localization\Loc;
?>
<?if($arResult["AJAX_MODE"] == "Y"):?>
    <?if($arResult["AJAX_STATUS"]):?>
        <div class="alert alert__success"> <?=Loc::getMessage("LK_ALERT_SUCCSESS")?> </div>
    <?else:?>
        <div class="alert alert__error"><?=Loc::getMessage("LK_ALERT_ERROR")?></div>
    <?endif;?>
<?endif;?>
<div class="lk-block"><span class="lk-block__head"><?=Loc::getMessage("LK_MAIN_HEADER")?></span>
    <div class="lk-block__main">
        <form class="form js-form-lk" data-type="changeMain" data-validate="data-validate">
            <div class="form__row form__row_columns">
                <?foreach($arResult["MAIN"] as $key => $prop):?>
                <?switch($prop["TYPE"]):
                    case "text":
                    case "email":?>
                        <div class="form__field">
                            <label class="form__label"><?=Loc::getMessage("LK_FIELD_".$prop["CODE"])?><?if($prop["REQUIRED"] == "Y"):?>*<?endif;?></label>
                            <input 
                            class="form__input <?if($prop["IS_PHONE"] == "Y"):?>js-input-phone<?endif;?> <?if($prop["IS_NAME"] == "Y"):?>js-input-name<?endif;?> <?if($prop["CHECKOUT_PHONE"] =="Y"):?> js-main-phone <?endif;?>" 
                            type="<?=$prop["TYPE"]?>" 
                            <?if($prop["REQUIRED"] == "Y"):?>required="required"<?endif;?> 
                            value="<?=$prop["VALUE"]?>" 
                            <?if($prop["CHECKOUT_PHONE"] =="Y" && $prop["VALUE"]):?>
                            data-valPhone = "<?=$prop["VALUE"]?>"
                            <?endif;?>
                            name="<?=$prop["CODE"]?>" />
                        </div>
                        <?break;?>
                <?endswitch;?>
                    <?if(($key + 1) % 2 == 0):?>
                        </div>
                        <div class="form__row form__row_columns">
                    <?endif;?>
                <?endforeach;?>
                <div class="form__field"></div>
            </div>
            <div class="form__row form__row_columns">
                <div class="form__buttons">
                    <button class="btn btn_green btn_form" type="submit"><?=Loc::getMessage("LK_SAVE_BTN")?></button>
                </div>
                <div class="form__field"></div>
            </div>
        </form>
    </div>
</div>