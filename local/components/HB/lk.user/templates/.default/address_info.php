<?
    use Bitrix\Main\Localization\Loc;
?>
<?if($arResult["AJAX_MODE"] == "Y"):?>
    <?if($arResult["AJAX_STATUS"]):?>
        <div class="alert alert__success"> <?=Loc::getMessage("LK_ALERT_SUCCSESS")?> </div>
    <?else:?>
        <div class="alert alert__error"><?=Loc::getMessage("LK_ALERT_ERROR")?></div>
    <?endif;?>
<?endif;?>
<div class="lk-block">
    <span class="lk-block__head">
        <?=Loc::getMessage("LK_ADDRESS_HEADER")?>
        <?if(Loc::getMessage('LK_TIP_ADDRESS_HEADER')){?>
            <span class="tip js-tip" title="<?=Loc::getMessage('LK_TIP_ADDRESS_HEADER')?>"></span>
        <?}?>
    </span>
    <div class="lk-block__main">
        <div class="lk-block__desc">
            <span><?=Loc::getMessage("LK_ADDRESS_SUBHEADER")?></span>
        </div>
        <form class="form js-form-lk" data-type="changeAddress" data-validate="data-validate">
            <?foreach($arResult["ADDRESS"] as $key => $prop):?>
                <?switch($prop["TYPE"]):
                    case "text":?>
                    <?if($key == 2):?><div class="form__row"><span class="form__row-title"><?=Loc::getMessage("LK_FIELD_".$prop["CODE"])?></span><div class="form__row-main"><?endif;?>
                        <?if($key < 2):?>
                            <div class="form__row">
                                <span class="form__row-title">
                                    <?=Loc::getMessage("LK_FIELD_".$prop["CODE"])?>
                                    <?if($prop["REQUIRED"] == "Y"){?>
                                        <span class="reg_star">*</span>
                                    <?}?>
                                </span>
                                <div class="form__row-main">
                                    <div class="form__field">
                                        <input class="form__input <?if($prop["IS_ADDERSS"] == "Y"):?>js-cdek<?endif;?>" placeholder="<?=Loc::getMessage("LK_FIELD_".$prop["CODE"]."_PLACEHOLDER")?>" type="text" value="<?=$prop["VALUE"]?>" <?if($prop["REQUIRED"] == "Y"):?>required="required"<?endif;?> name="<?=$prop["CODE"]?>" />
                                    </div>
                                </div>
                            </div>
                        <?else:?>
                            <div class="form__field">
                                <input class="form__input" value="<?=$prop["VALUE"]?>" <?if($prop["REQUIRED"] == "Y"):?>required="required"<?endif;?> type="text" name="<?=$prop["CODE"]?>" placeholder="<?=Loc::getMessage("LK_FIELD_".$prop["CODE"]."_PLACEHOLDER")?>" />
                            </div>
                        <?endif;?>
                        <?break;?>
                    <?case "hidden":?>
                        <input class="<?if($prop["IS_CDEK"] == "Y"):?>js-cdek-value<?endif;?>" type="hidden" name="<?=$prop["CODE"]?>" value="<?=$prop["VALUE"]?>">
                        <?break;?>
                <?endswitch;?>
            <?endforeach;?>
            <?if(count($arResult["ADDRESS"]) > 2):?>
                    </div>
                </div>
            <?endif;?>
            <div class="form__row"><span class="form__row-title"></span>
                <div class="form__row-main">
                    <button class="btn btn_green btn_form" type="submit"><?=Loc::getMessage("LK_SAVE_BTN")?></button>
                </div>
            </div>
        </form>
    </div>
</div>