<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Loader;
use HB\helpers\LocationHelpers;
use HB\helpers\Helpers;

class LocationDetermination extends CBitrixComponent{

    function onPrepareComponentParams($arParams) {
        return $arParams;
    }
    
    function executeComponent(){
        Loader::IncludeModule("hb.site");
        $this->arResult['CITY_MAIN'] = LocationHelpers::getMainCityList();
        $this->includeComponentTemplate();   
    }
}