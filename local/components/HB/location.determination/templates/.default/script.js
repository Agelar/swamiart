//выбор города (определение местоположение)
$("#JsSelectCity").autocomplete({
    source : function(request, response) {
        $.ajax({
            url : "/local/ajax/get_city_ajax.php?site_id=" + BX.message('SITE_ID'),
            dataType : "json",
            data : {
                q : function() {
                    return $("#JsSelectCity").val()
                }
            },
            success : function(data) {
                
                $('#JsSelectCity').parent('.form__field').removeClass('is-loader');
                response($.map(data, function(item) {
                    return {
                        label : item.UF_CITY_FULLNAME,
                        value : item.UF_CITY_FULLNAME,
                        name : item.UF_CITY_NAME,
                        id : item.UF_CITY_ID,
                        region : item.UF_REGION,
                        regionEn : item.UF_REGION_EN,
                        nameEn : item.UF_CITY_NAME_EN,
                    }
                }));
            }
        });
    },
    minLength : 1,
    select : function(event, ui) {
        $("#JsSelectCity").val(ui.item.value);
        $("#JsSelectCity").data('name', ui.item.name);
        $("#JsSelectCity").data('id', ui.item.id);
        $("#JsSelectCity").data('region', ui.item.region);
        $("#JsSelectCity").data('name-en', ui.item.nameEn);
        $("#JsSelectCity").data('region-en', ui.item.regionEn);
        
    }
});

//город определен верно
$('.button_city_yes').on('click', function (e) {
    
    var cityName =  $(this).data('city');
    var cityRegion =  $(this).data('region');
    
    $.ajax({
        type: 'GET',
        url: '/local/ajax/location_ajax.php?AJAX=Y&TYPE=GEO&CITY_NAME='+cityName+'&REGION_NAME=' + cityRegion,
        
        dataType: 'json',
        success: function(data){
            if(data.SUCCESS){
                $('.location__text').text(cityName);
                $('.js-city').removeClass('is-active');
            }
            
        }
    });
    return false;
});

//город выбран
$('#button_city_select').on('click', function (e) {
    var cityId =  $('#JsSelectCity').data('id');
    var cityName =  $('#JsSelectCity').data('name');
    var cityNameEn =  $('#JsSelectCity').data('name-en');
    var cityRegion =  $('#JsSelectCity').data('region');
    
    $.ajax({
        type: 'GET',
        url: '/local/ajax/location_ajax.php?AJAX=Y&TYPE=CDEK&CITY_NAME='+cityName+'&CITY_NAME_EN='+cityNameEn+'&REGION_NAME=' + cityRegion + '&CITY_ID=' + cityId,
        
        dataType: 'json',
        success: function(data){
            if(data.SUCCESS){
                $('.location__text').text(cityName);
                $('.location__select .js-select-close').trigger('click');
            }
            
        }
    });
    return false;
});


//выбор города из основного списка городов
$('.city_select_list input').on('change', function (e) {
    var cityId =  $(this).data('id');
    var cityName =  $(this).data('name');
    var cityRegion =  $(this).data('region');
    
    $.ajax({
        type: 'GET',
        url: '/local/ajax/location_ajax.php?AJAX=Y&TYPE=CDEK&CITY_NAME=' + cityName + '&REGION_NAME=' + cityRegion + '&CITY_ID=' + cityId,
        
        dataType: 'json',
        success: function(data){
            if(data.SUCCESS){
                $('.location__text').text(cityName);
                $('.location__select .js-select-close').trigger('click');
            }
            
        }
    });
    return false;
});
