<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

use HB\helpers\Helpers;
if($arResult["ITEMS"]){
    if($arParams["SORT_ARRAY"]){
        usort($arResult["ITEMS"],array(new Helpers($arParams["SORT_ARRAY"]),"sortByID"));
    }
}
$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();
