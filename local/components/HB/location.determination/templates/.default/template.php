<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="location__select select js-select">
        <div class="select__hide js-select-close"></div>
        <div class="select__title"><?=GetMessage('CHOOSE_YOUR_CITY');?></div>
        <form class="select__wrap">
          <div class="select__inp">
            <input class="select__city js-select-city" id="JsSelectCity" data-id="" data-name="" data-region="" data-name-en="" data-region-en=""/>
            <div class="select__search">
              <svg class="icon icon-search">
                <use xlink:href="#icon-search"></use>
              </svg>
            </div>
            <div class="select__close js-reset-result"></div>
          </div>
          <button id="button_city_select" class="select__btn" type="submit"><?=GetMessage('CHOOSE');?></button>
        </form>
        <?if($arResult['CITY_MAIN']):?>
          <div class="select__radio">
            <div class="select__radio-wrap  js-scroll city_select_list">
              <?foreach($arResult['CITY_MAIN'] as $idCity => $arCity):?>
                <div class="select__item">
                <input id="city_<?=$arCity['UF_CITY_ID']?>" type="radio" value="Y" name="city" data-id="<?=$arCity['UF_CITY_ID']?>" data-name="<?=$arCity['UF_CITY_NAME']?>" data-region="<?=$arCity['UF_REGION']?>" data-name-en="<?=$arCity['UF_CITY_NAME_EN']?>" data-region-en="<?=$arCity['UF_REGION_EN']?>">
                  <label class="check-style" for="city_<?=$arCity['UF_CITY_ID']?>"><span class="select__text"><?=$arCity['UF_CITY_NAME']?></span></label>
                </div>
              <?endforeach;?>
            </div>
          </div>
        <?endif;?>
      </div>
              
           