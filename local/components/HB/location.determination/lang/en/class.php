<?
$MESS["STATUS_ORDER"] = "About order status";
$MESS["STATUS_ORDER_DESC"] = "The appearance of the order, change its status";
$MESS["NEWSLETTERS"] = "Newsletters";
$MESS["NEWSLETTERS_DESC"] = "Mailing Description";
$MESS["REGULAR_DAYS_1"] = "Once a day";
$MESS["REGULAR_DAYS_7"] = "Once a week";
$MESS["REGULAR_DAYS_30"] = "Once a month";
?>