<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Loader;

use HB\helpers\CatalogHelpers;

class SeoFilterPage extends CBitrixComponent{

    function onPrepareComponentParams($arParams) {
        return $arParams;
    }
    
    function executeComponent(){
        global $APPLICATION;
        $this->arResult['SEO'] = CatalogHelpers::getSeoPropFilterPage($APPLICATION->GetCurPage());
        if($this->arResult['SEO']){
            if($this->arResult['SEO']['H1']) $APPLICATION->SetTitle($this->arResult['SEO']['H1']);
            if($this->arResult['SEO']['TITLE']) $APPLICATION->SetPageProperty('title', $this->arResult['SEO']['TITLE']);
            if($this->arResult['SEO']['DESCRIPTION']) $APPLICATION->SetPageProperty('description', $this->arResult['SEO']['DESCRIPTION']);
            
        }
        $this->includeComponentTemplate();

    }
}