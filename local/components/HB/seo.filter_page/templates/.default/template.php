<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arResult['SEO']['TEXT']):?>
  <div class="section blog-detail static-page-content">
    <div class="container">
      <div class="blog-detail__inner">
        <div class="blog-detail__content">
          <?=$arResult['SEO']['TEXT'];?>
        </div>
      </div>
    </div>
  </div>
<?else:?>
<div class="section blog-detail static-page-content" style="display:none;">
    <div class="container">
      <div class="blog-detail__inner">
        <div class="blog-detail__content">
          
        </div>
      </div>
    </div>
  </div>
<?endif;?>