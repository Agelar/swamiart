<?
//Это решение было принято для того чтобы пропустить страницу с одной кнопкой paypal и сразу перенаправить на оплату
if($arResult["PAYMENT"]->getPaymentSystemId()):?>
    <style>
        body{
            visibility:hidden;
        }
    </style>
    <script>
        var formPay = $('.wrapper.wrapper_inner form:not([class])');
        if(formPay.length > 0){
            formPay[0].submit();
        }
    </script>
<?endif;?>