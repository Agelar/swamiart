<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;
use HB\helpers\AuthorHelpers;
use HB\helpers\Helpers;
use HB\helpers\LangHelpers;
use HB\helpers\WorkHelpers;
use \Bitrix\Main\Loader;

Loader::IncludeModule("hb.site");
Loader::IncludeModule("iblock");
Loader::IncludeModule("sale");
Loader::IncludeModule("catalog");

Loc::loadMessages(__FILE__);

class WorkAddComponent extends CBitrixComponent
{

    public function onPrepareComponentParams($arParams)
    {
        $this->arParams = array();
        $this->arParams['WORK_ID'] = $arParams['WORK_ID'];
        return $this->arParams;
    }

    public function executeComponent()
    {
        global $USER;

        if ($this->request['WORK']) {
            $GLOBALS['APPLICATION']->RestartBuffer();
            $this->addUpdateWork();
            die();

        } else {
            $this->getData($this->arParams['WORK_ID']);
        }

        $this->includeComponentTemplate();
    }

    //Добавление/Изменение работы
    private function addUpdateWork()
    {
        $workId = ($this->arParams['WORK_ID'] ? $this->arParams['WORK_ID'] : false);
        $arWork = $this->request['WORK'];
        $arWork['PRICE'] = str_replace(array(',', ' ₽'), array('', ''), $arWork['PRICE']);

        //формируем размер из ширины, высоты и толщины
        $arWork['SIZE'] = $arWork['WIDTH'] . 'x' . $arWork['HEIGHT'] . 'x' . $arWork['DEPTH'];
        //поле для сортировки по размеру. Перемножаем ширину и высоту
        $arWork['SIZE_SORT'] = $arWork['WIDTH'] * $arWork['HEIGHT'];

        //Определяем тип размера (S/M/L/XL/XXL)
        /*
        Данные от клиента
        До 0,1 м.кв
        до 0, 24 м.кв
        До 0,56 м.кв
        До 1, 1   кв.м
        Больше 1, 1 кв.м
         */
        $sizeType = $arWork['WIDTH'] * $arWork['HEIGHT'];
        if ($sizeType < 1000) {
            $arWork['SIZE_TYPE'] = 16;
        } elseif ($sizeType < 2400) {
            $arWork['SIZE_TYPE'] = 17;
        } elseif ($sizeType < 5600) {
            $arWork['SIZE_TYPE'] = 18;
        } elseif ($sizeType < 11000) {
            $arWork['SIZE_TYPE'] = 19;
        } else {
            $arWork['SIZE_TYPE'] = 20;
        }

        //Формируем массив для основного изображения
        $arWork['DETAIL_PICTURE'] = \CFile::MakeFileArray($this->request['WORK']['PICTURE']['ID']['0']);
        //Формируем массив для доп. изображений
        $arWork['ADD_PICTURE'] = array();
        if (count($this->request['WORK']['PICTURE']['ID']) > 1) {
            foreach ($this->request['WORK']['PICTURE']['ID'] as $key => $value) {
                if ($key == 0) {
                    continue;
                }

                $arFile = \CFile::MakeFileArray($value);
                $arWork['ADD_PICTURE'][] = array("VALUE" => $arFile, 'DESCRIPTION' => $arFile['name']);
            }
        }

        if (!$arWork['PUBLISHED']) {
            $arWork['PUBLISHED'] = WORK_PROP_PUBLISHED_NO;
        }
        // устанавливаем значение свойства "Опубликована" в значение "Нет", если не задано

        //Формируем масси для цифровой копии
        if ($_FILES['DIGITAL_COPY']) {
            $arWork['DIGITAL_COPY'] = $_FILES['DIGITAL_COPY'];
        } elseif ($arWork['DIGITAL_COPY_ID']) {
            $arWork['DIGITAL_COPY'] = \CFile::MakeFileArray($arWork['DIGITAL_COPY_ID']);
        }

        //Добавляем/Изменяем работу
        $result = WorkHelpers::addWork($arWork, $workId);
        if (is_numeric($result)) {
            $this->arResult['WORK_ID'] = $result;
            if ($workId) {
                $this->arResult['TYPE'] = 'SAVE_WORK';
                $this->arResult['MESSAGE'] = 'Изменения сохранены';
            } else {
                //При добавлении работы генерируем артикул
                $vendorCode = $this->generateVendorCode($result);
                WorkHelpers::addVendorCode($result, $vendorCode);
                $this->arResult['TYPE'] = 'NEW_WORK';
                $this->arResult['MESSAGE'] = 'Работы добавлена';
            }
        } else {
            $this->arResult['ERROR'] = $result;
            $this->arResult['ERROR'] = str_replace('Обязательное поле "Детальная картинка" не заполнено.', 'Обязательное поле "Фотографии" не заполнено.', $this->arResult['ERROR']);
            $this->arResult['ERROR'] = str_replace('Required field "Детальная картинка" is not specified.', 'Required field "Photos" is not specified.', $this->arResult['ERROR']);
            $this->arResult['ERROR'] = str_replace('Элемент с таким символьным кодом уже существует.', 'Вы уже добавляли картину с таким названием.', $this->arResult['ERROR']);
            $this->arResult['ERROR'] = str_replace('An element with this symbolic code already exists.', 'You have already added a picture with the same name.', $this->arResult['ERROR']);
        }
        print json_encode($this->arResult);
    }

    //Получение информации о работе
    private function getData($workId)
    {
        //Подгружаем элементы справочников
        $this->arResult["EXECUTION"] = Helpers::getHandbookList(IBLOCK_EXECUTION_ID);
        $this->arResult["SIZE"] = Helpers::getHandbookList(IBLOCK_SIZE_ID);
        $this->arResult["ORIENTATION"] = Helpers::getHandbookList(IBLOCK_ORIENTATION_ID);
        $this->arResult["GENRE"] = Helpers::getHandbookSectionList(IBLOCK_GENRES_ID);
        $this->arResult["GENRE_SUB"] = ($_REQUEST['UPDATE_AJAX'] == "Y" && !empty($_REQUEST['SECTION_ID']) && $_REQUEST['PROP_CODE'] == "GENRE") ? $this->getHandbookElementsList(explode(",", $_REQUEST['SECTION_ID']), $_REQUEST['PROP_CODE']) : '';
        $this->arResult["THEME_SUB"] = ($_REQUEST['UPDATE_AJAX'] == "Y" && !empty($_REQUEST['SECTION_ID']) && $_REQUEST['PROP_CODE'] == "THEME") ? $this->getHandbookElementsList(explode(",", $_REQUEST['SECTION_ID']), $_REQUEST['PROP_CODE']) : '';
        $this->arResult["MATERIAL_SUB"] = ($_REQUEST['UPDATE_AJAX'] == "Y" && !empty($_REQUEST['SECTION_ID']) && $_REQUEST['PROP_CODE'] == "MATERIAL") ? $this->getHandbookElementsList(explode(",", $_REQUEST['SECTION_ID']), $_REQUEST['PROP_CODE']) : '';
        $this->arResult["STYLE_SUB"] = ($_REQUEST['UPDATE_AJAX'] == "Y" && !empty($_REQUEST['SECTION_ID']) && $_REQUEST['PROP_CODE'] == "STYLES") ? $this->getHandbookElementsList(explode(",", $_REQUEST['SECTION_ID']), $_REQUEST['PROP_CODE']) : '';
        $this->arResult["MATERIALS"] = Helpers::getHandbookSectionList(IBLOCK_MATERIALS_ID);
        $this->arResult["COLORS"] = Helpers::getHandbookList(IBLOCK_COLORS_ID);
        $this->arResult["STYLES"] = Helpers::getHandbookSectionList(IBLOCK_STYLES_ID);
        $this->arResult["THEME"] = Helpers::getHandbookSectionList(IBLOCK_THEME_ID);
        $this->arResult["CANVAS"] = Helpers::getHandbookList(IBLOCK_MADE_IN_COLOR_ID);

        //подсказки для свойств
        $this->arResult["HELP"] = Helpers::getHandbookList(IBLOCK_PROP_HELP_ID);
        $this->arResult["HELPER"] = array();
        foreach ($this->arResult["HELP"] as $arHelp) {
            $this->arResult["HELPER"][$arHelp['NAME']] = $arHelp['PROPERTIES']['HELP_TEXT']['~VALUE']['TEXT'];
        }

        //получаем все свойства работ
        $arProperties = WorkHelpers::getWorkProperty();
        //перевод свойств
        $arProperties = LangHelpers::getTranslationProperty($arProperties);
        $this->arResult["PROP_LIST"] = $arProperties;

        if ($workId) {
            //если изменяем работу, получаем информацию о работе
            $this->arResult["WORK"] = WorkHelpers::getWork($workId);
            $arSize = explode('x', $this->arResult["WORK"]['PROPERTIES']['SIZE']["VALUE"]);
            $this->arResult["WORK"]["WIDTH"] = $arSize["0"];
            $this->arResult["WORK"]["HEIGHT"] = $arSize["1"];
            $this->arResult["WORK"]["DEPTH"] = $arSize["2"];
            $this->arResult["AUTHOR"] = $this->arResult["WORK"]["PROPERTIES"]["AUTHOR"]["VALUE"];
            $this->arResult["WORK"]["YOUTUBE_VIDEO"] = implode("\n", $this->arResult["WORK"]["PROPERTIES"]["YOUTUBE_VIDEO"]["VALUE"]);
            $this->arResult["ACTIVE"] = $this->arResult["WORK"]["ACTIVE"];
            if (empty($this->arResult["GENRE_SUB"]) && !$_REQUEST['UPDATE_AJAX'] == "Y") {
                $this->arResult["GENRE_SUB"] = $this->getHandbookElementsList($this->arResult["WORK"]['PROPERTIES']["GENRE"]["VALUE"]["0"], "GENRE");
            }
            if (empty($this->arResult["THEME_SUB"]) && !$_REQUEST['UPDATE_AJAX'] == "Y") {
                $this->arResult["THEME_SUB"] = $this->getHandbookElementsList($this->arResult["WORK"]['PROPERTIES']["THEME"]["VALUE"], "THEME");
            }
            if (empty($this->arResult["MATERIAL_SUB"]) && !$_REQUEST['UPDATE_AJAX'] == "Y") {
                $this->arResult["MATERIAL_SUB"] = $this->getHandbookElementsList($this->arResult["WORK"]['PROPERTIES']["MATERIAL"]["VALUE"], "MATERIAL");
            }
            if (empty($this->arResult["STYLE_SUB"]) && !$_REQUEST['UPDATE_AJAX'] == "Y") {
                $this->arResult["STYLE_SUB"] = $this->getHandbookElementsList($this->arResult["WORK"]['PROPERTIES']["STYLE"]["VALUE"]["0"], "STYLES");
            }
            if ($this->arResult["WORK"]["PROPERTIES"]["MODERATION"]["VALUE"] == 'Пройдена') {
                $this->arResult["MODERATION"] = WORK_PROP_MODERATION_COMPLETED; // устанавливаем значение свойства "Статус модерации" в здачение "Пройдена"
            } else {
                $this->arResult["MODERATION"] = WORK_PROP_MODERATION_PROCESS; // устанавливаем значение свойства "Статус модерации" в здачение "В процессе"
            }
            $this->arResult["PUBLISHED"] = $this->arResult["WORK"]["PROPERTIES"]["PUBLISHED"]['VALUE_ENUM_ID'];
        } else {
            $this->arResult["AUTHOR"] = AuthorHelpers::getAuthorId();
            $this->arResult["PUBLISHED"] = WORK_PROP_PUBLISHED_YES; // устанавливаем значение свойства "Опубликована" в здачение "Да"
            $this->arResult["MODERATION"] = WORK_PROP_MODERATION_PROCESS; // устанавливаем значение свойства "Статус модерации" в здачение "В процессе"
        }

        //настройки сайта
        $arSettings = Helpers::getSiteSettings();
        //попап "требования к цифровой копии"
        $this->arResult['DIGITAL_COPY_TITLE'] = $arSettings['DIGITAL_COPY_TITLE']['VALUE'];
        $this->arResult['DIGITAL_COPY_TEXT'] = $arSettings['DIGITAL_COPY_TEXT']['~VALUE']['TEXT'];
    }

    //генерация артикула
    private function generateVendorCode($workId)
    {
        $len = strlen($workId);
        if ($len < 6) {
            for ($i = 0; $i < (6 - $len); $i++) {
                $workId = '0' . $workId;
            }
        }
        return $workId;
    }

    //получение списка подразделов справочника по ID раздела и ID инфоблока
    private function getHandbookElementsList($section_ID, $prop_code)
    {
        $iblock_ID = 0;
        switch ($prop_code) {
            case "GENRE": // жанр
                $iblock_ID = IBLOCK_GENRES_ID;
                break;
            case "THEME": // тема
                $iblock_ID = IBLOCK_THEME_ID;
                break;
            case "MATERIAL": // материал
                $iblock_ID = IBLOCK_MATERIALS_ID;
                break;
            case "STYLES": // стиль
                $iblock_ID = IBLOCK_STYLES_ID;
                break;
        }
        $elementsList = HB\helpers\Helpers::getHandbookSectionElementsList($section_ID, $iblock_ID);
        return $elementsList;
    }
}
