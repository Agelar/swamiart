<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("WORK_ADD_DESCRIPTION_NAME"),
	"DESCRIPTION" => GetMessage("WORK_ADD_DESCRIPTION_DESCRIPTION"),
	"ICON" => "/images/changepass.gif",
	"SORT" => 10,
	"PATH" => array(
			
			),
);

?>