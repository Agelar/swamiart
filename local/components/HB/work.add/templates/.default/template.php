<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) { die(); }
?>
<div class="lk__main-inner">
    <span class="lk-head"><?=$APPLICATION->GetCurPage() == '/personal/adding-work/' ? GetMessage("ADD_WORK") : GetMessage("UPDATE_WORK")?>
        <div class="lk-head__more"><a href="/personal/work-list/"><?=GetMessage("CANCEL")?></a></div>
    </span>
    <div class="lk-desc"><span><?=GetMessage("DESC_WORK")?></span></div>
    <div class="add_work_form_message">
        <?if ($_REQUEST['NEW']) {?>
            <div class="alert alert__success" style="display: block;"><?=GetMessage("DESC_ADDED")?></div>
        <?}?>
    </div>
    <form class="form"   method="POST" enctype="multipart/form-data" id="add_work_form" data-validate="data-validate">
        <div class="form__row">
            <span class="form__row-title">
                <?=GetMessage("NAME")?><span class="reg_star">*</span>
                <?if ($arResult["HELPER"]["NAME"]) {?>
                    <span class="tip js-tip" title="<?=$arResult["HELPER"]["NAME"]?>"></span>
                <?}?>
            </span>
            <div class="form__row-main">
                <div class="form__field">
                    <input class="form__input" type="text" name="WORK[NAME]" value="<?=$_POST["WORK"] ? $_POST["WORK"]["NAME"] : $arResult['WORK']['NAME']?>" required="required">
                </div>
            </div>
        </div>
        <div class="form__row"><span class="form__row-title form__row-title_small"><?=GetMessage("I_AM")?></span>
            <div class="form__row-main">
                <label class="form__radio">
                    <input class="radio js-type__author" type="radio" name="type" value="author" <?=!$arResult['WORK']['PROPERTIES']['TRUE_AUTHOR']['VALUE'] && !$_POST["WORK"]["TRUE_AUTHOR"] ? 'checked="checked"' : '';?>/><span><?=GetMessage("AUTHOR_PICTURE")?></span>
                </label>
                <label class="form__radio">
                    <input class="radio  js-type__author" type="radio" name="type" value="owner" <?=$arResult['WORK']['PROPERTIES']['TRUE_AUTHOR']['VALUE'] || $_POST["WORK"]["TRUE_AUTHOR"] ? 'checked="checked"' : '';?>/><span><?=GetMessage("OWNER")?></span>
                </label>
            </div>
        </div>
        <div class="form__row js-true__author" <?=!$arResult['WORK']['PROPERTIES']['TRUE_AUTHOR']['VALUE'] && !$_POST["WORK"]["TRUE_AUTHOR"] ? 'style="display: none;"' : '';?>>
            <span class="form__row-title">
                <?=$arResult["PROP_LIST"]["TRUE_AUTHOR"]?>
                <?if ($arResult["HELPER"]["TRUE_AUTHOR"]) {?>
                    <span class="tip js-tip" title="<?=$arResult["HELPER"]["TRUE_AUTHOR"]?>"></span>
                <?}?>
            </span>
            <div class="form__row-main">
                <div class="form__field">
                    <input class="form__input" type="text" name="WORK[TRUE_AUTHOR]" value="<?=$_POST["WORK"] ? $_POST["WORK"]["TRUE_AUTHOR"] : $arResult['WORK']['PROPERTIES']['TRUE_AUTHOR']['VALUE']?>">
                </div>
            </div>
        </div>
        <div class="form__row" data-code="STYLES">
            <span class="form__row-title">
                <?=$arResult["PROP_LIST"]["STYLE"]?>
                <?if ($arResult["HELPER"]["STYLE"]) {?>
                    <span class="tip js-tip" title="<?=$arResult["HELPER"]["STYLE"]?>"></span>
                <?}?>
            </span>
            <div class="form__row-main">
                <div class="form__field">
                    <div class="form__select">
                        <select name="WORK[STYLE][]" class="js-select2 parent-select" data-placeholder="<?=GetMessage("STYLE")?>">
                            <option value="0"><?=GetMessage("STYLE")?></option>
                            <?foreach ($arResult["STYLES"] as $style) {?>
                                <option
                                    value="<?=$style["ID"]?>"
                                    <?if ($_POST["WORK"]) {?>
                                        <?=$_POST["WORK"] && $style["ID"]["0"] == $_POST["WORK"]["STYLE"] ? 'selected' : ''?>
                                    <?} elseif ($arResult['WORK']) {?>
                                        <?=$style["ID"] == $arResult["WORK"]['PROPERTIES']["STYLE"]["VALUE"]["0"] ? 'selected' : ''?>
                                    <?}?>
                                ><?=$style["NAME"]?></option>
                            <?}?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form__row" style="<?=count($arResult['STYLE_SUB']) == 0 ? 'display: none' : ''?>" data-code="STYLES_SUB">
            <span class="form__row-title">
                <?=$arResult["PROP_LIST"]["STYLE_SUB"]?>
                <?if ($arResult["HELPER"]["STYLE_SUB"]) {?>
                    <span class="tip js-tip" title="<?=$arResult["HELPER"]["STYLE_SUB"]?>"></span>
                <?}?>
            </span>
            <div class="form__row-main">
                <div class="form__field">
                    <div class="form__select">
                        <?if ($_REQUEST['UPDATE_AJAX'] == "Y" && $_REQUEST['PROP_CODE'] == 'STYLES') {
                            $GLOBALS['APPLICATION']->RestartBuffer();
                        }?>
                            <select name="WORK[STYLE_SUB][]" class="js-select2" data-placeholder="<?=GetMessage("STYLE_SUB")?>">
                                <option value="0"><?=GetMessage("STYLE_SUB")?></option>
                                <?foreach ($arResult["STYLE_SUB"] as $theme) {?>
                                    <option
                                        value="<?=$theme["ID"]?>"
                                        <?if ($_POST["WORK"]) {?>
                                            <?=$_POST["WORK"] && $theme["ID"]["0"] == $_POST["WORK"]["STYLE_SUB"] ? 'selected' : ''?>
                                        <?} elseif ($arResult['WORK']) {?>
                                            <?=$theme["ID"] == $arResult["WORK"]['PROPERTIES']["STYLE_SUB"]["VALUE"]["0"] ? 'selected' : ''?>
                                        <?}?>
                                    ><?=$theme["NAME"]?></option>
                                <?}?>
                            </select>
                        <?if ($_REQUEST['UPDATE_AJAX'] == "Y" && $_REQUEST['PROP_CODE'] == 'STYLES') {
                            die();
                        }?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form__row form__row_detail_picture" data-text="<?=GetMessage("DELETE")?>">
            <span class="form__row-title"><?=GetMessage("PHOTO")?>
                <span class="reg_star">*</span>
                <?if ($arResult["HELPER"]["PHOTO"]) {?>
                    <span class="tip js-tip" title="<?=$arResult["HELPER"]["PHOTO"]?>"></span>
                <?}?>
                <span><?=GetMessage("PHOTO_REST")?></span>
            </span>
            <div class="form__row-main">
                <div class="form__field">
                    <div class="form__images js-form-images" data-to_add="<?=GetMessage("TO_ADD")?>">
                        <input class="form__images-input work_img js-input-images" type="file" multiple="multiple" maxlength="6" id="formImagesInput" name="DETAIL_PICTURE"/>
                        <div class="form__images-wrap photo_list">
                            <?if ($_POST['WORK']) {?>
                                <?foreach ($_POST['WORK']['PICTURE']['ID'] as $key => $img) {?>
                                    <div class="form__images-item js-form-images-item" style="background-image:url(<?=$_POST['WORK']['PICTURE']['SRC'][$key];?>)" data-text="<?=GetMessage("DELETE")?>" data-idfile="<?=$img?>">
                                        <input type="hidden" name="WORK[PICTURE][SRC][]" value="<?=$_POST['WORK']['PICTURE']['SRC'][$key];?>">
                                        <input type="hidden" name="WORK[PICTURE][ID][]" value="<?=$img?>">
                                    </div>
                                <?}?>
                            <?} elseif ($arResult['WORK']) {?>
                                <div class="form__images-item js-form-images-item" style="background-image:url(<?=$arResult['WORK']['DETAIL_PICTURE']['SRC'];?>)" data-text="<?=GetMessage("DELETE")?>" data-idfile="<?=$arResult['WORK']['DETAIL_PICTURE']['ID'];?>">
                                    <input type="hidden" name="WORK[PICTURE][SRC][]" value="<?=$arResult['WORK']['DETAIL_PICTURE']['SRC'];?>">
                                    <input type="hidden" name="WORK[PICTURE][ID][]" value="<?=$arResult['WORK']['DETAIL_PICTURE']['ID'];?>">
                                </div>
                                <?if ($arResult['WORK']['PROPERTIES']['ADD_PICTURE']['VALUE']['0']) {?>
                                    <?foreach ($arResult['WORK']['PROPERTIES']['ADD_PICTURE']['VALUE'] as $arImg) {?>
                                        <?if ($arImg["ID"]) {?>
                                            <div class="form__images-item js-form-images-item" style="background-image:url(<?=$arImg['SRC'];?>)" data-text="<?=GetMessage("DELETE")?>"  data-idfile="<?=$arImg['ID']?>">
                                                <input type="hidden" name="WORK[PICTURE][SRC][]" value="<?=$arImg['SRC']?>">
                                                <input type="hidden" name="WORK[PICTURE][ID][]" value="<?=$arImg['ID']?>">
                                            </div>
                                        <?}?>
                                    <?}?>
                                <?}?>
                            <?}?>

                            <label class="form__images-add" for="formImagesInput">
                                <svg class="icon icon-plus">
                                    <use xlink:href="#icon-plus"></use>
                                </svg><span><?=GetMessage("TO_ADD")?></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form__row">
            <span class="form__row-title">
                <?=GetMessage("VIDEO")?>
                <?if ($arResult["HELPER"]["YOUTUBE_VIDEO"]) {?>
                    <span class="tip js-tip" title="<?=$arResult["HELPER"]["YOUTUBE_VIDEO"]?>"></span>
                <?}?>
            </span>
            <div class="form__row-main">
                <div class="form__field">
                    <textarea class="form__area form__area__video js-form-area" type="text" name="WORK[YOUTUBE_VIDEO]" ><?=$_POST["WORK"] ? $_POST["WORK"]["YOUTUBE_VIDEO"] : $arResult["WORK"]["YOUTUBE_VIDEO"]?></textarea>
                </div>
            </div>
        </div>
        <div class="form__row">
            <span class="form__row-title">
                <?=GetMessage("DESC")?>
                <?if ($arResult["HELPER"]["DESC"]) {?>
                    <span class="tip js-tip" title="<?=$arResult["HELPER"]["DESC"]?>"></span>
                <?}?>
            </span>
            <div class="form__row-main">
                <div class="form__field">
                    <textarea class="form__area js-form-area" type="text" name="WORK[DETAIL_TEXT]" ><?=$_POST["WORK"] ? $_POST["WORK"]["DETAIL_TEXT"] : $arResult['WORK']['DETAIL_TEXT']?></textarea>
                </div>
            </div>
        </div>
        <div class="form__row">
            <span class="form__row-title">
                <?=GetMessage("PRICE")?><span class="reg_star">*</span>
                <?if ($arResult["HELPER"]["PRICE"]) {?>
                    <span class="tip js-tip" title="<?=$arResult["HELPER"]["PRICE"]?>"></span>
                <?}?>
            </span>
            <div class="form__row-main">
                <div class="form__field form__field_half">
                    <input class="form__input js-form-price" type="text" name="WORK[PRICE]" value="<?=$_POST["WORK"] ? $_POST["WORK"]["PRICE"] : $arResult['WORK']['PRICE']?>" required="required">
                </div>
            </div>
        </div>

        <div class="form__row">
            <span class="form__row-title">
                <?=$arResult["PROP_LIST"]["EXECUTION"]?>
                <?if ($arResult["HELPER"]["EXECUTION"]) {?>
                    <span class="tip js-tip" title="<?=$arResult["HELPER"]["EXECUTION"]?>"></span>
                <?}?>
            </span>
            <div class="form__row-main">
                <div class="form__field">
                    <div class="form__select">
                        <select name="WORK[EXECUTION]" class="js-select2" data-placeholder="<?=GetMessage("EXECUTION")?>">
                            <option value="0"><?=GetMessage("EXECUTION")?></option>
                            <?foreach ($arResult["EXECUTION"] as $execution) {?>
                                <option
                                    value="<?=$execution["ID"]?>"
                                    <?if ($_POST["WORK"]) {?>
                                        <?=$_POST["WORK"] && $execution["ID"] == $_POST["WORK"]["EXECUTION"] ? 'selected' : ''?>
                                    <?} elseif ($arResult['WORK']) {?>
                                        <?=$execution["ID"] == $arResult["WORK"]['PROPERTIES']["EXECUTION"]["VALUE"] ? 'selected' : ''?>
                                    <?}?>
                                ><?=$execution["NAME"]?></option>
                            <?}?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form__row" data-code="MATERIAL">
            <span class="form__row-title"><?=$arResult["PROP_LIST"]["MATERIAL"]?>
                <?if ($arResult["HELPER"]["MATERIAL"]) {?>
                    <span class="tip js-tip" title="<?=$arResult["HELPER"]["MATERIAL"]?>"></span>
               <?}?>
            </span>
            <div class="form__row-main">
                <div class="form__field">
                    <div class="form__select form__select_multiply">
                        <select name="WORK[MATERIAL]" class="js-select2 js-select-close parent-select" data-placeholder="<?=GetMessage("MATERIALS")?>" multiple="multiple">
                            <option value=""><?=GetMessage("MATERIALS")?></option>
                            <?foreach ($arResult["MATERIALS"] as $material) {?>
                                <option
                                    value="<?=$material["ID"]?>"
                                    <?if ($_POST["WORK"]) {?>
                                        <?=$_POST["WORK"] && (array_search($material["ID"], $_POST["WORK"]["MATERIAL"]) !== false &&
                                                              array_search($material["ID"], $_POST["WORK"]["MATERIAL"]) !== null) ? 'selected' : ''?>
                                    <?} elseif ($arResult['WORK']) {?>
                                        <?=(array_search($material["ID"], $arResult["WORK"]['PROPERTIES']["MATERIAL"]["VALUE"]) !== false &&
                                            array_search($material["ID"], $arResult["WORK"]['PROPERTIES']["MATERIAL"]["VALUE"]) !== null) ? 'selected' : ''?>
                                    <?}?>
                                ><?=$material["NAME"]?></option>
                            <?}?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form__row" style="<?=count($arResult['MATERIAL_SUB']) == 0 ? 'display: none' : ''?>" data-code="MATERIAL_SUB">
            <span class="form__row-title">
                <?=$arResult["PROP_LIST"]["MATERIAL_SUB"]?>
                <?if ($arResult["HELPER"]["MATERIAL_SUB"]) {?>
                    <span class="tip js-tip" title="<?=$arResult["HELPER"]["MATERIAL_SUB"]?>"></span>
                <?}?>
            </span>
            <div class="form__row-main">
                <div class="form__field">
                    <div class="form__select form__select_multiply">
                        <?if ($_REQUEST['UPDATE_AJAX'] == "Y" && $_REQUEST['PROP_CODE'] == 'MATERIAL') {
                            $GLOBALS['APPLICATION']->RestartBuffer();
                        }?>
                            <select name="WORK[MATERIAL_SUB][]" class="js-select2 js-select-close" data-placeholder="<?=GetMessage("MATERIAL_SUB")?>" multiple="multiple">
                                <option value=""><?=GetMessage("MATERIAL_SUB")?></option>
                                <?foreach ($arResult["MATERIAL_SUB"] as $theme) {?>
                                    <option
                                        value="<?=$theme["ID"]?>"
                                        <?if ($_POST["WORK"]) {?>
                                            <?=$_POST["WORK"] && (array_search($theme["ID"], $_POST["WORK"]["MATERIAL_SUB"]) !== false &&
                                                                  array_search($theme["ID"], $_POST["WORK"]["MATERIAL_SUB"]) !== null) ? 'selected' : ''?>
                                        <?} elseif ($arResult['WORK']) {?>
                                            <?=(array_search($theme["ID"], $arResult["WORK"]['PROPERTIES']["MATERIAL_SUB"]["VALUE"]) !== false &&
                                                array_search($theme["ID"], $arResult["WORK"]['PROPERTIES']["MATERIAL_SUB"]["VALUE"]) !== null) ? 'selected' : ''?>
                                        <?}?>
                                    ><?=$theme["NAME"]?></option>
                                <?}?>
                            </select>
                        <?if ($_REQUEST['UPDATE_AJAX'] == "Y" && $_REQUEST['PROP_CODE'] == 'MATERIAL') {
                            die();
                        }?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form__row"><span class="form__row-title"><?=$arResult["PROP_LIST"]["ORIENTATION"]?><span class="reg_star">*</span><?if ($arResult["HELPER"]["ORIENTATION"]): ?>
                <span class="tip js-tip" title="<?=$arResult["HELPER"]["ORIENTATION"]?>"></span>
            <?endif;?></span>
            <div class="form__row-main">
                <div class="form__field form__field_half_2">
                    <div class="form__select">
                        <select name="WORK[ORIENTATION][]" class="js-select2" data-placeholder="<?=GetMessage("ORIENTATION")?>" required>
                            <option value=""><?=GetMessage("ORIENTATION")?></option>
                            <?foreach ($arResult["ORIENTATION"] as $orientation) {?>
                                <option
                                    value="<?=$orientation["ID"]?>"
                                    <?if ($_POST["WORK"]) {?>
                                        <?=$_POST["WORK"] && $orientation["ID"] == $_POST["WORK"]["ORIENTATION"] ? 'selected' : ''?>
                                    <?} elseif ($arResult['WORK']) {?>
                                        <?=$orientation["ID"] == $arResult["WORK"]['PROPERTIES']["ORIENTATION"]["VALUE"] ? 'selected' : ''?>
                                    <?}?>
                                ><?=$orientation["NAME"]?></option>
                            <?}?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form__row">
            <span class="form__row-title">
                <?=$arResult["PROP_LIST"]["SIZE"]?><span class="reg_star">*</span>
                <?if ($arResult["HELPER"]["SIZE"]) {?>
                    <span class="tip js-tip" title="<?=$arResult["HELPER"]["SIZE"]?>"></span>
                <?}?>
            </span>
            <div class="form__row-main">
                <div class="form__field form__field_auto">
                    <input class="form__input js-input-number" type="text" name="WORK[WIDTH]" value="<?=$_POST["WORK"] ? $_POST["WORK"]["WIDTH"] : $arResult['WORK']['WIDTH']?>" placeholder="<?=GetMessage("WIDTH")?>" required="required">
                </div><span class="form__text">x</span>
                <div class="form__field form__field_auto">
                    <input class="form__input js-input-number" type="text" name="WORK[HEIGHT]" value="<?=$_POST["WORK"] ? $_POST["WORK"]["HEIGHT"] : $arResult['WORK']['HEIGHT']?>" placeholder="<?=GetMessage("HEIGHT")?>" required="required">
                </div><span class="form__text">x</span>
                <div class="form__field form__field_auto">
                    <input class="form__input js-input-number" type="text" name="WORK[DEPTH]" value="<?=$_POST["WORK"] ? $_POST["WORK"]["DEPTH"] : $arResult['WORK']['DEPTH']?>" placeholder="<?=GetMessage("DEPTH")?>" required="required">
                </div><span class="form__text"><?=GetMessage("CM")?></span>
            </div>
        </div>
        <div class="form__row">
            <span class="form__row-title">
                <?=$arResult["PROP_LIST"]["WEIGHT"]?><span class="reg_star">*</span>
                <?if ($arResult["HELPER"]["WEIGHT"]) {?>
                    <span class="tip js-tip" title="<?=$arResult["HELPER"]["WEIGHT"]?>"></span>
                <?}?>
            </span>
            <div class="form__row-main">
                <div class="form__field form__field_auto">
                    <input class="form__input js-input-float" type="text" name="WORK[WEIGHT]" value="<?=$_POST["WORK"] ? $_POST["WORK"]["WEIGHT"] : $arResult['WORK']['PROPERTIES']['WEIGHT']["VALUE"]?>" placeholder="" required="required" >

                </div><span class="form__text"><?=GetMessage("KG")?></span>
            </div>
        </div>
        <div class="form__row">
            <span class="form__row-title"><?=$arResult["PROP_LIST"]["YEAR"]?>
            <?if ($arResult["HELPER"]["YEAR"]) {?>
                <span class="tip js-tip" title="<?=$arResult["HELPER"]["YEAR"]?>"></span>
            <?}?>
            </span>
            <div class="form__row-main">
                <div class="form__field form__field_half_2">
                    <input class="form__input" type="text" name="WORK[YEAR]" value="<?=$_POST["WORK"] ? $_POST["WORK"]["YEAR"] : $arResult['WORK']['PROPERTIES']['YEAR']["VALUE"]?>">
                </div>
            </div>
        </div>
        <div class="form__row" data-code="THEME">
            <span class="form__row-title">
                <?=$arResult["PROP_LIST"]["THEME"]?><span class="reg_star">*</span>
                <?if ($arResult["HELPER"]["THEME"]) {?>
                    <span class="tip js-tip" title="<?=$arResult["HELPER"]["THEME"]?>"></span>
                <?}?>
            </span>
            <div class="form__row-main">
                <div class="form__field">
                    <div class="form__select form__select_multiply">
                    <select name="WORK[THEME][]" class="js-select2 js-select-close parent-select" data-placeholder="<?=GetMessage("THEME")?>" required multiple="multiple">
                        <option value=""><?=GetMessage("THEME")?></option>
                        <?foreach ($arResult["THEME"] as $theme) {?>
                            <option
                                value="<?=$theme["ID"]?>"
                                <?if ($_POST["WORK"]) {?>
                                    <?=$_POST["WORK"] && (array_search($theme["ID"], $_POST["WORK"]["THEME"]) !== false &&
                                                          array_search($theme["ID"], $_POST["WORK"]["THEME"]) !== null) ? 'selected' : ''?>
                                <?} elseif ($arResult['WORK']) {?>
                                    <?=(array_search($theme["ID"], $arResult["WORK"]['PROPERTIES']["THEME"]["VALUE"]) !== false &&
                                        array_search($theme["ID"], $arResult["WORK"]['PROPERTIES']["THEME"]["VALUE"]) !== null) ? 'selected' : ''?>
                                <?}?>
                            ><?=$theme["NAME"]?></option>
                        <?}?>
                    </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form__row" style="<?=count($arResult['THEME_SUB']) == 0 ? 'display: none' : ''?>" data-code="THEME_SUB">
            <span class="form__row-title">
                <?=$arResult["PROP_LIST"]["THEME_SUB"]?>
                <?if ($arResult["HELPER"]["THEME_SUB"]) {?>
                    <span class="tip js-tip" title="<?=$arResult["HELPER"]["THEME_SUB"]?>"></span>
                <?}?>
            </span>
            <div class="form__row-main">
                <div class="form__field">
                    <div class="form__select form__select_multiply">
                        <?if ($_REQUEST['UPDATE_AJAX'] == "Y" && $_REQUEST['PROP_CODE'] == 'THEME') {
                            $GLOBALS['APPLICATION']->RestartBuffer();
                        }?>
                            <select name="WORK[THEME_SUB][]" class="js-select2 js-select-close" data-placeholder="<?=GetMessage("THEME_SUB")?>" multiple="multiple">
                                <option value=""><?=GetMessage("THEME_SUB")?></option>
                                <?foreach ($arResult["THEME_SUB"] as $theme) {?>
                                    <option
                                        value="<?=$theme["ID"]?>"
                                        <?if ($_POST["WORK"]) {?>
                                            <?=$_POST["WORK"] && (array_search($theme["ID"], $_POST["WORK"]["THEME_SUB"]) !== false &&
                                                                  array_search($theme["ID"], $_POST["WORK"]["THEME_SUB"]) !== null) ? 'selected' : ''?>
                                        <?} elseif ($arResult['WORK']) {?>
                                            <?=(array_search($theme["ID"], $arResult["WORK"]['PROPERTIES']["THEME_SUB"]["VALUE"]) !== false &&
                                                array_search($theme["ID"], $arResult["WORK"]['PROPERTIES']["THEME_SUB"]["VALUE"]) !== null) ? 'selected' : ''?>
                                        <?}?>
                                    ><?=$theme["NAME"]?></option>
                                <?}?>
                            </select>
                        <?if ($_REQUEST['UPDATE_AJAX'] == "Y" && $_REQUEST['PROP_CODE'] == 'THEME') {
                            die();
                        }?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form__row" data-code="GENRE"><span class="form__row-title"><?=$arResult["PROP_LIST"]["GENRE"]?><?if ($arResult["HELPER"]["GENRE"]): ?>
                <span class="tip js-tip" title="<?=$arResult["HELPER"]["GENRE"]?>"></span>
            <?endif;?></span>
            <div class="form__row-main">
                <div class="form__field">
                    <div class="form__select">
                        <select name="WORK[GENRE][]" class="js-select2 parent-select" data-placeholder="<?=GetMessage("GENRE")?>">
                            <option value="0"><?=GetMessage("GENRE")?></option>
                            <?foreach ($arResult["GENRE"] as $theme): ?>
                                <option
                                    value="<?=$theme["ID"]?>"
                                    <?if ($_POST["WORK"]): ?>
                                        <?=$_POST["WORK"] && $theme["ID"]["0"] == $_POST["WORK"]["GENRE"] ? 'selected' : ''?>
                                    <?elseif ($arResult['WORK']): ?>
                                        <?=$theme["ID"] == $arResult["WORK"]['PROPERTIES']["GENRE"]["VALUE"]["0"] ? 'selected' : ''?>
                                    <?endif;?>
                                ><?=$theme["NAME"]?></option>
                            <?endforeach;?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="form__row" style="<?=count($arResult['GENRE_SUB']) == 0 ? 'display: none' : ''?>" data-code="GENRE_SUB"><span class="form__row-title"><?=$arResult["PROP_LIST"]["GENRE_SUB"]?><?if ($arResult["HELPER"]["GENRE_SUB"]): ?>
            <span class="tip js-tip" title="<?=$arResult["HELPER"]["GENRE_SUB"]?>"></span>
            <?endif;?></span>
            <div class="form__row-main">
                <div class="form__field">
                    <div class="form__select">
                        <?if ($_REQUEST['UPDATE_AJAX'] == "Y" && $_REQUEST['PROP_CODE'] == 'GENRE') {
                            $GLOBALS['APPLICATION']->RestartBuffer();
                        }?>
                            <select name="WORK[GENRE_SUB][]" class="js-select2" data-placeholder="<?=GetMessage("GENRE_SUB")?>">
                                <option value="0"><?=GetMessage("GENRE_SUB")?></option>
                                <?foreach ($arResult["GENRE_SUB"] as $theme): ?>
                                    <option
                                        value="<?=$theme["ID"]?>"
                                        <?if ($_POST["WORK"]): ?>
                                            <?=$_POST["WORK"] && $theme["ID"]["0"] == $_POST["WORK"]["GENRE_SUB"] ? 'selected' : ''?>
                                        <?elseif ($arResult['WORK']): ?>
                                            <?=$theme["ID"] == $arResult["WORK"]['PROPERTIES']["GENRE_SUB"]["VALUE"]["0"] ? 'selected' : ''?>
                                        <?endif;?>
                                    ><?=$theme["NAME"]?></option>
                                <?endforeach;?>
                            </select>
                        <?if ($_REQUEST['UPDATE_AJAX'] == "Y" && $_REQUEST['PROP_CODE'] == 'GENRE') {
                            die();
                        }?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form__row"><span class="form__row-title"><?=$arResult["PROP_LIST"]["TECHNIQUE"]?><?if ($arResult["HELPER"]["TECHNIQUE"]): ?>
                <span class="tip js-tip" title="<?=$arResult["HELPER"]["TECHNIQUE"]?>"></span>
            <?endif;?></span>
            <div class="form__row-main">
                <div class="form__field">
                    <input class="form__input" type="text" name="WORK[TECHNIQUE]" value="<?=$_POST["WORK"] ? $_POST["WORK"]["TECHNIQUE"] : $arResult['WORK']['PROPERTIES']['TECHNIQUE']["VALUE"]?>" placeholder=""  >
                </div>
            </div>
        </div>
        <div class="form__row"><span class="form__row-title"><?=$arResult["PROP_LIST"]["CANVAS"]?><?if ($arResult["HELPER"]["CANVAS"]): ?>
                <span class="tip js-tip" title="<?=$arResult["HELPER"]["CANVAS"]?>"></span>
            <?endif;?></span>
            <div class="form__row-main">
            <div class="form__field">
                <div class="form__select">
                <select name="WORK[CANVAS]" class="js-select2" data-placeholder="<?=GetMessage("CANVAS")?>">
                    <option value="0"><?=GetMessage("CANVAS")?></option>
                    <?foreach ($arResult["CANVAS"] as $theme): ?>
                        <option
                            value="<?=$theme["ID"]?>"
                            <?if ($_POST["WORK"]): ?>
                                <?=$_POST["WORK"] && $theme["ID"] == $_POST["WORK"]["CANVAS"] ? 'selected' : ''?>
                            <?elseif ($arResult['WORK']): ?>
                                <?=$theme["ID"] == $arResult["WORK"]['PROPERTIES']["CANVAS"]["VALUE"] ? 'selected' : ''?>
                            <?endif;?>
                        ><?=$theme["NAME"]?></option>
                    <?endforeach;?>
                </select>
                </div>
            </div>
            </div>
        </div>
        <div class="form__row"><span class="form__row-title form__row-title_small"><?=$arResult["PROP_LIST"]["LOCATION"]?><span class="reg_star">*</span><?if ($arResult["HELPER"]["LOCATION"]): ?>
                <span class="tip js-tip" title="<?=$arResult["HELPER"]["LOCATION"]?>"></span>
            <?endif;?><span><?=GetMessage("LOCATION_DESC")?></span></span>
            <div class="form__row-main">
                <div class="form__field">
                    <input class="form__input" id="city" type="text" name="WORK[LOCATION]" value="<?=$_POST["LOCATION"] ? $_POST["WORK"]["LOCATION"] : $arResult['WORK']['PROPERTIES']['LOCATION']['VALUE']?>" required="required">
                    <input id="location_id_hidden" type="hidden" name="WORK[LOCATION_ID]" value="<?=$_POST["LOCATION_ID"] ? $_POST["WORK"]["LOCATION_ID"] : $arResult['WORK']['PROPERTIES']['LOCATION_ID']['VALUE']?>">
                    <input id="region_hidden" type="hidden" name="WORK[REGION]" value="<?=$_POST["REGION"] ? $_POST["WORK"]["REGION"] : $arResult['WORK']['PROPERTIES']['REGION']['VALUE']?>">
                </div>
            </div>
        </div>
        <div class="form__row"><span class="form__row-title"><?=GetMessage("DIGITAL_COPY")?><?if ($arResult["HELPER"]["DIGITAL_COPY"]): ?>
                <span class="tip js-tip" title="<?=$arResult["HELPER"]["DIGITAL_COPY"]?>"></span>
            <?endif;?><span class="mobile-aside mobile-aside-digital"><a href="#modalRequire" data-fancybox-modal="data-fancybox-modal"><?=GetMessage("DIGITAL_COPY_REQ")?></a></span></span>
            <div class="form__row-main">
            <div class="form__field">
                <div class="form__drag js-form-drag">
                <div class="form__drag-field js-form-drag-field">
                    <input class="form__drag-input js-form-drag-input" type="file" name="DIGITAL_COPY" id="formDrag"/>
                    <label class="form__drag-label" for="formDrag">
                    <div class="form__drag-content">
                        <svg class="icon icon-upload">
                        <use xlink:href="#icon-upload"></use>
                        </svg><span class="hidden-tablet-down"><?=GetMessage("DRAG_FILE")?></span><span class="hidden-desktop-up"><?=GetMessage("SELECT_FILE")?></span>
                    </div>
                    </label>
                </div>
                <?if ($arResult['WORK']['PROPERTIES']['DIGITAL_COPY']['VALUE']['ID']): ?>
                    <div class="form__drag-file js-form-drag-file" style="display: flex;">
                    <svg class="icon icon-file">
                        <use xlink:href="#icon-file"></use>
                    </svg><span class="form__drag-name js-form-drag-name"><input type="hidden" value="<?=$arResult['WORK']['PROPERTIES']['DIGITAL_COPY']['VALUE']['ID']?>" name="DIGITAL_COPY_ID" id="digital_copy_id"><?=$arResult['WORK']['PROPERTIES']['DIGITAL_COPY']['VALUE']['ORIGINAL_NAME']?></span>
                    <div class="form__drag-remove js-form-drag-remove">
                        <svg class="icon icon-close">
                        <use xlink:href="#icon-close"></use>
                        </svg>
                    </div>
                    </div>
                <?else: ?>
                    <div class="form__drag-file js-form-drag-file">
                    <svg class="icon icon-file">
                        <use xlink:href="#icon-file"></use>
                    </svg><span class="form__drag-name js-form-drag-name"></span>
                    <div class="form__drag-remove js-form-drag-remove">
                        <svg class="icon icon-close">
                        <use xlink:href="#icon-close"></use>
                        </svg>
                    </div>
                    </div>
                <?endif;?>
                </div>
            </div>
            </div>
        </div>
        <div class="form__row"><span class="form__row-title form__row-title_small"><?=GetMessage("PUBLICATION")?><?if ($arResult["HELPER"]["PUBLICATION"]): ?>
                <span class="tip js-tip" title="<?=$arResult["HELPER"]["PUBLICATION"]?>"></span>
            <?endif;?></span>
            <div class="form__row-main">
            <label class="form__check">
                <input class="radio" type="checkbox" name="WORK[PUBLISHED]" value="<?=WORK_PROP_PUBLISHED_YES?>" <?=$arResult["PUBLISHED"] == WORK_PROP_PUBLISHED_YES ? 'checked="checked"' : '';?>/><span><?=GetMessage("POST_WORK")?></span>
            </label>
            </div>
        </div>
        <div class="form__row form__row_space_top">
            <div class="form__row-title"></div>
            <div class="form__row-main">
            <div class="form__buttons">
            <input type="hidden" name="WORK[AUTHOR]" value="<?=$arResult["AUTHOR"]?>">

                <input type="hidden" name="WORK[MODERATION]" value="<?=$arResult["MODERATION"]?>">

                <button class="btn btn_form btn_green" type="submit" name="WORK[ADD]"><?=GetMessage("SAVE_CHANGES")?></button>
            </div>
            </div>
        </div>
    </form>
    <span class="lk-subhead"><?=GetMessage("NOT_PARAM")?> <br/> <?=GetMessage("CONTACT_US")?></span>
    <div class="add_work_comment_message"></div>
    <form id="work_add_feedback" class="form" data-validate="data-validate">
        <div class="form__row "><span class="form__row-title form__row-title_small form__row-title_infinite"><?=GetMessage("SELECT_OPTIONS")?></span>
            <div class="form__row-main form__row-main_center">
                <label class="form__check">
                    <input class="radio" type="checkbox" name="found" value="Стиль"/><span><?=$arResult["PROP_LIST"]["STYLE"]?></span>
                </label>
                <label class="form__check">
                    <input class="radio" type="checkbox" name="found" value="Тема"/><span><?=$arResult["PROP_LIST"]["THEME"]?></span>
                </label>
                <label class="form__check">
                    <input class="radio" type="checkbox" name="found" value="Жанр"/><span><?=$arResult["PROP_LIST"]["GENRE"]?></span>
                </label>
                <label class="form__check">
                    <input class="radio" type="checkbox" name="found" value="Материал"/><span><?=$arResult["PROP_LIST"]["MATERIAL"]?></span>
                </label>
            </div>
        </div>
        <div class="form__row form__row_space_bottom">
            <label class="form__check sub__check">
                <input class="radio" type="checkbox" name="found" value="Стиль подраздел"/><span><?=$arResult["PROP_LIST"]["STYLE_SUB"]?></span>
            </label>
            <label class="form__check sub__check">
                <input class="radio" type="checkbox" name="found" value="Тема подраздел"/><span><?=$arResult["PROP_LIST"]["THEME_SUB"]?></span>
            </label>
            <label class="form__check sub__check">
                <input class="radio" type="checkbox" name="found" value="Жанр подраздел"/><span><?=$arResult["PROP_LIST"]["GENRE_SUB"]?></span>
            </label>
            <label class="form__check sub__check">
                <input class="radio" type="checkbox" name="found" value="Материал подраздел"/><span><?=$arResult["PROP_LIST"]["MATERIAL_SUB"]?></span>
            </label>
        </div>
        <div class="form__row"><span class="form__row-title"><?=GetMessage("COMMENT")?></span>
            <div class="form__row-main">
                <div class="form__field">
                    <textarea class="form__area js-form-area" name="comment" required="required"></textarea>
                </div>
            </div>
        </div>
        <div class="form__row">
            <div class="form__buttons form__buttons_right">
                <button class="btn btn_green_outline js-work__comment" type="submit"><?=GetMessage("SEND_MESSAGE")?></button>
            </div>
        </div>
    </form>
    <?if ($arResult["WORK"]) {?>
        <span class="lk-subhead"><?=GetMessage("ADD_DICOUNT")?></span>
        <div class="add_work_discount_message"></div>
        <form id="work_add_discount" class="form" data-validate="data-validate" data-work_id="<?=$arResult["WORK"]["ID"]?>" data-work_name="<?=$arResult["WORK"]["NAME"]?>" data-work_code="<?=$arResult["WORK"]["CODE"]?>" data-user_id="<?=$USER->GetID();?>" data-user_login="<?=$USER->GetLogin();?>">         <div class="form__row"><span class="form__row-title"><?=GetMessage("COMMENT")?></span>
            <div class="form__row-main">
            <div class="form__field">
                <textarea class="form__area js-form-area" name="comment" required="required"></textarea>
            </div>
            </div>
        </div>
        <div class="form__row">
            <div class="form__buttons form__buttons_right">
            <button class="btn btn_green_outline js-work__comment" type="submit"><?=GetMessage("SEND_MESSAGE")?></button>
            </div>
        </div>
        </form>
    <?}?>
</div>

<div class="modal" id="modalRequire"><span class="modal__header"><?=$arResult['DIGITAL_COPY_TITLE']?></span>
    <p><?=$arResult['DIGITAL_COPY_TEXT']?></p>
</div>