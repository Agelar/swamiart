//добавление фотографий
$(document).on('change', '.work_img', function(){
    var files; // переменная. будет содержать данные файлов

    files = this.files;
    var arrType = files[0]['type'].split("/");
    
    // Проверка на соответствиие
    var isError = false;
    if(arrType[0] == 'image')
    {
        $('.form__images-add').remove();
        $('.photo_list').append('<div class="form__images-item js-form-images-item is-loader"></div>');
        var nameFile = files[0]['name'];
        var $that = $('#add_work_form');
        // создадим объект данных формы
        var data = new FormData($that.get(0));

        data.append('example',files[0]);
        data.append('ajax','Y');
        // AJAX запрос
        setTimeout(
            function(){
                $.ajax({
                    url         : '/local/ajax/upload_file_ajax.php?ajax=Y',
                    type        : 'POST', // важно!
                    data        : data,
                    cache       : false,
                    dataType    : 'json',
                    processData : false,
                    contentType : false,
                    success     : function( respond){
                        // ОК - файлы загружены
                        if( typeof respond.error === 'undefined' ){
                            
                            block = '<div class="form__images-item js-form-images-item" style="background-image:url('+respond.src+');" data-idfile="' + respond.id + '" data-text="' + $('.form__row_detail_picture').data('text') + '">' +
                                '<input type="hidden" name="WORK[PICTURE][SRC][]" value="' + respond.src + '">' +
                                '<input type="hidden" name="WORK[PICTURE][ID][]" value="' + respond.id + '">' +
                            '</div>' +
                            '<label class="form__images-add" for="formImagesInput">' +
                            '<svg class="icon icon-plus">' +
                            '<use xlink:href="#icon-plus"></use>' +
                            '</svg><span>'+$('.js-form-images').data('to_add')+'</span>' +
                            '</label>';
                            $('.is-loader').remove();
                            $('.photo_list').append(block);
                            $('.work_img').val('');
                            $('.js-input-images').valid();
                        }
                        // ошибка
                        else {
                            
                            console.log('ОШИБКА: ' + respond.error );
                        }
                    },
                    // функция ошибки ответа сервера
                    error: function( jqXHR, status, errorThrown ){
                    
                        console.log( 'ОШИБКА AJAX запроса: ' + status, jqXHR );
                    }

                });
            }, 1000
        );

        


    }
    else
    {
        isError = true;
    }
});

//удаление фотографий
$(document).on('click', '.form__images-item', function(){
    var thisPhoto = $(this);
    var fileId = thisPhoto.data('idfile');
    $.ajax({
        type: 'GET',
        url: '/local/ajax/ajax.php?AJAX=Y&DELETE_FILE=Y&FILE_ID=' + fileId,
        dataType: 'json',
        success: function(data){
            if(data.SUCCESS){
                thisPhoto.remove();
            }
        }
  
    });
    return false;
});


var authorName = '';
// Автор/Владелец
$(document).on('change', '.js-type__author', function(){
    if($(this).val()=='owner'){
        $('.js-true__author').show();
        if(authorName && $('.js-true__author .form__input').val()=='')
        $('.js-true__author .form__input').val(authorName);
    }
    else{
        $('.js-true__author').hide();
        authorName = $('.js-true__author .form__input').val();
        $('.js-true__author .form__input').val('');
    }
});

//сохранение работы
$(document).on('formSubmit', '#add_work_form', function(){

        var $that = $(this);
        var files = $('#digital_copy_id');
        // создадим объект данных формы
        var data = new FormData($that.get(0));

        data.append('DIGITAL_COPY',files[0]);
        data.append('AJAX','Y');
        // AJAX запрос
        $.ajax({
            url         : location.pathname,
            type        : 'POST', // важно!
            data        : data,
            cache       : false,
            dataType    : 'json',
            processData : false,
            contentType : false,
            success     : function( respond){
                console.log(respond);
                if(!respond.ERROR){
                    if(respond.TYPE=='SAVE_WORK'){
                        var message = '<div class="alert alert__success" style="display: block;">'+respond.MESSAGE+'</div>';
                        $('.add_work_form_message').html(message);
                        var $target = $('.lk__main-inner');
		                $('html,body').animate({scrollTop: $target.offset().top - 200}, 300, 'swing');
                        
                    }
                    else{
                        var message = '<div class="alert alert__success" style="display: block;">'+respond.MESSAGE+'</div>';
                        $('.add_work_form_message').html(message);
                        location.href = location.pathname.replace('/personal/adding-work/', '/personal/work-list/')+respond.WORK_ID+'/?NEW=Y';
                    }
                }
                else{
                    var message = '<div class="alert alert__error" style="display: block;">'+respond.ERROR+'</div>';
                    $('.add_work_form_message').html(message);
                    var $target = $('.lk__main-inner');
		            $('html,body').animate({scrollTop: $target.offset().top - 200}, 300, 'swing');
                }
            },
            // функция ошибки ответа сервера
            error: function( jqXHR, status, errorThrown ){
               
                console.log( 'ОШИБКА AJAX запроса: ' + status, jqXHR );
            }

        });
});


//запрос на добавление параметров
$(document).on('formSubmit', '#work_add_feedback', function(){
    var param = '';
    $(this).find('[name="found"]:checked').each(
        function(){
            if(!param) param += $(this).val();
            else param += ', ' + $(this).val();
            
        }
        
    );
    var comment = $(this).find('[name="comment"]').val();
    $.ajax({
        type: 'GET',
        url: '/local/ajax/ajax.php?AJAX=Y&WORK_ADD_PARAM=Y&PARAM=' + param + '&COMMENT=' + comment + '&SITE=' + BX.message('SITE_ID'),
        dataType: 'json',
        success: function(data){
            if(data.SUCCESS){
                var message = '<div class="alert alert__success" style="display: block;">'+data.MESSAGE+'</div>';
                $('.add_work_comment_message').html(message);
                $('#work_add_feedback').find('[name="comment"]').val('');
                $('#work_add_feedback').find('[name="found"]:checked').each(
                    function(){
                        $(this).prop('checked', false);
                        
                    }
                    
                );
            }
        }
  
    });
    return false;
});


//запрос на добавление скидки
$(document).on('formSubmit', '#work_add_discount', function(){
    var work = $(this).data('work_id');
    var workName = $(this).data('work_name');
    var workCode = $(this).data('work_code');
    var user = $(this).data('user_id');
    var userLogin = $(this).data('user_login');
    var comment = $(this).find('[name="comment"]').val();
    $.ajax({
        type: 'GET',
        url: '/local/ajax/ajax.php?AJAX=Y&WORK_ADD_DISCOUNT=Y&WORK=' + work + '&USER=' + user + '&WORK_NAME=' + workName + '&WORK_CODE=' + workCode + '&USER_LOGIN=' + userLogin + '&COMMENT=' + comment + '&SITE=' + BX.message('SITE_ID'),
        dataType: 'json',
        success: function(data){
            if(data.SUCCESS){
                var message = '<div class="alert alert__success" style="display: block;">'+data.MESSAGE+'</div>';
                $('.add_work_discount_message').html(message);
                $('#work_add_discount').find('[name="comment"]').val('');
                
            }
        }
  
    });
    return false;
});

//запрос списка поджанров
$(document).on('change', '.parent-select', function(){

    var $that = $(this);
    var data = new FormData();

    var parentRow = $that.parents('.form__row');

    data.append('UPDATE_AJAX','Y');
    data.append('SECTION_ID', $that.val());
    data.append('PROP_CODE', parentRow.data('code'));

    $.ajax({
        url         : location.pathname,
        type        : 'POST', 
        data        : data,
        cache       : false,
        dataType    : 'html',
        processData : false,
        contentType : false,
        success     : function(respond){
            var block = $('body').find('.form__row[data-code="'+parentRow.data('code')+'_SUB"]').find('.form__select');
            if($(respond).find('option').length > 1) {
                block.children('.selectize-control').remove();
                block.html(respond);
                block.find('.js-select2').each(function(index, elem) {
                    var placeholderText = $(this).attr('data-placeholder');
                    var multi = $(this).is('.js-select-close');
                    $(this).selectize({
                        plugins: multi ? ['remove_button'] : [],
                        highlight: true,
                        closeAfterSelect: true,
                        hideSelected: false,
                        onDropdownClose: function($dropdown){
                            $($dropdown).find('.selected').not('.active').removeClass('selected');
                        },
                        onChange: function() {
                            if(this.$input.is('[required]')) {
                                this.$input.valid();
                            }
                        }
                    });
                });
                block.parents('.form__row').show();
            } else {
                block.parents('.form__row').hide();
            }
        },
        error: function( jqXHR, status, errorThrown ){
            console.log( 'ОШИБКА AJAX запроса: ' + status, jqXHR );
        }

    });
});
