<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Loader;
use \Bitrix\Main\Application;
use \Bitrix\Main\Context;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class ChangePasswordComponent extends CBitrixComponent{

    function onPrepareComponentParams($arParams) {
        return $arParams;
    }

    function executeComponent(){
        global $USER;
        $this->arResult["IS_AUTHORIZED"] = $USER->IsAuthorized();
        $this->arResult["CURRENT_USER"] = $USER->GetByID($USER->GetID())->GetNext();

        if($this->request["changepass"] == "Y"){
            if(check_bitrix_sessid()){
                $this->arResult["AJAX_RELOAD"] = "Y";
                $last_pass_match = true;

                if($this->arParams["LAST_PASS"] == "Y"){
                    $res = $USER->Login((string)$this->arResult["CURRENT_USER"]["LOGIN"], (string)$this->request["last_pass"]);
                    $last_pass_match = (!is_array($res)) ? true : false;
                }

                if(strlen($this->request["new_pass"]) < 8) {
                    $this->arResult["ANSWER"]=Loc::getMessage("CHPASS_PASS_MIN");
                } else {
                    if(!$last_pass_match) {
                        $this->arResult["ANSWER"] = Loc::getMessage("CHPASS_LAST_PASS_NOT_MATCH");
                    } else {
                        $USER->Update($this->arResult["CURRENT_USER"]["ID"], array(
                            "PASSWORD"         => $this->request["new_pass"],
                            "CONFIRM_PASSWORD" => $this->request["new_pass"]
                        ));
                        $this->arResult["ANSWER"] = Loc::getMessage("CHPASS_CHANGE_SUCCESS");
                    }
                }
            } else {
                $this->arResult["ANSWER"] = Loc::getMessage("CHPASS_TIME_SESSION");
            } 
        }
        $this->includeComponentTemplate();   
    }
}