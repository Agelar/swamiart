<?
$MESS["CHPASS_CAPTION"] = "Change password";
$MESS["CHPASS_SUBMIT"] = "Change password";
$MESS["CHPASS_DESC"] = "You can change your password here";
$MESS["CHPASS_TOOLTIP"] = "Passwords must be at least 8 characters long";
$MESS["CHPASS_LAST_PASS"] = "Current password";
$MESS["CHPASS_NEW_PASS"] = "New password";
$MESS["CHPASS_NEW_PASS2"] = "Confirm new password";
$MESS["CHPASS_SUBMIT"] = "Save changes";
?>