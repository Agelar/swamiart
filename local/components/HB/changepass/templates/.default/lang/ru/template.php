<?
$MESS["CHPASS_NEED_AUTH"] = "Для смены пароля необходимо авторизоваться";
$MESS["CHPASS_CAPTION"] = "Изменить пароль";
$MESS["CHPASS_DESC"] = "Здесь вы можете изменить ваш пароль";
$MESS["CHPASS_TOOLTIP"] = "Пароль должен содержать не менее 8 символов";
$MESS["CHPASS_LAST_PASS"] = "Текущий пароль";
$MESS["CHPASS_NEW_PASS"] = "Новый пароль";
$MESS["CHPASS_NEW_PASS2"] = "Подтвердить новый пароль";
$MESS["CHPASS_SUBMIT"] = "Сохранить изменения";
?>