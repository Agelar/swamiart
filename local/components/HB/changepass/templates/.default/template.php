<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="lk__main-inner"><span class="lk-head"><?=GetMessage("CHPASS_CAPTION")?></span>
    <div class="lk-desc"><span><?=GetMessage("CHPASS_DESC")?></span></div>
    <div class="lk-block">
        <span class="lk-block__delimiter"></span>
        <div class="lk-block__main changepass">
            <?if($arResult["AJAX_RELOAD"] == "Y") $APPLICATION->RestartBuffer();?>
                <form class="form" data-validate="data-validate">
                    <?=bitrix_sessid_post();?>
                    <input type="hidden" name="changepass" value="Y"/>

                    <? if($arResult["ANSWER"]): ?>
                        <div class="changepass_answer"><?= $arResult["ANSWER"] ?></div>
                        <script>
                            setTimeout(function(){
                                $(".changepass_answer").fadeOut();
                            },2000);
                        </script>
                    <? endif; ?>

                    <div class="form__row">
                        <span class="form__row-title"><?=GetMessage("CHPASS_LAST_PASS")?></span>
                        <div class="form__row-main">
                            <div class="form__field">
                                <input class="form__input" type="password" required="required" name="last_pass"/>
                            </div>
                        </div>
                    </div>

                    <div class="form__row">
                        <span class="form__row-title"><?=GetMessage("CHPASS_NEW_PASS")?></span>
                        <div class="form__row-main">
                            <div class="form__field">
                                <input class="form__input" type="password" required="required" name="new_pass"/>
                            </div>
                        </div>
                    </div>

                    <div class="form__row">
                        <span class="form__row-title lk-block__tooltip"><?=GetMessage("CHPASS_TOOLTIP")?></span>
                        <div class="form__row-main">
                            <div class="form__buttons">
                                <button class="btn btn_green btn_form btn_form--top-margin" type="submit"><?=GetMessage("CHPASS_SUBMIT");?></button>
                            </div>
                        </div>
                    </div>
                </form>
            <?if($arResult["AJAX_RELOAD"] == "Y") die();?>
        </div>
    </div>
</div>