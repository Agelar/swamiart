<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Loader;
use \Bitrix\Main\Application;
use \Bitrix\Main\Context;
use Bitrix\Main\Localization\Loc;
use HB\userEntity\Notification;
use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Engine\ActionFilter;

Loc::loadMessages(__FILE__);

class NotificateUserComponent extends CBitrixComponent implements Controllerable{

    function onPrepareComponentParams($arParams) {
        return $this->arParams;
    }

    public function configureActions()
    {
        return [
            'changeUserData' => [
                'prefilters' => [
                    new ActionFilter\Authentication(),
                ],
            ],
        ];
    }

    private function getInfoByNotificate($userID){
        $notificate = new Notification($userID);
        $arUser = $notificate->getAllInfoByNotification();
        $result = array();
        if ($arUser) {
            $result["EMAIL_NOTIFICATION"] = array(
                "STATUS_ORDER" => array(
                    "NAME" => Loc::getMessage("STATUS_ORDER"),
                    "DESCRIPTION" => Loc::getMessage("STATUS_ORDER_DESC"),
                    "VALUE" => $arUser["UF_STATUS_ORDER"] > 0 ? "Y" : "N",
                ),
                "NEWSLETTERS" => array(
                    "NAME" => Loc::getMessage("NEWSLETTERS"),
                    "DESCRIPTION" => Loc::getMessage("NEWSLETTERS_DESC"),
                    "VALUE" => $arUser["UF_NEWSLETTERS"] > 0 ? "Y" : "N",
                )
            );
            foreach($arUser["UF_REGULARITY"] as &$regular){
                $regular["NAME"] = Loc::getMessage("REGULAR_DAYS_".$regular["XML_ID"]);
            }

            if($arUser["UF_AUTHORS"]){
                foreach($arUser["UF_AUTHORS"] as $author){
                    $result["AUTHORS"][] = array(
                        "ID" => $author["ID"],
                        "NAME" => $author["NAME"],
                        "LINK" => $author["DETAIL_PAGE_URL"],
                        "PICTURE" => CFile::ResizeImageGet($author["DETAIL_PICTURE"], array("width"=>"100","height"=>"100"), BX_RESIZE_IMAGE_PROPORTIONAL)["src"],
                    );
                }
            }
            $result["SITE_LANG"] = array(
                array(
                    "NAME" => Loc::getMessage("RUS"),
                    "VALUE" => "s1",
                    "SELECT" => ($arUser["UF_SITE_LANG"] == "s1") ? "Y" : "N",
                ),
                array(
                    "NAME" => Loc::getMessage("ENG"),
                    "VALUE" => "en",
                    "SELECT" => ($arUser["UF_SITE_LANG"] == "en") ? "Y" : "N",
                )
            );
            $result["REGULAR_LIST"] = $arUser["UF_REGULARITY"];
        }
        return $result;
    }

    public function changeUserDataAction($entity, $value){
        global $USER;
        Loader::IncludeModule("hb.site");

        $notificate = new Notification($USER->getID());

        $status = false;

        switch($entity) {
            case "REGULARITY":
            case "NEWSLETTERS":
            case "STATUS_ORDER":
            case "SITE_LANG":
                $status = $notificate->changeProps("UF_" . $entity,$value);
                break;
            case "AUTHORS_UNSUBSCRIBE":
                $status = $notificate->unsubscribeAuthor($value);
                break;
        }
        ob_start();
            if($status){
                $this->includeComponentTemplate("success_alert");    
            }else{
                $this->includeComponentTemplate("error_alert");
            }
            $template = ob_get_contents();
        ob_end_clean();
        

        return array(
            "STATUS" => $status,
            "ENTITY" => $entity,
            "VALUE" => $value,
            "HTML" => $template
        );
    }

    function executeComponent(){
        Loader::IncludeModule("hb.site");
        global $USER;
        global $APPLICATION;

        $this->arResult = $this->getInfoByNotificate($USER->getId());

        $this->includeComponentTemplate();   
    }
}