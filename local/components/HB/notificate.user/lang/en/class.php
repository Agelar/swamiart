<?
$MESS["STATUS_ORDER"] = "Order status";
$MESS["STATUS_ORDER_DESC"] = "The appearance of the order, change its status";
$MESS["NEWSLETTERS"] = "News distribution";
$MESS["NEWSLETTERS_DESC"] = "Description of disribution";
$MESS["REGULAR_DAYS_1"] = "Once a day";
$MESS["REGULAR_DAYS_7"] = "Once a week";
$MESS["REGULAR_DAYS_30"] = "Once a month";
$MESS["RUS"] = "Russian";
$MESS["ENG"] = "English";
?>