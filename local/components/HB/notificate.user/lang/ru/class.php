<?
$MESS["STATUS_ORDER"] = "О статусе заказа";
$MESS["STATUS_ORDER_DESC"] = "Появление заказа, изменение его статуса";
$MESS["NEWSLETTERS"] = "Новостные рассылки";
$MESS["NEWSLETTERS_DESC"] = "Описание рассылки";
$MESS["REGULAR_DAYS_1"] = "Раз в день";
$MESS["REGULAR_DAYS_7"] = "Раз в неделю";
$MESS["REGULAR_DAYS_30"] = "Раз в месяц";
$MESS["RUS"] = "Русский";
$MESS["ENG"] = "Английский";
?>