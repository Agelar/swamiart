<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use Bitrix\Main\Localization\Loc;
?>
<div class="lk__main-inner">
    <span class="lk-head"><?=Loc::getMessage("SUBTITLE")?></span>
    <div class="lk-desc"><span><?=Loc::getMessage("REGULAR_LIST_LABEL")?></span></div>
    <div class="lk-block">
        <span class="lk-block__delimiter"></span>
        <div class="lk-block__main">
            <form class="form" data-validate="data-validate">
                <div class="form-notification__row">
                    <span class="form-notification__title"><?=Loc::getMessage("REGULAR_LIST_LABEL2")?></span>
                    <div class="form__row-main form-notification__select">
                        <div class="form__select">
                            <select class="js-select2 js-change-notificate" name="REGULARITY">
                                <?foreach($arResult["REGULAR_LIST"] as $regular):?>
                                    <option <?=($regular["SELECT"] == "Y") ? "selected" : ""?> value="<?=$regular["ID"]?>"><?=$regular["NAME"]?></option>
                                <?endforeach;?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-notification__row">
                    <span class="form-notification__title"><?=Loc::getMessage("LANG_SEND")?></span>
                    <div class="form__row-main form-notification__select">
                        <div class="form__select">
                            <?foreach($arResult["SITE_LANG"] as $lang):?>
                                <label class="form__radio">
                                    <input class="radio js-radio-notificate" value="<?=$lang["VALUE"]?>" type="radio" name="SITE_LANG" value="M" <?if($lang["SELECT"] == "Y"):?>checked="checked"<?endif;?>><span><?=$lang["NAME"]?></span>
                                </label>
                            <?endforeach;?>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="lk-block">
        <span class="lk-block__head lk-block__head--margin"><?=Loc::getMessage("EMAIL_MAILING")?></span>
        <div class="lk-block__main">
            <form class="form" data-validate="data-validate">
                <?foreach($arResult["EMAIL_NOTIFICATION"] as $nameNotificate => $notificate):?>  
                    <div class="form-notification__row-checkbox">
                        <label class="label label-inline form-notification__label-checkbox">
                            <input class="input input-checkbox js-change-notificate" type="checkbox" name="<?=$nameNotificate?>" <?=($notificate["VALUE"] == "Y") ? "checked" : ""?> />
                            <div class="input-checkbox-block">
                                <svg class="icon icon-check-w">
                                    <use xlink:href="#icon-check"></use>
                                </svg>
                            </div>
                            <div class="input-checkbox-text form-notification__text-checkbox"><?=$notificate["NAME"]?></div>
                        </label>
                        <div class="form-notification__desc-checkbox"><?=$notificate["DESCRIPTION"]?></div>
                    </div>  
                <?endforeach;?>
            </form>
        </div>
    </div>
    <?if($arResult["AUTHORS"]):?>
        <div class="lk-block js-authors-wrap">
            <span class="lk-block__head lk-block__head--margin"><?=Loc::getMessage("SUBSCRIBE_AUTHORS")?></span>
            <div class="lk-block__main">
                <?foreach($arResult["AUTHORS"] as $author):?>
                    <form class="sign-author js-author" data-id="<?=$author["ID"]?>">
                        <div class="sign-author__info">
                            <?if($author["PICTURE"]):?>
                                <div class="sign-author__img" style="background-image:url(<?=$author["PICTURE"]?>)"></div>
                            <?else:?>
                            <div class="sign-author__img sign-author__img--no-picture">
                                <svg class="icon icon-user">
                                    <use xlink:href="#icon-user"></use>
                                </svg>
                            </div>
                            <?endif;?>
                            <a class="sign-author__name" href="<?=$author["LINK"]?>"><?=$author["NAME"]?></a>
                        </div>
                        <button class="sign-author__btn js-click-notificate" data-id="<?=$author["ID"]?>"><?=Loc::getMessage("UNSUBSCRIBE")?></button>
                    </form>
                <?endforeach;?>
            </div>
        </div>
    <?endif;?>
</div>
<script>
    new CNotificateLk();
</script>