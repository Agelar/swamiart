var CNotificateLk = function(){
    this.sendAjax = false;

    this.eventInit();
}
CNotificateLk.prototype.eventInit = function(){
    $(document).on("click",".js-click-notificate",$.proxy(this.clickHandler,this));
    $(document).on("change",".js-change-notificate",$.proxy(this.changeHandler,this));
    $(document).on("change",".js-radio-notificate",$.proxy(this.radioHandler,this));
}

CNotificateLk.prototype.clickHandler = function(e){
    e.preventDefault();
    var self = $(e.target);
    var result = {
        value: self.attr("data-id"),
        entity: "AUTHORS_UNSUBSCRIBE",
    }
    this.submit_change(result, self);
}

CNotificateLk.prototype.changeHandler = function(e){
    if(this.sendAjax){
        e.peventDefault();
        e.stopPropagation();
        return;
    }

    var self = $(e.target);
    var result = {
        value: (self.is("select")) ? self.val() : self.prop("checked"),
        entity: self.attr("name"),
    }
    this.submit_change(result, self);
}

CNotificateLk.prototype.radioHandler = function(e){
    if(this.sendAjax){
        e.peventDefault();
        e.stopPropagation();
        return;
    }

    var self = $(e.target);
    var result = {
        value: self.val(),
        entity: self.attr("name"),
    }
    this.submit_change(result, self);
}

CNotificateLk.prototype.submit_change = function(data,el){
    var that = this;
    if(!that.sendAjax){
        that.sendAjax = true;
        BX.ajax.runComponentAction('HB:notificate.user',
        'changeUserData', { 
            mode: 'class',
            data: data,
        })
        .then(function(result) {
            if(result.data.STATUS){
                var parentBlock = el.parents(".lk-block");
                if(result.data.ENTITY == "AUTHORS_UNSUBSCRIBE"){
                    $(".js-author[data-id=" + result.data.VALUE + "]").remove();
                    if($(".js-author").length == 0){
                        $(".js-authors-wrap").remove();
                    }
                }
                if(parentBlock.siblings(".alert").length == 0){
                    parentBlock.before(result.data.HTML);
                    parentBlock.siblings(".alert").fadeIn(300);
                    setTimeout(function(){
                        parentBlock.siblings(".alert").fadeOut(300,function(){
                            parentBlock.siblings(".alert").remove();
                        });
                    },2000);
                }
                that.sendAjax = false;
            }
        });
    }
}