<?
$MESS["SUBTITLE"] = "Уведомления и подписки";
$MESS["REGULAR_LIST_LABEL"] = "Здесь вы можете управлять уведомлениями и подписками";
$MESS["REGULAR_LIST_LABEL2"] = "Отправлять уведомления о новых работах авторов не чаще чем";
$MESS["EMAIL_MAILING"] = "E-mail рассылки";
$MESS["SUBSCRIBE_AUTHORS"] = "Подписки на авторов";
$MESS["UNSUBSCRIBE"] = "Отписаться";
$MESS["LANG_SEND"] = "Предпочитаемый язык рассылки";
?>