<?
$MESS["SUBTITLE"] = "Notifications and Subscriptions";
$MESS["REGULAR_LIST_LABEL"] = "You can manage your notifications and subscriptions here";
$MESS["REGULAR_LIST_LABEL2"] = "Send notifications of new works of authors no more than";
$MESS["EMAIL_MAILING"] = "Email distribution";
$MESS["SUBSCRIBE_AUTHORS"] = "Subscriptions to authors";
$MESS["UNSUBSCRIBE"] = "Unsubscribe";
$MESS["LANG_SEND"] = "Preferable language of the distribution";

?>