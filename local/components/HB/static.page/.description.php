<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "Статичные страницы",
    "DESCRIPTION" => "Выводит конетент на страницу из инфоблока",
    "PATH" => array(
		"ID" => "content",
		"CHILD" => array(
			"ID" => "static_page",
			"NAME" => "Статичные страницы",
			"SORT" => 10,
			"CHILD" => array(
				"ID" => "static_page_ch",
			),
		),
	),
);

?>