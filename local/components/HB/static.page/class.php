<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Loader;
use \Bitrix\Main\Application;
use \Bitrix\Main\Context;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Engine\ActionFilter;
use Bitrix\Iblock\InheritedProperty\ElementValues;
Loc::loadMessages(__FILE__);

class StaticPageComponent extends CBitrixComponent{

    function onPrepareComponentParams($arParams) {
        return $arParams;
    }

    function executeComponent(){
        global $APPLICATION;
        if($this->arParams["IBLOCK_ID"]){
            $url = $APPLICATION->getCurPage();
            $db = CIBlockElement::GetList(array(), array("IBLOCK_ID" => $this->arParams["IBLOCK_ID"], "ACTIVE" => "Y", "PROPERTY_LINK" => $url), false,false, array("PREVIEW_PICTURE", "DETAIL_TEXT", "ID", "NAME"));

            if($res = $db->fetch()){
                $this->arResult["PICTURE"] = CFile::getPath($res["PREVIEW_PICTURE"]);
                $this->arResult["TEXT"] = $res["DETAIL_TEXT"];
                $this->arResult["TITLE"] = $res["NAME"];

                $ipropValues = new ElementValues($this->arParams["IBLOCK_ID"], $res["ID"]);
                $arElMetaProp = $ipropValues->getValues();

                if($arElMetaProp["ELEMENT_META_TITLE"]){
                    $APPLICATION->SetPageProperty("title", $arElMetaProp["ELEMENT_META_TITLE"]);
                }

                if($arElMetaProp["ELEMENT_META_DESCRIPTION"]){
                    $APPLICATION->SetPageProperty("description", $arElMetaProp["ELEMENT_META_DESCRIPTION"]);
                }

                if($arElMetaProp["ELEMENT_META_KEYWORDS"]){
                    $APPLICATION->SetPageProperty("keywords", $arElMetaProp["ELEMENT_META_KEYWORDS"]);
                }
                $this->includeComponentTemplate();
            }
        }
        
    }
}