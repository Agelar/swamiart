<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="section blog-detail static-page-content">
	<div class="container">
        <div class="blog-detail__inner">
            <h1 class="blog-detail__heading"><?=$arResult["TITLE"]?></h1>
            <div class="blog-detail__content">
                <?if($arResult["PICTURE"]):?>
					<div class="blog-detail__image blog-detail__image--full">
						<img class="blog-detail__image-pic" src="<?=$arResult["PICTURE"]?>" alt="">
					</div>
				<?endif;?>
                <?=$arResult["TEXT"]?>
            </div>
        </div>
    </div>
</div>
