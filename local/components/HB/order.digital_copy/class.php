<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
CModule::IncludeModule("iblock");
CModule::IncludeModule("main");
CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");
CModule::IncludeModule("hb.site");
use Bitrix\Sale,
    Bitrix\Sale\Order,
    HB\helpers\WorkHelpers;


class OrderDigitalCopyComponent extends CBitrixComponent{

    function onPrepareComponentParams($arParams) {
        return $this->arParams;
    }

    function executeComponent(){
        if($this->request["user_id"] && $this->request["author_id"] && $this->request["work_id"]){
            $elementId = $this->addOrderElement($this->request["user_id"], $this->request["author_id"], $this->request["work_id"], 5000);
            if(is_numeric($elementId)) $this->addBasketElement($elementId);
        }
        $this->includeComponentTemplate();   
    }

    //создание элемента заказа цифровой копии 
    function addOrderElement($userId, $authorId, $workId, $price){
        $rsOrder = \CIBlockElement::GetList(array(), array("IBLOCK_ID"=> IBLOCK_ORDER_DIGITAL_COPY_ID, 'ACTIVE' => 'Y', 'PROPERTY_USER' => $userId, 'PROPERTY_AUTHOR' => $authorId, 'PROPERTY_WORK' => $workId), false, false, array('ID'));
        if($arOrder = $rsOrder->GetNext()) return $arOrder['ID'];
        $elOrder = new CIBlockElement;
        $arProp = array();
        $arProp['USER'] = $userId;
        $arProp['AUTHOR'] = $authorId;
        $arProp['WORK'] = $workId;
       
        $arLoadProductArray = Array(
            "IBLOCK_ID"      => IBLOCK_ORDER_DIGITAL_COPY_ID,
            "PROPERTY_VALUES"=> $arProp,
            "NAME"           => 'Заказ пользователем ' . $userId . ' цифровой копии работы ' . $workId,
            "ACTIVE"         => "Y", 
        );

        $elementId = $elOrder->Add($arLoadProductArray);
        if(!$elementId) return $elOrder->LAST_ERROR;
        else{
            $priceTypeId = 1;
            $arFields = Array(
                "PRODUCT_ID" => $elementId,
                "CATALOG_GROUP_ID" => $priceTypeId,
                "PRICE" => $price,
                "CURRENCY" => "RUB",
            );
            $res = CPrice::GetList(
                array(),
                array(
                    "PRODUCT_ID" => $elementId,
                    "CATALOG_GROUP_ID" => $priceTypeId
                )
            );
            if ($arr = $res->Fetch()){
                CPrice::Update($arr["ID"], $arFields);
            }
            else{
                CPrice::Add($arFields);
            }
            $arFields = array(
                "ID" => $elementId, 
                "QUANTITY" => 1,
            );
            CCatalogProduct::Add($arFields);
            return $elementId;
        }
    }

    //добавление элемента заказа в корзину
    function addBasketElement($elementId){
        $siteId = \Bitrix\Main\Context::getCurrent()->getSite();
		//Создаем объект корзины
        $basket = Sale\Basket::create($siteId);
        //Кладем товар с id 31 и задаем ей параметры
        $item = $basket->createItem('catalog', $elementId);
        $item->setFields([
            'QUANTITY' => 1,
            'CURRENCY' => "RUB",
            'LID' => $siteId,
            'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
        ]);
        $basketItems = $basket->getOrderableItems();
    }


    //создание заказа
    function addOrder($email, $basket, $userId){
        
		$order = Order::create($siteId, $userId);
		$order->setPersonTypeId(1);
		$order->setBasket($basket);
        $currencyCode = 'RUB';

        /*Shipment*/
        $shipmentCollection = $order->getShipmentCollection();
        $shipment = $shipmentCollection->createItem();
        $shipmentItemCollection = $shipment->getShipmentItemCollection();
        $shipment->setField('CURRENCY', $order->getCurrency());
        foreach ($order->getBasket() as $item)
        {
            $shipmentItem = $shipmentItemCollection->createItem($item);
            $shipmentItem->setQuantity($item->getQuantity());
        }
        $arDeliveryServiceAll = Delivery\Services\Manager::getRestrictedObjectsList($shipment);
        $shipmentCollection = $shipment->getCollection();

        if (!empty($arDeliveryServiceAll)) {
            reset($arDeliveryServiceAll);
            $deliveryObj = current($arDeliveryServiceAll);

            if ($deliveryObj->isProfile()) {
                $name = $deliveryObj->getNameWithParent();
            } else {
                $name = $deliveryObj->getName();
            }

            $shipment->setFields(array(
                'DELIVERY_ID' => $deliveryObj->getId(),
                'DELIVERY_NAME' => $name,
                'CURRENCY' => $order->getCurrency()
            ));

            $shipmentCollection->calculateDelivery();
        }
        /**/

        /*Payment*/
        $arPaySystemServiceAll = [];
        $paySystemId = 1;
        $paymentCollection = $order->getPaymentCollection();

        $remainingSum = $order->getPrice() - $paymentCollection->getSum();
        if ($remainingSum > 0 || $order->getPrice() == 0)
        {
            $extPayment = $paymentCollection->createItem();
            $extPayment->setField('SUM', $remainingSum);
            $arPaySystemServices = PaySystem\Manager::getListWithRestrictions($extPayment);

            $arPaySystemServiceAll += $arPaySystemServices;

            if (array_key_exists($paySystemId, $arPaySystemServiceAll))
            {
                $arPaySystem = $arPaySystemServiceAll[$paySystemId];
            }
            else
            {
                reset($arPaySystemServiceAll);

                $arPaySystem = current($arPaySystemServiceAll);
            }

            if (!empty($arPaySystem))
            {
                $extPayment->setFields(array(
                    'PAY_SYSTEM_ID' => $arPaySystem["ID"],
                    'PAY_SYSTEM_NAME' => $arPaySystem["NAME"]
                ));
            }
            else
                $extPayment->delete();
        }
        /**/

        $order->doFinalAction(true);
        $propertyCollection = $order->getPropertyCollection();

        $emailProperty = getPropertyByCode($propertyCollection, 'EMAIL');
        $emailProperty->setValue($email);

        $phoneProperty = getPropertyByCode($propertyCollection, 'PHONE');
        $phoneProperty->setValue($phone);

        $order->setField('CURRENCY', $currencyCode);
        //Комментарий менеджера
        $order->setField('COMMENTS', 'Комментарии менеджера');
        //Комментарий покупателя
        $order->setField('USER_DESCRIPTION', 'Комментарии покупателя');

        $order->save();
		
		
		$orderId = $order->GetId();
    }
}