<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="lk__col-item lk__col-item--pas changepass">
<?if($arResult["AJAX_RELOAD"] == "Y") $APPLICATION->RestartBuffer();?>
    <form method="POST">
        <?=bitrix_sessid_post();?>
        <input type="hidden" name="changepass" value="Y"/>

        <div class="form__title"><?=GetMessage("CHPASS_CAPTION")?></div>
        <?if($arResult["ANSWER"]):?>
            <div class="changepass_answer"><?=$arResult["ANSWER"]?></div>
            <script>
                setTimeout(function(){
                    $(".changepass_answer").fadeOut();
                },2000);
            </script>
        <?endif;?>
        <div class="form__item form__item--warning">
            <input type="password" name="last_pass" placeholder="<?=GetMessage("CHPASS_LAST_PASS")?>" required="" />
        </div>
        <div class="form__item form__item--good">
            <input type="password" name="new_pass" placeholder="<?=GetMessage("CHPASS_NEW_PASS")?>" required="" />
        </div>
        <div class="form__item form__item--good">
            <input type="password" name="new_pass_confirm" placeholder="<?=GetMessage("CHPASS_NEW_PASS2")?>" required="" />
        </div>
        <div class="form__action">
            <button type="submit" class="btn btn--empty-blue btn--sml"><?=GetMessage("CHPASS_SUBMIT");?></button>
        </div>
    </form>
    <?if($arResult["AJAX_RELOAD"] == "Y") die();?>
</div>