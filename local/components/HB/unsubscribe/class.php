<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Loader;
use \Bitrix\Main\Application;
use \Bitrix\Main\Context;
use Bitrix\Main\Localization\Loc;
use HB\userEntity\Notification;

Loc::loadMessages(__FILE__);

class UnsubscribeComponent extends CBitrixComponent{

    function onPrepareComponentParams($arParams) {
        return $this->arParams;
    }

    function executeComponent(){
        if($this->request["type"] && $this->request["email"] && $this->request["code"]){
            $this->arResult["CONFIRM"] = Notification::checkCodeUnsubscribe($this->request["email"], $this->request["type"], $this->request["code"]) ? "Y" : "N";
            $this->arResult["TYPE"] = $this->request["type"];
        }else{
            LocalRedirect("/");
        }
        
        $this->includeComponentTemplate();   
    }
}