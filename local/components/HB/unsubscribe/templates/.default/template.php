<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="container access-denied">
    <div class="blog-detail__subheading">
        <?if($arResult["CONFIRM"] == "Y"):?>
            <p><?=GetMessage("CONFIRM_MESSAGE")?></p>
        <?else:?>
            <p><?=GetMessage("FAIL_MESSAGE")?></p>
        <?endif;?>
    </div>
</div>