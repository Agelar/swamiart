<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="popup__content popup__content--select-role">
    <div class="popup-form-reg popup__content-wrapper">
        <span class="popup__heading popup__heading--center"><?=GetMessage("SELECT_ROLE")?></span>
        <div class="popup__label blog-detail__content">
            <form>
                <div class="popup__role">
                    <label class="popup__role-label">
                        <input class="radio" type="radio" name="GROUP_USER" value="<?=BUYER_GROUP?>" checked="checked"/>
                        <div class="popup__role-radio"></div>
                        <div class="popup__role-text"><?=GetMessage("REGISTER_BUYER")?></div>
                    </label>
                    <label class="popup__role-label">
                        <input class="radio" type="radio" name="GROUP_USER" value="<?=AUTHOR_GROUP?>"/>
                        <div class="popup__role-radio"></div>
                        <div class="popup__role-text"><?=GetMessage("REGISTER_PAINTER")?></div>
                    </label>
                </div>
                <button class="popup__mail-submit js-save-role" type="submit" value="submit" name="Save"><?=GetMessage("SAVE")?></button>
            </form>
        </div>
    </div>
</div>