$(document).ready(function(){
    $(document).trigger('showPopup', ["select-role"]);
});

$(document).on("click", ".js-save-role",function(e){
    console.log("sta");
    e.preventDefault();
    BX.ajax.runComponentAction('HB:user.select.role',
    "selectRole", { 
    mode: 'class',
    data:{
        "role" : $(this).parents("form").find("input[name=GROUP_USER]:checked").val(),
    }
    })
    .then(function(result) {
        if(result.data.status){
            location.reload();
        }
    });
});
