<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Loader;
use HB\helpers\Helpers;
use HB\userEntity\IblockAuthor;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Engine\ActionFilter;

Loc::loadMessages(__FILE__);

class SelectUserRoleComponent extends CBitrixComponent  implements Controllerable{

    function onPrepareComponentParams($arParams) {
        return $this->arParams;
    }

    protected function listKeysSignedParameters(){
        return [];
    }

    public function configureActions()
    {
        return [
            'selectRole' => [
                'prefilters' => [
                    new ActionFilter\Authentication(),
                ],
            ],
        ];
    }

    public function selectRoleAction($role = false){
        Loader::IncludeModule("hb.site");
        global $USER;

        if($role != BUYER_GROUP && $role != AUTHOR_GROUP){
            $role = BUYER_GROUP;
        }

        $userID = $USER->getID();
        $status = IblockAuthor::updateGroup($userID, $role);

        if($role == AUTHOR_GROUP){
            $rsUsers = \CUser::GetList(($by = "NAME"), ($order = "desc"), array("ID" => $userID));
            if ($arUser = $rsUsers->Fetch()) {
                if($arUser["PERSONAL_PHOTO"]){
                    $personalPhoto = $arUser["PERSONAL_PHOTO"];
                }
            }

            IblockAuthor::add($userID, array(
                "NAME" => $USER->GetFullName(),
                "DETAIL_PICTURE" => $personalPhoto ? CFile::MakeFileArray($personalPhoto) : $personalPhoto,
                "PROPERTIES" => array(
                    "NAME_EN" => $USER->GetFullName()
                )
            ));
        }
        
        return [
            "status" => true
        ];
    }

    function executeComponent(){
        if(!Helpers::hasUserRole()){
            $this->selectRoleAction();
            $this->includeComponentTemplate();   
        }
    }
}