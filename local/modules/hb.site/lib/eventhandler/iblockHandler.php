<?
namespace HB\eventhandler;

\Bitrix\Main\Loader::IncludeModule("hb.site");
use HB\helpers\RatingHelpers;

/**
 * Класс для обработки событий модуля iblock
 */

class IblockHandler
{
    const IBLOCK_WORK_ID = IBLOCK_WORK_ID;
    const IBLOCK_AUTHORS_ID = IBLOCK_AUTHORS_ID;
    /**
     * изменяем свойства инфоблоков при сохранении элемента
     *
     * @param array $arFields
     * @return void
     */
    public function OnBeforeIBlockElementUpdateHandler(&$arFields)
    {
        if (!empty($arFields['PROPERTY_VALUES'])) {
            if ($arFields['IBLOCK_ID'] == self::IBLOCK_WORK_ID) {
                $work_prop_auto_rating = reset($arFields['PROPERTY_VALUES'][WORK_PROP_AUTO_RATING_ID])['VALUE'];
                /* пересчитываем рейтинг работы при изменении работы */
                if ($work_prop_auto_rating == WORK_PROP_AUTO_RATING_YES) {
                    $time = time();
                    $time_created = strtotime($arFields['ACTIVE_FROM']);
                    $createdDay = round(($time - $time_created) / (60 * 60 * 24));
                    $countLikes = reset($arFields['PROPERTY_VALUES']['100'])['VALUE'];
                    $countFavorites = reset($arFields['PROPERTY_VALUES']['101'])['VALUE'];
                    $authorId = reset($arFields['PROPERTY_VALUES']['18'])['VALUE'];
                    $authorRating = RatingHelpers::getAuthorRating($authorId);
                    $expertRating = reset($arFields['PROPERTY_VALUES']['12'])['VALUE'];
                    $teamRating = reset($arFields['PROPERTY_VALUES']['11'])['VALUE'];
                    $additionalRating = reset($arFields['PROPERTY_VALUES'][WORK_PROP_ADDITIONAL_RATING_ID])['VALUE'];
                    $arFields['PROPERTY_VALUES'][WORK_PROP_FINAL_RATING_ID] = RatingHelpers::calculateWorkRating($countLikes, $countFavorites, $authorRating, $expertRating, $teamRating, $createdDay, $additionalRating);
                    $arFields['PROPERTY_VALUES'][WORK_PROP_AUTO_RATING_ID] = $work_prop_auto_rating;
                }
                if ($arFields['PROPERTY_VALUES'][WORK_PROP_MODERATION_ID][0]['VALUE'] == WORK_PROP_MODERATION_COMPLETED) { // проверяем чтобы свойство "Статус модерации" = "Пройдена"
                    $arFields['PROPERTY_VALUES'][WORK_PROP_MODERATION_COMMENT_ID] = ""; // очищаем комментарий модератора
                } elseif ($arFields['PROPERTY_VALUES'][WORK_PROP_MODERATION_ID][0]['VALUE'] == WORK_PROP_MODERATION_UNCOMPLETED &&
                    !empty(current($arFields['PROPERTY_VALUES'][WORK_PROP_OLD_VALUES_JSON])['VALUE'])) { // проверяем чтобы свойство "Статус модерации" = "Не пройдена" и свойство "Старые изменения" не пустое
                    $oldValues = json_decode(current($arFields['PROPERTY_VALUES'][WORK_PROP_OLD_VALUES_JSON])['VALUE'], true); // декодируем старые изменения из JSON и выполняем возврат старых значений полей и свойств
                    foreach ($oldValues as $propKey => $propValue) {
                        if ($propKey == 'PROPERTIES' || !$propValue) {
                            continue;
                        }
                        $arFields[$propKey] = $propValue;
                    }
                    foreach ($oldValues['PROPERTIES'] as $propKey => $propValue) {
                        if ($propKey == 'MODERATION' || !$propValue) {
                            continue;
                        }
                        $arFields['PROPERTY_VALUES'][$propKey]['VALUE'] = $propValue;
                    }
                    $arFields['PROPERTY_VALUES']['OLD_VALUES_JSON']['VALUE'] = ''; // удаляем старые изменения из свойства
                }

                // проверяем чтобы свойство "Опубликована" = "Да" и свойство "Статус модерации" = "Пройдена"
                if ($arFields['PROPERTY_VALUES'][WORK_PROP_PUBLISHED_ID][0]['VALUE'] == WORK_PROP_PUBLISHED_YES && $arFields['PROPERTY_VALUES'][WORK_PROP_MODERATION_ID][0]['VALUE'] == WORK_PROP_MODERATION_COMPLETED ||
                    $arFields['PROPERTY_VALUES']['PUBLISHED']['VALUE'] == WORK_PROP_PUBLISHED_YES && $arFields['PROPERTY_VALUES']['MODERATION']['VALUE'] == WORK_PROP_MODERATION_COMPLETED) {
                    $arFields['ACTIVE'] = 'Y';
                } else {
                    $arFields['ACTIVE'] = 'N';
                }
            } elseif ($arFields['IBLOCK_ID'] == self::IBLOCK_AUTHORS_ID) { // изменения в инфоблоке авторов
                if ($arFields['PROPERTY_VALUES'][AUTHOR_PROP_MODERATION_ID][0]['VALUE'] == AUTHOR_PROP_MODERATION_COMPLETED) { // проверяем чтобы свойство "Статус модерации" = "Пройдена"
                    $arFields['PROPERTY_VALUES']['MODERATION_COMMENT']['VALUE'] = ''; // очищаем комментарий модератора
                } elseif ($arFields['PROPERTY_VALUES'][AUTHOR_PROP_MODERATION_ID][0]['VALUE'] == AUTHOR_PROP_MODERATION_UNCOMPLETED &&
                    !empty(current($arFields['PROPERTY_VALUES'][AUTHOR_PROP_OLD_VALUES_JSON])['VALUE'])) { // проверяем чтобы свойство "Статус модерации" = "Не пройдена" и свойство "Старые изменения" не пустое
                    $oldValues = json_decode(current($arFields['PROPERTY_VALUES'][AUTHOR_PROP_OLD_VALUES_JSON])['VALUE'], true); // декодируем старые изменения из JSON и выполняем возврат старых значений полей и свойств
                    foreach ($oldValues as $propKey => $propValue) {
                        if ($propKey == 'PROPERTIES') {
                            continue;
                        }
                        $arFields[$propKey] = $propValue;
                    }
                    foreach ($oldValues['PROPERTIES'] as $propKey => $propValue) {
                        if ($propKey == 'MODERATION') {
                            continue;
                        }
                        $arFields['PROPERTY_VALUES'][$propKey]['VALUE'] = $propValue;
                    }
                    $arFields['PROPERTY_VALUES']['OLD_VALUES_JSON']['VALUE'] = ''; // удаляем старые изменения из свойства
                }
            }
        }
    }
}
