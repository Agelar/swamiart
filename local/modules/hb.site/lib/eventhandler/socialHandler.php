<?
namespace HB\eventhandler;
use \Bitrix\Main\Loader;
use \HB\userEntity\Notification;
use \Bitrix\Main\Type\DateTime;

/**
 * Класс для событий модуля socialservices
 */
class SocialHandler{

    function OnFindSocialservicesUserHandler(&$socservUserFields) {
        if ($socservUserFields['EMAIL'] != '') 
        {
            $dbUsersOld = \CUser::GetList($by = 'ID', $ord = 'ASC', array('EMAIL' => $socservUserFields['EMAIL'], 'ACTIVE' => 'Y'), array('NAV_PARAMS' => array('nTopCount' => '1')));
            $socservUser = $dbUsersOld->Fetch();
            if ($socservUser) {
                return $socservUser['ID'];
            }
        }
    }
}?>