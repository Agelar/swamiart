<?
namespace HB\eventhandler;
use \Bitrix\Main\Loader;
use HB\helpers\WorkHelpers;

Loader::IncludeModule("iblock");
Loader::IncludeModule("hb.site");
/**
 * Класс для обработки событий модуля search
 */
class SearchHandler{
    function BeforeIndexHandler($arFields)
    {
		if($arFields["MODULE_ID"] == "iblock")
		{
            if($arFields["PARAM2"] == IBLOCK_BLOG_ID || $arFields["PARAM2"] == IBLOCK_WORK_ID){
                $arrFilter = array("IBLOCK_ID" => $arFields["PARAM2"], "ID" => $arFields["ITEM_ID"]);
                
                if($arFields["PARAM2"] == IBLOCK_WORK_ID){
                    $arrFilter['PROPERTY_MODERATION_VALUE'] = 'Пройдена';
                    $arrFilter['PROPERTY_WORK_PURCHASED'] = false;
                }
                    
                $db = \CIBlockElement::GetList(array(), $arrFilter, false, false);
                if($ob = $db->GetNextElement()){
                    $arPropertires = $ob->getProperties();

                    if($arPropertires["NAME_EN"]["VALUE"]){
                        $arFields["BODY"] .= " " . $arPropertires["NAME_EN"]["VALUE"];
                    }

                    if($arPropertires["DETAIL_TEXT_EN"]["VALUE"]["TEXT"]){
                        $arFields["BODY"] .= " " . strip_tags($arPropertires["DETAIL_TEXT_EN"]["VALUE"]["TEXT"]);
                    }

                    if($arFields["PARAM2"] == IBLOCK_WORK_ID){
                        foreach(["AUTHOR", "GENRE", "STYLE"] as $prop){
                            if($arPropertires[$prop]["VALUE"]){
                                $propRes = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => $arPropertires[$prop]["LINK_IBLOCK_ID"],"ID" => $arPropertires[$prop]["VALUE"]), false, false, array("NAME","PROPERTY_NAME_EN"));
                                while($arProp = $propRes->fetch()){
                                    $arFields["BODY"] .= " " . $arProp["NAME"] . " " . $arProp["PROPERTY_NAME_EN_VALUE"];
                                }
                            }
                        }
                    }
                }else{
                    $arFields["BODY"] = "";
                    $arFields["TITLE"] = "";
                }
            }
        }
        
    	return $arFields;
    }
}
?>