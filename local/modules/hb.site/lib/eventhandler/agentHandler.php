<?
namespace HB\eventhandler;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Mail\Event;
use \HB\helpers\Helpers;
use \HB\helpers\LangHelpers;
use \HB\helpers\AuthorHelpers;
use \HB\helpers\RatingHelpers;
use \HB\userEntity\Notification;
use Bitrix\Main\UserTable;

/**
 * Класс для написания агентов
 */
class AgentHandler{

    const IBLOCK_ID_NEWS = IBLOCK_BLOG_ID;
    const IBLOCK_ID_PAINTS = IBLOCK_WORK_ID;

    /**
     * агент проверяет надо ли отправить пользователю обновления о новостях
     *
     * @return void
     */
    public static function sendUserNewsNotification(){
        \Bitrix\Main\Loader::IncludeModule("hb.site");
        $arNotificate = Notification::getUsersNewsNotification();
        $nowDate = new \DateTime();
        foreach($arNotificate as $user){
            $user["UF_DATE_SEND_NEWS"] = (new \DateTime())->setTimestamp($user["UF_DATE_SEND_NEWS"]);

            $dataUpdate = $user["UF_DATE_SEND_NEWS"];
            if($dataUpdate){
                $dataUpdate->add(new \DateInterval("P".$user["UF_REGULARITY"]["XML_ID"]."D"));
            } else {
                $user["UF_DATE_SEND_NEWS"] = $dataUpdate = new \DateTime("01.01.1970");
            }
        
            if($nowDate >= $dataUpdate){
                if(intval($user["UF_NEWSLETTERS"]) > 0){
                    $arNews = self::getNewsFromDate($user["UF_DATE_SEND_NEWS"],$user["UF_SITE_LANG"]);
                }

                if(!empty($arNews)){
                    $content = "";
                    foreach($arNews as $news){
                        $content .= "<div>
                                        <h2><a href='" . $news["DETAIL_PAGE_URL"] . "'>" . $news["NAME"] . "</a></h2>
                                        <div>" . $news["PREVIEW_TEXT"] . "</div>
                                    </div>";
                    }

                    $notificate = new Notification($user["UF_USER_ID"]);
                    
                    $success = Event::send(array(
                        "EVENT_NAME" => "NOTIFICATION_NEWS",
                        "LID" => $user["UF_SITE_LANG"],
                        "C_FIELDS" => array(
                            "FIO" => Helpers::getUserName($user["UF_USER_ID"], LangHelpers::isEnVersion($user["UF_SITE_LANG"])),
                            "EMAIL" => Helpers::getUserEmail($user["UF_USER_ID"]),
                            "USER_ID" => $user["UF_USER_ID"],
                            "CONTENT" => $content,
                            "UNSUBSCRIBE" => $notificate->getLinkForUnsubscribe("NEWS", LangHelpers::isEnVersion($user["UF_SITE_LANG"])),
                        ),
                    ));
                    if($success->isSuccess()){
                        $notificate->updateDateSend("NEWS");
                    }
                }
            }
        }
        return "\HB\\eventhandler\AgentHandler::sendUserNewsNotification();";
    }

    /**
     * агент проверяет надо ли отправить пользователю обновления о авторах
     *
     * @return void
     */
    public static function sendUserAuthorNotification(){
        \Bitrix\Main\Loader::IncludeModule("hb.site");
        $arNotificate = Notification::getUsersAuthorsNotification();
        $nowDate = new \DateTime();

        foreach($arNotificate as $user){
            $user["UF_DATE_SEND_AUTHORS"] = (new \DateTime())->setTimestamp($user["UF_DATE_SEND_AUTHORS"]);
            
            $dataUpdate = $user["UF_DATE_SEND_AUTHORS"];
            if($dataUpdate){
                $dataUpdate->add(new \DateInterval("P".$user["UF_REGULARITY"]["XML_ID"]."D"));
            }else{
                $user["UF_DATE_SEND_AUTHORS"] = $dataUpdate = new \DateTime("01.01.1970");
            }

            if($nowDate >= $dataUpdate){
                if($user["UF_AUTHORS"]){
                    $arPainting = self::getPaintingFromDate($user["UF_DATE_SEND_AUTHORS"], $user["UF_AUTHORS"], $user["UF_SITE_LANG"]);
                }
                
                if(!empty($arPainting)){
                    $content = "";
                    foreach($arPainting as $authors){
                        $content .= "<div><a href='" . $authors["DETAIL_PAGE_URL"] . "'>" . $authors["NAME"] . "</a></div>";
                        foreach($authors["PICTURES"] as $paint){
                            $content .= "<div>
                                        <h2><a href='" . $paint["DETAIL_PAGE_URL"] . "'>" . $paint["NAME"] . "</a></h2>
                                        <div><img src='" . $paint["PREVIEW_PICTURE"] . "'></div>
                                    </div>";
                        }
                        $content.="<hr>";
                    }
                    $notificate = new Notification($user["UF_USER_ID"]);

                    $success = Event::send(array(
                        "EVENT_NAME" => "NOTIFICATION_AUTHORS",
                        "LID" => $user["UF_SITE_LANG"],
                        "C_FIELDS" => array(
                            "FIO" => Helpers::getUserName($user["UF_USER_ID"], LangHelpers::isEnVersion($user["UF_SITE_LANG"])),
                            "EMAIL" => Helpers::getUserEmail($user["UF_USER_ID"]),
                            "USER_ID" => $user["UF_USER_ID"],
                            "CONTENT" => $content,
                            "UNSUBSCRIBE" => $notificate->getLinkForUnsubscribe("AUTHORS", LangHelpers::isEnVersion($user["UF_SITE_LANG"])),
                        ),
                    ));
                    if($success->isSuccess()){
                        $notificate->updateDateSend("AUTHORS");
                    }
                }
            }
        }

        return "\HB\\eventhandler\AgentHandler::sendUserAuthorNotification();";
    }

    public static function getNewsFromDate($date, $site_id){
        $arNews = array();
        $db = \CIBlockElement::GetList(array("created_date" => "asc"), array("ACTIVE" => "Y", "IBLOCK_ID" => self::IBLOCK_ID_NEWS,">DATE_CREATE" => ConvertTimeStamp($date->getTimestamp())), false, false, array("NAME","PREVIEW_TEXT","PREVIEW_PICTURE","DETAIL_PAGE_URL", "PROPERTY_NAME_EN","PROPERTY_PREVIEW_TEXT_EN"));
        while($res = $db->getNext()){
            $res = LangHelpers::getTranslation($res, LangHelpers::isEnVersion($site_id));
            $arNews[] = $res;
        }
        return $arNews;
    }
    public static function getPaintingFromDate($date, $arAuthorsIDs, $site_id){
        $arPaints = array();
        $arAuthors = AuthorHelpers::getAuthors($arAuthorsIDs, LangHelpers::isEnVersion($site_id));
        $db = \CIBlockElement::GetList(array("created_date" => "asc"), array("ACTIVE" => "Y", "IBLOCK_ID" => self::IBLOCK_ID_PAINTS, "PROPERTY_AUTHOR" => $arAuthorsIDs, ">DATE_CREATE" => ConvertTimeStamp($date->getTimestamp())), false, false, array("NAME","PREVIEW_PICTURE","DETAIL_PAGE_URL","PROPERTY_AUTHOR","PROPERTY_NAME_EN"));
        while($res = $db->getNext()){
            $res = LangHelpers::getTranslation($res, LangHelpers::isEnVersion($site_id));
            if($res["PREVIEW_PICTURE"]){
                $res["PREVIEW_PICTURE"] = Helpers::getSiteUrl() . \CFile::GetPath($res["PREVIEW_PICTURE"]);
                if(!is_array($arPaints[$res["PROPERTY_AUTHOR_VALUE"]])){
                    $arPaints[$res["PROPERTY_AUTHOR_VALUE"]] = $arAuthors[$res["PROPERTY_AUTHOR_VALUE"]];
                    $arPaints[$res["PROPERTY_AUTHOR_VALUE"]]["PICTURES"][] = $res;
                }else{
                    $arPaints[$res["PROPERTY_AUTHOR_VALUE"]]["PICTURES"][] = $res;
                }
            }
        }

        return $arPaints;
    }



    /**
     * уведомление об оценках экспертов за день
     *
     * @return void
     */
    public static function sendExpertRatingDay(){
        \Bitrix\Main\Loader::IncludeModule("hb.site");
        $arExpertList = RatingHelpers::getRatingAllExpertDay();
        $arExpertId = array();
        $arExpertCountWork = array();
        if($arExpertList){
            foreach($arExpertList as $expertId => $arRating){
                $arExpertId[] = $expertId;
                $arExpertCountWork[$expertId] = count($arRating);
            }
            $resUser = UserTable::getList(array(
                "filter" => array("ID" => $arExpertId),
                "select" => array("ID", "NAME", "LAST_NAME", "EMAIL"),
            ));
            $text = '<h3>Обзор активности экспертов.</h3>';
            while($arExpert = $resUser->fetch()){
                $text .= '<p>Эксперт ' . $arExpert['LAST_NAME'] . ' ' . $arExpert['NAME'] . ' оценил работ: ' . $arExpertCountWork[$arExpert['ID']] . '</p>';
            } 

            $notificate = new Notification($user["UF_USER_ID"]);
                    
                    $success = Event::send(array(
                        "EVENT_NAME" => "EXPERT_RATING_DAY",
                        "LID" => 's1',
                        "C_FIELDS" => array(
                            "TEXT" => $text,
                            
                        ),
                    ));
        }
        return "\HB\\eventhandler\AgentHandler::sendExpertRatingDay();";
    }
}
?>