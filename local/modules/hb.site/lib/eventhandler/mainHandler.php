<?
namespace HB\eventhandler;
use \HB\helpers\WorkHelpers;
use \HB\helpers\LangHelpers;
use \HB\userEntity\IblockAuthor;
use \Bitrix\Main\Loader;
use \HB\userEntity\Confirm;
use \HB\userEntity\Notification;
use \Bitrix\Main\Mail\Event;


/**
 * Класс для обработки событий модуля main
 */
class MainHandler{
    public const AUTHOR_GROUP = 5;
    public const BUYER_GROUP = 6;

    /**
     * добавление пользователя при создании заказа
     *
     * @param array $arFields
     * @return void
     */
    function OnBeforeUserAddHandler(&$arFields){
        if($_REQUEST['soa-action'] == 'saveOrderAjax') {
            $arFields['LAST_NAME'] = $_REQUEST['ORDER_PROP_2'];
            $arFields['UF_CITY'] = $_REQUEST['ORDER_PROP_5'];
            $arFields['UF_STREET'] = $_REQUEST['ORDER_PROP_6'];
            $arFields['UF_HOUSE'] = $_REQUEST['ORDER_PROP_7'];
            $arFields['UF_APARTAMENT'] = $_REQUEST['ORDER_PROP_8'];
            $arFields['UF_CITY_ID'] = $_SESSION['CITY_ID'];

            if(!(array_search(self::BUYER_GROUP, $arFields["GROUP_ID"])!==FALSE || array_search(self::AUTHOR_GROUP, $arFields["GROUP_ID"])!==FALSE)) {
                $arFields["GROUP_ID"][] = self::BUYER_GROUP;
            }
        }
    }


    /**
     * Определяем нового пользователя в определенную группу если она задана
     *
     * @param array $arFields
     * @return void
     */
    function OnBeforeUserUpdateHandler(&$arFields){
        
        if($arFields["GROUP_USER"]){
            $arFields["GROUP_ID"][] = $arFields["GROUP_USER"];
        }
        if($arFields["NAME"]){
            $arFields["UF_NAME_EN"] = \ucfirst(\CUtil::translit($arFields["NAME"],"ru",array("replace_space"=>"-","replace_other"=>"-")));
        }
        if($arFields["LAST_NAME"]){
            $arFields["UF_LAST_NAME"] = \ucfirst(\CUtil::translit($arFields["LAST_NAME"],"ru",array("replace_space"=>"-","replace_other"=>"-")));
        }
    }

    /**
     * Добавляем запись в hlblock при добавлении пользователя
     * Нужно для подписок
     *
     * @param array $arFields
     * @return void
     */
    function OnAfterUserAddHandler($arFields){
        if(intval($arFields["ID"]) > 0){
            if(array_search(5,$arFields["GROUP_ID"]) !== false){
                IblockAuthor::add($arFields["ID"],array(
                    "NAME" => $arFields["NAME"] . ($arFields["LAST_NAME"] ? " " . $arFields["LAST_NAME"] : ""),
                    "DETAIL_PICTURE" => $arFields["PERSONAL_PHOTO"] ? $arFields["PERSONAL_PHOTO"] : "",
                    "PROPERTIES" => array(
                        "NAME_EN" => $arFields["UF_NAME_EN"] . ($arFields["UF_LAST_NAME"] ? " " . $arFields["UF_LAST_NAME"] : "")
                    )
                ));
            }
            Loader::IncludeModule("hb.site");
            $notificate = new Notification($arFields["ID"]);
            $notificate->addNewUserNotifitace();
            $hash = Confirm::createHash($arFields["EMAIL"],$arFields["ID"], true);
            Event::send(array(
                "EVENT_NAME" => "CONFIRM_EMAIL",
                "LID" => LangHelpers::isEnVersion() ? "en" : "s1",
                "C_FIELDS" => array(
                    "EMAIL" => $arFields["EMAIL"],
                    "USER_ID" => $arFields["ID"],
                    "HASH" => $hash
                ),
            ));


            //добавляем в избранное при регистрации пользователя
            if($_SESSION['FAVORITES']){
                foreach($_SESSION['FAVORITES'] as $workFav){
                    WorkHelpers::addFavoritesWorks($_SESSION['FAVORITES'], $arFields["ID"]);
                }
            }

        }
    }
    /**
     * удаляем запись из hlblock о пользователе
     *
     * @param int $id
     * @return void
     */
    function OnUserDeleteHandler($id){
        if(intval($id) > 0){
            Loader::IncludeModule("hb.site");
            $notificate = new Notification($id);
            $notificate->removeUserNotifitace();
        }
    }



    //делаем merge избранного при авторизации пользователя
    function OnAfterUserLoginHandler($arFields)
    {
        
        if($arFields['USER_ID'])
        {
            if($_SESSION['FAVORITES']){
                WorkHelpers::addFavoritesWorks($_SESSION['FAVORITES'], $arFields['USER_ID']);
            }
        }
    }

    /* 
        Поиск и аттрибутация внешних ссылок
    */
    function OnEndBufferContentHandler(&$buffer) {
        $buffer = preg_replace_callback(
        [
            '/href="https:\\/\\/(.*)"/',
            '/href="http:\\/\\/(.*)"/'
        ], 
        function($m) {
            if(strpos($m[1], $_SERVER['HTTP_HOST']) === FALSE)
                $ret = 'rel="nofollow" '.$m[0];
            else
                $ret = $m[0];
        
            return $ret;
        }, $buffer);
    }

}
?>