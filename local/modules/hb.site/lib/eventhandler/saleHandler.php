<?
namespace HB\eventhandler;
use \Bitrix\Main\Loader;
use HB\helpers\RatingHelpers;
use HB\helpers\WorkHelpers;
use HB\helpers\AuthorHelpers;
use HB\helpers\Helpers;
use Bitrix\Sale\Order;
use HB\userEntity\Notification;

Loader::IncludeModule("hb.site");
Loader::IncludeModule("sale");
/**
 * Класс для обработки событий модуля sale
 */

class SaleHandler{

    //Вызывается после создания и расчета обьекта заказа. 
    function OnSaleComponentOrderCreatedHandler($order, &$arUserResult, $request, &$arParams, &$arResult, &$arDeliveryServiceAll, &$arPaySystemServiceAll){
        $userId = $order->getUserId();
        $basket = $order->getBasket();
        $basketItems = $basket->getBasketItems();
        $item = $basketItems[0];
        $productId = $item->getProductId();
        \CIBlockElement::SetPropertyValuesEx($productId, IBLOCK_ORDER_DIGITAL_COPY_ID, array('USER' => $userId));
    }



    //Вызывается после получения данных (свойств заказа, платежной системы, службы доставки и т.п.), отправленных клиентом. 
    function OnSaleComponentOrderUserResultHandler(&$arUserResult, $request, &$arParams){
        $result = $request->getPostList();
        if($result['order']) $arOrder = $result['order'];
        else $arOrder = $result;
        
        if($arOrder['soa-action']=='saveOrderAjax'){
            global $USER;
            $user = new \CUser;
            $arFields['LAST_NAME'] = $arOrder['ORDER_PROP_2'];
            $arFields['PERSONAL_PHONE'] = $arOrder['ORDER_PROP_3'];
            //if($arOrder['ORDER_PROP_4']) $arFields['EMAIL'] = $arOrder['ORDER_PROP_4'];
            $arFields['UF_STREET'] = $arOrder['ORDER_PROP_6'];
            $arFields['UF_HOUSE'] = $arOrder['ORDER_PROP_7'];
            $arFields['UF_APARTAMENT'] = $arOrder['ORDER_PROP_8'];
            $user->Update($USER->GetID(), $arFields);
            
            

        }
    }

    /**
     * пересчитываем рейтинг художника и его работ при оплате/отмене заказа
     *
     * @param int $id
     * @return void
     */
    function OnSalePayOrderHandler($id,$val){
        $arOrder = \CSaleOrder::GetByID($id);
        //file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/log_order_result4', var_export($arOrder, true));
        $dbBasketItems = \CSaleBasket::GetList(
            array(
                  "NAME" => "ASC",
                  "ID" => "ASC"
               ),
            array(
                  "ORDER_ID" => $id
               ),
            false,
            false,
            array("*")
        );
        
        while ($arItems = $dbBasketItems->Fetch()) {
            $arElement = Helpers::getElementList($arItems['PRODUCT_ID'])[$arItems['PRODUCT_ID']];
            if ($val=='Y') {
                if ($arElement["IBLOCK_ID"]==IBLOCK_WORK_ID) {
                    WorkHelpers::purchasedWork($arItems['PRODUCT_ID']);
                    AuthorHelpers::sendMessageAuthor($arElement['PROPERTIES']['AUTHOR']['VALUE'], $arElement['NAME'], $arElement['DETAIL_PAGE_URL']);
                } else {
                    WorkHelpers::addDigitalCopyUser($arElement['PROPERTIES']['WORK']['VALUE'], $arOrder['USER_ID']);
                }
            } else {
                if ($arElement["IBLOCK_ID"]==IBLOCK_WORK_ID) {
                    WorkHelpers::cancelPurchasedWork($arItems['PRODUCT_ID']);
                } else {
                    WorkHelpers::deactiveDigitalCopyUser($arElement['PROPERTIES']['WORK']['VALUE'], $arOrder['USER_ID']);
                }
            }
            $arRatingWork = RatingHelpers::getInfoRatingWork($arItems['PRODUCT_ID']);
            $ratingAuthor = RatingHelpers::getAuthorRating($arRatingWork['AUTHOR']);
            $newRatingAuthor = RatingHelpers::calculateAuthorRating($arRatingWork['AUTHOR']);
            if ($ratingAuthor!=$newRatingAuthor) {
                RatingHelpers::updateAuthorRating($arRatingWork['AUTHOR'], $newRatingAuthor);
                RatingHelpers::updateAuthorWorkRating($arRatingWork['AUTHOR'], $newRatingAuthor);
            }
        }

        if ($arOrder['STATUS_ID'] == "N" && $arOrder['CANCELED'] == "N" && $val == "Y") {
            \CSaleOrder::StatusOrder($id, "OP");
        }
    }

    /**
     * Событие на проверку пользователя и отмену отправки письма с состоянием заказа
     *
     * @return void
     */
    function OnSendMailOrderNew($orderID, &$eventName, &$arFields){
        $order = Order::load($orderID);
        $userID = $order->getUserId();
        if(intval($userID) > 0){
            $notification = new Notification($userID);
            $status = $notification->getProps("UF_STATUS_ORDER");
            if(!$status){
                return false;
            }
        }
    }

    
}