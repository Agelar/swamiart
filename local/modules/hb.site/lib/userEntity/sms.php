<?
namespace HB\userEntity;
use Bitrix\Main\Loader;
use HB\helpers\HightLoadHelpers;
use HB\helpers\LangHelpers;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\UserTable;

/**
 * Класс направлен на работу с сообщениями
 */
class Sms{
    //UF_SMS_CODE
    public static function sendSMS($phone, $msg){
        $result = array();
        $ch = curl_init("https://sms.ru/sms/send");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
            "api_id" => "A32148ED-A17E-9E45-9A8B-F955626D5EBC",
            "to" => $phone,
            "msg" => $msg,
            "json" => 1
        )));
        $body = curl_exec($ch);

        curl_close($ch);
        $json = json_decode($body);
        if ($json) {
            
            if ($json->status == "OK") { 
                foreach ($json->sms as $phone => $data) {
                    if ($data->status == "OK") { // Сообщение отправлено
                        $result["STATUS"] = "Y";
                        $result["MESSAGE"] ="Сообщение на номер $phone успешно отправлено. ";
                    } else { // Ошибка в отправке
                        $result["STATUS"] = "N";
                        $result["MESSAGE"] = "Сообщение на номер $phone не отправлено.\n "."Текст ошибки: $data->status_text. ";
                    }
                }
            } else { // Запрос не выполнился (возможно ошибка авторизации, параметрах, итд...)
                $result["STATUS"] = "N";
                $result["MESSAGE"] = "Запрос не выполнился. "."Текст ошибки: $json->status_text. ";
            }
        } else{
            $result["STATUS"] = "N";
            $result["MESSAGE"] = "Запрос не выполнился. Не удалось установить связь с сервером.";
        }

        return $result;
    }

    public static function SendUserSms($userID, $phone){
        if(self::checkUser($userID) && self::checkPhone($phone)){
            $code = self::generateCode($userID);
            $status = self::sendSMS($phone, $code);
            //$status = array("STATUS" =>"Y");
            return $status;
        }
    }

    public static function generateCode($userID, $save = true){
        $code = rand(1111,9999);
        if($save){
            $user = new \CUser;
            $user->Update($userID,array("UF_SMS_CODE" => $code));   
        }
        
        return $code;
    }

    public static function checkUser($userID){
        if(intval($userID) > 0){
            $count = UserTable::getList(array(
                'select' =>array("ID"),
                'filter' => array("ID" => $userID)
            ))->getSelectedRowsCount();
            if($count > 0){
                return true;
            }
        }
        
        return false;
    }

    public static function checkPhone($phone){
        $phone = preg_replace("#[^0-9]*#","",$phone);
        if(intval($phone) > 0){
            return true;
        }

        return false;
    }

    public static function savePhoneConfirm($userID, $phone){
        $user = new \CUser;
        $user->Update($userID,array("UF_CONFIRM_PHONE" => $phone));
    }

    public static function confirmSms($userID, $phone, $code){
        $arUser = UserTable::getList(array(
            'select' =>array("UF_SMS_CODE"),
            'filter' => array("ID" => $userID)
        ))->fetch();

        if($arUser["UF_SMS_CODE"] == $code){
            self::savePhoneConfirm($userID, $phone);
            return true;
        }else{
            return false;
        }
    }

    public static function avalibleUpdatePhone($userID, $phone){
        $arUser = UserTable::getList(array(
            'select' =>array("UF_CONFIRM_PHONE","PERSONAL_PHONE"),
            'filter' => array("ID" => $userID)
        ))->fetch();
        if($arUser["UF_CONFIRM_PHONE"] == $phone || $arUser["PERSONAL_PHONE"] == $phone){
            return true;
        } else{
            return false;
        }
    }
}