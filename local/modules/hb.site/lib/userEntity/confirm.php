<?
namespace HB\userEntity;
use Bitrix\Main\Loader;
use HB\helpers\HightLoadHelpers;
use HB\helpers\LangHelpers;
use Bitrix\Main\Type\DateTime;

/**
 * Класс для работу с подтверждением email
 */
class Confirm{
    public static function createHash($email, $idUser, $save = false){
        $hash = sha1($email . "_" . $idUser);
        if($save){
            $user = new \CUser;
            $fields = Array( 
                "UF_CONFIRM_HASH" => $hash, 
            ); 
            $user->Update($idUser, $fields);
        }
        
        return $hash;
    }

    public static function checkHash($idUser, $hash){
        $arFilter = array(
            "ID" => $idUser
        );
        $user = \CUser::getList(($by="ID"), ($order="desc"), $arFilter,array("SELECT" => array("UF_CONFIRM_HASH")))->fetch();

        return $hash == $user["UF_CONFIRM_HASH"] ? true : false;
    }
    
    public static function checkEmailConfirm($idUser){
        $arFilter = array(
            "ID" => $idUser
        );
        $user = \CUser::getList(($by="ID"), ($order="desc"), $arFilter,array("SELECT" => array("UF_CONFIRM_EMAIL")))->fetch();
        return $user["UF_CONFIRM_EMAIL"] ? true : false;
    }

    public static function confirmEmail($idUser){
        $user = new \CUser;
        $fields = Array( 
            "UF_CONFIRM_EMAIL" => true, 
        ); 
        return $user->Update($idUser, $fields);
    }
}
?>