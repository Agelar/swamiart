<?
namespace HB\userEntity;
use Bitrix\Main\Loader;
use HB\helpers\HightLoadHelpers;
use HB\helpers\Helpers;
use HB\helpers\LangHelpers;
Use Bitrix\Main\UserTable;

/**
 * Класс направлен на работу с уведомлениями
 */
class Notification{
    private $userId;
    private const HLID = 4;
    private const IBLOCK_ID_AUTHORS = IBLOCK_AUTHORS_ID;
    private const SAULT = "_NOTIFICATE";

    function __construct($userId){
        $this->userId = $userId;
    }

    /**
     * Получение всех полей из hlblock у определенного пользователя
     *
     * @return array
     */
    public function getAllInfoByNotification(){
        Loader::IncludeModule("iblock");
        $result = HightLoadHelpers::getAll(self::HLID, array("UF_USER_ID" => $this->userId))[0];

        $regularList = array();
        $dbRegular = \CUserFieldEnum::getlist(array(),array("USER_FIELDS_ID" => "UF_REGULARITY"));
        while($resRegular = $dbRegular->getNext()){
            if($resRegular["ID"] == $result["UF_REGULARITY"]){
                $resRegular["SELECT"] = "Y";
            }
        
            $regularList[] = $resRegular;
        }
        $result["UF_REGULARITY"] = $regularList;
        
        if($result["UF_AUTHORS"]){
            $arAuthors = array();
            $db = \CIBlockElement::GetList(array(), array("ACTIVE" => "Y", "ID" => $result["UF_AUTHORS"], "IBLOCK_ID" => self::IBLOCK_ID_AUTHORS), false, false, array("NAME","DETAIL_PICTURE","DETAIL_PAGE_URL","ID","PROPERTY_NAME_EN"));
            while($res = $db->getNext()){
                $arAuthors[$res["ID"]] = LangHelpers::getTranslation($res);
            }
            $result["UF_AUTHORS"] = $arAuthors;
        }
        
        return $result;
    }

    /**
     * Статичный метод получает список всех пользователей с подпиской на новости
     *
     * @return void
     */
    public static function getUsersNewsNotification(){
        Loader::IncludeModule("iblock");
        $result = HightLoadHelpers::getAll(self::HLID,array(
            "!UF_NEWSLETTERS" => false
         ));
        foreach($result as &$el){
            if($el["UF_REGULARITY"]){
                $dbRegular = \CUserFieldEnum::getlist(
                    array(),
                    array("USER_FIELDS_ID" => "UF_REGULARITY")
                );
                while($resRegular = $dbRegular->getNext()){
                    if($resRegular["ID"] == $el["UF_REGULARITY"]){
                        $el["UF_REGULARITY"] = $resRegular;
                    }
                }
            }   
        }
            
        return $result;
    }
    /**
     * Статичный метод получает список всех пользователей с подпиской на авторов
     *
     * @return void
     */
    public static function getUsersAuthorsNotification(){
        Loader::IncludeModule("iblock");
        $result = HightLoadHelpers::getAll(self::HLID,array(
            ">UF_AUTHORS" => 0
         ));
        foreach($result as &$el){
            if($el["UF_REGULARITY"]){
                $dbRegular = \CUserFieldEnum::getlist(
                    array(),
                    array("USER_FIELDS_ID" => "UF_REGULARITY")
                );
                while($resRegular = $dbRegular->getNext()){
                    if($resRegular["ID"] == $el["UF_REGULARITY"]){
                        $el["UF_REGULARITY"] = $resRegular;
                    }
                }
            }   
        }
            
        return $result;
    }


    /**
     * Отписка от опредленного автора
     *
     * @param int $idAuthor
     * @return boolean
     */
    public function unsubscribeAuthor(int $idAuthor){
        if(intval($idAuthor) <= 0) return false;
        $status = false;
        $element = HightLoadHelpers::getAll(self::HLID, array("UF_USER_ID" => $this->userId),array("ID","UF_AUTHORS"))[0];
        if($element["UF_AUTHORS"]){
            $key = array_search($idAuthor, $element["UF_AUTHORS"]);
            if($key !== false){
                unset($element["UF_AUTHORS"][$key]);
                $status = HightLoadHelpers::Update(self::HLID, $element["ID"], $element);
            }
        }

        return $status;
    }

    /**
     * Отписка от опредленного автора
     *
     * @param int $idAuthor
     * @return boolean
     */
    public function subscribeAuthor(int $idAuthor){
        if(intval($idAuthor) <= 0) return false;
        $status = false;
        $element = HightLoadHelpers::getAll(self::HLID, array("UF_USER_ID" => $this->userId),array("ID","UF_AUTHORS"))[0];
        if($element["UF_AUTHORS"]){
            $key = array_search($idAuthor, $element["UF_AUTHORS"]);
            if($key === false){
                $element["UF_AUTHORS"][] = $idAuthor;
                $status = HightLoadHelpers::Update(self::HLID, $element["ID"], $element);
            }
        }else{
            $element["UF_AUTHORS"][] = $idAuthor;
            $status = HightLoadHelpers::Update(self::HLID, $element["ID"], $element);
        }

        return $status;
    }
    /**
     * Проверка подписал ли user на автора
     *
     * @param integer $idAuthor
     * @return void
     */
    public function checkSubscribeAuthor(int $idAuthor){
        if(intval($idAuthor) <= 0) return false;
        $status = false;
        $element = HightLoadHelpers::getAll(self::HLID, array("UF_USER_ID" => $this->userId, "UF_AUTHORS" => $idAuthor),array("ID","UF_AUTHORS"))[0];

        return isset($element) ? true : false;
    }
    

    /**
     * Изменение свойства по его коду (не множественное)
     *
     * @param string $code
     * @param mixed $value
     * @return boolean
     */
    public function changeProps($code, $value = null){
        if(empty($code) || $value === null) return false;
        $status = false;
        $element = HightLoadHelpers::getAll(self::HLID, array("UF_USER_ID" => $this->userId),array("ID",$code))[0];
        if($element["ID"]){
            $element[$code] = ($value === 'false') ? false : $value;
            $status = HightLoadHelpers::Update(self::HLID, $element["ID"], $element);
        }

        return $status;
    }

    /**
     * Получить значение свойства по его коду
     *
     * @param string $code
     * @param mixed $value
     * @return boolean
     */
    public function getProps($code){
        if(empty($code)) return false;
        $status = false;
        $element = HightLoadHelpers::getAll(self::HLID, array("UF_USER_ID" => $this->userId),array("ID",$code))[0];
        if($element["ID"]){
            return $element[$code];
        }

        return null;
    }

    /**
     * Добавление пользователя в hlblock уведомления
     *
     * @return boolean
     */
    public function addNewUserNotifitace(){
        $element = HightLoadHelpers::getAll(self::HLID, array("UF_USER_ID" => $this->userId),array("ID"))[0];
        $status = false;

        if($element){
            return false;
        } else {
            global $USER_FIELD_MANAGER;
            $arrFilter = array();

            $data = $USER_FIELD_MANAGER->getUserFields('HLBLOCK_' . self::HLID);
            foreach($data as $key => $value){
                if($value["USER_TYPE"]["USER_TYPE_ID"] == "enumeration"){
                    $listEnum = \CUserFieldEnum::getlist(array(),array("USER_FIELDS_ID" => $key, "DEF" => "Y"))->fetch();
                    if($listEnum){
                        $arrFilter[$key] = $listEnum["ID"];
                    }
                } elseif($key == "UF_DATE_SEND_NEWS" || $key == "UF_DATE_SEND_AUTHORS") {
                    $date = new \DateTime;
                    $arrFilter[$key] = $date->getTimestamp();
                    continue;
                } else {
                    $arrFilter[$key] = $value["SETTINGS"]["DEFAULT_VALUE"];
                }
            }
            
            $arrFilter["UF_SITE_LANG"] = SITE_ID;
            $arrFilter["UF_USER_ID"] = $this->userId;

            $status = HightLoadHelpers::Add(self::HLID, $arrFilter);
        }

        return $status;
    }
    /**
     * Удаление пользователя из hlblock уведомлений
     *
     * @return boolean
     */
    public function removeUserNotifitace(){
        $element = HightLoadHelpers::getAll(self::HLID, array("UF_USER_ID" => $this->userId),array("ID"))[0];
        $status = false;
        
        if($element){
            $status = HightLoadHelpers::Delete(self::HLID,$element["ID"]);
        } else {
            return false;
        }

        return $status;
    }

    /**
     * Обновление даты отправки уведомлений
     *
     * @return boolean
     */
    public function updateDateSend($type = "NEWS"){
        $element = HightLoadHelpers::getAll(self::HLID, array("UF_USER_ID" => $this->userId),array("ID"))[0];
        $status = HightLoadHelpers::Update(self::HLID, $element["ID"], array("UF_DATE_SEND_" . $type => (new \DateTime())->getTimestamp()));

        return $status;
    }
    /**
     * Получаем ссылку на отписку от уведомлений
     *
     * @param [type] $type
     * @return void
     */
    public function getLinkForUnsubscribe($type, $isEng = false){
        if($type){
            $email = Helpers::getUserEmail($this->userId);
            $link = Helpers::getSiteUrl()."/unsubscribe/".($isEng ? "en/" : "")."?email=" . $email . "&type=" . $type . "&code=".sha1($this->userId . $type . self::SAULT);
            return $link;
        }
        return false;
    }

    /**
     * 
     *
     * @param [type] $type
     * @return void
     */
    public static function checkCodeUnsubscribe($email, $type, $code){
        if($type && $email && $code){
            $user = UserTable::getList(array(
                "filter" => array("EMAIL" => $email),
                "select" => array("ID"),
            ))->fetch();    
            if($user){
                if($code == sha1($user["ID"] . $type . self::SAULT)){
                    $notificate = new Notification($user["ID"]);
                    switch($type){
                        case "NEWS":
                            return $notificate->changeProps("UF_NEWSLETTERS",false);
                            break;
                        case "AUTHORS":
                            return $notificate->changeProps("UF_AUTHORS",false);
                            break;
                    }
                }
            }
        }
        return false;
    }
}