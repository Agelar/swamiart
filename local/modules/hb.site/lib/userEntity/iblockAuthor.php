<?
namespace HB\userEntity;

use Bitrix\Main\Loader;

Loader::IncludeModule("iblock");
/**
 * Класс направлен на работу c инфоблоком автора
 */
class IblockAuthor
{
    private const IBLOCK_ID = IBLOCK_AUTHORS_ID;

    public static function add($idUser, $arFields)
    {
        if (!self::checkIsset($idUser)) {
            $element = new \CIBlockElement();
            $code = \CUtil::translit($arFields["NAME"], "ru", array("replace_space" => "-", "replace_other" => "-"));
            $hash = mb_strimwidth(sha1($code . rand(11111, 99999)), 0, 6);

            $arLoadProductArray = array(
                "ACTIVE" => "Y",
                "NAME" => $arFields["NAME"],
                "CODE" => $code . "_" . $hash,
                "IBLOCK_ID" => self::IBLOCK_ID,
            );
            if ($arFields["DETAIL_PICTURE"]) {
                $arLoadProductArray["DETAIL_PICTURE"] = $arFields["DETAIL_PICTURE"];
            }
            if ($arFields["PROPERTIES"]) {
                $arLoadProductArray["PROPERTY_VALUES"] = $arFields["PROPERTIES"];
            }

            $arLoadProductArray["PROPERTY_VALUES"]["USER"] = $idUser;

            $ID = $element->Add($arLoadProductArray);
        }
        return false;
    }

    public static function UpdateAuthor($idUser, $arFields)
    {
        $element = new \CIBlockElement();
        $arLoadProductArray = array();
        $oldFieldsValueArray = array();

        $authorSelectFields = array("ID", "NAME", "DETAIL_TEXT", "DETAIL_PICTURE", "PROPERTY_STYLE", "PROPERTY_NAME_EN", "PROPERTY_DETAIL_TEXT_EN", "PROPERTY_TOWN", "PROPERTY_TOWN_EN");
        $author = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_AUTHORS_ID, "PROPERTY_USER" => $idUser), false, false, $authorSelectFields)->fetch();

        if (isset($arFields["NAME"])) {
            $arLoadProductArray["NAME"] = $arFields["NAME"];
        }

        if (isset($arFields["DETAIL_PICTURE"])) {
            $arLoadProductArray["DETAIL_PICTURE"] = $arFields["DETAIL_PICTURE"];
            $oldFieldsValueArray["DETAIL_PICTURE"] = $author["DETAIL_PICTURE"];
        }

        if (isset($arFields["DETAIL_TEXT"])) {
            $arLoadProductArray["DETAIL_TEXT"] = $arFields["DETAIL_TEXT"];
            $oldFieldsValueArray["DETAIL_TEXT"] = $author["DETAIL_TEXT"];
        }

        if (isset($arFields["PROPERTIES"])) {
            foreach ($arFields["PROPERTIES"] as $propName => $prop) {
                $oldFieldsValueArray["PROPERTIES"][$propName] = $author["PROPERTY_" . $propName . "_VALUE"];
                \CIBlockElement::SetPropertyValuesEx($author["ID"], IBLOCK_AUTHORS_ID, array($propName => $prop));
            }
        }

        // запись старых значений свойств в свойство "Новые изменения" в формате JSON
        \CIBlockElement::SetPropertyValuesEx($author["ID"], IBLOCK_AUTHORS_ID, array("OLD_VALUES_JSON" => json_encode($oldFieldsValueArray)));

        $ID = $element->Update($author["ID"], $arLoadProductArray);

        //отправляем письмо об изменении информации об авторе
        $arEventFields = array(
            "LINK" => "/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=" . IBLOCK_AUTHORS_ID . "&type=catalogs&ID=" . $author['ID'] . "&lang=ru&find_section_section=" . $author['SECTION_ID'] . "&WF=Y",
            "NAME" => $author["NAME"],
            "DATE" => date('d.m.Y H:i'),
        );
        $сEventResult = \CEvent::Send("UPDATE_AUTHOR", SITE_ID, $arEventFields);
    }

    public static function remove($idUser)
    {
        $user = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_AUTHORS_ID, "PROPERTY_USER" => $idUser), false, false, array("ID"))->fetch();
        \CIBlockElement::Delete($user["ID"]);
    }

    public static function checkIsset($idUser)
    {
        $db = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_AUTHORS_ID, "PROPERTY_USER" => $idUser), false, false, array("ID"));
        return ($db->SelectedRowsCount() > 0) ? true : false;
    }

    public static function getPersonalPicture($id = false, $size = array())
    {
        if (!$id) {
            return false;
        }

        $personalPhoto = false;

        $rsUsers = \CUser::GetList(($by = "NAME"), ($order = "desc"), array("ID" => $id));
        if ($arUser = $rsUsers->Fetch()) {
            if ($arUser["PERSONAL_PHOTO"]) {
                $personalPhoto = $arUser["PERSONAL_PHOTO"];
            }
        }
        if (!empty($size)) {
            $personalPhoto = \CFile::ResizeImageGet($personalPhoto, $size)["src"];
        } else {
            $personalPhoto = \CFile::GetPath($personalPhoto);
        }

        return $personalPhoto;

    }

    public static function updatePersonalPicture($idUser, $arFile)
    {
        $user = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_AUTHORS_ID, "PROPERTY_USER" => $idUser), false, false, array("ID"))->fetch();
        if ($user) {
            $element = new \CIBlockElement();
            $arLoadProductArray = array(
                "IBLOCK_ID" => self::IBLOCK_ID,
                "DETAIL_PICTURE" => $arFile,
            );

            $element->Update($user["ID"], $arLoadProductArray);
        }
    }

    public static function updateGroup($idUser, $groupNum)
    {
        $arGroup = \CUser::GetUserGroup($idUser);
        if (!in_array($groupNum, $arGroup)) {
            if ($groupNum == AUTHOR_GROUP && $num = array_search(BUYER_GROUP, $arGroup)) {
                unset($arGroup[$num]);
            } elseif ($groupNum == BUYER_GROUP && $num = array_search(AUTHOR_GROUP, $arGroup)) {
                unset($arGroup[$num]);
            }
            $arGroup[] = $groupNum;
            $user = new \CUser;
            $status = $user->Update($idUser, array(
                "GROUP_ID" => $arGroup,
            ));
            if ($status) {
                return true;
            }
        }

        return false;
    }
}
