<?
namespace HB\orm;

use Bitrix\Main;
use Bitrix\Main\Entity;

/**
 * Class TagTable
 * 
 * Fields:
 * <ul>
 * <li> SITE_ID string(2) optional
 * <li> CACHE_SALT string(4) optional
 * <li> RELATIVE_PATH string(255) optional
 * <li> TAG string(100) optional
 * </ul>
 *
 **/

class TagTable extends Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_cache_tag';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap(){
        return array(
            new Entity\StringField('SITE_ID',array(
            )),
            new Entity\StringField('CACHE_SALT',array(
            )),
            new Entity\StringField('RELATIVE_PATH',array(
                "primary"       => true,
            )),
            new Entity\StringField('TAG',array(
            ))
        );
    }
}
?>