<?
namespace HB\helpers;

/**
 * Данный класс предназначен для работы так или иначе связанных с языком
 */
class LangHelpers {

    const SITE_NAME_EN = "en";
    const IBLOCK_PROP_TRANSLATION_ID = IBLOCK_PROP_TRANSLATION_ID;

    /**
     * Функция определяет на какой версии сайта в данный момент находится пользователь
     *
     * @return boolean
     */
    public static function isEnVersion($site_id = false) {
        if($site_id){
            return $site_id == self::SITE_NAME_EN;
        }
        return LANGUAGE_ID == self::SITE_NAME_EN;
    }

    /**
     * Функция принимает в себя массив который отдает news.list или CIBlockElement::GetList
     * и в зависимости от сайта подменяет value на нужный язык.
     *
     * @param array $arItem
     * @return array
     */
    public static function getTranslation($arItem, $isEng = false) {
        if(!$isEng){
            if (!self::isEnVersion()) {
                return $arItem;
            }
        }

        foreach ($arItem as $code => $arField) {
            if (strpos($code, "_EN") !== false) {
                continue;
            }

            if ($arFieldEN = self::issetField($arItem, $code)) {
                if (is_string($arField) && is_array($arFieldEN)) {
                    if (isset($arFieldEN["VALUE"]['TEXT'])) {
                        if($arFieldEN["~VALUE"]['TEXT']) $arItem[$code] = $arFieldEN["~VALUE"]['TEXT'];
                    } elseif(isset($arFieldEN['TEXT'])){
                        $arItem[$code] = $arFieldEN['TEXT'];
                    } else {
                        if($arFieldEN["~VALUE"]) $arItem[$code] = $arFieldEN["~VALUE"];
                    }
                } else {
                    if($arFieldEN) $arItem[$code] = $arFieldEN;
                }
            }

            if ($code == 'PROPERTIES' && is_array($arItem[$code])) {
                foreach($arItem[$code] as $codeProp => $arProp) {
                    if (strpos($codeProp, "_EN") !== false) {
                        continue;
                    }
                    
                    if ($arFieldEN = self::issetField($arItem, $codeProp)) {
                        if($arFieldEN) $arItem[$code][$codeProp] = $arFieldEN;
                    }
                }
            }
        }

        return $arItem;
    }

    /**
     * Проверяет на существование английской версии поля
     *
     * @param array $arItem
     * @param string $nameField
     * @return boolean
     */
    private static function issetField($arItem, $nameField) {
        $arField = array();

        if (isset($arItem['PROPERTIES']) && is_array($arItem['PROPERTIES'])) {
            $nameField = $nameField . "_EN";

            if ($arItem['PROPERTIES'][$nameField]) {
                $arField = $arItem['PROPERTIES'][$nameField];
            }
        } else {
            if (strpos($nameField, "PROPERTY") === 0) {
                $nameField = str_replace("_VALUE", "_EN_VALUE", $nameField);
            } else {
                $nameField = "PROPERTY_" . $nameField . "_EN_VALUE";
            }

            if ($arItem[$nameField]) {
                $arField = $arItem[$nameField];
            }
        }


        return empty($arField) ? false : $arField;
    }


    /**
     * Аналог функции getTranslation, только направлен на работу с разделами
     *
     * @param array $arItem
     * @return array
     */
    public static function getTranslationSection($arItem, $isEng = false) {

        if(!$isEng){
            if (!self::isEnVersion()) {
                return $arItem;
            }
        }
        

        foreach ($arItem as $code => $arField) {
            if (strpos($code, "_EN") !== false) {
                continue;
            }

            if ($arFieldEN = self::issetFieldSection($arItem, $code)) {
                $arItem[$code] = $arFieldEN;
            }
        }

        return $arItem;
    }
    
    /**
     * Проверяет на существование английской версии поля
     *
     * @param array $arItem
     * @param string $nameField
     * @return boolean
     */
    private static function issetFieldSection($arItem, $nameField) {
        $arField = array();

        if (strpos($nameField, "UF") === 0) {
            $nameField = $nameField . "_EN";
        } else {
            $nameField = "UF_" . $nameField . "_EN";
        }
        
        if ($arItem[$nameField]) {
            $arField = $arItem[$nameField];
        }

        return empty($arField) ? false : $arField;
    }

    //
    public static function getTranslationProperty($arProperties) {
        $arLangProperties = self::getPropNameTranslation();
        if($arLangProperties){
            foreach($arProperties as $code => $prop){
                if($arLangProperties[$code]) $arProperties[$code] = $arLangProperties[$code];
            }
        }
        return $arProperties;
    }

    /**
     * получает перевод названия свойств
     *
     * @return array
     */
    private static function getPropNameTranslation(){
        if(LANGUAGE_ID == 'ru') return array();
        $lang = strtoupper(LANGUAGE_ID);
        $rsPropTranslation = \CIBlockElement::GetList(array(), array("IBLOCK_ID"=> self::IBLOCK_PROP_TRANSLATION_ID, 'ACTIVE' => 'Y'), false, false, array("IBLOCK", "ID", "NAME", "PROPERTY_NAME_" . $lang));
        $arResultProp = array();
        while($arPropTranslation = $rsPropTranslation->GetNext()){
            $arResultProp[$arPropTranslation['NAME']] = $arPropTranslation["PROPERTY_NAME_" . $lang . "_VALUE"];
        }
        return $arResultProp;
    }



    public static function getTranslationUser($arUser) {

        if (!self::isEnVersion()) {
            return $arUser;
        }

        foreach ($arUser as $code => $arField) {
            if (strpos($code, "_EN") !== false) {
                continue;
            }
            $codeNew = str_replace('UF_', '', $code);
            if ($arFieldEN = $arUser['UF_' . $codeNew . '_EN']) {
                if($arFieldEN) $arUser[$code] = $arFieldEN;
            }
            elseif ($arFieldEN = $arUser['UF_' . $codeNew]) {
                if($arFieldEN) $arUser[$code] = $arFieldEN;
            }

            
        }

        return $arUser;
    }

    /**
     * перевод городов
     *
     * @return array
     */
    public static function getTranslationCity($arCity) {
        if (!self::isEnVersion()) {
            return $arCity;
        }
        foreach($arCity as &$city){
            $city['UF_CITY_NAME'] = $city['UF_CITY_NAME_EN'];
            $city['UF_REGION'] = $city['UF_REGION_EN'];
            $city['UF_CITY_FULLNAME'] = $city['UF_CITY_FULLNAME_EN'];
        }
        return $arCity;
    }


}

?>