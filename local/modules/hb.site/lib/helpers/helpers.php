<?
namespace HB\helpers;

\Bitrix\Main\Loader::IncludeModule("iblock");
\Bitrix\Main\Loader::includeModule("sale");
\Bitrix\Main\Loader::includeModule("catalog");

use Bitrix\Main\UserTable;
use HB\helpers\LangHelpers;

/**
 * Класс для написания общих сквозных функций по сайту.
 */

class Helpers
{

    const IBLOCK_SETTINGS_ID = IBLOCK_SETTINGS_ID; //настройки сайта
    const IBLOCK_WORK_ID = IBLOCK_WORK_ID;
    const AUTHOR_GROUP = AUTHOR_GROUP;
    const BUYER_GROUP = BUYER_GROUP;

    public static $something;

    public function __construct($something)
    {
        self::$something = $something;
    }
    /**
     * Метод сортирует массив по ID в нужном порядке как указано в $something
     * ВНИМАНИЕ! метод вызывается через функцию usort, и для передачи параметров него, используется замыкание
     *
     * @param mixed $a
     * @param mixed $b
     * @return array
     */
    public function sortByID($a, $b)
    {
        if (empty(self::$something)) {
            return 0;
        }

        $posA = (int) array_search($a["ID"], self::$something);
        $posB = (int) array_search($b["ID"], self::$something);

        if ($posA > $posB) {
            return 1;
        } elseif ($posA < $posB) {
            return -1;
        } else {
            return 0;
        }
    }

    /**
     * получает настройки сайта
     *
     * @return array
     */
    public static function getSiteSettings()
    {
        $rsSettings = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => self::IBLOCK_SETTINGS_ID, 'ACTIVE' => 'Y'), false, false);
        if ($obSettings = $rsSettings->GetNextElement()) {
            $arSettings['PROPERTIES'] = $obSettings->getProperties();
            $arSettings = LangHelpers::getTranslation($arSettings);
            $arProperties = $arSettings['PROPERTIES'];
            return $arProperties;
        } else {
            return array();
        }
    }

    /**
     * получает определенную настройку
     *
     * @return array
     */
    public static function getSiteSetting($code)
    {
        if (!empty($code)) {
            $setting = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => self::IBLOCK_SETTINGS_ID, 'ACTIVE' => 'Y'), false, false, array("PROPERTY_" . $code))->fetch();
            return ($setting["PROPERTY_" . $code . "_VALUE"]) ? $setting["PROPERTY_" . $code . "_VALUE"] : false;
        }
        return false;
    }

    /**
     * получает список элементов справочника
     *
     * @return array
     */
    public static function getHandbookList($iblockId)
    {
        $rsHandBook = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => $iblockId, 'ACTIVE' => 'Y'), false, false);
        $arResultHandbook = array();
        while ($obHandBook = $rsHandBook->GetNextElement()) {
            $arHandBook = $obHandBook->GetFields();
            $arHandBook['PROPERTIES'] = $obHandBook->GetProperties();
            $arHandBook = LangHelpers::getTranslation($arHandBook);
            if ($arHandBook['DETAIL_PICTURE']) {
                $arHandBook['DETAIL_PICTURE'] = \CFile::GetPath($arHandBook['DETAIL_PICTURE']);
            }

            $arResultHandbook[$arHandBook["ID"]] = $arHandBook;
        }
        return $arResultHandbook;
    }

    /**
     * получает список разделов справочника
     *
     * @return array
     */
    public static function getHandbookSectionList($iblockId)
    {
        $rsHandBook = \CIBlockSection::GetList(array(), array("IBLOCK_ID" => $iblockId, 'ACTIVE' => 'Y'), false, false);
        $arResultHandbookSectionList = array();
        while ($obHandBook = $rsHandBook->GetNextElement()) {
            $arHandBook = $obHandBook->GetFields();
            $arHandBook['PROPERTIES'] = $obHandBook->GetProperties();
            $arHandBook = LangHelpers::getTranslation($arHandBook);
            $arResultHandbookSectionList[$arHandBook["ID"]] = $arHandBook;
        }
        return $arResultHandbookSectionList;
    }

    /**
     * получает список элементов раздела справочника
     *
     * @return array
     */
    public static function getHandbookSectionElementsList($sectionID, $iblockId)
    {
        $arResultHandbookSectionElementsList = array();
        if (!empty($sectionID)) {
            $rsHandBook = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => $iblockId, "IBLOCK_SECTION_ID" => $sectionID, 'ACTIVE' => 'Y'), false, false);
            while ($obHandBook = $rsHandBook->GetNextElement()) {
                $arHandBook = $obHandBook->GetFields();
                $arHandBook['PROPERTIES'] = $obHandBook->GetProperties();
                $arHandBook = LangHelpers::getTranslation($arHandBook);
                $arResultHandbookSectionElementsList[$arHandBook["ID"]] = $arHandBook;
            }
        }
        return $arResultHandbookSectionElementsList;
    }

    /**
     * получение элементов по ID
     *
     * @return array
     */
    public static function getElementList($id)
    {
        $rsElement = \CIBlockElement::GetList(array(), array("ID" => $id, 'ACTIVE' => 'Y'), false, false);
        $arResult = array();
        while ($obElement = $rsElement->GetNextElement()) {
            $arElement = $obElement->GetFields();
            $arElement['PROPERTIES'] = $obElement->GetProperties();
            $arElement = LangHelpers::getTranslation($arElement);
            $arResult[$arElement["ID"]] = $arElement;
        }
        return $arResult;
    }

    /**
     * получает информацию о разделах по ID
     *
     * @return array
     */
    public static function getSectionById($iblockId, $arSectionId)
    {
        $rsSection = \CIBlockSection::GetList(array("SORT" => "­­ASC"), array("IBLOCK_ID" => $iblockId, "ID" => $arSectionId), false, array("ID", "IBLOCK_ID", "NAME", "SECTION_PAGE_URL", "UF_*"));
        $arResultSection = array();
        while ($arSection = $rsSection->GetNext()) {
            $arSection = LangHelpers::getTranslationSection($arSection);
            $arResultSection[$arSection['ID']] = $arSection;
        }
        return $arResultSection;
    }

    /**
     * получает аватарку пользователя
     *
     * @return string
     */
    public static function getAvatar()
    {
        global $USER;
        $arFilter = array("ID" => $USER->GetID());
        $rsUsers = \CUser::GetList(($by = "NAME"), ($order = "desc"), $arFilter);
        if ($arUser = $rsUsers->Fetch()) {
            if ($arUser['PERSONAL_PHOTO']) {
                return \CFile::ResizeImageGet(
                    $arUser['PERSONAL_PHOTO'],
                    array("width" => 80, "height" => 80),
                    BX_RESIZE_IMAGE_PROPORTIONAL)['src'];
            }

        }
        return false;
    }

    /**
     * информация о свойстве по ID
     *
     * @return array
     */
    public static function getProperty($propId, $iblockId)
    {
        $res = \CIBlockProperty::GetByID($propId, $iblockId);
        if ($arRes = $res->GetNext()) {
            return $arRes;
        } else {
            return false;
        }

    }

    /**
     * function return word with correct ending
     * Example: endingWord( 2, "товар","товара","товаров")
     *
     * @param [int] $number
     * @param [string] $after
     * @return string
     */
    public static function endingWord($number, $after)
    {
        $cases = array(2, 0, 1, 1, 1, 2);
        return $after[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
    }

    /**
     * Обрезает строку до определенного символа, но учитывает конец слова
     *
     * @param string $str
     * @param integer $length
     * @return array|string
     */
    public static function cutStr($str, $length = 50)
    {
        if (strlen($str) <= $length) {
            return $str;
        }

        $temp = substr($str, 0, $length);
        return array(
            "BEFORE" => substr($temp, 0, strrpos($temp, ' ')),
            "AFTER" => substr($str, strrpos($temp, ' ')),
        );
    }
    /**
     * Получаем протокол сайта
     *
     * @return string
     */
    public static function getProtocol()
    {
        if (!empty($_SERVER['HTTPS']) && 'off' !== strtolower($_SERVER['HTTPS'])) {
            return "https://";
        } else {
            return "http://";
        }
    }

    /**
     * Вывыодим полный адрес текущей страницы
     *
     * @return string
     */
    public static function getCurPageUrl()
    {
        global $APPLICATION;
        return self::getProtocol() . $_SERVER["SERVER_NAME"] . $APPLICATION->getCurPage();
    }
    /**
     * Вывыодим адресс сайта
     *
     * @return string
     */
    public static function getSiteUrl()
    {
        global $APPLICATION;
        return self::getProtocol() . $_SERVER["SERVER_NAME"];
    }
    /**
     * Возвращает массив с ссылками на социальные сети
     *
     * @param boolean|string $title
     * @param boolean|string $desc
     * @param boolean|string $url
     * @return array
     */
    public static function getSocialLinks($title = false, $desc = false, $url = false)
    {
        global $APPLICATION;

        $title = ($title) ? $title : $APPLICATION->getTitle(false);
        $desc = ($desc) ? $desc : $APPLICATION->getPageProperty("description");
        $url = ($url) ? $url : self::getCurPageUrl();

        return array(
            "VK" => self::encodeURL("https://vk.com/share.php?url=" . $url . "&title=" . $title . "&description=" . $desc),
            "FACEBOOK" => self::encodeURL("https://www.facebook.com/sharer.php?src=sp&u=" . $url . "&title=" . $title . "&description=" . $desc),
            "OK" => self::encodeURL("https://connect.ok.ru/offer?url=" . $url . "&title=" . $title . "&description=" . $desc),
            "VIBER" => self::encodeURL("viber://forward?text=" . $title . ". " . $desc),
            "WHATSAPP" => self::encodeURL("https://api.whatsapp.com/send?text=" . $title . ". " . $desc),
        );
    }
    /**
     * Функция берет все get параметры из url и кодирует русские символы ибо без этого не работает
     *
     * @param string $url
     * @return void
     */
    public static function encodeURL($url)
    {
        preg_match("/\?(.*)/", $url, $matches);

        $url = preg_replace("/\?.*/", "", $url);
        $s = $matches[1];

        $res = array();
        foreach (explode('&', $s) as $tmp) {
            $tmp2 = explode('=', $tmp);
            $key = $tmp2[0];
            unset($tmp2[0]);
            $res[$key] = str_replace("+", "%20", urlencode(implode('=', $tmp2)));
        }

        $query = "?";

        $i = 0;
        foreach ($res as $key => $value) {
            if ($i == 0) {
                $query .= $key . "=" . $value;
            } else {
                $query .= "&" . $key . "=" . $value;
            }
            $i++;
        }
        return $url . $query;
    }

    /**
     * При вызове функция проверяет есть ли переменная которая запрещает вывод хлебных крошек
     * Если нет то через отложенную функцию мы выводим хлебные крошки на странице
     *
     * @return void
     */
    public static function SetBreadCrumb()
    {
        global $APPLICATION;
        if ($APPLICATION->GetCurPage(false) !== '/') {
            if (!defined("HIDE_BREADCRUMB")) {
                \ob_start();
                $APPLICATION->IncludeComponent(
                    "bitrix:breadcrumb",
                    "",
                    array(
                        "PATH" => "",
                        "SITE_ID" => SITE_ID,
                        "START_FROM" => "0",
                    )
                );
                $result = \ob_get_contents();
                \ob_end_clean();
                $APPLICATION->AddViewContent("BREADCRUMB", $result);
            }
        }
    }

    /**
     * Формируем список месяцев
     * Лучшего способо не придумал
     * @return array
     */

    public static function getArrayMonth()
    {
        $arMonth = array();
        if (LangHelpers::isEnVersion()) {
            $arMonth = array(
                "01" => "January",
                "02" => "February",
                "03" => "March",
                "04" => "April",
                "05" => "May",
                "06" => "June",
                "07" => "July",
                "08" => "Augusta",
                "09" => "September",
                "10" => "October",
                "11" => "November",
                "12" => "December",
            );
        } else {
            $arMonth = array(
                "01" => "Января",
                "02" => "Февраля",
                "03" => "Марта",
                "04" => "Апреля",
                "05" => "Мая",
                "06" => "Июня",
                "07" => "Июля",
                "08" => "Августа",
                "09" => "Сентября",
                "10" => "Октября",
                "11" => "Ноября",
                "12" => "Декабря",
            );
        }
        return $arMonth;
    }

    /**
     * Метод получает id города пользователя по id в противном случае берет из сессии
     *
     * @param boolean $userId
     * @return int
     */
    public static function getIdCitiesUser($userId = false)
    {
        global $USER;
        if ($USER->IsAuthorized()) {
            if (!$userId) {

                $userId = $USER->GetID();
            }
            $arSelect = array("NAME", "LAST_NAME", "PERSONAL_PHONE", "EMAIL", "UF_CITY_ID");
            $arResult['USER'] = \CUser::GetList(($by = "id"), ($order = "desc"), array("ID" => $userId), array("SELECT" => $arSelect, "FIELDS" => $arSelect))->fetch();
            return $arResult['USER']['UF_CITY_ID'] ? $arResult['USER']['UF_CITY_ID'] : false;
        } else {
            return $_SESSION['CITY_ID'];
        }
    }

    /**
     * Метод обновляет ID города пользователя
     *
     * @param string $cityName
     * @param int $cityId
     * @param boolean $userId
     * @return bool
     */
    public static function updateIdCitiesUser($cityName, $cityId, $userId = false)
    {
        global $USER;
        if ($USER->IsAuthorized()) {
            if (!$userId) {

                $userId = $USER->GetID();
            }
            $user = new \CUser;
            $arFields = array(
                "UF_CITY_ID" => $cityId,
                "UF_CITY" => $cityName,
            );
            return $user->Update($userId, $arFields);
        } else {
            $_SESSION['CITY_ID'] = $cityId;
            return true;
        }
    }

    /**
     * Метод возвращает имя пользователя в зависимости от языка
     *
     * @param boolean $userId
     * @param boolean $isEng
     * @return string
     */
    public static function getUserName($userId = false, $isEng = false)
    {
        $result = "";
        global $USER;

        if (!$userId) {
            $userId = $USER->GetId();
        }

        $user = UserTable::getList(array(
            "filter" => array("ID" => $userId),
            "select" => array("NAME", "LAST_NAME", "UF_NAME_EN", "UF_LAST_NAME", "LOGIN"),
        ))->fetch();

        if (LangHelpers::isEnVersion() || $isEng) {
            $name = "UF_NAME_EN";
            $lastName = "UF_LAST_NAME";
        } else {
            $name = "NAME";
            $lastName = "LAST_NAME";
        }

        if ($user[$name] || $user[$lastName]) {
            $result = $user[$name] . " " . $user[$lastName];
        } else {
            $result = $user["LOGIN"];
        }
        return trim($result);
    }

    /**
     * Метод получает email пользователя по его id
     *
     * @param boolean $userId
     * @return string
     */
    public static function getUserEmail($userId = false)
    {
        $result = "";
        global $USER;

        if (!$userId) {
            $userId = $USER->GetId();
        }

        $user = UserTable::getList(array(
            "filter" => array("ID" => $userId),
            "select" => array("EMAIL"),
        ))->fetch();

        return $user["EMAIL"];
    }

    /**
     * Метод шифрует строку по заданному ключу
     *
     * @param string $unencoded
     * @param string $key
     * @return string
     */
    public static function encode($unencoded, $key)
    {
        $string = base64_encode($unencoded);

        $arr = array();
        $x = 0;
        while ($x++ < strlen($string)) {
            $arr[$x - 1] = md5(md5($key . $string[$x - 1]) . $key);
            $newstr = $newstr . $arr[$x - 1][3] . $arr[$x - 1][6] . $arr[$x - 1][1] . $arr[$x - 1][2];
        }
        return $newstr;
    }

    /**
     * Метод расшифровывает строку по заданному ключу
     *
     * @param string $encoded
     * @param string $key
     * @return string
     */
    public static function decode($encoded, $key)
    {
        $strofsym = "qwertyuiopasdfghjklzxcvbnm1234567890QWERTYUIOPASDFGHJKLZXCVBNM=";
        $x = 0;
        while ($x++ <= strlen($strofsym)) {
            $tmp = md5(md5($key . $strofsym[$x - 1]) . $key);
            $encoded = str_replace($tmp[3] . $tmp[6] . $tmp[1] . $tmp[2], $strofsym[$x - 1], $encoded);
        }
        return base64_decode($encoded);
    }

    /**
     * получаем регион пользователя
     *
     * @param boolean $userId
     * @return string
     */
    public static function getUserRegion($userId = false)
    {
        $result = "";
        global $USER;
        if ($USER->IsAuthorized()) {
            if (!$userId) {
                $userId = $USER->GetId();
            }

            $user = UserTable::getList(array(
                "filter" => array("ID" => $userId),
                "select" => array("UF_REGION"),
            ))->fetch();

            return $user["UF_REGION"];
        } else {
            return $_SESSION['REGION'];
        }
    }
    /**
     * Метод возвращает ссылку на страницу с пользовательскимм соглашением
     *
     * @return string
     */
    public static function getTermsLink()
    {
        return self::getSiteSetting("LINK_TERMS");
    }

    public static function hasUserRole($userID = false)
    {
        global $USER;
        $userID = $userID ? $userID : $USER->GetID();

        $arGroup = \CUser::GetUserGroup($userID);
        if (in_array(self::AUTHOR_GROUP, $arGroup) || in_array(self::BUYER_GROUP, $arGroup)) {
            return true;
        }

        return false;
    }

    /**
     * Метод возвращает текст соглашения авторизации
     *
     * @return string
     */
    public static function getAuthTerms()
    {
        return self::getSiteSetting("TERMS_AUTH")["TEXT"];
    }

    /**
     * получает список интерьеров для слайдера изображений
     *
     * @return array
     */
    public static function getInteriorList()
    {
        $interiorSelect = array("ID", "PREVIEW_PICTURE", "PROPERTY_MARGIN_TOP", "PROPERTY_MARGIN_LEFT", "PROPERTY_WIDTH");
        $rsInterior = \CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID"=> IBLOCK_INTERIOR_ID, 'ACTIVE' => 'Y'), false, false, $interiorSelect);
        $arResultInterior = array();
        while($arInterior = $rsInterior->Fetch()){
            if($arInterior['PREVIEW_PICTURE']) {
                $arInterior['PREVIEW_PICTURE_SMALL'] = \CFile::ResizeImageGet($arInterior['PREVIEW_PICTURE'], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                $arInterior['PREVIEW_PICTURE'] = \CFile::GetPath($arInterior['PREVIEW_PICTURE']);
            }
            $arResultInterior[$arInterior["ID"]] = $arInterior;
        }
        return $arResultInterior;
    }

    /**
     * Метод возвращает набор статусов модерации и комментариев по модерации личной информации пользователя, личной информации автора, картин автора
     *
     * @param boolean $userId
     * @return array
     */
    public static function getAuthorModerationStatuses($siteSettings = array())
    {
        global $USER;
        $authorModerationStatuses = array();
        $languagePrefix = LANGUAGE_ID == 'ru' ? '' : '_EN';
        $arSettings = !empty($siteSettings) ? $siteSettings : Helpers::getSiteSettings();

        //статус модерации личной информации пользователя
        $user = UserTable::getList(array(
            "filter" => array("ID" => $USER->GetID()),
            "select" => array("UF_MODERATION", "UF_MODER_COMMENT"),
        ))->fetch();
        if ($user['UF_MODERATION']) {
            $rsEnum = \CUserFieldEnum::GetList(array(), array("USER_FIELD_NAME" => "UF_MODERATION", "ID" => $user['UF_MODERATION']));
            $moderationStatusProp = $rsEnum->GetNext();
            $authorModerationStatuses['USER']['STATUS_TEXT'] = str_replace('#STATUS#', $moderationStatusProp['VALUE'], $arSettings['USER_MODERATING_STATUS' . $languagePrefix]['VALUE']);
            if ($moderationStatusProp['ID'] == USER_PROP_MODERATION_UNCOMPLETED && $user["UF_MODER_COMMENT"]) {
                $authorModerationStatuses['USER']['COMMENT'] = str_replace('#COMMENT#', $user["UF_MODER_COMMENT"], $arSettings['MODERATING_COMMENT' . $languagePrefix]['VALUE']);
            }
        }

        //статус модерации личной информации автора
        $authorSelectFields = array("ID", "PROPERTY_MODERATION", "PROPERTY_MODERATION_COMMENT");
        $author = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_AUTHORS_ID, "PROPERTY_USER" => $USER->GetID()), false, false, $authorSelectFields)->fetch();
        if ($author["PROPERTY_MODERATION_VALUE"] && $author['ID']) {
            $authorModerationStatuses['AUTHOR']['STATUS_TEXT'] = str_replace('#STATUS#', $author["PROPERTY_MODERATION_VALUE"], $arSettings['AUTHOR_MODERATING_STATUS' . $languagePrefix]['VALUE']);
            if ($author['PROPERTY_MODERATION_VALUE'] == "Не пройдена" && $author["PROPERTY_MODERATION_COMMENT_VALUE"] != '') {
                $authorModerationStatuses['AUTHOR']['COMMENT'] = str_replace('#COMMENT#', $author["PROPERTY_MODERATION_COMMENT_VALUE"], $arSettings['MODERATING_COMMENT' . $languagePrefix]['VALUE']);
            }

            //статус модерации картин автора
            $workSelectFields = array("ID", "NAME", "PROPERTY_MODERATION", "PROPERTY_MODERATION_COMMENT");
            $rsWorks = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_WORK_ID, "PROPERTY_AUTHOR" => $author['ID']), false, false, $workSelectFields);
            while ($work = $rsWorks->GetNext()) {
                $workFields = $work;
                $statusText = str_replace('#NAME#', $workFields["NAME"], $arSettings['WORK_MODERATING_STATUS' . $languagePrefix]['VALUE']);
                if ($workFields["PROPERTY_MODERATION_VALUE"]) {
                    $authorModerationStatuses['WORK_' . $workFields['ID']]['STATUS_TEXT'] = str_replace('#STATUS#', $workFields["PROPERTY_MODERATION_VALUE"], $statusText);
                    if ($workFields["PROPERTY_MODERATION_ENUM_ID"] == WORK_PROP_MODERATION_UNCOMPLETED && $workFields["PROPERTY_MODERATION_COMMENT_VALUE"] != '') {
                        $authorModerationStatuses['WORK_' . $workFields['ID']]['COMMENT'] = str_replace('#COMMENT#', $workFields["PROPERTY_MODERATION_COMMENT_VALUE"], $arSettings['MODERATING_COMMENT' . $languagePrefix]['VALUE']);
                    }
                }
            }
        }

        return $authorModerationStatuses;
    }

    /**
     * Метод создает новую корзину на основе корзины предыдущего неоплаченного заказа
     *
     * @param integer $last_order_ID
     * @return string
     */
    public static function recreacteLastOrder($last_order_ID = 0)
    {
        if ($last_order_ID) {
            $obBasket = \Bitrix\Sale\Basket::getList(array('filter' => array('ORDER_ID' => $last_order_ID)));
            while ($bItem = $obBasket->Fetch()) {
                $lastBasketItems[] = $bItem;
            }
            if ($lastBasketItems) {
                $basket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(), \Bitrix\Main\Context::getCurrent()->getSite());
                foreach ($lastBasketItems as $lastBasketItem) {
                    $item = $basket->createItem('catalog', $lastBasketItem['PRODUCT_ID']);
                    $item->setFields(array(
                        'QUANTITY' => $lastBasketItem['QUANTITY'],
                        'CURRENCY' => $lastBasketItem['CURRENCY'],
                        'LID' => $lastBasketItem['LID'],
                        'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
                    ));
                }
                $basket->save();
                \Bitrix\Sale\Order::delete($last_order_ID);
                return 'Y';
            }
            return 'N';
        } else {
            return 'N';
        }
    }

    /**
     * Метод проверяет оплачен ли последней заказ, и если нет возвращает его ID
     *
     * @return integer|boolean
     */
    public static function getLastOrder()
    {
        global $USER;
        $orderData = \Bitrix\Sale\Order::getList([
            'select' => ['ID', 'PAYED'],
            'filter' => ['=USER_ID' => $USER->GetID()],
            'order' => ['ID' => 'DESC'],
            'limit' => 1,
        ]);
        $order = $orderData->fetch();
        if ($order['ID'] && $order['PAYED'] == 'N') {
            return $order['ID'];
        } else {
            return false;
        }
    }
}
