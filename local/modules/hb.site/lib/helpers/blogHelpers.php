<?
namespace HB\helpers;
use HB\helpers\LangHelpers;

class BlogHelpers{
    const IBLOCK_ID = IBLOCK_BLOG_ID;
    /**
     * пользователь посмотрел блог
     *
     * @return boolean
     */
    public static function userViewBlog($blogId){
        $PROPERTY_CODE = "COUNT_SHOW";

        $element = \CIBlockElement::GetList(array(), array("ACTIVE" =>"Y", "=ID" => $blogId, "IBLOCK_ID" => self::IBLOCK_ID), false, false, array("PROPERTY_" . $PROPERTY_CODE))->fetch();

        $countShow = intval($element["PROPERTY_" . $PROPERTY_CODE . "_VALUE"]) + 1;
        \CIBlockElement::SetPropertyValuesEx($blogId, false, array($PROPERTY_CODE => $countShow));
    }
}