<?
namespace HB\helpers;

use HB\helpers\HightLoadHelpers;

class RatingHelpers
{

    const IBLOCK_WORK_ID = IBLOCK_WORK_ID;
    const IBLOCK_AUTHORS_ID = 6;

    const KOEF_TEAM_RATING = 10;
    const KOEF_EXPERT_RATING = 40;
    const KOEF_LIKES_RATING = 25;
    const KOEF_FAVORITES_RATING = 5;
    const KOEF_AUTHOR_RATING = 20;
    const KOEF_CREATED_DAY = 100;
    const HL_EXPERT_RATING_ID = 6;

    /**
     * получение информации по рейтингу работы
     *
     * @return array
     */
    public static function getInfoRatingWork($workId)
    {
        $rsWork = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => self::IBLOCK_WORK_ID, 'ID' => $workId, 'ACTIVE' => 'Y'), false, false, array('IBLOCK_ID', 'ID', 'DATE_CREATE', 'ACTIVE_FROM', 'PROPERTY_COUNT_LIKES', 'PROPERTY_COUNT_FAVORITES', 'PROPERTY_TEAM_RATING', 'PROPERTY_EXPERT_RATING', 'PROPERTY_FINAL_RATING', 'PROPERTY_AUTHOR', 'PROPERTY_COUNT_SHOW'));
        if ($arWork = $rsWork->GetNext()) {
            return array(
                'ID' => $arWork['ID'],
                'DATE_CREATE' => $arWork['DATE_CREATE'],
                'ACTIVE_FROM' => $arWork['ACTIVE_FROM'],
                'COUNT_LIKES' => $arWork['PROPERTY_COUNT_LIKES_VALUE'],
                'COUNT_FAVORITES' => $arWork['PROPERTY_COUNT_FAVORITES_VALUE'],
                'COUNT_SHOW' => $arWork['PROPERTY_COUNT_SHOW_VALUE'],
                'TEAM_RATING' => $arWork['PROPERTY_TEAM_RATING_VALUE'],
                'EXPERT_RATING' => $arWork['PROPERTY_EXPERT_RATING_VALUE'],
                'FINAL_RATING' => $arWork['PROPERTY_FINAL_RATING_VALUE'],
                'AUTHOR' => $arWork['PROPERTY_AUTHOR_VALUE'],
            );
        }
        return false;
    }

    /**
     * получение рейтинга художника
     *
     * @return int
     */
    public static function getAuthorRating($authorId)
    {
        $rsAuthor = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => self::IBLOCK_AUTHORS_ID, 'ID' => $authorId, 'ACTIVE' => 'Y'), false, false, array('IBLOCK_ID', 'ID', 'PROPERTY_AUTHOR_RATING'));
        if ($arAuthor = $rsAuthor->GetNext()) {
            return $arAuthor['PROPERTY_AUTHOR_RATING_VALUE'];
        }
        return false;
    }

    /**
     * подсчет рейтинга работы
     *
     * @return void
     */
    public static function calculateWorkRating($countLikes, $countFavorites, $authorRating, $expertRating, $teamRating, $createdDay, $additionalRating = 0)
    {
        return ceil(
            self::KOEF_LIKES_RATING * $countLikes + self::KOEF_FAVORITES_RATING * $countFavorites +
            self::KOEF_AUTHOR_RATING * $authorRating + self::KOEF_EXPERT_RATING * $expertRating +
            self::KOEF_TEAM_RATING * $teamRating + self::KOEF_CREATED_DAY * $createdDay + $additionalRating
        );
    }

    /**
     * подсчет рейтинг художника
     *
     * @return int
     */
    public static function calculateAuthorRating($authorId)
    {
        $arSettings = Helpers::getSiteSettings();
        $numberPaintings = self::getNumberPaintingsPurchased($authorId);
        $arAuthorRating = array();
        $minNumber = 1000;
        $maxNumber = 0;
        $firstNumber = -1;
        $lastNumber = -1;
        foreach ($arSettings['ARTING_RATING']['VALUE'] as $rating) {
            if (stripos($rating, '/') === false) {
                $arAuthorRating[$rating] = $rating;
                if ($firstNumber == -1) {
                    $firstNumber = $rating;
                } else {
                    $lastNumber = $rating;
                }

            } else {
                $arRating = explode('/', $rating);
                $arAuthorRating[$arRating['0']] = $arRating['1'];
                if ($numberPaintings == $arRating['0']) {
                    return $arRating['1'];
                }

                if ($arRating['0'] < $minNumber) {
                    $minNumber = $arRating['0'];
                }

                if ($arRating['0'] > $maxNumber) {
                    $maxNumber = $arRating['0'];
                }

            }
        }

        if ($numberPaintings < $minNumber) {
            return $arAuthorRating[$firstNumber];
        }

        if ($numberPaintings > $maxNumber) {
            return $arAuthorRating[$lastNumber];
        }

    }

    /**
     * подсчет количества купленных работ художника
     *
     * @return int
     */
    public static function getNumberPaintingsPurchased($authorId)
    {
        $rsWork = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => self::IBLOCK_WORK_ID, 'ACTIVE' => 'Y', 'PROPERTY_AUTHOR' => $authorId, 'PROPERTY_WORK_PURCHASED_VALUE' => 'Да'), false, false);
        return $rsWork->SelectedRowsCount();
    }

    /**
     * изменение рейтинга художника
     *
     * @return void
     */
    public static function updateAuthorRating($authorId, $newRating)
    {
        $PROPERTY_CODE = "AUTHOR_RATING";
        \CIBlockElement::SetPropertyValuesEx($authorId, false, array($PROPERTY_CODE => $newRating));
    }

    /**
     * изменение рейтинга работы
     *
     * @return void
     */
    public static function updateWorkRating($workrId, $newRating)
    {
        $PROPERTY_CODE = "FINAL_RATING";
        \CIBlockElement::SetPropertyValuesEx($workrId, false, array($PROPERTY_CODE => $newRating));
    }

    /**
     * изменение рейтинга работ автора
     *
     * @return void
     */
    public static function updateAuthorWorkRating($authorId, $newRatingAuthor)
    {
        $rsWork = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => self::IBLOCK_WORK_ID, 'ACTIVE' => 'Y', 'PROPERTY_AUTHOR' => $authorId, 'PROPERTY_WORK_PURCHASED_VALUE' => false), false, false, array('IBLOCK_ID', 'ID', 'DATE_CREATE', 'PROPERTY_COUNT_LIKES', 'PROPERTY_COUNT_FAVORITES', 'PROPERTY_TEAM_RATING', 'PROPERTY_EXPERT_RATING', 'PROPERTY_FINAL_RATING', 'PROPERTY_AUTHOR'));
        while ($arWork = $rsWork->GetNext()) {
            $time = time();
            $time_created = strtotime($arWork['DATE_CREATE']);
            $createdDay = ceil(($time - $time_created) / (60 * 60 * 24) + 1);
            $countLikes = $arWork['PROPERTY_COUNT_LIKES_VALUE'];
            $countFavorites = $arWork['PROPERTY_COUNT_FAVORITES_VALUE'];
            $authorRating = $newRatingAuthor;
            $expertRating = $arWork['PROPERTY_EXPERT_RATING_VALUE'];
            $teamRating = $arWork['PROPERTY_TEAM_RATING_VALUE'];
            $newRatingWork = self::calculateWorkRating($countLikes, $countFavorites, $authorRating, $expertRating, $teamRating, $createdDay);
            self::updateWorkRating($arWork['ID'], $newRatingWork);
        }

    }

    /**
     * обновление рейтинга работы
     *
     * @return void
     */
    public static function changeWorkRating($arRatingWork)
    {
        $time = time();
        $time_created = strtotime($arRatingWork['ACTIVE_FROM'] ? $arRatingWork['ACTIVE_FROM'] : $arRatingWork['DATE_CREATE']);
        $createdDay = ceil(($time - $time_created) / (60 * 60 * 24) + 1);
        $countLikes = ($arRatingWork['PROPERTY_COUNT_LIKES_VALUE'] ? $arRatingWork['PROPERTY_COUNT_LIKES_VALUE'] : $arRatingWork['COUNT_LIKES']);
        $countFavorites = ($arRatingWork['PROPERTY_COUNT_FAVORITES_VALUE'] ? $arRatingWork['PROPERTY_COUNT_FAVORITES_VALUE'] : $arRatingWork['COUNT_FAVORITES']);
        $authorId = ($arRatingWork['PROPERTY_AUTHOR_VALUE'] ? $arRatingWork['PROPERTY_AUTHOR_VALUE'] : $arRatingWork['AUTHOR']);
        $authorRating = RatingHelpers::getAuthorRating($authorId);
        $expertRating = ($arRatingWork['PROPERTY_EXPERT_RATING_VALUE'] ? $arRatingWork['PROPERTY_EXPERT_RATING_VALUE'] : $arRatingWork['EXPERT_RATING']);
        $teamRating = ($arRatingWork['PROPERTY_TEAM_RATING_VALUE'] ? $arRatingWork['PROPERTY_TEAM_RATING_VALUE'] : $arRatingWork['TEAM_RATING']);
        $newRatingWork = self::calculateWorkRating($countLikes, $countFavorites, $authorRating, $expertRating, $teamRating, $createdDay);
        RatingHelpers::updateWorkRating($arRatingWork['ID'], $newRatingWork);
    }

    //эксперт оценил работу
    public static function expertAppreciatedWork($rating, $workId, $userId)
    {
        if (!$userId) {
            global $USER;
            $userId = $USER->GetID();
        }
        $rsWork = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => self::IBLOCK_WORK_ID, 'ID' => $workId), false, false);
        $arExpertRating = array();
        $sumRating = 0;
        if ($obWork = $rsWork->GetNextElement()) {
            $arPropWork = $obWork->GetProperties();
            if ($arPropWork['EXPERTS_RATING']['VALUE']) {
                foreach ($arPropWork['EXPERTS_RATING']['VALUE'] as $numValue => $expertId) {
                    $arExpertRating[$expertId] = $arPropWork['EXPERTS_RATING']['DESCRIPTION'][$numValue];
                    $sumRating += $arPropWork['EXPERTS_RATING']['DESCRIPTION'][$numValue];
                }
            }

        }
        if (!isset($arExpertRating[$userId])) {
            $arExpertRating[$userId] = $rating;
            $sumRating += $rating;
        } else {
            $sumRating -= $arExpertRating[$userId];
            $arExpertRating[$userId] = $rating;
            $sumRating += $rating;
        }

        $expertRating = ceil($sumRating / count($arExpertRating));
        $PROPERTY_CODE = "EXPERT_RATING";
        \CIBlockElement::SetPropertyValuesEx($workId, false, array($PROPERTY_CODE => $expertRating));

        $arExpertsRating = array();
        foreach ($arExpertRating as $expertId => $rating) {
            $arExpertsRating[] = array("VALUE" => $expertId, "DESCRIPTION" => $rating);
        }
        $PROPERTY_CODE = "EXPERTS_RATING";
        \CIBlockElement::SetPropertyValuesEx($workId, false, array($PROPERTY_CODE => $arExpertsRating));

        $arRatingWork = self::getInfoRatingWork($workId);
        self::changeWorkRating($arRatingWork);

        //запись оценки эксперта в highload-блок
        self::addExpertRatingToHighload($userId, $workId, $arPropWork['AUTHOR']['VALUE'], $rating);
    }

    //запись оценки эксперта в highload-блок
    public static function addExpertRatingToHighload($expertId, $workId, $authorId, $rating)
    {
        if (self::getRatingWorkFromExpert($expertId, $workId)) {
            return false;
        }

        $time = date('d.m.Y H:i:s');
        $status = HightLoadHelpers::Add(self::HL_EXPERT_RATING_ID, array("UF_EXPERT" => $expertId, 'UF_WORK' => $workId, 'UF_AUTHOR' => $authorId, 'UF_RATING' => $rating, 'UF_TIME' => $time));
        return $status;
    }

    //проверка наличия оценки эксперта
    public static function getRatingWorkFromExpert($expertId, $workId)
    {
        $elements = HightLoadHelpers::getAll(self::HL_EXPERT_RATING_ID, array("UF_EXPERT" => $expertId, "UF_WORK" => $workId), array("UF_RATING"));
        if (!$elements) {
            return false;
        }

        $result = array();
        foreach ($elements as $element) {
            $result = $element;
            return $result['UF_RATING'];
        }
        return $result['UF_RATING'];
    }

    //оценки эксперта за день
    public static function getRatingExpertDay($expertId = false)
    {
        if (!$expertId) {
            global $USER;
            $expertId = $USER->GetID();
        }
        $time = date('d.m.Y') . " 00:00:00";
        $elements = HightLoadHelpers::getAll(self::HL_EXPERT_RATING_ID, array("UF_EXPERT" => $expertId, ">UF_TIME" => $time), array("*"));
        if (!$elements) {
            return array();
        }

        $result = array();
        foreach ($elements as $element) {
            $result[] = $element;
        }
        return $result;
    }

    //все оценки за день
    public static function getRatingAllExpertDay()
    {
        $time = date('d.m.Y') . " 00:00:00";
        $elements = HightLoadHelpers::getAll(self::HL_EXPERT_RATING_ID, array(">UF_TIME" => $time), array("*"));
        if (!$elements) {
            return array();
        }

        $result = array();
        foreach ($elements as $element) {
            $result[$element['UF_EXPERT']][] = $element;
        }
        return $result;
    }

    //работы, оцененные экспертом
    public static function getExpertWork($expertId = false)
    {
        if (!$expertId) {
            global $USER;
            $expertId = $USER->GetID();
        }
        $elements = HightLoadHelpers::getAll(self::HL_EXPERT_RATING_ID, array("UF_EXPERT" => $expertId), array("*"));
        if (!$elements) {
            return array();
        }

        $result = array();
        foreach ($elements as $element) {
            $result[] = $element['UF_WORK'];
        }
        return $result;
    }

    /**
     * обнуление рейтинга работы и её художника
     *
     * @return array
     */
    public static function resetWorkRating($workId)
    {
        if ($workId) {
            $rsWork = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => self::IBLOCK_WORK_ID, '=ID' => $workId), false, false, array('IBLOCK_ID', 'ID', 'PROPERTY_AUTHOR'));
            if ($arWork = $rsWork->Fetch()) {
                if ($arWork['PROPERTY_AUTHOR_VALUE']) {
                    self::updateAuthorRating($arWork['PROPERTY_AUTHOR_VALUE'], 0); // обнуляем рейтинг автора
                }
                $arWorkArray = array(
                    'COUNT_LIKES' => 0,
                    'COUNT_FAVORITES' => 0,
                    'COUNT_SHOW' => 0,
                    'TEAM_RATING' => 0,
                    'EXPERT_RATING' => 0,
                    'FINAL_RATING' => 0,
                );
                \CIBlockElement::SetPropertyValuesEx($workId, self::IBLOCK_WORK_ID, $arWorkArray); // обнуляем рейтинги работы

                global $USER;
                $el = new \CIBlockElement;
                $arLoadProductArray = array(
                    "MODIFIED_BY" => $USER->GetID(),
                    "DATE_ACTIVE_FROM" => date('d.m.Y H:i:s'),
                );
                $res = $el->Update($workId, $arLoadProductArray); // устанавливаем текущую дату в дату активности работы
                $result['res'] = $res;
                if ($res) {
                    $result['STATUS'] = true;
                } else {
                    $result['ERROR'] = "Ошибка во время обновления работы";
                    $result['STATUS'] = false;
                }
            } else {
                $result['ERROR'] = "Не найдена работа";
                $result['STATUS'] = false;
            }
        } else {
            $result['STATUS'] = false;
            $result['ERROR'] = "Неверный ID";
        }
        return $result;
    }

}
