<?
namespace HB\helpers;
use HB\helpers\WorkHelpers;
use HB\helpers\Helpers;
use Bitrix\Iblock\Template\Engine;
use Bitrix\Iblock\Template\Entity\Element;
use Bitrix\Iblock\InheritedProperty\ElementTemplates;

class CatalogHelpers{
    const IBLOCK_ID = IBLOCK_WORK_ID;
    const CITY_MOSKOW_ID = 44;
    const IBLOCK_SEO_FILTER_PAGE_ID = 27;

    const PROPS_NOT_LOWER = [
        "AUTHOR"
    ];
    /**
     * Получает нужные парамерты у товара
     *
     * @return void
     */
    public function getInfoByProducts($ids,$arProps, $resize = false){
        if(is_array($ids)){
            $arProps = array_flip($arProps);
            $db = \CIBlockElement::GetList(array(), array(/*"IBLOCK_ID"=> self::IBLOCK_ID,*/"=ID" => $ids['0']), false, false);
            while($ob = $db->GetNextElement()){
                $arFields = $ob->getFields();
                $arProperties = $ob->getProperties();

                if($arProperties['WORK']['VALUE']){
                    global $USER;
                    $userId = $USER->GetID();
                    $arWork = WorkHelpers::getWork($arProperties['WORK']['VALUE']);
                    $arFields['DETAIL_PAGE_URL'] = $arWork['DETAIL_PAGE_URL'];
                    $arFields['PREVIEW_PICTURE'] = $arWork['PREVIEW_PICTURE'];
                    $arFields['DETAIL_PICTURE'] = $arWork['DETAIL_PICTURE']['ID'];
                    $arFields['WORK_ID'] = Helpers::encode($arWork['ID'], $userId);
                    $arFields['DIGITAL_COPY_ID'] = Helpers::encode($arWork['PROPERTIES']['DIGITAL_COPY']['VALUE']['ID'], $userId);
                }
                foreach($arFields as $nameField => $field){
                    if(!isset($arProps[$nameField]) && $nameField != "ID"){
                        unset($arFields[$nameField]);
                        continue;
                    }
                    switch ($nameField) {
                        case 'PREVIEW_PICTURE':
                        case 'DETAIL_PICTURE':
                            if($resize){
                                $arFields[$nameField] = \CFile::ResizeImageGet($field, $resize, BX_RESIZE_IMAGE_PROPORTIONAL)["src"];
                            }else{
                                $arFields[$nameField] = \CFile::getPath($field);
                            }
                            break;
                    }
                }

                foreach($arProperties as $nameField => $field){
                    if(empty($field["VALUE"])) continue;
                    if(!isset($arProps[$nameField])){
                        unset($arProperties[$nameField]);
                        continue;
                    }

                    switch ($field["PROPERTY_TYPE"]) {
                        case "E":
                            $dbProp = \CIBlockElement::GetList(array(), array("ACTIVE" => "Y", "IBLOCK_ID" => $field["LINK_IBLOCK_ID"], "ID" => $field["VALUE"]), false, false, array("NAME","DETAIL_PAGE_URL","CODE","PROPERTY_NAME_EN"));
                            $count = $dbProp->SelectedRowsCount();
                            while($resProp = $dbProp->getNext()){
                                if($count > 1){
                                    $arProperties[$nameField]["ELEMENTS"][] = LangHelpers::getTranslation($resProp);
                                }else{
                                    $arProperties[$nameField]["ELEMENTS"] = LangHelpers::getTranslation($resProp);
                                }
                            }
                            break;
                    }
                }
                $arFields["PROPERTIES"] = $arProperties;
                $arResult[$arFields["ID"]] = $arFields;
                $arResult = LangHelpers::getTranslation($arResult);
            }

            return $arResult;
        }
    }

    public function getDeliveryInfo($arIds){
        if(!empty($arIds)){
            $result = array();
            $db = \CIBlockElement::GetList(array(), array("ACTIVE"=>"Y", "IBLOCK_ID" => self::IBLOCK_ID, "=ID" => $arIds), false, false, array("PROPERTY_SIZE","PROPERTY_WEIGHT","ID","PROPERTY_LOCATION_ID"));
            while($res = $db->fetch()){
                //if($res["PROPERTY_SIZE_VALUE"]){
                    $arDimension = \explode("x",\strtolower($res["PROPERTY_SIZE_VALUE"]));
                    $addingHeight = Helpers::getSiteSetting("ADDING_HEIGHT");
                    $addingHeight = ($addingHeight) ? intval($addingHeight) : 0;
                    $result[$res["ID"]] = array(
                        "LENGTH" => ($arDimension[1] ? $arDimension[1] : 200),
                        "WIDTH"  => ($arDimension[0] ? $arDimension[0] : 10),
                        "HEIGHT" => ($arDimension[2] ? $arDimension[2] + $addingHeight : 100),
                    );
                //}
                //if($res["PROPERTY_WEIGHT_VALUE"]){
                    $result[$res["ID"]]["WEIGHT"] = ($res["PROPERTY_WEIGHT_VALUE"] ? $res["PROPERTY_WEIGHT_VALUE"] : 10);
                //}
                //if($res["PROPERTY_LOCATION_ID_VALUE"]){
                    $result[$res["ID"]]["CITY_ID"] = ($res["PROPERTY_LOCATION_ID_VALUE"] ? $res["PROPERTY_LOCATION_ID_VALUE"] : self::CITY_MOSKOW_ID);
               // }
            }
            return $result;
        }
        return false;
    }



    /**
     * Получает seo свойства страниц фильтра
     *
     * @return array
     */
    public function getSeoPropFilterPage($url){
        if(!$url) return false;
        if(stripos($url, 'price-base-from')!==FALSE){
            $url = str_replace(
                array('/works/pictures/filter/'),
                array(''), 
                $url
            );
            $arUrl = explode('/', $url);
            $url = str_replace(
                array($arUrl['0']),
                array(''), 
                $url
            );
        } else {
            $url = str_replace(
                array('/works/pictures/filter'),
                array(''), 
                $url
            );
        }
        $dbSeo = \CIBlockElement::GetList(array(), array("ACTIVE"=>"Y", "IBLOCK_ID" => self::IBLOCK_SEO_FILTER_PAGE_ID, "PROPERTY_URL" => $url), false, false, array("NAME", "DETAIL_TEXT", "PROPERTY_URL", "PROPERTY_TITLE" ,"PROPERTY_DESCRIPTION"));
        if($arSeo = $dbSeo->fetch()){
            return array(
                'H1' => $arSeo['NAME'],
                'URL' => $arSeo['PROPERTY_URL_VALUE'],
                'TITLE' => $arSeo['PROPERTY_TITLE_VALUE'],
                'DESCRIPTION' => $arSeo['PROPERTY_DESCRIPTION_VALUE'],
                'TEXT' => $arSeo['DETAIL_TEXT'],
            );
        }
        else return false;
    }

    /**
     * Метод адаптирует seo данные для 
     *
     * @param  $iblock
     * @param int $id
     * @param array $data
     * @return array
     */
    public static function getSeoField($iblock, $id, $data, $notLower = false)
    {
        $lowerPropsArray = $notLower ? array_merge(self::PROPS_NOT_LOWER, $notLower) : self::PROPS_NOT_LOWER;
        $isEn = LangHelpers::isEnVersion();
        $result = [];
        $ipropTemplates = new ElementTemplates($iblock, $id);
        $entity = new Element($id);
        $templates = $ipropTemplates->findTemplates();
    
        foreach($templates as $code => &$values){
            $template = explode("|",$values["TEMPLATE"]);
            $template = $isEn && isset($template[1]) ? trim($template[1]) : trim($template[0]);
            preg_match_all("#{=this\.(.*?)}#",$template,$matches);
            $replace = [];
            foreach($matches[1] as $key => $match){
                $codeIblock = strtoupper(str_replace("property.","",$match));
                $value=false;
                if(is_string($data[$codeIblock])){
                    $value = $data[$codeIblock];
                }elseif(is_array($data[$codeIblock])){
                    $value = $data[$codeIblock]["NAME"] ? $data[$codeIblock]["NAME"] : implode(",", array_column($data[$codeIblock],"NAME"));
                }

                if($value){
                    $replace[$matches[0][$key]] = (array_search($codeIblock, $lowerPropsArray) === false) ? strtolower($value) : $value;
                }
            }
            if($replace){
                $template = str_replace(array_keys($replace), array_values($replace), $template);
            }
            $result[$code] = Engine::process($entity, $template);
        }
        return $result;
    }
}

?>