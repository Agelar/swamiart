<?
namespace HB\helpers;
use HB\orm\TagTable;

/**
 * Класс для работы с тегированным кешем.
 */
class CacheHelpers{
    
    const CACHE_PATH = "HB.site";
    const CACHE_TIME_DEFAULT = 604800;

    /**
     * Функия предназначена для получения кеша по его id
     *
     * @param string $cacheId
     * @param int $cacheTime
     * @return array|boolean
     */

    public static function getCache($cacheId, $cacheTime = self::CACHE_TIME_DEFAULT){
        $cache = new \CPHPCache();
        if ($cacheTime > 0 && $cache->InitCache($cacheTime, $cacheId, self::CACHE_PATH . "/" . $cacheId)){
            $arCache = $cache->GetVars();
            
            if(!empty($arCache)){
                return $arCache;
            }
        }

        return false;
    }

    /**
     * Функия предназначена для сохранения кеша.
     * $cacheId так же выступает тегом кеша
     *
     * @param array $arResult 
     * @param string $cacheId
     * @param int $cacheTime
     * @return void
     */

    public static function setCache($arResult, $cacheId, $cacheTime = self::CACHE_TIME_DEFAULT){
        global $CACHE_MANAGER;
        $pathCache = self::CACHE_PATH . "/" . $cacheId;
        $CACHE_MANAGER->StartTagCache($pathCache);
        
        if(!empty($arResult)){
            $cache = new \CPHPCache();

            $cache->InitCache($cacheTime, $cacheId, $pathCache);
            $cache->StartDataCache($cacheTime, $cacheId, $pathCache);
            $CACHE_MANAGER->RegisterTag('TAG_' . $cacheId);
            $CACHE_MANAGER->EndTagCache();
            $cache->EndDataCache($arResult);
        }
    }
    
    /**
     * Функция удаляет кеши по определенному тегу
     *
     * @param string $tag
     * @return void
     */
    public static function clearCache($tag){
        global $CACHE_MANAGER;

        $db = TagTable::getList(array(
            'select' => array("TAG"),
            'filter' => array("%TAG" =>'TAG_' . $tag),
        ));
        while($res = $db->fetch()){
            $CACHE_MANAGER->ClearByTag($res["TAG"]);
        }        
    }

    /**
     * Функция удаляет весь кеш собранный модулем
     *
     * @return void
     */
    public static function clearAllCache(){
        global $CACHE_MANAGER;

        $db = TagTable::getList(array(
            'select' => array("TAG"),
            'filter' => array("%TAG" =>'TAG_'),
        ));
        while($res = $db->fetch()){
            $CACHE_MANAGER->ClearByTag($res["TAG"]);
        }        
    }
}
?>