<?
namespace HB\helpers;
use Bitrix\Main\Loader;
use Bitrix\Highloadblock\HighloadBlockTable;
Loader::IncludeModule("highloadblock");

/**
 * Класс направлен на работу с hightload блоками
 */
class HightLoadHelpers{

    public static function getAll(int $idTable, $arFilter = array(), $arSelect = array("*"), $arOrder = array("ID" => "asc")){
        $hlblock = HighloadBlockTable::getById($idTable)->fetch();
        $entity = HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();
        $result = $entity_data_class::getList(array(
            'select' => $arSelect,
            'order'  => $arOrder,
            'filter' => $arFilter
        ))->fetchAll();

        return ($result) ? $result : false;
    }

    public static function getOne(int $idTable,int $id, $arSelect = array("*"), $arOrder = array("ID" => "asc")){
        $hlblock = HighloadBlockTable::getById($idTable)->fetch();
        $entity = HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();

        $result = $entity_data_class::getList(array(
            'select' => $arSelect,
            'order'  => $arOrder,
            'filter' => array("ID" => $id)
        ))->fetch();

        return ($result) ? $result : false;
    }

    public static function Update(int $idTable,int $id, $data){
        if(!empty($data)){
            $hlblock = HighloadBlockTable::getById($idTable)->fetch();
            $entity = HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();
            
            $result = $entity_data_class::update($id,$data);
            return $result->isSuccess();
        } else {
            return false;
        }
        
    }
    public static function Add(int $idTable, $data){
        if(!empty($data)){
            $hlblock = HighloadBlockTable::getById($idTable)->fetch();
            $entity = HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();

            $result = $entity_data_class::add($data);
            return $result->isSuccess();
        } else {
            return false;
        }
        
    }

    public static function Delete(int $idTable,int $id){
        if(intval($id) > 0){
            $hlblock = HighloadBlockTable::getById($idTable)->fetch();
            $entity = HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();

            $result = $entity_data_class::delete($id);
            return $result->isSuccess();
        } else {
            return false;
        }
        
    }
}

?>