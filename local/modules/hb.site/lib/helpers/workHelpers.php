<?
namespace HB\helpers;

use HB\helpers\AuthorHelpers;
use HB\helpers\Helpers;
use HB\helpers\HightLoadHelpers;
use HB\helpers\LangHelpers;
use HB\helpers\RatingHelpers;

\Bitrix\Main\Loader::IncludeModule("iblock");
\Bitrix\Main\Loader::IncludeModule("catalog");
\Bitrix\Main\Loader::IncludeModule("sale");

class WorkHelpers
{
    const IBLOCK_WORK_ID = IBLOCK_WORK_ID;
    const IBLOCK_SECTION_WORK_ID = IBLOCK_SECTION_WORK_ID;
    const HL_LIKES_ID = 3;
    const HL_FAVORITES_ID = 1;
    const HL_NOT_SHOW_PICTURE = 2;
    const IBLOCK_DIGITAL_COPIES_USERS_ID = IBLOCK_DIGITAL_COPIES_USERS_ID;
    const IBLOCK_ORDER_DIGITAL_COPY_ID = IBLOCK_ORDER_DIGITAL_COPY_ID;

    /**
     * работа куплена
     *
     * @return void
     */
    public static function purchasedWork($workId)
    {
        $PROPERTY_CODE = "WORK_PURCHASED";
        \CIBlockElement::SetPropertyValuesEx($workId, false, array($PROPERTY_CODE => 15));
        \CIBlockElement::UpdateSearch($workId, true);
    }

    /**
     * отмена покупки
     *
     * @return void
     */
    public static function cancelPurchasedWork($workId)
    {
        $PROPERTY_CODE = "WORK_PURCHASED";
        \CIBlockElement::SetPropertyValuesEx($workId, false, array($PROPERTY_CODE => false));
        \CIBlockElement::UpdateSearch($workId, true);
    }

    /**
     * пользователь лайкнул работу
     *
     * @return boolean
     */
    public static function addLikesWork($workId)
    {
        $arRatingWork = RatingHelpers::getInfoRatingWork($workId);
        if (!$arRatingWork['COUNT_LIKES']) {
            $arRatingWork['COUNT_LIKES'] = 0;
        }

        $countLikes = $arRatingWork['COUNT_LIKES'] + 1;
        $arRatingWork['COUNT_LIKES']++;
        $PROPERTY_CODE = "COUNT_LIKES";
        \CIBlockElement::SetPropertyValuesEx($workId, false, array($PROPERTY_CODE => $countLikes));
        RatingHelpers::changeWorkRating($arRatingWork);

        global $USER;
        $_SESSION['LIKES'][$workId] = $workId;
        if ($USER->IsAuthorized()) {
            $status = HightLoadHelpers::Add(self::HL_LIKES_ID, array("UF_USER_LIKES" => $USER->GetID(), 'UF_WORK_LIKES' => $workId));
            return $status;
        } else {
            return true;
        }

    }

    /**
     * пользователь убрал лайк
     *
     * @return boolean
     */
    public static function deleteLikesWork($workId)
    {
        $arRatingWork = RatingHelpers::getInfoRatingWork($workId);
        $countLikes = $arRatingWork['COUNT_LIKES'] - 1;
        $arRatingWork['COUNT_LIKES']--;
        $PROPERTY_CODE = "COUNT_LIKES";
        \CIBlockElement::SetPropertyValuesEx($workId, false, array($PROPERTY_CODE => $countLikes));
        RatingHelpers::changeWorkRating($arRatingWork);

        global $USER;
        unset($_SESSION['LIKES'][$workId]);
        if ($USER->IsAuthorized()) {
            $element = HightLoadHelpers::getAll(self::HL_LIKES_ID, array("UF_USER_LIKES" => $USER->GetID(), "UF_WORK_LIKES" => $workId), array("ID"))[0];
            $status = false;

            if ($element) {
                $status = HightLoadHelpers::Delete(self::HL_LIKES_ID, $element["ID"]);
            } else {
                return false;
            }

            return $status;
        } else {
            return true;
        }

    }

    /**
     * Лайкнутые работы пользователя
     *
     * @return array
     */
    public static function getLikesUserWork()
    {
        global $USER;
        if ($USER->IsAuthorized()) {
            $elements = HightLoadHelpers::getAll(self::HL_LIKES_ID, array("UF_USER_LIKES" => $USER->GetID()), array("UF_WORK_LIKES"));
            if (!$elements) {
                return array();
            }

            $result = array();
            foreach ($elements as $element) {
                $result[$element['UF_WORK_LIKES']] = $element['UF_WORK_LIKES'];
            }
            return $result;
        } else {
            return $_SESSION['LIKES'];
        }

    }

    /**
     * добавления работ в избранное
     *
     * @return boolean
     */
    public static function addFavoritesWorks($workIds, $userId)
    {
        foreach ($workIds as $workId) {
            $element = HightLoadHelpers::getAll(self::HL_FAVORITES_ID, array("UF_USER_FAV" => $userId, "UF_WORK_FAV" => $workId), array("ID"))[0];
            if (!$element) {
                $status = HightLoadHelpers::Add(self::HL_FAVORITES_ID, array("UF_USER_FAV" => $userId, 'UF_WORK_FAV' => $workId));
            }

        }
    }

    /**
     * пользователь добавил работу в избранное
     *
     * @return boolean
     */
    public static function addFavoritesWork($workId)
    {
        $arRatingWork = RatingHelpers::getInfoRatingWork($workId);
        if (!$arRatingWork['COUNT_FAVORITES']) {
            $arRatingWork['COUNT_FAVORITES'] = 0;
        }

        $countLikes = $arRatingWork['COUNT_FAVORITES'] + 1;
        $arRatingWork['COUNT_FAVORITES']++;
        $PROPERTY_CODE = "COUNT_FAVORITES";
        \CIBlockElement::SetPropertyValuesEx($workId, false, array($PROPERTY_CODE => $countLikes));
        RatingHelpers::changeWorkRating($arRatingWork);

        global $USER;
        $_SESSION['FAVORITES'][$workId] = $workId;
        if ($USER->IsAuthorized()) {
            $status = HightLoadHelpers::Add(self::HL_FAVORITES_ID, array("UF_USER_FAV" => $USER->GetID(), 'UF_WORK_FAV' => $workId));
            return $status;
        } else {
            return true;
        }

    }

    /**
     * пользователь удалил работу из избранного
     *
     * @return boolean
     */
    public static function deleteFavoritesWork($workId)
    {
        $arRatingWork = RatingHelpers::getInfoRatingWork($workId);
        $countLikes = $arRatingWork['COUNT_FAVORITES'] - 1;
        $arRatingWork['COUNT_FAVORITES']--;
        $PROPERTY_CODE = "COUNT_FAVORITES";
        \CIBlockElement::SetPropertyValuesEx($workId, false, array($PROPERTY_CODE => $countLikes));
        RatingHelpers::changeWorkRating($arRatingWork);

        global $USER;
        unset($_SESSION['FAVORITES'][$workId]);

        if ($USER->IsAuthorized()) {
            $element = HightLoadHelpers::getAll(self::HL_FAVORITES_ID, array("UF_USER_FAV" => $USER->GetID(), "UF_WORK_FAV" => $workId), array("ID"))[0];
            $status = false;

            if ($element) {
                $status = HightLoadHelpers::Delete(self::HL_FAVORITES_ID, $element["ID"]);
            } else {
                return false;
            }

            return $status;
        } else {
            return true;
        }

    }

    /**
     * Больше не показывать работу
     *
     * @return boolean
     */
    public static function addNotShowWork($workId)
    {
        global $USER;
        $_SESSION['NOT_SHOW_PICTURE'][$workId] = $workId;
        if ($USER->IsAuthorized()) {
            $status = HightLoadHelpers::Add(self::HL_NOT_SHOW_PICTURE, array("UF_USER_NOT_SHOW" => $USER->GetID(), 'UF_WORK_NOT_SHOW' => $workId));
            return $status;
        } else {
            return true;
        }

    }

    /**
     * пользователь удалил работу из черного списка
     *
     * @return boolean
     */
    public static function deleteNotShowWork($workId)
    {

        global $USER;
        unset($_SESSION['NOT_SHOW_PICTURE'][$workId]);

        if ($USER->IsAuthorized()) {
            $element = HightLoadHelpers::getAll(self::HL_NOT_SHOW_PICTURE, array("UF_USER_NOT_SHOW" => $USER->GetID(), "UF_WORK_NOT_SHOW" => $workId), array("ID"))[0];
            $status = false;

            if ($element) {
                $status = HightLoadHelpers::Delete(self::HL_NOT_SHOW_PICTURE, $element["ID"]);
            } else {
                return false;
            }

            return $status;
        } else {
            return true;
        }

    }

    /**
     * Избранные работы пользователя
     *
     * @return array
     */
    public static function getFavoritesUserWork()
    {
        global $USER;
        if ($USER->IsAuthorized()) {
            $elements = HightLoadHelpers::getAll(self::HL_FAVORITES_ID, array("UF_USER_FAV" => $USER->GetID()), array("UF_WORK_FAV"));
            if (!$elements) {
                return array();
            }

            $result = array();
            foreach ($elements as $element) {
                $result[$element['UF_WORK_FAV']] = $element['UF_WORK_FAV'];
            }
            return $result;
        } else {
            return $_SESSION['FAVORITES'];
        }

    }

    /**
     * количество работ в избранном
     *
     * @return array
     */
    public static function getCountFavoritesUserWork()
    {
        global $USER;
        if ($USER->IsAuthorized()) {
            $elements = HightLoadHelpers::getAll(self::HL_FAVORITES_ID, array("UF_USER_FAV" => $USER->GetID()), array("UF_WORK_FAV"));
            if (!$elements) {
                return 0;
            }

            $result = array();
            foreach ($elements as $element) {
                $result[$element['UF_WORK_FAV']] = $element['UF_WORK_FAV'];
            }
            return count($result) > 0 ? count($result) : 0;
        } else {
            return count($_SESSION['FAVORITES']);
        }

    }

    /**
     * работы пользователя в корзине
     *
     * @return array
     */
    public static function getCartWork()
    {
        global $USER;
        $arBasketItems = array();

        $dbBasketItems = \CSaleBasket::GetList(
            array(
                "NAME" => "ASC",
                "ID" => "ASC",
            ),
            array(
                "FUSER_ID" => \CSaleBasket::GetBasketUserID(),
                //"LID" => SITE_ID,
                "ORDER_ID" => "NULL",
            ),
            false,
            false,
            array("ID", "PRODUCT_ID")
        );
        while ($arItems = $dbBasketItems->Fetch()) {
            $arBasketItems[$arItems["PRODUCT_ID"]] = $arItems["ID"];
        }
        return $arBasketItems;
    }

    /**
     * количество работы пользователя в корзине
     *
     * @return array
     */
    public static function getCountCartWork()
    {
        global $USER;
        $arBasketItems = array();

        $dbBasketItems = \CSaleBasket::GetList(
            array(
                "NAME" => "ASC",
                "ID" => "ASC",
            ),
            array(
                "FUSER_ID" => \CSaleBasket::GetBasketUserID(),
                //"LID" => SITE_ID,
                "ORDER_ID" => "NULL",
            ),
            false,
            false,
            array("ID", "PRODUCT_ID")
        );
        return $dbBasketItems->SelectedRowsCount();
    }

    /**
     * чёрный список работ пользователя
     *
     * @return array
     */
    public static function getNotShowWorkUser()
    {
        global $USER;
        if ($USER->IsAuthorized()) {
            $elements = HightLoadHelpers::getAll(self::HL_NOT_SHOW_PICTURE, array("UF_USER_NOT_SHOW" => $USER->GetID()), array("UF_WORK_NOT_SHOW"));
            if (!$elements) {
                return array();
            }

            $result = array();
            foreach ($elements as $element) {
                $result[$element['UF_WORK_NOT_SHOW']] = $element['UF_WORK_NOT_SHOW'];
            }
            return $result;
        } else {
            return $_SESSION['NOT_SHOW_PICTURE'];
        }

    }

    /**
     * похожие работы
     *
     * @return int
     */
    public static function getSimilarWork($workId, $percent, $arFilter)
    {
        $arResultId = array();
        $arFilter['IBLOCK_ID'] = self::IBLOCK_WORK_ID;
        $arFilter['!ID'][] = $workId;

        //получаем стиль и жанр картины и формируем фильтр
        $db = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => self::IBLOCK_WORK_ID, 'ID' => $workId), false, false);
        if ($ob = $db->GetNextElement()) {
            $arProperties = $ob->getProperties();
            if ($arProperties['GENRE']['VALUE']) {
                $arFilter['PROPERTY_GENRE'] = $arProperties['GENRE']['VALUE'];
            }

            if ($arProperties['STYLE']['VALUE']) {
                $arFilter['PROPERTY_STYLE'] = $arProperties['STYLE']['VALUE'];
            }

        }

        //получаем стоимость картины и формируем фильтр по цене
        $priceWork = \CCatalogProduct::GetOptimalPrice($workId, 1)['DISCOUNT_PRICE'];
        $arFilter[">catalog_PRICE_1"] = $priceWork * (1 - $percent / 100);
        $arFilter["<catalog_PRICE_1"] = $priceWork * (1 + $percent / 100);

        //получаем похожие работы такого же стиля, жанра и ценового диапазона
        $rsWork = \CIBlockElement::GetList(array('PROPERTY_RATING' => 'DESC'), $arFilter, false, array('nTopCount' => 12), array("ID"));
        while ($arWork = $rsWork->GetNext()) {
            $arResultId[] = $arWork['ID'];
            $arFilter['!ID'][] = $arWork['ID'];
        }
        $countWork = count($arResultId);

        if ($countWork < 6) {

            //если работ меньше 6, то убираем фильтр по цене
            unset($arFilter[">catalog_PRICE_1"]);
            unset($arFilter["<catalog_PRICE_1"]);

            //считаем, сколько еще нужно работ
            $countWorkNoPrice = 6 - $countWork;

            //получаем похожие работы такого же стиля и жанра
            $rsWork = \CIBlockElement::GetList(array('PROPERTY_RATING' => 'DESC'), $arFilter, false, array('nTopCount' => $countWorkNoPrice), array("ID"));
            while ($arWork = $rsWork->GetNext()) {
                $arResultId[] = $arWork['ID'];
                $arFilter['!ID'][] = $arWork['ID'];
                $countWork++;
            }

            if ($countWork < 6) {

                //считаем сколько нужно добрать работ
                $countWorkNoPrice = 6 - $countWork;

                //убираем фильтр по стилю и жанру
                unset($arFilter["PROPERTY_GENRE"]);
                unset($arFilter["PROPERTY_STYLE"]);

                //добираем работы
                $rsWork = \CIBlockElement::GetList(array('PROPERTY_RATING' => 'DESC'), $arFilter, false, array('nTopCount' => $countWorkNoPrice), array("ID"));
                while ($arWork = $rsWork->GetNext()) {
                    $arResultId[] = $arWork['ID'];
                    $countWork++;
                }
            }

        }

        return $arResultId;

    }

    /**
     * добавление работы
     *
     * @return string
     */
    public static function addWork($arWork, $workId)
    {

        //параметры для формирования символьного кода
        $params = array(
            "max_len" => "100", // обрезает символьный код до 100 символов
            "change_case" => "L", // буквы преобразуются к нижнему регистру
            "replace_space" => "_", // меняем пробелы на нижнее подчеркивание
            "replace_other" => "_", // меняем левые символы на нижнее подчеркивание
            "delete_repeat_replace" => "true", // удаляем повторяющиеся нижние подчеркивания
            "use_google" => "false", // отключаем использование google
        );

        //получаем информация об авторе работы
        $arAuthor = AuthorHelpers::getAuthors($arWork["AUTHOR"])[$arWork["AUTHOR"]];

        if ($arWork["MODERATION"] == WORK_PROP_MODERATION_COMPLETED && $arWork["PUBLISHED"] == WORK_PROP_PUBLISHED_YES) { // если свойство "Статус модерации"="Пройдена" и свойство "Опубликована"="Да", то активируем работу
            $arWork['ACTIVE'] = "Y";
        }

        //формируем массив для добавления/изменения работы
        $arLoadProductArray = array(
            "IBLOCK_ID" => self::IBLOCK_WORK_ID,
            "IBLOCK_SECTION" => self::IBLOCK_SECTION_WORK_ID,
            "ACTIVE" => $arWork["ACTIVE"],
            "NAME" => $arWork["NAME"],
            "CODE" => \CUtil::translit($arWork["NAME"], "ru", $params) . '-' . $arAuthor['CODE'], // исмвольный код работы состоит из траслитирированного названия и символьного кода автора
            "DATE_ACTIVE_FROM" => ConvertTimeStamp(time(), "FULL"),
            "DETAIL_TEXT" => $arWork["DETAIL_TEXT"],
            "DETAIL_PICTURE" => $arWork["DETAIL_PICTURE"],
            "PROPERTY_VALUES" => array(
                "DIGITAL_COPY" => $arWork["DIGITAL_COPY"],

                "NAME_EN" => $arWork["NAME_EN"],
                "TRUE_AUTHOR" => $arWork["TRUE_AUTHOR"],
                "WEB_MATERIAL" => $arWork["WEB_MATERIAL"],
                "SIZE" => $arWork["SIZE"],
                "SIZE_SORT" => $arWork["SIZE_SORT"],
                "WEIGHT" => $arWork["WEIGHT"],
                "YEAR" => $arWork["YEAR"],
                "TECHNIQUE" => $arWork["TECHNIQUE"],
                "CANVAS" => $arWork["CANVAS"],
                "LOCATION" => $arWork["LOCATION"],
                "LOCATION_ID" => $arWork["LOCATION_ID"],
                "REGION" => \Cutil::translit($arWork["REGION"], "ru", array("replace_space" => "-", "replace_other" => "-")),
                "AUTHOR" => $arWork["AUTHOR"],
                "EXECUTION" => $arWork["EXECUTION"],
                "COLOR" => $arWork["COLOR"],
                "THEME" => $arWork["THEME"],
                "GENRE" => $arWork["GENRE"],
                "STYLE" => $arWork["STYLE"],
                "MATERIAL" => $arWork["MATERIAL"],
                "THEME_SUB" => $arWork["THEME_SUB"],
                "GENRE_SUB" => $arWork["GENRE_SUB"],
                "STYLE_SUB" => $arWork["STYLE_SUB"],
                "MATERIAL_SUB" => $arWork["MATERIAL_SUB"],
                "YOUTUBE_VIDEO" => explode("\n", $arWork["YOUTUBE_VIDEO"]),

                "SIZE_TYPE" => $arWork["SIZE_TYPE"],
                "ORIENTATION" => $arWork["ORIENTATION"],

                "MODERATION" => array("VALUE" => ($arWork["MODERATION"] ? $arWork["MODERATION"] : WORK_PROP_MODERATION_PROCESS)),
                "PUBLISHED" => array("VALUE" => ($arWork["PUBLISHED"] ? $arWork["PUBLISHED"] : WORK_PROP_PUBLISHED_NO)),
                "WORK_PURCHASED" => array("VALUE" => ($arWork["WORK_PURCHASED"] ? $arWork["WORK_PURCHASED"] : false)),
                "NEW" => array("VALUE" => ($arWork["NEW"] ? $arWork["NEW"] : false)),
            )
        );

        $work = new \CIBlockElement;
        if ($workId) { //если работы изменяется

            //свойство "Дополнительные изображения" изменяем отдельно
            \CIBlockElement::SetPropertyValuesEx($workId, self::IBLOCK_WORK_ID, array("ADD_PICTURE" => $arWork["ADD_PICTURE"]));

            //сохраняем старые значения изменившихся полей и свойств в свойство "Старые изменения"
            $newValuesArray = $arLoadProductArray;
            $newValuesArray['PROPERTY_VALUES']['ADD_PICTURE'] = $arWork["ADD_PICTURE"];
            $oldValuesJSON = self::saveOldValuesToJsonProp($newValuesArray, $workId);
            $arLoadProductArray['PROPERTY_VALUES']['OLD_VALUES_JSON'] = $oldValuesJSON;

            //изменяем работу
            $res = $work->Update($workId, $arLoadProductArray);

            if ($res) {

                //отправляем письмо об изменении работы
                $arEventFields = array(
                    "LINK" => "/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=" . self::IBLOCK_WORK_ID . "&type=catalogs&ID=" . $workId . "&lang=ru&find_section_section=" . self::IBLOCK_SECTION_WORK_ID . "&WF=Y",
                    "NAME" => $arWork["NAME"],
                    "DATE" => date('d.m.Y H:i'),
                );
                $arrSITE = SITE_ID;
                $result['SUCCESS'] = \CEvent::Send("ADD_UPDATE_WORK", $arrSITE, $arEventFields);

                //изменяем цену
                $arFields = array(
                    'PRODUCT_ID' => $workId,
                    'PRICE' => $arWork["PRICE"],
                    'CURRENCY' => 'RUB',
                    'CATALOG_GROUP_ID' => 1,
                );
                $res = \CPrice::GetList(
                    array(),
                    array(
                        "PRODUCT_ID" => $workId,
                        "CATALOG_GROUP_ID" => 1,
                    )
                );

                if ($arr = $res->Fetch()) {
                    \CPrice::Update($arr["ID"], $arFields);
                } else {
                    \CPrice::Add($arFields);
                }

                return $workId;
            } else {
                return $work->LAST_ERROR;
            }

        } else {
            if ($newWorkId = $work->Add($arLoadProductArray)) {
                $arEventFields = array(
                    "LINK" => "/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=" . self::IBLOCK_WORK_ID . "&type=catalogs&ID=" . $newWorkId . "&lang=ru&find_section_section=" . self::IBLOCK_SECTION_WORK_ID . "&WF=Y",
                    "NAME" => $arWork["NAME"],
                    "DATE" => date('d.m.Y H:i'),
                );
                $arrSITE = SITE_ID;
                $result['SUCCESS'] = \CEvent::Send("ADD_UPDATE_WORK", $arrSITE, $arEventFields);
                \CCatalogProduct::Add(array(
                    'ID' => $newWorkId,
                    'QUANTITY' => 1,
                ));
                \CPrice::Add(array(
                    'PRODUCT_ID' => $newWorkId,
                    'PRICE' => $arWork["PRICE"],
                    'CURRENCY' => 'RUB',
                    'CATALOG_GROUP_ID' => 1,
                ));
                global $APPLICATION;
                $APPLICATION->GetException();
                return $newWorkId;
            } else {
                return $work->LAST_ERROR;
            }

        }

    }

    //добавление артикула
    public static function addVendorCode($workId, $vendorCode)
    {
        \CIBlockElement::SetPropertyValuesEx($workId, self::IBLOCK_WORK_ID, array("VENDOR_CODE" => $vendorCode));
    }

    /**
     * получение информации о работе
     *
     * @return array
     */
    public static function getWork($workId)
    {
        $rsWork = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => self::IBLOCK_WORK_ID, "=ID" => $workId), false, false);
        $arResultWork = array();
        if ($obWork = $rsWork->GetNextElement()) {
            $arWork = $obWork->GetFields();
            $arWork['PROPERTIES'] = $obWork->GetProperties();
            $arWork = LangHelpers::getTranslation($arWork);
            if ($arWork['DETAIL_PICTURE']) {
                $arWork['DETAIL_PICTURE'] = \CFile::GetFileArray($arWork['DETAIL_PICTURE']);
            }

            if ($arWork['PROPERTIES']['DIGITAL_COPY']['VALUE']) {
                $arWork['PROPERTIES']['DIGITAL_COPY']['VALUE'] = \CFile::GetFileArray($arWork['PROPERTIES']['DIGITAL_COPY']['VALUE']);
            }

            if ($arWork['PROPERTIES']['ADD_PICTURE']['VALUE']['0']) {
                foreach ($arWork['PROPERTIES']['ADD_PICTURE']['VALUE'] as $key => $img) {
                    $arWork['PROPERTIES']['ADD_PICTURE']['VALUE'][$key] = \CFile::GetFileArray($img);
                }
            }
            $arWork['PRICE'] = \CCatalogProduct::GetOptimalPrice($workId, 1)['RESULT_PRICE']['BASE_PRICE'];
            $arResultWork = $arWork;
        }
        return $arResultWork;
    }

    /**
     * получение списка свойств работы
     *
     * @return string
     */
    public static function getWorkProperty()
    {
        $rsProperties = \CIBlockProperty::GetList(array("sort" => "asc", "name" => "asc"), array("ACTIVE" => "Y", "IBLOCK_ID" => self::IBLOCK_WORK_ID));
        $arPropResult = array();
        while ($arProperties = $rsProperties->GetNext()) {
            $arPropResult[$arProperties['CODE']] = $arProperties['NAME'];
        }
        return $arPropResult;
    }

    /**
     * подсчет количества работ на главной
     *
     * @return int
     */
    public static function getCountWorkFront($arFilter)
    {
        $arFilter["IBLOCK_ID"] = self::IBLOCK_WORK_ID;
        $arFilter["ACTIVE"] = 'Y';
        $arFilter["CATALOG_AVAILABLE"] = "Y";
        $rsWork = \CIBlockElement::GetList(array(), $arFilter, false, false);
        return $rsWork->SelectedRowsCount();
    }

    /**
     * цифровые копии пользователя
     *
     * @return int
     */
    public static function getPayDigitalCopy()
    {
        global $USER;
        if ($USER->IsAuthorized()) {
            $arFilter["IBLOCK_ID"] = self::IBLOCK_DIGITAL_COPIES_USERS_ID;
            $arFilter["ACTIVE"] = 'Y';
            $arResult = array();
            $arFilter["PROPERTY_USER"] = $USER->GetID();
            $rsWork = \CIBlockElement::GetList(array(), $arFilter, false, false, array("ID", "PROPERTY_WORK"));
            while ($arWork = $rsWork->GetNext()) {
                $arResult[$arWork["PROPERTY_WORK_VALUE"]] = $arWork["ID"];
            }
            return $arResult;
        } else {
            return false;
        }
    }

    /**
     * заказы на цифровые копии пользователя
     *
     * @return int
     */
    public static function getOrderDigitalCopy()
    {
        global $USER;
        if ($USER->IsAuthorized()) {
            $arFilter["IBLOCK_ID"] = self::IBLOCK_ORDER_DIGITAL_COPY_ID;
            $arFilter["ACTIVE"] = 'Y';
            $arResult = array();
            $arFilter["PROPERTY_USER"] = $USER->GetID();
            $rsWork = \CIBlockElement::GetList(array(), $arFilter, false, false, array("ID", "PROPERTY_WORK"));
            while ($arWork = $rsWork->GetNext()) {
                $rsOrder = \CSaleOrder::GetList(array('ID' => 'DESC'), array('BASKET_PRODUCT_ID' => $arWork["ID"]));
                if ($arOrder = $rsOrder->fetch()) {
                    $arResult[$arWork["PROPERTY_WORK_VALUE"]] = $arOrder['ID'];
                }

            }
            return $arResult;
        } else {
            return false;
        }
    }

    /**
     * пользователь посмотрел работу
     *
     * @return boolean
     */
    public static function userViewWork($workId)
    {
        $arRatingWork = RatingHelpers::getInfoRatingWork($workId);
        $countShow = $arRatingWork['COUNT_SHOW'] + 1;
        $PROPERTY_CODE = "COUNT_SHOW";
        \CIBlockElement::SetPropertyValuesEx($workId, false, array($PROPERTY_CODE => $countShow));
    }

    //добавление цифровой копии пользователя
    public static function addDigitalCopyUser($workId, $userId)
    {
        if (!$userId) {
            global $USER;
            $userId = $USER->GetID();
        }
        $arProp = array();
        $arProp['USER'] = $userId;
        $arProp['WORK'] = $workId;

        $arLoadProductArray = array(
            "IBLOCK_ID" => IBLOCK_DIGITAL_COPIES_USERS_ID,
            "PROPERTY_VALUES" => $arProp,
            "NAME" => 'Цифровая копия картины ' . $workId . ' пользователя ' . $userId,
            "ACTIVE" => "Y",
        );
        $elDigitalCopy = new \CIBlockElement;

        $rsDigitalCopy = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_DIGITAL_COPIES_USERS_ID, 'PROPERTY_USER' => $userId, 'PROPERTY_WORK' => $workId), false, false, array('ID'));
        if ($arDigitalCopy = $rsDigitalCopy->GetNext()) {
            $elDigitalCopy->Update($arDigitalCopy['ID'], $arLoadProductArray);
        } else {
            $elementId = $elDigitalCopy->Add($arLoadProductArray);
        }

    }

    //деактивация цифровой копии пользователя
    public static function deactiveDigitalCopyUser($workId, $userId)
    {
        if (!$userId) {
            global $USER;
            $userId = $USER->GetID();
        }
        $arLoadProductArray = array(
            "ACTIVE" => "N",
        );
        $elDigitalCopy = new \CIBlockElement;
        $rsDigitalCopy = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => IBLOCK_DIGITAL_COPIES_USERS_ID, 'PROPERTY_USER' => $userId, 'PROPERTY_WORK' => $workId), false, false, array('ID'));
        if ($arDigitalCopy = $rsDigitalCopy->GetNext()) {
            $elDigitalCopy->Update($arDigitalCopy['ID'], $arLoadProductArray);
        }
    }

    //получение списка авторов по фильтру работ
    public static function getAuthorIdByFilterWork($arFilterWork)
    {
        $arFilterWork['IBLOCK_ID'] = self::IBLOCK_WORK_ID;
        $rsElement = \CIBlockElement::GetList(array(), $arFilterWork, false, false, array('ID', 'PROPERTY_AUTHOR'));
        $arAuthorId = array();
        while ($arElement = $rsElement->GetNext()) {
            $arAuthorId[$arElement['PROPERTY_AUTHOR_VALUE']] = $arElement['PROPERTY_AUTHOR_VALUE'];
        }
        return $arAuthorId;
    }

    //получение предыдущих значений полей и свойств работы в свойство "Старые значения" в формате JSON если они изменились
    private static function saveOldValuesToJsonProp($newValues = array(), $workId = 0)
    {
        $oldValuesJSON = "";
        if (!empty($newValues) && $workId > 0) {
            $arWork = array();
            $oldValues = array();
            $rsWork = \CIBlockElement::GetList(array(), array("IBLOCK_ID" => self::IBLOCK_WORK_ID, "=ID" => $workId), false, false);
            if ($obWork = $rsWork->GetNextElement()) {
                $arWork = $obWork->GetFields();
                $arWork['PROPERTIES'] = $obWork->GetProperties();
            }
            foreach ($newValues as $paramName => $paramValue) {
                if (in_array($paramName, array('PROPERTY_VALUES', 'IBLOCK_ID', 'IBLOCK_SECTION'))) {
                    continue;
                }
                $oldValues[$paramName] = $arWork[$paramName];
            }
            foreach ($newValues['PROPERTY_VALUES'] as $paramName => $paramValue) {
                $oldValues['PROPERTIES'][$paramName] = $arWork['PROPERTIES'][$paramName]['VALUE'];
            }
            $oldValuesJSON = json_encode($oldValues);
        }
        return $oldValuesJSON;
    }

}
