<?
namespace HB\helpers;
use HB\helpers\HightLoadHelpers;
use HB\helpers\LangHelpers;
use Bitrix\Main\UserTable;

class LocationHelpers{
    const HL_CITY_ID = 5;

    public static $cidr_optim_file_path = 'local/modules/hb.site/lib/helpers/geo/cidr_optim.txt';
 	public static $cities_file_path = 'local/modules/hb.site/lib/helpers/geo/cities.txt';
 	public static $csvRowLength = 1000;
	public static $csvDelimiter = "	";

    /**
     * Функция определения IP пользователя
     */
    public static function getRealIpAddr($ip=false) {
        if($ip){
			return $ip;
		}else{
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				$ip=$_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
				$ip=$_SERVER['REMOTE_ADDR'];
			}
        
			return $ip;
		}
		
    }

    /**
     * Определение города по IP
     * @param $ip = '', IP пользователя (Если не указан получается из функции getRealIpAddr() )
     * @param $reset_cache = TRUE, Запись города в COOKIE['geo'];
     * @param $type = 'city', 'region'
     */
    public static function getCityByIp($ip = '', $reset_cache = true,$type_reg = true) {
        if(!$_COOKIE["geo_name"] or $reset_cache) {
            if (!$ip) {
                $ip = self::getRealIpAddr();
            }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'http://ipgeobase.ru:7020/geo?ip='.$ip);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 3);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
            curl_setopt($ch, CURLOPT_USERAGENT, 'PHP Bot');
            $data = curl_exec($ch);
            $city = ( !curl_errno($ch) && $xml = simplexml_load_string($data) &&  $xml->ip->city) ? $xml->ip->city->__toString() : false;
            $region = ( !curl_errno($ch) && $xml = simplexml_load_string($data) && $xml->ip->region) ? $xml->ip->region->__toString() : false;
            $lat = ( !curl_errno($ch) && $xml = simplexml_load_string($data) && $xml->ip->lat) ? $xml->ip->lat ->__toString(): "";
            $lng = ( !curl_errno($ch) && $xml = simplexml_load_string($data) && $xml->ip->lng) ? $xml->ip->lng->__toString() : "";
            curl_close($ch);  

            if(!$city){
                $arInfo = self::getInfoAboutIP($ip);
                $city = $arInfo['cityName'];
                $region = $arInfo['oblastName'];
            }
            if(!$city) $city = 'Москва';
            if(!$region) $region = 'Москва';
        } else {   
            $city = $_COOKIE['geo_name'];
            $region = $_COOKIE['geo_reg'];
        }
		setcookie('geo_lat', $lat, time()+3600*24*7,'/');
		setcookie('geo_lng', $lng, time()+3600*24*7,'/');
		setcookie('geo_name', $city, time()+3600*24*7,'/');
        setcookie('geo_reg', $region, time()+3600*24*7,'/');
        if($type_reg){
            return array("city"=>$city,"region"=>$region);
        }else{
            return $city;
        }
    }


    /**
     * добавление города
     *
     * @return boolean
     */
    public static function addCity($arCity){
        $status = HightLoadHelpers::Add(self::HL_CITY_ID,array("UF_CITY_ID" => $arCity['ID'], 'UF_CITY_NAME' => $arCity['NAME'], 'UF_CITY_FULLNAME' => $arCity['FULLNAME'], 'UF_REGION' => $arCity['REGION'], 'UF_CITY_NAME_EN' => $arCity['NAME_EN'], 'UF_CITY_FULLNAME_EN' => $arCity['FULLNAME_EN'], 'UF_REGION_EN' => $arCity['REGION_EN'], 'UF_CITY_MAIN' => $arCity['CITY_MAIN'], 'UF_SORT' => $arCity['SORT']));
        return $status;
    }

    /**
     * получаем список основных городов
     *
     * @return array
     */
    public static function getMainCityList(){
            $elements = HightLoadHelpers::getAll(self::HL_CITY_ID, array("UF_CITY_MAIN" => true), array("*"), array("UF_SORT" => "ASC", "UF_CITY_NAME" => "ASC"));
            if(!$elements) return array();
            $result = array();
            $elements = LangHelpers::getTranslationCity($elements);
            foreach($elements as $element){
                $result[$element['UF_CITY_ID']] = $element;
            }
            return $result;
    }

    /**
     * получаем список всех городов по названию
     *
     * @return array
     */
    public static function getCityList($cityName){
        $elements = HightLoadHelpers::getAll(self::HL_CITY_ID, array(array("LOGIC" => "OR", "UF_CITY_NAME" => "%". $cityName ."%", "UF_CITY_NAME_EN" => "%". $cityName ."%")), array("*"), array("UF_SORT" => "ASC", "UF_CITY_NAME" => "ASC"));
        if(!$elements) return array();
        $elements = LangHelpers::getTranslationCity($elements);
        $result = array();
        foreach($elements as $element){
            $result[$element['UF_CITY_ID']] = $element;
        }
        return $result;
    }

    /**
     * получаем город по ID
     *
     * @return array
     */
    public static function getCityById($cityId){
        $elements = HightLoadHelpers::getAll(self::HL_CITY_ID, array("UF_CITY_ID" => $cityId), array("*"), array("UF_SORT" => "ASC", "UF_CITY_NAME" => "ASC"));
        if(!$elements) return array();
        $elements = LangHelpers::getTranslationCity($elements);
        $result = array();
        foreach($elements as $element){
            $result[$element['UF_CITY_ID']] = $element;
        }
        return $result;
    }

    /**
     * получаем город по названию и региону
     *
     * @return array
     */
    public static function getCityByName($cityName, $regionName){
        $elements = HightLoadHelpers::getAll(self::HL_CITY_ID, array("UF_CITY_NAME" => "%" . $cityName ."%", "UF_REGION" => "%". $regionName ."%"), array("*"), array("UF_SORT" => "ASC", "UF_CITY_NAME" => "ASC"));
        if(!$elements) return array();
        $elements = LangHelpers::getTranslationCity($elements);
        $result = array();
        foreach($elements as $element){
            $result = $element;
            return $result;
        }
        return $result;
    }

    //пользователь выбрал город
    public static function updateUserCity($cityName, $regionName, $cityId, $cityFullName, $cityNameEn){
        global $USER;
        if($USER->IsAuthorized()){
            $user = new \CUser;
            $data['PERSONAL_CITY'] = $cityName;
            $data['UF_PERSONAL_CITY_EN'] = $cityNameEn;
            $data['UF_REGION'] = $regionName;
            $data['UF_PERSONAL_CITY_ID'] = $cityId;
            if($user->Update($USER->GetID(), $data)){
                return true;
            }else{
                return false;
            }
        }
        else{
            $_SESSION['PERSONAL_CITY_ID'] = $cityId;
            $_SESSION['PERSONAL_CITY'] = $cityName;
            $_SESSION['PERSONAL_CITY_EN'] = $cityNameEn;
            $_SESSION['REGION'] = $regionName;
            return true;
        }
    }

    //получаем город пользователя
    public static function getUserCity($lang = 'ru', $userId = false){
        $result = "";
        global $USER;
        if($USER->IsAuthorized()){
            if(!$userId){
                $userId = $USER->GetId();
            }

            $user = UserTable::getList(array(
                "filter" => array("ID" => $userId),
                "select" => array("PERSONAL_CITY", "UF_PERSONAL_CITY_EN"),
            ))->fetch();
            if($lang == 'ru') return $user["PERSONAL_CITY"];
            else return $user["UF_PERSONAL_CITY_EN"];
        }
        else{
            if($lang == 'ru') return $_SESSION['PERSONAL_CITY'];
            else return $_SESSION['UF_PERSONAL_CITY_EN'];
        }
    }

    /**
     * добавление городов из текстового файла
     *
     * @return boolean
     */
    public static function addCityFromTxtFile($filePath){
        $arCities = array();
        $lines = file($filePath);
        foreach ($lines as $line_num => $line) {
            //if($line_num<30000) continue;
            $arLine = explode('	', $line);
            if($arLine['0'] && is_numeric($arLine['0']) && !self::getCityById($arLine['0'])){
                $arCity = array(
                    'ID' => $arLine['0'],
                    'NAME' => $arLine['2'],
                    'FULLNAME' => $arLine['1'],
                    'REGION' => $arLine['3'],
                    'NAME_EN' => $arLine['4'],
                    'FULLNAME_EN' => $arLine['5'],
                    'REGION_EN' => $arLine['6'],
                    'CITY_MAIN' => false,
                    'SORT' => 500,
                );
                self::addCity($arCity);
                if($line_num>100) break;
            }
        }
        
    }


    public static function getInfoAboutIP($ip = false){
        
        if(!$ip)
            $ip = static::getRealIpAddr();
        
		$codedIp = static::getIpSum($ip);

		$ipRowArr = static::findIpRow($codedIp);

		$cityArr = Array();
		if ($ipRowArr[4]){
			$cityArr = static::getCityInfo($ipRowArr[4]);
		}

		return Array(
			"id"         => $cityArr[0],
			"cityName"   => $cityArr[1],
			"oblastName" => $cityArr[2],
			"okrugName"  => $cityArr[3],
			"countyCode" => $ipRowArr[3],
			"lat"        => $cityArr[4],
			"lon"        => $cityArr[5],
		);
	}

	public static function getIpSum($ip = false){
		$multiplier = 256;

		$ipArr = explode('.', $ip);

		return $ipArr[0] * $multiplier * $multiplier * $multiplier + $ipArr[1] * $multiplier * $multiplier + $ipArr[2] * $multiplier + $ipArr[3];
	}

	public static function findIpRow($codedIp = false){
		$answer = false;
		if (($handle = fopen($_SERVER["DOCUMENT_ROOT"].'/'.static::$cidr_optim_file_path, "r")) !== false){
			while (($data = fgetcsv($handle, static::$csvRowLength, static::$csvDelimiter)) !== false){
				if ($data[0] > $codedIp || $codedIp > $data[1]){
					continue;
				}

				$answer = $data;
				break;
			}
			fclose($handle);
		}

		return $answer;
	}

	public static function getCityInfo($cityId = false){
		$answer = false;
		if (($handle = fopen($_SERVER["DOCUMENT_ROOT"].'/'.static::$cities_file_path, "r")) !== false){
			while (($data = fgetcsv($handle, static::$csvRowLength, static::$csvDelimiter)) !== false){
				if ($data[0] != $cityId){
					continue;
				}

				$answer = $data;
				break;
			}
			fclose($handle);
		}

		return $answer;
	}

}