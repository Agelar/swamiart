<?
namespace HB\helpers;

/**
 * Класс направлен на помощь с поиском
 */
class SearchHelpers{
    public static function searchMathes(String $str,String $find,String  $tag = "span",String  $class = "search__result-text"){
        $result = \preg_replace("#(".$find.")#iu","<" . $tag . " class='" . $class . "'>$1</" . $tag . ">",$str);
        return $result;
    }
}