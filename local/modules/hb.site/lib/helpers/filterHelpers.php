<?
namespace HB\helpers;
use HB\helpers\WorkHelpers;
use HB\helpers\Helpers;
use Bitrix\Iblock\Template\Engine;
use Bitrix\Iblock\Template\Entity\Element;
use Bitrix\Iblock\InheritedProperty\ElementTemplates;

class FilterHelpers {

    /**
     * Получает список ID элементов поджанров указанного жанра
     *
     * @return array
     */
    public function getElementIDsBySectionID($sectionID, $iblockID) {
        $arResult = array();
        $db = \CIBlockElement::GetList(array(), array("IBLOCK_ID"=> $iblockID, "IBLOCK_SECTION_ID" => $sectionID, "ACTIVE" => "Y"), false, false, array("ID", "NAME", "IBLOCK_ID", "IBLOCK_SECTION_ID"));
        while($ob = $db->GetNextElement()) {
            $arFields = $ob->getFields();
            $arResult[] = $arFields['ID'];
        }
        return $arResult;
    }
}

?>