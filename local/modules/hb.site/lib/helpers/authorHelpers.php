<?
namespace HB\helpers;

use HB\helpers\LangHelpers;
use Bitrix\Main\UserTable;

/**
 * Данный класс предназначен для работы с авторами
 */
class AuthorHelpers 
{
    const IBLOCK_AUTHORS_ID = IBLOCK_AUTHORS_ID;
    const AUTHOR_GROUP_ID = 5;
    const EXPERT_GROUP_ID = 10;
    /**
     * получение авторов работ
     *
     * @return array
     */
    public static function getAuthors($arAuthorsId, $isEng = false)
    {
        $rsAuthor = \CIBlockElement::GetList(array(), array("IBLOCK_ID"=> self::IBLOCK_AUTHORS_ID, "ID" => $arAuthorsId, 'ACTIVE' => 'Y'), false, false);
        $arResultAuthors = array();
        while ($obAuthor = $rsAuthor->GetNextElement()) {
            $arAuthor = $obAuthor->GetFields();
            $arAuthor['PROPERTIES'] = $obAuthor->GetProperties();
            $arAuthor = LangHelpers::getTranslation($arAuthor, $isEng);
            $arResultAuthors[$arAuthor['ID']] = $arAuthor;
        }
        return $arResultAuthors;
    }


    /**
     * получение ID элемента автора
     *
     * @return array
     */
    public static function getAuthorId()
    {
        global $USER;
        $rsAuthor = \CIBlockElement::GetList(array(), array("IBLOCK_ID"=> self::IBLOCK_AUTHORS_ID, 'ACTIVE' => 'Y', 'PROPERTY_USER' => $USER->GetID()), false, false, array('ID'));
        if ($arAuthor = $rsAuthor->GetNext()) {
            return $arAuthor['ID'];
        }
        return false;
    }


    /**
     * проверяет, является ли пользователь автором (художником)
     *
     * @return boolean
     */
    public static function isAuthor()
    {
		return \CSite::InGroup([self::AUTHOR_GROUP_ID]);
    }
    

    /**
     * проверяет, является ли пользователь экспертом (художником)
     *
     * @return boolean
     */
    public static function isExpert()
    {
		return \CSite::InGroup([self::EXPERT_GROUP_ID]);
    }
    

    /**
     * Отправка сообщения автору при покупке его работы
     */
    public static function sendMessageAuthor($authorId, $workName, $workLink)
    {
        $arAuthor = self::getAuthors($authorId)[$authorId];

        $user = UserTable::getList(array(
            "filter" => array("ID" => $arAuthor['PROPERTIES']['USER']['VALUE']),
            "select" => array("NAME","EMAIL"),
        ))->fetch();

        $arEventFields = [
            'AUTHOR' => $arAuthor['NAME'],
            'AUTHOR_MAIL' => $user['EMAIL'],
            'WORK' => $workName,
            'WORK_LINK' => 'http://' . $_SERVER['HTTP_HOST'] . $workLink
        ];
        $arSite =  ['s1', 'en'];
        return \CEvent::Send("AUTHOR_WORK_PAY", $arSite, $arEventFields);
    }
}