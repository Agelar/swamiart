<?
\Bitrix\Main\Loader::registerAutoloadClasses(
	"hb.site",
	array(
		"HB\helpers\Helpers" => "lib/helpers/helpers.php",
		"HB\helpers\CacheHelpers" => "lib/helpers/cacheHelpers.php",
		"HB\helpers\LangHelpers" => "lib/helpers/langHelpers.php",
		"HB\helpers\CatalogHelpers" => "lib/helpers/catalogHelpers.php",
		"HB\helpers\FilterHelpers" => "lib/helpers/filterHelpers.php",
		"HB\helpers\HightLoadHelpers" => "lib/helpers/hightLoadHelpers.php",
		"HB\helpers\BlogHelpers" => "lib/helpers/blogHelpers.php",
		"HB\userEntity\Notification" => "lib/userEntity/notification.php",
		"HB\userEntity\Confirm" => "lib/userEntity/confirm.php",
		"HB\userEntity\IblockAuthor" => "lib/userEntity/iblockAuthor.php",
		"HB\userEntity\Sms" => "lib/userEntity/sms.php",
		"HB\orm\TagTable" => "lib/orm/TagTable.php",
		"HB\\eventhandler\MainHandler" => "lib/eventhandler/mainHandler.php",
		"HB\\eventhandler\AgentHandler" => "lib/eventhandler/agentHandler.php",
		"HB\helpers\RatingHelpers" => "lib/helpers/ratingHelpers.php",
		"HB\helpers\WorkHelpers" => "lib/helpers/workHelpers.php",
		"HB\helpers\AuthorHelpers" => "lib/helpers/authorHelpers.php",
		"HB\helpers\SearchHelpers" => "lib/helpers/searchHelpers.php",
		"HB\\eventhandler\IblockHandler" => "lib/eventhandler/iblockHandler.php",
		"HB\\eventhandler\SaleHandler" => "lib/eventhandler/saleHandler.php",
		"HB\\eventhandler\SearchHandler" => "lib/eventhandler/searchHandler.php",
		"HB\\eventhandler\SocialHandler" => "lib/eventhandler/socialHandler.php",
		"HB\helpers\LocationHelpers" => "lib/helpers/locationHelpers.php",
	)
);
?>