<?

IncludeModuleLangFile(__FILE__);
use \Bitrix\Main\ModuleManager;

Class hb_site extends CModule
{

    var $MODULE_ID = "hb.site";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $errors;

    function __construct()
    {
        $this->MODULE_VERSION = "1.0.0";
        $this->MODULE_VERSION_DATE = "2018-06-11";
        $this->MODULE_NAME = "HB модуль для сайта";
        $this->MODULE_DESCRIPTION = "Модуль для сайта";
    }

    function DoInstall()
    {
        global $APPLICATION;
        
        \Bitrix\Main\ModuleManager::RegisterModule($this->MODULE_ID);

        //Регистрация событий
        //Пример: RegisterModuleDependences("iblock", "OnAfterIBlockElementAdd","hb.site", "HB\\eventhandler\\EntityCatalogEvent","changeIblockElement");
        RegisterModuleDependences("main", "OnBeforeUserAdd","hb.site", "HB\\eventhandler\\MainHandler","OnBeforeUserAddHandler");
        RegisterModuleDependences("main", "OnBeforeUserRegister","hb.site", "HB\\eventhandler\\MainHandler","OnBeforeUserUpdateHandler");
        RegisterModuleDependences("main", "OnAfterUserAdd","hb.site", "HB\\eventhandler\\MainHandler","OnAfterUserAddHandler");
        RegisterModuleDependences("main", "OnUserDelete","hb.site", "HB\\eventhandler\\MainHandler","OnUserDeleteHandler");
        RegisterModuleDependences("iblock", "OnBeforeIBlockElementUpdate","hb.site", "HB\\eventhandler\\IBlockHandler","OnBeforeIBlockElementUpdateHandler");
        RegisterModuleDependences("sale", "OnSalePayOrder","hb.site", "HB\\eventhandler\\SaleHandler","OnSalePayOrderHandler");
        RegisterModuleDependences("sale", "OnOrderNewSendEmail","hb.site", "HB\\eventhandler\\SaleHandler","OnSendMailOrderNew");
        RegisterModuleDependences("sale", "OnOrderStatusSendEmail","hb.site", "HB\\eventhandler\\SaleHandler","OnSendMailOrderNew");
        RegisterModuleDependences("sale", "OnOrderRecurringCancelSendEmail","hb.site", "HB\\eventhandler\\SaleHandler","OnSendMailOrderNew");
        RegisterModuleDependences("search", "BeforeIndex","hb.site", "HB\\eventhandler\\SearchHandler","BeforeIndexHandler");
        RegisterModuleDependences("socialservices", "OnFindSocialservicesUser","hb.site", "HB\\eventhandler\\SocialHandler","OnFindSocialservicesUserHandler");
        RegisterModuleDependences("sale", "OnSaleComponentOrderUserResult","hb.site", "HB\\eventhandler\\SaleHandler","OnSaleComponentOrderUserResultHandler");
        RegisterModuleDependences("sale", "OnSaleComponentOrderCreated","hb.site", "HB\\eventhandler\\SaleHandler","OnSaleComponentOrderCreatedHandler");
        RegisterModuleDependences("main", "OnAfterUserLogin","hb.site", "HB\\eventhandler\\MainHandler","OnAfterUserLoginHandler");
        RegisterModuleDependences("main", "OnEndBufferContent","hb.site", "HB\\eventhandler\\MainHandler","OnEndBufferContentHandler");

		$APPLICATION->IncludeAdminFile("Установка модуля ", $_SERVER["DOCUMENT_ROOT"] . "/local/modules/" . $this->MODULE_ID . "/install/step.php");
        return true;
    }

    function DoUninstall()
    {
		global $APPLICATION;
		
        \Bitrix\Main\ModuleManager::UnRegisterModule($this->MODULE_ID);
        
        //Отвязка от событий
        //Пример: UnRegisterModuleDependences("iblock", "OnAfterIBlockElementAdd","hb.site", "HB\\eventhandler\\EntityCatalogEvent","changeIblockElement");
        UnRegisterModuleDependences("main", "OnBeforeUserAdd","hb.site", "HB\\eventhandler\\MainHandler","OnBeforeUserAddHandler");
        UnRegisterModuleDependences("main", "OnBeforeUserRegister","hb.site", "HB\\eventhandler\\MainHandler","OnBeforeUserUpdateHandler");
        UnRegisterModuleDependences("main", "OnAfterUserAdd","hb.site", "HB\\eventhandler\\MainHandler","OnAfterUserAddHandler");
        UnRegisterModuleDependences("main", "OnUserDelete","hb.site", "HB\\eventhandler\\MainHandler","OnUserDeleteHandler");
        UnRegisterModuleDependences("iblock", "OnBeforeIBlockElementUpdate","hb.site", "HB\\eventhandler\\IBlockHandler","OnBeforeIBlockElementUpdateHandler");
        UnRegisterModuleDependences("sale", "OnSalePayOrder","hb.site", "HB\\eventhandler\\SaleHandler","OnSalePayOrderHandler");
        UnRegisterModuleDependences("sale", "OnOrderNewSendEmail","hb.site", "HB\\eventhandler\\SaleHandler","OnSendMailOrderNew");
        UnRegisterModuleDependences("sale", "OnOrderStatusSendEmail","hb.site", "HB\\eventhandler\\SaleHandler","OnSendMailOrderNew");
        UnRegisterModuleDependences("sale", "OnOrderRecurringCancelSendEmail","hb.site", "HB\\eventhandler\\SaleHandler","OnSendMailOrderNew");
        UnRegisterModuleDependences("search", "BeforeIndex","hb.site", "HB\\eventhandler\\SearchHandler","BeforeIndexHandler");
        UnRegisterModuleDependences("socialservices", "OnFindSocialservicesUser","hb.site", "HB\\eventhandler\\SocialHandler","OnFindSocialservicesUserHandler");
        UnRegisterModuleDependences("sale", "OnSaleComponentOrderUserResult","hb.site", "HB\\eventhandler\\SaleHandler","OnSaleComponentOrderUserResultHandler");
        UnRegisterModuleDependences("sale", "OnSaleComponentOrderCreated","hb.site", "HB\\eventhandler\\SaleHandler","OnSaleComponentOrderCreatedHandler");
        UnRegisterModuleDependences("main", "OnAfterUserLogin","hb.site", "HB\\eventhandler\\MainHandler","OnAfterUserLoginHandler");
        UnRegisterModuleDependences("main", "OnEndBufferContent","hb.site", "HB\\eventhandler\\MainHandler","OnEndBufferContentHandler");

		$APPLICATION->IncludeAdminFile("Деинсталляция модуля " . $this->MODULE_ID . "", $_SERVER["DOCUMENT_ROOT"] . "/local/modules/" . $this->MODULE_ID . "/install/unstep.php");
        return true;
    }
}