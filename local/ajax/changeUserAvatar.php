<?php require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Loader;
use HB\userEntity\IblockAuthor;
Loader::IncludeModule("hb.site");

global $USER;
$status = false;
$text = "";
if($_FILES['PERSONAL_PHOTO'] && $_FILES['PERSONAL_PHOTO']['error'] == 0 && $USER->IsAuthorized()){
    $fid = CFile::SaveFile($_FILES['PERSONAL_PHOTO'], "iblock");
    $arFile = CFile::MakeFileArray($fid);
    if(preg_match("#image#",$arFile["type"])){
        $user = new CUser;
        $status = $user->Update($USER->GetID(),array(
            "PERSONAL_PHOTO" => $arFile,
        ));
        IblockAuthor::updatePersonalPicture($USER->GetID(),$arFile);
    }else{
        $text = "Not picture";
    }
    
}
echo json_encode(array(
    "STATUS" => $status,
    "TEXT" => $text,
));
?>