<?php
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
global $USER;
/*Отправка сообщения*/
if($USER->IsAuthorized()){
	if($_FILES['example'] && $_FILES['example']['error'] == 0 && $_REQUEST['ajax'] == 'Y'){
				$fid = CFile::SaveFile($_FILES['example'], "iblock");
				$src = CFile::GetPath($fid);
				$result = array(
					'src' => $src,
					'id' => $fid,
				);

				print json_encode($result);

	}
	elseif($_REQUEST['ajax'] == 'Y' && is_numeric($_REQUEST['del_file'])){
		CFile::Delete($_REQUEST['del_file']);
	}
	else{
		print 0;
	}
}