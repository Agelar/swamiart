<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php";
CModule::IncludeModule("hb.site");
use HB\helpers\Helpers;

if ($_REQUEST['last_order_ID']) {
    print Helpers::recreacteLastOrder($_REQUEST['last_order_ID']);
}
