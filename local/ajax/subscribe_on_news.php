<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use HB\userEntity\Notification;
use Bitrix\Main\Loader;
global $USER;
Loader::IncludeModule("hb.site");
$status = false;

$lang = (htmlspecialchars($_REQUEST["lang"]) == "en") ? "en" : "s1";
$action = htmlspecialchars($_REQUEST["action"]);

if(check_bitrix_sessid() && $action){
    $notification = new Notification($USER->getID());
    if($action == "subscribe"){
        $status = $notification->changeProps("UF_NEWSLETTERS",true);
    }else{
        $status = $notification->changeProps("UF_NEWSLETTERS",false);
    }
    
}
$filename = ($status) ? "success_subscribe_news.php" : "error_subscribe.php";

ob_start();

$APPLICATION->IncludeFile(
    $APPLICATION->GetTemplatePath(SITE_TEMPLATE_PATH . "/include/" . $lang . "/".$filename),
    Array(
        "ACTION" => $action,
    ),
    Array("MODE"=>"html")
);

$popup = ob_get_contents();
ob_end_clean();

echo json_encode(
    array(
        "STATUS" => $status,
        "HTML" => $popup,
    )
);