<?php
if($_REQUEST["site_id"] == "en"){
	define("LANGUAGE_ID","en");
}
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("hb.site");
use HB\helpers\LocationHelpers;
$arResult = array();
if($_REQUEST['q']){
    $arResult = LocationHelpers::getCityList($_REQUEST['q']);
}
print(json_encode($arResult));