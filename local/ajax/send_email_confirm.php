<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use HB\userEntity\Confirm;
use Bitrix\Main\Loader;
use Bitrix\Main\Mail\Event;
Loader::IncludeModule("hb.site");

global $USER;
$MESS = array(
    "SUCCESS" => "Ссылка подтверждения была выслана заново", 
    "SUCCESS_EN" => "Verification link has been sent again", 
    "ALLREADY" => "Ваш адресс электронной почты уже подтвержден", 
    "ALLREADY_EN" => "Your email has already been verified.", 
);
$lang = $_REQUEST["lang"];

if(!Confirm::checkEmailConfirm($USER->GetID())){
    $hash = Confirm::createHash($USER->GetEmail(), $USER->GetID(), true);
    Event::send(array(
        "EVENT_NAME" => "CONFIRM_EMAIL",
        "LID" => $lang,
        "C_FIELDS" => array(
            "EMAIL" => $USER->GetEmail(),
            "USER_ID" => $USER->GetID(),
            "HASH" => $hash
        ),
    ));
    echo json_encode(array(
        "STATUS" => "OK",
        "MESSAGE" => $MESS["SUCCESS".($lang == "en" ? "_EN" : "")]
    ));
} else {
    echo json_encode(array(
        "STATUS" => "ERROR",
        "MESSAGE" => $MESS["ALLREADY".($lang == "en" ? "_EN" : "")]
    ));
}
$_SESSION["CONFIRM_EMAIL"] = "Y";
die;