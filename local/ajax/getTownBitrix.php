<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Loader;
Loader::IncludeModule("sale");
$result = array();
$lang = $_REQUEST["lang"] ? htmlspecialchars($_REQUEST["lang"]) : "ru";
$query = htmlspecialchars(trim($_REQUEST["q"]));
if($query){
    $db_vars = CSaleLocation::GetList(
        array("SORT" => "ASC", "CITY_NAME_LANG" => "ASC"),
        array("LID" => $lang, "!CITY_ID" =>false, "%CITY_NAME" => $query),
        false,
        array("nTopCount" => 10),
        array()
    );
    while ($vars = $db_vars->Fetch()){
        $result["city"][] = array(
            "ID" => $vars["CITY_ID"],
            "NAME" => $vars["CITY_NAME_LANG"]
        );
    }
}
echo json_encode($result);
die;
?>