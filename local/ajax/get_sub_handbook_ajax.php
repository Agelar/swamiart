<?php
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("hb.site");
CModule::IncludeModule("iblock");
if($_REQUEST['section_ID'] && $_REQUEST['prop_code']) {
    $iblock_ID = 0;
    switch ($_REQUEST['prop_code']) {
        case "GENRE": // жанр
            $iblock_ID = IBLOCK_GENRES_ID;
            break;
        case "THEME": // тема
            $iblock_ID = IBLOCK_THEME_ID;
            break;
        case "MATERIAL": // материал
            $iblock_ID = IBLOCK_MATERIALS_ID;
            break;
        case "STYLES": // стиль
            $iblock_ID = IBLOCK_STYLES_ID;
            break;
    }	
    $result = HB\helpers\Helpers::getHandbookSectionElementsList($_REQUEST['section_ID'], $iblock_ID);
    print json_encode($result);
}