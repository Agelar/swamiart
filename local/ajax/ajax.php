<?php
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("hb.site");
if($_REQUEST['AJAX']=='Y'){
    $result = array();
    if($_REQUEST['CART'] && $_REQUEST['PRODUCT_ID']){
        
        if($_REQUEST['METHOD']=='ADD'){
            CModule::IncludeModule("catalog");
            $result['SUCCESS'] = Add2BasketByProductID($_REQUEST['PRODUCT_ID'], 1, array('LID' => SITE_ID));
            if($result['SUCCESS']) $_SESSION['CART'][$_REQUEST['PRODUCT_ID']] = $result['SUCCESS'];
        }
        elseif($_REQUEST['METHOD']=='DELETE' && $_REQUEST['CART_ID']){
            CModule::IncludeModule("sale");
            $result['SUCCESS'] = CSaleBasket::Delete($_REQUEST['CART_ID']);
            unset($_SESSION['CART'][$_REQUEST['PRODUCT_ID']]);
        }
    }
    elseif($_REQUEST['FAVORITES'] && $_REQUEST['PRODUCT_ID']){
        CModule::IncludeModule("hb.site");
        if($_REQUEST['METHOD']=='ADD'){
            $result['SUCCESS'] = HB\helpers\WorkHelpers::addFavoritesWork($_REQUEST['PRODUCT_ID']);
        }
        elseif($_REQUEST['METHOD']=='DELETE'){
            $result['SUCCESS'] = HB\helpers\WorkHelpers::deleteFavoritesWork($_REQUEST['PRODUCT_ID']);
        }
    }
    elseif($_REQUEST['LIKES'] && $_REQUEST['PRODUCT_ID']){
        CModule::IncludeModule("hb.site");
        if($_REQUEST['METHOD']=='ADD'){
            $result['SUCCESS'] = HB\helpers\WorkHelpers::addLikesWork($_REQUEST['PRODUCT_ID']);
        }
        elseif($_REQUEST['METHOD']=='DELETE'){
            $result['SUCCESS'] = HB\helpers\WorkHelpers::deleteLikesWork($_REQUEST['PRODUCT_ID']);
        }
    }
    elseif($_REQUEST['NOT_SHOW'] && $_REQUEST['PRODUCT_ID']){
        CModule::IncludeModule("hb.site");
        if($_REQUEST['METHOD']=='ADD'){
            $result['SUCCESS'] = HB\helpers\WorkHelpers::addNotShowWork($_REQUEST['PRODUCT_ID']);
        }
        elseif($_REQUEST['METHOD']=='DELETE'){
            $result['SUCCESS'] = HB\helpers\WorkHelpers::deleteNotShowWork($_REQUEST['PRODUCT_ID']);
        }
    }
    elseif($_REQUEST['SHARE_MAIL']){
        if($_REQUEST['MAIL'] && $_REQUEST['NAME'] && $_REQUEST['LINK']){
            $arEventFields = array(
                "WORK_NAME"                  => htmlspecialchars($_REQUEST['NAME']),
                "WORK_LINK"             => htmlspecialchars($_REQUEST['LINK']),
                "WORK_IMG"                  => htmlspecialchars($_REQUEST['IMG']),
                "WORK_TEXT"             => htmlspecialchars($_REQUEST['TEXT']),
                "EMAIL"            => htmlspecialchars($_REQUEST['MAIL']),
                );
            $arrSITE =  SITE_ID;
            $result['SUCCESS'] = CEvent::Send("SHARE_MAIL", $arrSITE, $arEventFields);
        }
    }
    elseif($_REQUEST['DELETE_WORK']){
        if($_REQUEST['WORK_ID']){
            CModule::IncludeModule("iblock");
            $result['SUCCESS'] = CIBlockElement::Delete($_REQUEST['WORK_ID']);
        }
    }
    elseif($_REQUEST['DELETE_FILE']){
        if($_REQUEST['FILE_ID']){
            CFile::Delete($_REQUEST['FILE_ID']);
            $result['SUCCESS'] = true;
        }
    }
    elseif($_REQUEST['WORK_ADD_PARAM']){
        
            $arEventFields = array(
                "PARAM"                  => htmlspecialchars($_REQUEST['PARAM']),
                "COMMENT"             => htmlspecialchars($_REQUEST['COMMENT']),
                
                );
            $arrSITE =  SITE_ID;
            $result['SUCCESS'] = CEvent::Send("ADD_PARAMS_FORM", $arrSITE, $arEventFields);
            $result['MESSAGE'] = 'Сообщение отправлено';
            if($_REQUEST['SITE']=='en'){
                $result['MESSAGE'] = 'Message sent';
            }
    }
    elseif($_REQUEST['WORK_ADD_DISCOUNT']){
        CModule::IncludeModule("iblock");
        $arLoadProductArray = Array(
            "IBLOCK_ID" => 24,
            
            "ACTIVE" => $arWork["ACTIVE"],
            "NAME" => "Запрос на добавление скидки от пользователя " . htmlspecialchars($_REQUEST['USER_LOGIN']) . " для работы " . htmlspecialchars($_REQUEST['WORK_NAME']),
            "DETAIL_TEXT" => htmlspecialchars($_REQUEST['COMMENT']),
            "PROPERTY_VALUES" => array(
                
                "USER" => htmlspecialchars($_REQUEST['USER']),
                "WORK" => htmlspecialchars($_REQUEST['WORK']),

                


            )
                
            
        );
        $discount = new \CIBlockElement;
        $discountId = $discount->Add($arLoadProductArray);
        if($discountId){
            $arEventFields = array(
                "WORK"                  => htmlspecialchars($_REQUEST['WORK']),
                "USER"                  => htmlspecialchars($_REQUEST['USER']),
                "WORK_CODE"                  => htmlspecialchars($_REQUEST['WORK_CODE']),
                "WORK_NAME"                  => htmlspecialchars($_REQUEST['WORK_NAME']),
                "USER_LOGIN"                  => htmlspecialchars($_REQUEST['USER_LOGIN']),
                "COMMENT"             => htmlspecialchars($_REQUEST['COMMENT']),
                
                );
            $arrSITE =  SITE_ID;
            $result['SUCCESS'] = CEvent::Send("WORK_ADD_DISCOUNT", $arrSITE, $arEventFields);
            $result['MESSAGE'] = 'Запрос на добавление скидки отправлен';
            if($_REQUEST['SITE']=='en'){
                $result['MESSAGE'] = 'Discount request has been sent';
            }
        }
    }
    elseif($_REQUEST['EXPERTS_RATING']){
        CModule::IncludeModule("iblock");
        global $USER;
        if(HB\helpers\AuthorHelpers::isExpert())
            HB\helpers\RatingHelpers::expertAppreciatedWork($_REQUEST['RATING'], $_REQUEST['WORK_ID'], $USER->GetID());
    }
    
    print json_encode($result);
}