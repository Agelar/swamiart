<?php
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("hb.site");

if($_REQUEST['AJAX']=='Y' && $_REQUEST['CITY_NAME'] && $_REQUEST['CITY_ID']){
    $result['SUCCESS'] = HB\helpers\Helpers::updateIdCitiesUser($_REQUEST['CITY_NAME'], $_REQUEST['CITY_ID']);
}
print json_encode($result);