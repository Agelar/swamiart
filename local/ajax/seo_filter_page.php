<?php
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("hb.site");
CModule::IncludeModule("iblock");
if($_REQUEST['url']){
    $result = HB\helpers\CatalogHelpers::getSeoPropFilterPage($_REQUEST['url']);
    print json_encode($result);
}