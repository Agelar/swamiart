<?php
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("hb.site");
CModule::IncludeModule("iblock");
if($_REQUEST['id'] && $_REQUEST['propID']){
    $sub_section_id = 0;
    switch ($_REQUEST['propID']) {
        case WORK_PROP_GENRE_ID: // жанр
            $sub_section_id = 5;
            break;
        case WORK_PROP_THEME_ID: // тема
            $sub_section_id = 1;
            break;
        case WORK_PROP_MATERIAL_ID: // материал
            $sub_section_id = 4;
            break;
        case WORK_PROP_STYLE_ID: // стиль
            $sub_section_id = 2;
            break;
    }	

    $result = HB\helpers\FilterHelpers::getElementIDsBySectionID($_REQUEST['id'], $sub_section_id);
    print json_encode($result);
}