<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use HB\userEntity\IblockAuthor;
use Bitrix\Main\Loader;
global $USER;
Loader::IncludeModule("hb.site");

$status = false;
$lang = (htmlspecialchars($_REQUEST["lang"]) == "en") ? "en" : "s1";

if(check_bitrix_sessid()){
    $status = IblockAuthor::updateGroup($USER->GetId(),5);
    $rsUsers = \CUser::GetList(($by = "NAME"), ($order = "desc"), array("ID" => $USER->GetId()));
    if ($arUser = $rsUsers->Fetch()) {
        if($arUser["PERSONAL_PHOTO"]){
            $personalPhoto = $arUser["PERSONAL_PHOTO"];
        }
    }

    IblockAuthor::add($USER->GetId(),array(
        "NAME" => $USER->GetFullName(),
        "DETAIL_PICTURE" => $personalPhoto ? CFile::MakeFileArray($personalPhoto) : $personalPhoto,
        "PROPERTIES" => array(
            "NAME_EN" => $USER->GetFullName()
        )
    ));
}

$filename = ($status) ? "success_groupAuthor.php" : "error_subscribe.php";

ob_start();

$APPLICATION->IncludeFile(
    $APPLICATION->GetTemplatePath(SITE_TEMPLATE_PATH . "/include/" . $lang . "/".$filename),
    Array(
    ),
    Array("MODE"=>"html")
);

$popup = ob_get_contents();
ob_end_clean();

echo json_encode(
    array(
        "STATUS" => $status,
        "HTML" => $popup,
    )
);