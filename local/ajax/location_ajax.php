<?php
require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("hb.site");
use HB\helpers\LocationHelpers;

if($_REQUEST['AJAX']){
    switch($_REQUEST['TYPE'])
    {
        case "GEO":
            $cityName = $_REQUEST['CITY_NAME'];
            $regionName = explode(' ', $_REQUEST['REGION_NAME'])['0'];
            $arCity = LocationHelpers::getCityByName($cityName, $regionName);
            if($arCity){
                $cityName = $arCity['UF_CITY_NAME'];
                $cityNameEn = $arCity['UF_CITY_NAME_EN'];
                $regionName = $arCity['UF_REGION'];
                $cityId = $arCity['UF_CITY_ID'];
                $cityFullName = $arCity['UF_CITY_FULLNAME'];
            }
            break;
        case "CDEK":
            $cityName = $_REQUEST['CITY_NAME'];
            $cityNameEn = $_REQUEST['CITY_NAME_EN'];
            $regionName = $_REQUEST['REGION_NAME'];
            $cityId = $_REQUEST['CITY_ID'];
            $cityFullName = '';
            break;
        
        default:
            break;
    }
    $result = array();
    if($cityName && $regionName && $cityId){
        $regionName = \Cutil::translit($regionName, "ru", array("replace_space"=>"-","replace_other"=>"-"));
        $result['SUCCESS'] = LocationHelpers::updateUserCity($cityName, $regionName, $cityId, $cityFullName, $cityNameEn);
    }
    else{
        $result['SUCCESS'] = false;
    }
    print json_encode($result);
}