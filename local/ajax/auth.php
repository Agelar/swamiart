<?
if($_REQUEST["site_id"] == "en"){
	define("LANGUAGE_ID","en");
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $APPLICATION, $USER;

switch($_REQUEST['TYPE'])
{
	case "SEND_PWD":
		$APPLICATION->IncludeComponent("bitrix:system.auth.forgotpasswd","",Array());
		break;

	case "REGISTRATION":
		$APPLICATION->IncludeComponent(
			"bitrix:main.register",
			"",
			Array(
				"SHOW_FIELDS" => array("EMAIL","NAME","LAST_NAME","GROUP_USER"),
				"REQUIRED_FIELDS" => array("EMAIL","NAME","LAST_NAME"),
				"AUTH" => "Y",
				"USE_BACKURL" => "Y",
				"SUCCESS_PAGE" => "",
				"SET_TITLE" => "N",
				"USER_PROPERTY" => array(),
				"USER_PROPERTY_NAME" => "",
			)
		);
		break;
	case "AUTH":
		$arParams["FIND_ERROR"] = array();
		if($USER->GetByLogin($_REQUEST["USER_LOGIN"])->SelectedRowsCount()){
			$res = $USER->Login($_REQUEST["USER_LOGIN"], $_REQUEST["USER_PASSWORD"]);
			if(is_array($res)){
				$arParams["FIND_ERROR"]["PASSWORD"] = "Y";
			}
		}else{
			$arParams["FIND_ERROR"]["LOGIN"] = "Y";
		}
		$APPLICATION->IncludeComponent("bitrix:system.auth.authorize","",$arParams);
		break;
	default:
		

		// if($USER->IsAuthorized())
		// {
		// 	$APPLICATION->RestartBuffer();
		// 	$backurl = $_REQUEST["backurl"] ? $_REQUEST["backurl"] : '/';
        // }
    break;
}

?>