<?
namespace Sale\Handlers\Delivery;

use Bitrix\Sale\Delivery\CalculationResult;
use Bitrix\Sale\Delivery\Services\Base;
use Bitrix\Main\Error;
use Bitrix\Main\Loader;
use \Bitrix\Main\Type\DateTime;
use \HB\helpers\CatalogHelpers;
use \HB\helpers\Helpers;

class CheckDeliveryHandler extends Base
{
    //Доставка от двери до двери экспресс
    public const LIGHT_DELIVERY_D_D  = "1";
    //Доставка от двери до двери тяжеловес
    public const HARD_DELIVERY_D_D   = "18";
    //Доставка от двери до двери международный
    public const CONTRY_DELIVERY_D_D = "8";
    
    //ID доставки города Москва
    public const CITY_MOSKOW_ID = 44;
    //Тип доставки Дверь-Дверь
    public const TYPE_DELIVERY_D_D = 1;

    public static function getClassTitle()
        {
            return 'Доставка с проверкой картины';
        }
        
    public static function getClassDescription()
        {
            return 'Доставка осуществляется в Москву на проверку, после чего доставка осуществляется клиенту';
        }
    protected function getConfigStructure()
        {
            return array();
        }

    protected function calculateConcrete(\Bitrix\Sale\Shipment $shipment)
        {
            include_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/hb.site/library/cdek/CalculatePriceDeliveryCdek.php");

            Loader::IncludeModule("hb.site");
            Loader::IncludeModule("iblock");
            Loader::IncludeModule("sale");

            $order = $shipment->getCollection()->getOrder();
            $props = $order->getPropertyCollection();
            $result = new CalculationResult();
            $arIds = array();
            foreach($order->getBasket() as $item){
                $arIds[] = $item->getProductID();
            }
            $arDeliveryInfo = CatalogHelpers::getDeliveryInfo($arIds);

            $deliveryFirstPrice = 0;
            $deliveryTime = false;
            $finalPrice = 0;
            $finalDeliveryTime = false;


            $userCityId = Helpers::getIdCitiesUser();
            if(!$userCityId) $userCityId = self::CITY_MOSKOW_ID;
            

            foreach ($arDeliveryInfo as $delivery) {
                $calcDeliv = new \CalculatePriceDeliveryCdek();

                $calcDeliv->setSenderCityId($delivery["CITY_ID"]);
                $calcDeliv->setReceiverCityId(self::CITY_MOSKOW_ID);

                $calcDeliv->addTariffPriority(self::LIGHT_DELIVERY_D_D);
                $calcDeliv->addTariffPriority(self::HARD_DELIVERY_D_D);
                $calcDeliv->addTariffPriority(self::CONTRY_DELIVERY_D_D);

                $calcDeliv->setModeDeliveryId(self::TYPE_DELIVERY_D_D);
                //todo:: решить что-то с шириной картины
                $calcDeliv->addGoodsItemBySize(preg_replace("#,#",".",$delivery["WEIGHT"]), $delivery["LENGTH"], $delivery["WIDTH"], $delivery["HEIGHT"]);
                if ($calcDeliv->calculate() === true) {
                    $resDeliv = $calcDeliv->getResult();
                    $deliveryFirstPrice += $resDeliv["result"]["priceByCurrency"];
                    if(!$deliveryTime){
                        $deliveryTime = (new DateTime($resDeliv["result"]["deliveryDateMax"],"Y-m-d"))->getTimestamp();
                    }else{
                        $delivTime = (new DateTime($resDeliv["result"]["deliveryDateMax"],"Y-m-d"))->getTimestamp();
                        $deliveryTime = ($delivTime > $deliveryTime) ? $delivTime : $deliveryTime;
                    }
                }  
            }

            $calcFinal = new \CalculatePriceDeliveryCdek();
            $calcFinal->setSenderCityId(self::CITY_MOSKOW_ID);

            //todo::подставить значение из-заказа
            $calcFinal->setReceiverCityId($userCityId);

            $dateFinalDelivery = DateTime::createFromTimestamp($deliveryTime);
            $dateFinalDelivery->add("1 day");
            $calcFinal->setDateExecute($dateFinalDelivery->format("Y-m-d"));

            $calcFinal->addTariffPriority(self::LIGHT_DELIVERY_D_D);
            $calcFinal->addTariffPriority(self::HARD_DELIVERY_D_D);
            $calcFinal->addTariffPriority(self::CONTRY_DELIVERY_D_D);

            $calcFinal->setModeDeliveryId(self::TYPE_DELIVERY_D_D);
            foreach ($arDeliveryInfo as $delivery) {
                //todo:: решить что-то с шириной картины
                $calcFinal->addGoodsItemBySize(preg_replace("#,#",".",$delivery["WEIGHT"]), $delivery["LENGTH"], $delivery["WIDTH"], $delivery["HEIGHT"]);
            }
            if ($calcFinal->calculate() === true){
                $resFinal = $calcFinal->getResult();
                $finalPrice = $resFinal["result"]["priceByCurrency"] + $deliveryFirstPrice;
                $finalDeliveryTime = $resFinal["result"]["deliveryDateMax"];
            }      
            $result->setDeliveryPrice($finalPrice);
            $result->setPeriodDescription($finalDeliveryTime);

            return $result;
        }
        
        
    public function isCalculatePriceImmediately()
        {
            return true;
        }
        
    public static function whetherAdminExtraServicesShow()
        {
            return true;
        }

    public function isCompatible(\Bitrix\Sale\Shipment $shipment)
        {
            $calcResult = self::calculateConcrete($shipment);
            return $calcResult->isSuccess();
        }
}

?>