<?
namespace Sale\Handlers\Delivery;

use Bitrix\Sale\Delivery\CalculationResult;
use Bitrix\Sale\Delivery\Services\Base;
use Bitrix\Main\Error;
use Bitrix\Main\Loader;
use \Bitrix\Main\Type\DateTime;
use \HB\helpers\CatalogHelpers;
use \HB\helpers\Helpers;


class StandartHandler extends Base
{

    //Доставка от двери до двери экспресс
    public const LIGHT_DELIVERY_D_D  = "1";
    //Доставка от двери до двери тяжеловес
    public const HARD_DELIVERY_D_D   = "18";
    //Доставка от двери до двери международный
    public const CONTRY_DELIVERY_D_D = "8";

    //Тип доставки Дверь-Дверь
    public const TYPE_DELIVERY_D_D = 1;
    const CITY_MOSKOW_ID = 44;

    public static function getClassTitle()
        {
            return 'Стандартная доставка';
        }
        
    public static function getClassDescription()
        {
            return 'Доставка от автора к покупателю';
        }
    protected function getConfigStructure()
        {
            return array();
        }

    protected function calculateConcrete(\Bitrix\Sale\Shipment $shipment)
        {
            include_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/hb.site/library/cdek/CalculatePriceDeliveryCdek.php");

            Loader::IncludeModule("hb.site");
            Loader::IncludeModule("iblock");
            Loader::IncludeModule("sale");

            $order = $shipment->getCollection()->getOrder();
            $props = $order->getPropertyCollection();
            $result = new CalculationResult();
            $arIds = array();
            foreach($order->getBasket() as $item){
                $arIds[] = $item->getProductID();
            }
            $deliveryPrice = 0;
            $deliveryTime = false;

            $arDeliveryInfo = CatalogHelpers::getDeliveryInfo($arIds);

            $userCityId = Helpers::getIdCitiesUser();
            if(!$userCityId) $userCityId = self::CITY_MOSKOW_ID;

            foreach ($arDeliveryInfo as $delivery) {
                $calcDeliv = new \CalculatePriceDeliveryCdek();

                $calcDeliv->setSenderCityId($delivery["CITY_ID"]);

                //todo::подставить значение из-заказа
                $calcDeliv->setReceiverCityId($userCityId);

                $calcDeliv->addTariffPriority(self::LIGHT_DELIVERY_D_D);
                $calcDeliv->addTariffPriority(self::HARD_DELIVERY_D_D);
                $calcDeliv->addTariffPriority(self::CONTRY_DELIVERY_D_D);

                $calcDeliv->setModeDeliveryId(self::TYPE_DELIVERY_D_D);

                //todo:: решить что-то с шириной картины
                $calcDeliv->addGoodsItemBySize(preg_replace("#,#",".",$delivery["WEIGHT"]), $delivery["LENGTH"], $delivery["WIDTH"], $delivery["HEIGHT"]);
                if ($calcDeliv->calculate() === true) {
                    $resDeliv = $calcDeliv->getResult();
                    $deliveryPrice += $resDeliv["result"]["priceByCurrency"];
                    if(!$deliveryTime){
                        $deliveryTime = (new DateTime($resDeliv["result"]["deliveryDateMax"],"Y-m-d"))->getTimestamp();
                    }else{
                        $delivTime = (new DateTime($resDeliv["result"]["deliveryDateMax"],"Y-m-d"))->getTimestamp();
                        $deliveryTime = ($delivTime > $deliveryTime) ? $delivTime : $deliveryTime;
                    }
                } 
            }
            $result->setDeliveryPrice($deliveryPrice);
            $result->setPeriodDescription(DateTime::createFromTimestamp($deliveryTime)->format("Y-m-d"));

            return $result;
        }
        
        
    public function isCalculatePriceImmediately()
        {
            return true;
        }
        
    public static function whetherAdminExtraServicesShow()
        {
            return true;
        }

    public function isCompatible(\Bitrix\Sale\Shipment $shipment)
        {
            $calcResult = self::calculateConcrete($shipment);
            return $calcResult->isSuccess();
        }
}

?>