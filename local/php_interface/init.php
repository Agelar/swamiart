<?php
/**
 * Constants
 */
include $_SERVER["DOCUMENT_ROOT"] . '/local/php_interface/inc/const.php';

//roistat start
use Bitrix\Main;

Main\EventManager::getInstance()->addEventHandler(
    'sale',
    'OnSaleOrderSaved',
    'PutRoistat'
);

//в обработчике получаем сумму, с которой планируются некоторые действия в дальнейшем:

function PutRoistat(Main\Event $event)
{
    /** @var Order $order */
    $order = $event->getParameter("ENTITY");
    $isNew = $event->getParameter("IS_NEW");

    if ($isNew) {

        $orderId = $order->getId();
        $paid = $order->isPaid() ? 'Оплачен' : 'Не оплачен';
        $price = $order->getPrice();
        $propertyCollection = $order->getPropertyCollection();
        $arProperty = $propertyCollection->getArray();

        $data = array();
        foreach ($arProperty['properties'] as $k => $v) {
            $data[strtolower($v['CODE'])] = $v['VALUE'][0];
        }

        $deliveryCollection = $order->getShipmentCollection();
        $deliveryName = $deliveryCollection->current()->getField('DELIVERY_NAME');
        $paymentCollection = $order->getPaymentCollection();
        $payment = $paymentCollection->current()->getField('PAY_SYSTEM_NAME');

        $basket = $order->getBasket();
        $goods = 'Товары:' . PHP_EOL;
        foreach ($basket as $basketItem) {
            $goods .= $basketItem->getField('NAME') . ' - ' . $basketItem->getQuantity() . PHP_EOL;
        }
        $roistatData = array(
            'roistat' => $data['roistat'],
            'key' => 'MTkxODg5OjEyNzU3MDplOGY5YzkyNjA4NjBjZDY1YTUwNTgzZDlmMWQ0ZTFkZg==',
            'title' => 'Заказ №' . $orderId,
            'comment' => 'Цена: ' . $price . PHP_EOL . $paid . PHP_EOL . $goods,
            'name' => $data['name'] . ' ' . $data['last_name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'fields' => array(
                'UF_CRM_1561714178' => 'Корзина',
                'UF_CRM_1561714185' => $deliveryName, //Доставка
                'UF_CRM_1561714191' => $payment, //оплата
                'UF_CRM_1561714199' => '{city}', //Город
                'STAGE_ID' => 'NEW', //статус
                'OPPORTUNITY' => $price,
            ),
        );

        $result = file_get_contents("https://cloud.roistat.com/api/proxy/1.0/leads/add?" . http_build_query($roistatData));

    }
}
//roistat end

AddEventHandler("main", "OnBeforeUserUpdate", array("UserHandler", "OnBeforeUserUpdateHandler"));

/**
 * Класс для обработки событий связанных с пользователем
 */

class UserHandler
{
    /**
     * метод откатывает изменения полей пользователя если модерация не пройдена
     *
     * @param array $arFields
     * @return void
     */
    public function OnBeforeUserUpdateHandler(&$arFields)
    {
        if ($arFields['UF_MODERATION'] == USER_PROP_MODERATION_COMPLETED) { // проверяем чтобы свойство "Статус модерации" = "Пройдена"
            $arFields['UF_MODER_COMMENT'] = ''; // очищаем комментарий модератора
        } elseif ($arFields['UF_MODERATION'] == USER_PROP_MODERATION_UNCOMPLETED &&
            !empty($arFields['UF_OLD_VALUES_JSON'])) { // проверяем чтобы свойство "Статус модерации" = "Не пройдена" и свойство "Старые изменения" не пустое
            $oldValues = json_decode($arFields['UF_OLD_VALUES_JSON'], true); // декодируем старые изменения из JSON и выполняем возврат старых значений полей и свойств
            foreach ($oldValues as $propKey => $propValue) {
                if($propValue) {
                    $arFields[$propKey] = $propValue;
                }
            }
            $arFields['UF_OLD_VALUES_JSON'] = ''; // удаляем старые изменения из свойства
        }
    }
}
