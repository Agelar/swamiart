<?php
//каталоги
define('IBLOCK_WORK_ID', 7);//инфоблок работ
define('IBLOCK_SECTION_WORK_ID', 2);//основной раздел инфоблока работ
define('IBLOCK_AUTHORS_ID', 6);//инфоблок авторов
define('IBLOCK_BLOG_ID', 11);//инфоблок блога
define('IBLOCK_INTERIOR_ID', 29);//инфоблок интерьеров

define('IBLOCK_SETTINGS_ID', 12);//настройки сайта
define('IBLOCK_PROP_TRANSLATION_ID', 23);//перевод названия свойств
define('IBLOCK_PROP_HELP_ID', 25);//подсказки для свойств

//справочники
define('IBLOCK_EXECUTION_ID', 21);//инфоблок исполнения работ
define('IBLOCK_SIZE_ID', 20);//инфоблок размера работ
define('IBLOCK_ORIENTATION_ID', 19);//инфоблок работ
define('IBLOCK_GENRES_ID', 5);//инфоблок жанра работ
define('IBLOCK_MATERIALS_ID', 4);//инфоблок материалов работ
define('IBLOCK_COLORS_ID', 3);//инфоблок цветов работ
define('IBLOCK_STYLES_ID', 2);//инфоблок стилей работ
define('IBLOCK_THEME_ID', 1);//инфоблок тем работ
define('IBLOCK_MADE_IN_COLOR_ID', 28);//инфоблок Выполнена в цвете

define('IBLOCK_DIGITAL_COPIES_USERS_ID', 9);//инфоблок Цифровые копии пользователей

define('IBLOCK_ORDER_DIGITAL_COPY_ID', 8);//инфоблок заказы цифровыx копий

define('AUTHOR_GROUP', 5);//Id группы Художников
define('BUYER_GROUP', 6);//Id группы Покупателей

define('WORK_PROP_PUBLISHED_NO', 20);//Id значения "Нет" свойства работы "Опубликована"
define('WORK_PROP_PUBLISHED_YES', 19);//Id значения "ДА" свойства работы "Опубликована"

define('WORK_PROP_AUTO_RATING_YES', 25);///Id значения "ДА" свойства работы "Автоматический расчет рейтинга"
define('WORK_PROP_FINAL_RATING_ID', 14);///Id свойства работы "Итоговый рейтинг"

define('WORK_PROP_MODERATION_PROCESS', 10);//Id значения "В процессе" свойства работы "Статус модерации"
define('WORK_PROP_MODERATION_COMPLETED', 11);//Id значения "Пройдена" свойства работы "Статус модерации"
define('WORK_PROP_MODERATION_UNCOMPLETED', 24);//Id значения "Отклонена" свойства работы "Статус модерации"

define('AUTHOR_PROP_MODERATION_PROCESS', 21);//Id значения "В процессе" свойства автора "Статус модерации"
define('AUTHOR_PROP_MODERATION_COMPLETED', 22);//Id значения "Пройдена" свойства автора "Статус модерации"
define('AUTHOR_PROP_MODERATION_UNCOMPLETED', 23);//Id значения "Не пройдена" свойства автора "Статус модерации"

define('USER_PROP_MODERATION_PROCESS', 7);//Id значения "В процессе" доп. свойства работы "Статус модерации"
define('USER_PROP_MODERATION_COMPLETED', 8);//Id значения "Пройдена" доп. свойства работы "Статус модерации"
define('USER_PROP_MODERATION_UNCOMPLETED', 9);//Id значения "Не пройдена" доп. свойства работы "Статус модерации"

define('WORK_PROP_GENRE_ID', 166);//Id свойства работы "Жанр"
define('WORK_PROP_THEME_ID', 167);//Id свойства работы "Тема"
define('WORK_PROP_STYLE_ID', 168);//Id свойства работы "Стиль"
define('WORK_PROP_MATERIAL_ID', 169);//Id свойства работы "Материал"

define('WORK_PROP_ADDITIONAL_RATING_ID', 192);//Id свойства работы "Добавочный рейтинг"
define('WORK_PROP_AUTO_RATING_ID', 193);//Id свойства работы "Автоматический расчет рейтинга"

define('WORK_PROP_GENRE_SUB_ID', 170);//Id свойства работы "Жанр подраздел"
define('WORK_PROP_THEME_SUB_ID', 171);//Id свойства работы "Тема подраздел"
define('WORK_PROP_STYLE_SUB_ID', 172);//Id свойства работы "Стиль подраздел"
define('WORK_PROP_MATERIAL_SUB_ID', 173);//Id свойства работы "Материал подраздел"

define('WORK_PROP_MODERATION_ID', 29);//Id свойства работы "Статус модерации"
define('WORK_PROP_MODERATION_COMMENT_ID', 179);//Id свойства работы "Комментарий модератора"
define('WORK_PROP_OLD_VALUES_JSON', 180);//Id свойства работы "Старые изменения"
define('WORK_PROP_PUBLISHED_ID', 165);//Id свойства работы "Опубликована"

define('AUTHOR_PROP_MODERATION_ID', 177);//Id свойства автора "Статус модерации"
define('AUTHOR_PROP_OLD_VALUES_JSON', 178);//Id свойства автора "Старые изменения"
