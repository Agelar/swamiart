<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
  die();
  use HB\helpers\Helpers;
  global $APPLICATION;
?>
    <div class="footer footer_desktop">
        <div class="footer__main">
          <div class="container">
            <div class="footer__main-inner">
              <div class="footer__column hidden-tablet-down">
                <div class="footer-block"><a class="footer-block__head"><?=GetMessage('ABOUT');?></a>
                  <?$APPLICATION->IncludeComponent(
                      "bitrix:menu",
                      "footer_menu",
                      Array(
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "left",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => array(""),
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "about",
                        "USE_EXT" => "N"
                      )
                  );?>
                </div>
              </div>
              <div class="footer__column hidden-tablet-down">
                <div class="footer-block"><a class="footer-block__head"><?=GetMessage('BUYERS');?></a>
                  <?$APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "footer_menu",
                        Array(
                          "ALLOW_MULTI_SELECT" => "N",
                          "CHILD_MENU_TYPE" => "left",
                          "DELAY" => "N",
                          "MAX_LEVEL" => "1",
                          "MENU_CACHE_GET_VARS" => array(""),
                          "MENU_CACHE_TIME" => "3600",
                          "MENU_CACHE_TYPE" => "A",
                          "MENU_CACHE_USE_GROUPS" => "Y",
                          "ROOT_MENU_TYPE" => "buyers",
                          "USE_EXT" => "N"
                        )
                    );?>
                </div>
              </div>
              <div class="footer__column hidden-tablet-down">
                <div class="footer-block"><a class="footer-block__head" href="/works/"><?=GetMessage('CATALOG');?></a>
                  <?$APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "footer_menu",
                        Array(
                          "ALLOW_MULTI_SELECT" => "N",
                          "CHILD_MENU_TYPE" => "left",
                          "DELAY" => "N",
                          "MAX_LEVEL" => "1",
                          "MENU_CACHE_GET_VARS" => array(""),
                          "MENU_CACHE_TIME" => "3600",
                          "MENU_CACHE_TYPE" => "A",
                          "MENU_CACHE_USE_GROUPS" => "Y",
                          "ROOT_MENU_TYPE" => "catalog",
                          "USE_EXT" => "N"
                        )
                    );?>
                </div>
              </div>
              <div class="footer__column">
                <div class="footer-block"><a class="footer-block__head" href="/contacts/"><?=GetMessage('CONTACTS');?></a>
                  <div class="footer-block__content">
                    <div><a class="footer-block__link footer-block__link_icon footer-block__link_phone roistat-phone1" href="tel:+<?=preg_replace("/[^0-9]/", '', $arSettings["PHONE"]["VALUE"])?>">
                        <svg class="icon icon-phone">
                          <use xlink:href="#icon-phone"></use>
                        </svg><?=$arSettings["PHONE"]["VALUE"]?></a></div>
                    <div><a class="footer-block__link footer-block__link_icon footer-block__link_email" href="mailto:<?=$arSettings["EMAIL"]["VALUE"]?>">
                        <svg class="icon icon-mail">
                          <use xlink:href="#icon-mail"></use>
                        </svg><?=$arSettings["EMAIL"]["VALUE"]?></a></div>
                    <div><span class="footer-block__link footer-block__link_icon footer-block__link_text" href="#">
                        <svg class="icon icon-map">
                          <use xlink:href="#icon-map"></use>
                        </svg><?=$arSettings["ADRESS"]["~VALUE"]?></span></div>
                  </div>
                </div>
              </div>
              <div class="footer__column">
                <div class="footer-block"><span class="footer-block__head"><?=GetMessage('SOCIAL');?></span>
                  <div class="social"><a class="social__link" rel="nofollow" href="<?=$arSettings["VK_LINK"]["VALUE"]?>" target="_blank">
                      <svg class="icon social-vk">
                        <use xlink:href="#social-vk"></use>
                      </svg></a><a class="social__link" rel="nofollow" href="<?=$arSettings["FB_LINK"]["VALUE"]?>" target="_blank">
                      <svg class="icon social-fb">
                        <use xlink:href="#social-fb"></use>
                      </svg></a><a class="social__link" rel="nofollow" href="<?=$arSettings["PIN_LINK"]["VALUE"]?>" target="_blank">
                      <svg class="icon social-pinterest">
                        <use xlink:href="#social-pinterest"></use>
                      </svg></a><a class="social__link" rel="nofollow" href="<?=$arSettings["INS_LINK"]["VALUE"]?>" target="_blank">
                      <svg class="icon social-ins">
                        <use xlink:href="#social-ins"></use>
                      </svg></a><a class="social__link" rel="nofollow" href="<?=$arSettings["YT_LINK"]["VALUE"]?>" target="_blank">
                      <svg class="icon social-youtube">
                        <use xlink:href="#social-youtube"></use>
                      </svg></a>
                    </div>
                </div>
                <div class="footer-block"><span class="footer-block__head"><?=GetMessage('PAYMENT');?></span>
                  <div class="payment"><?=$arSettings["PAY_SYSTEM_BLOCK"]["~VALUE"]['TEXT']?></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer__bottom">
          <div class="container">
            <div class="footer__bottom-inner">
                <?$APPLICATION->IncludeComponent(
										"bitrix:menu",
										"bottom_menu",
										Array(
											"ALLOW_MULTI_SELECT" => "N",
											"CHILD_MENU_TYPE" => "left",
											"DELAY" => "N",
											"MAX_LEVEL" => "1",
											"MENU_CACHE_GET_VARS" => array(""),
											"MENU_CACHE_TIME" => "3600",
											"MENU_CACHE_TYPE" => "A",
											"MENU_CACHE_USE_GROUPS" => "Y",
											"ROOT_MENU_TYPE" => "bottom",
											"USE_EXT" => "N"
										)
								);?>
              <span class="footer__copyright"><?=$arSettings["COPYRIGHT"]["VALUE"]?><?=date('Y');?></span>
             <noindex><!-- <a class="footer__logo" rel="nofollow" href="https ://hawkingbrothers.com/" target="_blank">
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/images/logo-hb.png" alt=""/>
                <span><?=GetMessage('DEVELOP');?></span>
</a> --></noindex>
            </div>
          </div>
        </div>
      </div>
      <div class="footer footer_mobile">
        <div class="footer__main">
          <div class="container">
            <div class="footer__inner">
              <div class="social"><a class="social__link" rel="nofollow" href="<?=$arSettings["VK_LINK"]["VALUE"]?>" target="_blank">
                  <svg class="icon social-vk">
                    <use xlink:href="#social-vk"></use>
                  </svg></a><a class="social__link" rel="nofollow" href="<?=$arSettings["FB_LINK"]["VALUE"]?>" target="_blank">
                  <svg class="icon social-fb">
                    <use xlink:href="#social-fb"></use>
                  </svg></a><a class="social__link" rel="nofollow" href="<?=$arSettings["PIN_LINK"]["VALUE"]?>" target="_blank">
                  <svg class="icon social-pinterest">
                    <use xlink:href="#social-pinterest"></use>
                  </svg></a><a class="social__link" rel="nofollow" href="<?=$arSettings["INS_LINK"]["VALUE"]?>" target="_blank">
                  <svg class="icon social-ins">
                    <use xlink:href="#social-ins"></use>
                  </svg></a><a class="social__link" rel="nofollow" href="<?=$arSettings["YT_LINK"]["VALUE"]?>" target="_blank">
                      <svg class="icon social-youtube">
                        <use xlink:href="#social-youtube"></use>
                      </svg></a>
                </div>
              <div class="footer-block">
                <div class="footer-block__content">
                  <div><a class="footer-block__link footer-block__link_icon footer-block__link_phone roistat-phone1" href="tel:+<?=preg_replace("/[^0-9]/", '', $arSettings["PHONE"]["VALUE"])?>">
                      <svg class="icon icon-phone">
                        <use xlink:href="#icon-phone"></use>
                      </svg><?=$arSettings["PHONE"]["VALUE"]?></a></div>
                  <div><a class="footer-block__link footer-block__link_icon footer-block__link_email" href="mailto:<?=$arSettings["EMAIL"]["VALUE"]?>">
                      <svg class="icon icon-mail">
                        <use xlink:href="#icon-mail"></use>
                      </svg><?=$arSettings["EMAIL"]["VALUE"]?></a></div>
                  <div><span class="footer-block__link footer-block__link_icon footer-block__link_text">
                      <svg class="icon icon-map">
                        <use xlink:href="#icon-map"></use>
                      </svg><?=$arSettings["ADRESS"]["~VALUE"]?></span></div>
                </div>
              </div>
              <div class="payment"><?=$arSettings["PAY_SYSTEM_BLOCK"]["~VALUE"]['TEXT']?></div>
            </div>
          </div>
        </div>
        <div class="footer__bottom">
          <div class="container">
            <div class="footer__inner"><span class="footer__copyright"><?=$arSettings["COPYRIGHT"]["VALUE"]?><?=date('Y');?></span></div>
          </div>
        </div>
        <div class="footer__main">
          <div class="container">
            <div class="footer__inner">
            <?$APPLICATION->IncludeComponent(
										"bitrix:menu",
										"bottom_menu",
										Array(
											"ALLOW_MULTI_SELECT" => "N",
											"CHILD_MENU_TYPE" => "left",
											"DELAY" => "N",
											"MAX_LEVEL" => "1",
											"MENU_CACHE_GET_VARS" => array(""),
											"MENU_CACHE_TIME" => "3600",
											"MENU_CACHE_TYPE" => "A",
											"MENU_CACHE_USE_GROUPS" => "Y",
											"ROOT_MENU_TYPE" => "bottom",
											"USE_EXT" => "N"
										)
								);?><span class="footer__author"><?=GetMessage('DEVELOP_MOBILE');?> <a href="https://hawkingbrothers.com/" target="_blank">Hawking Brothers</a></span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="up-button__block">
      <button type="button" class="slick-arrow slick-prev" style=""><svg class="icon icon-arrow"><use xlink:href="#icon-arrow"></use></svg></button>
    </div>
    <div class="popup" style="display:none;">
      <div class="popup__bg"></div>
      <div class="popup__container">
        <div class="popup__window">
          <div class="popup__close">
            <div class="popup__close-item"></div>
            <div class="popup__close-item"></div>
          </div>
          <?if(!$USER->isAuthorized()):?>
            <div class="popup__content popup__content--reg js-wrap-reg">
              <?$APPLICATION->IncludeComponent(
                "bitrix:main.register", 
                "", 
                array(
                  "AUTH" => "Y",
                  "REQUIRED_FIELDS" => array(
                    0 => "EMAIL",
                    1 => "NAME",
                    2 => "LAST_NAME",
                  ),
                  "SET_TITLE" => "N",
                  "SHOW_FIELDS" => array(
                    0 => "EMAIL",
                    1 => "NAME",
                    2 => "LAST_NAME",
                    3 => "GROUP_USER",
                  ),
                  "SUCCESS_PAGE" => "",
                  "USER_PROPERTY" => array(
                  ),
                  "USER_PROPERTY_NAME" => "",
                  "USE_BACKURL" => "N",
                  "COMPONENT_TEMPLATE" => "main"
                ),
                false
              );?>
            </div>
            <div class="popup__content popup__content--auth js-wrap-auth">
              <?$APPLICATION->IncludeComponent(
                  "bitrix:system.auth.authorize",
                  "",
                  Array(
                  )
              );?>
            </div>
            <div class="popup__content popup__content--restore js-wrap-fogot">
              <?$APPLICATION->IncludeComponent("bitrix:system.auth.forgotpasswd","",Array());?>
            </div>
          <?else:?>
            <div class="popup__content popup__content--reg js-wrap-reg">
              <div class="popup-form-reg popup__content-wrapper">
                <span class="popup__heading popup__heading--center"><?=GetMessage("LOGGED_IN");?></span>
                <div class="popup__label blog-detail__content"><?=GetMessage("GO_TO");?><a href="<?=SITE_DIR?>personal/my-details/"><?=GetMessage("PERSONAL_AREA");?></a></div>
              </div>
            </div>
            <?$APPLICATION->IncludeComponent(
                "HB:user.select.role",
                "",
                Array(
                )
            );?>
          <?endif;?>
            <div class="popup__content popup__content--share_mail">
              <form class="popup__content-wrapper popup__content-wrapper--share_mail" action=""><span class="popup__heading popup__heading--center"><?=GetMessage('SEND_MAIL');?></span>
                <label class="label">
                  <div class="popup__label"><?=GetMessage('ENTER_MAIL');?></div>
                  <input class="input input-text" type="email" name="email" required="required"/>
                </label>
                <button class="popup__mail-submit popup__mail-submit--share_mail" type="#"><?=GetMessage('TO_SEND');?></button>
              </form>
              <form class="popup__content-wrapper popup__content-wrapper--share_mail_success" action="" style="display: none;"><span class="popup__heading popup__heading--center"><?=GetMessage('SUCCESS');?></span><span class="popup__subheading"><?=GetMessage('SENT_LINK_MAIL');?></span>
                <div class="popup__icon">
                  <svg class="icon check_smooth">
                    <use xlink:href="#check_smooth"></use>
                  </svg>
                </div>
              </form>
            </div>

            <div class="popup__content popup__content--delete_confirm">
              <form class="popup__content-wrapper popup__content-wrapper--delete_confirm" action=""><span class="popup__heading popup__heading--center"><?=GetMessage('WORK_REMOVAL');?></span>
                <label class="label">
                  <div class="popup__label"><?=GetMessage('DELETE_WORK');?> <span class="work-delete__name"></span>?</div>
                   <input id="work_delete_id" type="hidden" name="work_delete_id" >
                </label>
                <button class="popup__mail-submit popup__mail-submit--delete_confirm" type="#" data-id=""><?=GetMessage('DELETE');?></button>
              </form>
              <form class="popup__content-wrapper popup__content-wrapper--delete_confirm_success" action="" style="display: none;"><span class="popup__heading popup__heading--center"><?=GetMessage('WORK_SUCCESS_DELETED');?></span>
                <div class="popup__icon">
                  <svg class="icon check_smooth">
                    <use xlink:href="#check_smooth"></use>
                  </svg>
                </div>
              </form>
            </div>
        </div>
      </div>
    </div>
    <!-- BEGIN JIVOSITE CODE {literal} -->
    <script type='text/javascript'>
    (function(){ var widget_id = 'nrAgcGL5Cf';var d=document;var w=window;function l(){
    var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
    s.src = '//code.jivosite.com/script/widget/'+widget_id
    ; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}
    if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}
    else{w.addEventListener('load',l,false);}}})();
    </script>
    <!-- {/literal} END JIVOSITE CODE -->

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
      (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
      m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
      (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

      ym(53106508, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
      });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/53106508" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
    <script>
        //roistat start
        var getCookie = window.getCookie = function (name) {
            var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
            return matches ? decodeURIComponent(matches[1]) : undefined;
        };
        function jivo_onLoadCallback() {
            jivo_api.setUserToken(getCookie('roistat_visit'));
        }
        (function(w, d, s, h, id) {
            w.roistatProjectId = id; w.roistatHost = h;
            var p = d.location.protocol == "https:" ? "https://" : "http://";
            var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
            var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
        })(window, document, 'script', 'cloud.roistat.com', '781e5774a45bd68b53ac2a1e86ecd7f0');


        //roistat end
    </script> 
	</body>
</html>
