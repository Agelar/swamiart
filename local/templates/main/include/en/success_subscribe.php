<div class="popup__content-wrapper" action="">
    <span class="popup__heading popup__heading--center">Success!</span>
    <?if($arParams["ACTION"] == "subscribe"):?>
        <span class="popup__subheading">You've successfully subscribed to the author</span>
    <?else:?>
        <span class="popup__subheading">You've successfully unsubscribed from the author</span>
    <?endif;?>
    <div class="popup__icon">
        <svg class="icon icon-check-w">
            <use xlink:href="#icon-check"></use>
        </svg>
    </div>
</div>