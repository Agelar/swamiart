<div class="popup__content-wrapper" action="">
    <span class="popup__heading popup__heading--center">Success!</span>
    <?if($arParams["ACTION"] == "subscribe"):?>
        <span class="popup__subheading">You have successfully subscribed to the newsletter.</span>
    <?else:?>
        <span class="popup__subheading">You have successfully unsubscribed from the newsletter</span>
    <?endif;?>
    <div class="popup__icon">
        <svg class="icon icon-check-w">
            <use xlink:href="#icon-check"></use>
        </svg>
    </div>
</div>