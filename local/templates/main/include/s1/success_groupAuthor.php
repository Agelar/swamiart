<div class="popup__content-wrapper" action="">
    <span class="popup__heading popup__heading--center">Успех!</span>
    <span class="popup__subheading">Ваш статус изменен на автора</span>
    <div class="popup__icon">
        <svg class="icon icon-check-w">
            <use xlink:href="#icon-check"></use>
        </svg>
    </div>
</div>