<div class="popup__content-wrapper" action="">
    <span class="popup__heading popup__heading--center">Успех!</span>
    <?if($arParams["ACTION"] == "subscribe"):?>
        <span class="popup__subheading">Вы успешно подписались на автора</span>
    <?else:?>
        <span class="popup__subheading">Вы успешно отписались от автора</span>
    <?endif;?>
    <div class="popup__icon">
        <svg class="icon icon-check-w">
            <use xlink:href="#icon-check"></use>
        </svg>
    </div>
</div>