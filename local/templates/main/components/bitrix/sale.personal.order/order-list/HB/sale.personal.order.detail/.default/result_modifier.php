<?
use HB\helpers\CatalogHelpers;
use HB\helpers\LangHelpers;
use Bitrix\Main\Loader;

Loader::IncludeModule("HB.site");

$arProps = array();
foreach($arResult["ORDER_PROPS"] as $prop){
    $arProps[$prop["CODE"]] = $prop;
}
$arResult["ORDER_PROPS"] = $arProps;

foreach($arResult["BASKET"] as $product){
    $arResult["PRODUCT_IDS"][] = $product["PRODUCT_ID"];
}
$arResult["FORMATED_PRODUCT"] = CatalogHelpers::getInfoByProducts($arResult["PRODUCT_IDS"],array("NAME","NAME_EN","PREVIEW_PICTURE","SIZE","AUTHOR","STYLE","DETAIL_PAGE_URL","WORK_ID","DIGITAL_COPY_ID"),array("width" => 200,"heigth" => 200));
foreach($arResult["FORMATED_PRODUCT"] as &$product){
    $product = LangHelpers::getTranslation($product);
}

switch($arResult["STATUS"]["ID"]){
    case "N": 
        $arResult["STATUS"]["CLASS"] = "status-error";
        break;
    case "OP": 
        $arResult["STATUS"]["CLASS"] = "status-warning";
        break;
    case "F": 
        $arResult["STATUS"]["CLASS"] = "status-good"; 
        break;
}
?>