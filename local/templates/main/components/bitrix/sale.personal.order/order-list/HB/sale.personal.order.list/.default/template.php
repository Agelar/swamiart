<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;
Loc::loadMessages(__FILE__);?>
<div class="lk__main-inner"><span class="lk-head"><?=Loc::getMessage("MY_ORDERS")?></span>
	<div class="lk-desc"><span><?=Loc::getMessage("SUBHEADEAR")?></span></div>
	<div class="lk-orders">
		<?foreach ($arResult['ORDERS'] as $key => $order):?>
			<div class="lk-orders__item order-item">
				<div class="order-item__row">
					<div class="order-item__column"><span class="order-item__title">
						<span><?=Loc::getMessage('SPOL_TPL_ORDER')?></span> <?=Loc::getMessage('SPOL_TPL_NUMBER_SIGN').$order['ORDER']['ACCOUNT_NUMBER']?></span>
						<span class="order-item__date"><?=$order['ORDER']['DATE_INSERT']->format($arParams['ACTIVE_DATE_FORMAT'])?></span>
					</div>
					<div class="order-item__column">
						<span class="order-item__price"><?=number_format(intval($order['ORDER']['PRICE']), 0, ".", " ")?> <span class="rubl">₽</span></span>
					</div>
				</div>
				<div class="order-item__row">
					<div class="order-item__column">
						<span class="order-item__status <?=$arResult['INFO']['STATUS'][$order['ORDER']['STATUS_ID']]["CLASS"]?>"><?=$arResult['INFO']['STATUS'][$order['ORDER']['STATUS_ID']]["NAME"]?></span>
					</div>
					<div class="order-item__column">
						<a class="order-item__more" href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_DETAIL"])?>"><?=Loc::getMessage('SPOL_TPL_MORE_ON_ORDER')?><div class="arrow"></div></a>
					</div>
				</div>
			</div>
		<?endforeach;?>
	</div>
	<?echo $arResult["NAV_STRING"];?>
</div>