<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset,
	Bitrix\Main\Loader;
$APPLICATION->SetTitle("");?>
<div class="lk__main-inner">
	<span class="lk-head"><?=Loc::getMessage("SPOD_ORDER")?> №<?=$arResult["ACCOUNT_NUMBER"]?></span>
	<table class="lk-detail">
		<tr>
			<td><?=Loc::getMessage("DATE_ORDER")?></td>
			<td><?=$arResult["DATE_INSERT_FORMATED"]?></td>
		</tr>
		<tr>
			<td><?=Loc::getMessage("STATUS")?></td>
			<td class="<?=$arResult["STATUS"]["CLASS"]?>"><?=$arResult["STATUS"]["NAME"]?></td>
		</tr>
		<tr>
			<td><?=Loc::getMessage("FIO")?></td>
			<td>
				<div><span><?=$arResult["ORDER_PROPS"]["NAME"]["VALUE"] . " " . $arResult["ORDER_PROPS"]["LAST_NAME"]["VALUE"]?></span></div>
				<?if($arResult["ORDER_PROPS"]["PHONE"]["VALUE"]):?>
					<div><span><?=$arResult["ORDER_PROPS"]["PHONE"]["VALUE"]?></span></div>
				<?endif;?>
				<?if($arResult["ORDER_PROPS"]["EMAIL"]["VALUE"]):?>
					<div><a href="mailto:<?=$arResult["ORDER_PROPS"]["EMAIL"]["VALUE"]?>"><?=$arResult["ORDER_PROPS"]["EMAIL"]["VALUE"]?></a></div>
				<?endif;?>
			</td>
		</tr>
		<tr>
			<td><?=Loc::getMessage("ADDRESS_DELIV")?></td>
			<td><?=$arResult["ORDER_PROPS"]["CITY"]["VALUE"]?> <br> <?=$arResult["ORDER_PROPS"]["STREET"]["VALUE"] . " " . $arResult["ORDER_PROPS"]["HOUSE"]["VALUE"] . " " . $arResult["ORDER_PROPS"]["APARTAMENT"]["VALUE"]?></td>
		</tr>
		<tr>
			<td><?=Loc::getMessage("METHOD_DELIVERY")?></td>
			<td><?=Loc::getMessage(current($arResult["SHIPMENT"])["DELIVERY_NAME"])?></td>
		</tr>
		<tr>
			<td><?=Loc::getMessage("METHOD_PAYMENT")?></td>
			<td><?=Loc::getMessage(current($arResult["PAYMENT"])["PAY_SYSTEM_NAME"])?></td>
		</tr>
	</table>
	<div class="lk-block">
		<span class="lk-block__head lk-block__head_nomargin"><?=Loc::getMessage("BASKET_LIST")?></span>
		<div class="lk-block__main">
			<?foreach($arResult["BASKET"] as $product):?>
				<div class="cart-item">
					<?if($arResult["FORMATED_PRODUCT"][$product["PRODUCT_ID"]]["PREVIEW_PICTURE"]):?>
						<div class="cart-item__img" style="background-image:url(<?=$arResult["FORMATED_PRODUCT"][$product["PRODUCT_ID"]]["PREVIEW_PICTURE"]?>)"></div>
					<?elseif($arResult["FORMATED_PRODUCT"][$product["PRODUCT_ID"]]["DETAIL_PICTURE"]):?>
						<div class="cart-item__img" style="background-image:url(<?=$arResult["FORMATED_PRODUCT"][$product["PRODUCT_ID"]]["DETAIL_PICTURE"]?>)"></div>
					<?endif;?>
					<div class="cart-item__main">
						<a class="cart-item__title" href="<?=$arResult["FORMATED_PRODUCT"][$product["PRODUCT_ID"]]["DETAIL_PAGE_URL"]?>"><?=$arResult["FORMATED_PRODUCT"][$product["PRODUCT_ID"]]["NAME"]?></a>
						<a class="cart-item__author" href="<?=$arResult["FORMATED_PRODUCT"][$product["PRODUCT_ID"]]["PROPERTIES"]["AUTHOR"]["ELEMENTS"]["DETAIL_PAGE_URL"]?>"><?=$arResult["FORMATED_PRODUCT"][$product["PRODUCT_ID"]]["PROPERTIES"]["AUTHOR"]["ELEMENTS"]["NAME"]?></a>
						<div class="cart-item__info">
							<div class="cart-item__descs">
								<?if($arResult["FORMATED_PRODUCT"][$product["PRODUCT_ID"]]["PROPERTIES"]["SIZE"]["VALUE"]):?>
									<div class="cart-item__desc">
										<span><?=Loc::getMessage("SIZE")?>: </span><span><?=$arResult["FORMATED_PRODUCT"][$product["PRODUCT_ID"]]["PROPERTIES"]["SIZE"]["VALUE"]?>.</span>
									</div>
								<?endif;?>
								<?if($arResult["FORMATED_PRODUCT"][$product["PRODUCT_ID"]]["PROPERTIES"]["STYLE"]["ELEMENTS"]["NAME"]):?>
									<div class="cart-item__desc"><span><?=Loc::getMessage("STYLE")?>: </span>
									<span><?=$arResult["FORMATED_PRODUCT"][$product["PRODUCT_ID"]]["PROPERTIES"]["STYLE"]["ELEMENTS"]["NAME"]?></span></div>
								<?endif;?>
							</div>
							<div class="cart-item__prices">
								<?if(intval($product["DISCOUNT_PRICE"]) > 0):?>
									<span class="cart-item__price"><?=number_format($product["PRICE"], 0, ",", " ")?> <span class="rubl">₽</span></span>
									<span class="cart-item__oldprice"><?=number_format($product["BASE_PRICE"], 0, ",", " ")?> ₽</span>
								<?else:?>
									<span class="cart-item__price"><?=number_format($product["PRICE"], 0, ",", " ")?> <span class="rubl">₽</span></span>
								<?endif;?>
							</div>
							<?if($arResult["STATUS"]["CLASS"] == 'status-good' && $arResult["FORMATED_PRODUCT"][$product["PRODUCT_ID"]]['WORK_ID'] && $arResult["FORMATED_PRODUCT"][$product["PRODUCT_ID"]]['DIGITAL_COPY_ID']):?>
								<div class="picture">
									<a class="link link_icon is-active" href="/personal/download_digital_copy.php?work_id=<?=$arResult["FORMATED_PRODUCT"][$product["PRODUCT_ID"]]['WORK_ID']?>&file_id=<?=$arResult["FORMATED_PRODUCT"][$product["PRODUCT_ID"]]['DIGITAL_COPY_ID']?>" target="_blank">
										<svg class="icon icon-download">
											<use xlink:href="#icon-download"></use>
										</svg>Цифровая копия                        
									</a>
								</div>
							<?endif;?>
						</div>
					</div>
				</div>
			<?endforeach;?>
		</div>
	</div>
	<div class="lk-block"><span class="lk-block__head"><?=Loc::getMessage("SUM_ORDER")?></span>
		<div class="lk-block__main">
			<div class="cart-result cart-result_detail">
				<div class="cart-result__main">
					<div class="cart-result__row">
						<span><?=Loc::getMessage("SUM_PRODUCT")?></span>
						<span><?=number_format($arResult["PRODUCT_SUM"], 0, ",", " ")?> <span class="rubl">₽</span></span>
					</div>
					<div class="cart-result__row">
						<span><?=Loc::getMessage("DELIVERY")?></span>
						<span><?=number_format($arResult["PRICE_DELIVERY"], 0, ",", " ")?> <span class="rubl">₽</span></span>
					</div>
					<?if($arResult["DISCOUNT_VALUE"] > 0):?>
						<div class="cart-result__row">
							<a href="#"><?=Loc::getMessage("PROMOCODE")?></a>
							<span>— <?=number_format($arResult["DISCOUNT_VALUE"], 0, ",", " ")?> <span class="rubl">₽</span></span>
						</div>
					<?endif;?>
				</div>
				<div class="cart-result__bottom">
					<div class="cart-result__final">
						<span><?=Loc::getMessage("SUM_TOTAL")?>:</span>
						<span> <?=number_format($arResult["PRICE"], 0, ",", " ")?> <span class="rubl">₽</span></span>
					</div>
				</div>
				<?if(current($arResult["PAYMENT"])["BUFFERED_OUTPUT"] && $arResult["STATUS"]["ID"] != "OP"):?>
					<button class="btn btn_green_border js-payment-order"><?=Loc::getMessage("PAYMENT")?></div>
					<div class="js-payment-form hidden_block">
						<?=current($arResult["PAYMENT"])["BUFFERED_OUTPUT"]?>
					</div>
				<?endif;?>
			</div>
		</div>
	</div>
</div>