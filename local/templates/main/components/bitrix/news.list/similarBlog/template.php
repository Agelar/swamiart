<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use HB\helpers\LangHelpers;
?>

<div class="section similar-articles js-section">
    <div class="section-head">
        <div class="container">
            <div class="section-head__inner"><span class="section-head__title"><?=GetMessage("SIMILAR")?></span></div>
        </div>
    </div>
    <div class="section-tools">
        <div class="container">
            <div class="section-tools__inner js-section-tools"></div>
        </div>
    </div>
    <div class="container">
        <div class="similar-articles__inner js-similar-articles">
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				$arItem = LangHelpers::getTranslation($arItem);
				?>
				<div class="similar-articles__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<a class="similar-articles__item-img" href="<?=$arItem["DETAIL_PAGE_URL"]?>" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["RESIZE"]["src"]?>)"></a>
					<div class="similar-articles__item-category"><?=$arResult["SECTION"][$arItem["IBLOCK_SECTION_ID"]]["NAME"]?>
					<span class="similar-articles__item-category-date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span></div>
					<h3 class="similar-articles__item-heading"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h3>
					<div class="similar-articles__item-text"><?=$arItem["PREVIEW_TEXT"]?></div>
					<a class="similar-articles__item-link" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
						<?=GetMessage("READ_MORE")?>
						<div class="arrow"></div>
					</a>
				</div>
			<?endforeach;?>
        </div>
    </div>
</div>