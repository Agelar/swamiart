<?
use HB\helpers\Helpers;
use HB\helpers\SearchHelpers;
use HB\helpers\LangHelpers;

$arIDs = array();
foreach($arResult["ITEMS"] as $key => $arItem){
    $arResult["ITEMS"][$key] = LangHelpers::getTranslation($arItem);
    if($arItem["PREVIEW_PICTURE"]["SRC"]){
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["RESIZE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array("width"=>"145","height"=>"145", BX_RESIZE_IMAGE_PROPORTIONAL));
        
        $arResult["ITEMS"][$key]["NAME"] = SearchHelpers::searchMathes($arResult["ITEMS"][$key]["NAME"],$arParams["SEARCH_QUERY"]);
        $arResult["ITEMS"][$key]["PREVIEW_TEXT"] = SearchHelpers::searchMathes($arResult["ITEMS"][$key]["PREVIEW_TEXT"],$arParams["SEARCH_QUERY"]);
    }
}
$sort = new Helpers($arParams["SORT_ARRAY"]);
usort($arResult["ITEMS"],array($sort,"sortByID"));
?>