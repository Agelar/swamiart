<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if($arResult["ITEMS"]):?>
	<div class="section js-section">
        <div class="section-head">
          <div class="container">
						<div class="section-head__inner"><span class="section-head__title"><?=GetMessage('NEWS');?></span></div>
           </div>
         </div>
         	<div class="section-tools">
           <div class="container">
						 <div class="section-tools__inner js-section-tools"></div>
						</div>
        	</div>
        <div class="container">
          <div class="news js-news">
		  	<?foreach($arResult["ITEMS"] as $arItem):?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
				<div class="news__slide">
					<div class="note note_news">
						<div class="note__preview" style="background-image:url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>')"></div>
						<div class="note__content"><a class="note__category" href="<?=$arResult["SECTION"][$arItem['IBLOCK_SECTION_ID']]["SECTION_PAGE_URL"]?>"><span><?=$arResult["SECTION"][$arItem['IBLOCK_SECTION_ID']]["NAME"]?></span></a><span class="note__date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span><a class="note__title" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a><span class="note__desc"><?=$arItem["PREVIEW_TEXT"]?></span><a class="note__more" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage('VIEW');?>
							<div class="arrow"></div></a></div>
					</div>
				</div>
			<?endforeach;?>

            
          </div>
		</div>
		<div class="section__buttons"><a class="btn" href="<?=SITE_DIR?>blog/"><?=GetMessage('SEE_ALL');?></a></div>
	</div>
<?endif;
