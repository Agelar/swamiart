<?php
use HB\helpers\LangHelpers;
use HB\helpers\Helpers;
$arSectionId = array();
foreach($arResult["ITEMS"] as $key => $arItem){
    $arResult["ITEMS"][$key] = LangHelpers::getTranslation($arItem);
    $arSectionId[$arItem['IBLOCK_SECTION_ID']] = $arItem['IBLOCK_SECTION_ID'];
}
$arResult["SECTION"] = Helpers::getSectionById($arParams['IBLOCK_ID'], $arSectionId);



