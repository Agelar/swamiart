<?php
use HB\helpers\LangHelpers;
foreach($arResult["ITEMS"] as $key => $arItem){
    $arResult["ITEMS"][$key] = LangHelpers::getTranslation($arItem);
}

$arSettings = HB\helpers\Helpers::getSiteSettings();

if($arSettings['CAP_PLUG_PICTURE']['VALUE']) $arResult['CAP_PLUG_PICTURE'] = CFile::ResizeImageGet($arSettings['CAP_PLUG_PICTURE']['VALUE'], array('width'=>480, 'height'=>360), BX_RESIZE_IMAGE_PROPORTIONAL)['src'];  