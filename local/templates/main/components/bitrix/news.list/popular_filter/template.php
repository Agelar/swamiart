<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(count($arResult["ITEMS"])):?>
	<div class="popular-filter">
        <div class="container">
          <div class="popular-filter__inner js-popular-filter"><span class="popular-filter__title"><?=GetMessage("STYLE_REVIEW")?></span>
            <div class="popular-filter__tools js-popular-filter-tools"></div>
            <div class="popular-filter__carousel js-popular-filter-carousel">
							<?php $key = 0; foreach($arResult["ITEMS"] as $arItem):?>
								<?
								$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
								$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
								?>
								<div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
									<a class="popular-filter__item" href="<?=SITE_DIR;?><?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>" style="background-image:url(<?=$arItem["DETAIL_PICTURE"]["SRC"] ? $arItem["DETAIL_PICTURE"]["SRC"] : $arResult['CAP_PLUG_PICTURE'] ?>)"><span><?=$arItem["NAME"]?></span></a>
								</div>		
							<?php $key++; endforeach;?>
						</div>
          </div>
        </div>
  </div>
	
<?endif;?>