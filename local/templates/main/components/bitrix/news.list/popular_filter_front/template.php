<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(count($arResult["ITEMS"])):?>
	<div class="section section_light js-section">
        <div class="section-head">
          <div class="container">
						<div class="section-head__inner"><span class="section-head__title"><?=GetMessage("STYLE_REVIEW")?></span></div>
           </div>
         </div>
         <div class="section-tools">
           <div class="container">
             <div class="section-tools__inner js-section-tools"></div>

          </div>
        </div>
        <div class="container">
          <div class="review-carousel js-review-carousel">
		  	<div class="review-carousel__slide">
              	<div class="review-carousel__slide-inner">
		  			<?php $key = 0; foreach($arResult["ITEMS"] as $arItem):?>
						<?
						$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
						$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
						$capPicture = $arResult['CAP_PLUG_PICTURE'];
						if($key%6==0 || ((($key-1)%5==0 || $key==5) && $key!=1)) $capPicture = $arResult['CAP_WIDE_PICTURE'];
						?>
						<?= ($key%3==0 && $key!=0) ? '</div></div><div class="review-carousel__slide"><div class="review-carousel__slide-inner">' : ''?>
						<a id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="review-carousel__item <?= ($key%6==0 || ((($key-1)%5==0 || $key==5) && $key!=1)) ? 'review-carousel__item_big' : '';?>" href="<?=SITE_DIR;?><?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>" style="background-image:url(<?=$arItem["DETAIL_PICTURE"]["SRC"] ? $arItem["DETAIL_PICTURE"]["RESIZE"] : $capPicture;?>)"><span class="review-carousel__item-category"><?=$arItem["DETAIL_TEXT"];?></span><span class="review-carousel__item-title"><?=$arItem["NAME"]?></span></a>
					<?php $key++; endforeach;?>
				</div>
			</div>
					
          </div>
        </div>
	</div>
<?endif;?>