<?php
use HB\helpers\LangHelpers;
use HB\helpers\Helpers;
foreach($arResult["ITEMS"] as $key => &$arItem){
    $arResult["ITEMS"][$key] = LangHelpers::getTranslation($arItem);
    if($arItem["DETAIL_PICTURE"]){
        if($key%6==0 || ((($key-1)%5==0 || $key==5) && $key!=1)){
            $arItem["DETAIL_PICTURE"]["RESIZE"] = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]["ID"], array('width'=>600, 'height'=>280), BX_RESIZE_IMAGE_PROPORTIONAL)['src'];  
        }else{
            $arItem["DETAIL_PICTURE"]["RESIZE"] = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]["ID"], array('width'=>280, 'height'=>280), BX_RESIZE_IMAGE_PROPORTIONAL)['src'];  
        }
        
    }

}

$arSettings = HB\helpers\Helpers::getSiteSettings();

if($arSettings['CAP_WIDE_PICTURE']['VALUE']) $arResult['CAP_WIDE_PICTURE'] = CFile::ResizeImageGet($arSettings['CAP_WIDE_PICTURE']['VALUE'], array('width'=>600, 'height'=>280), BX_RESIZE_IMAGE_PROPORTIONAL)['src'];  

if($arSettings['CAP_PLUG_PICTURE']['VALUE']) $arResult['CAP_PLUG_PICTURE'] = CFile::ResizeImageGet($arSettings['CAP_PLUG_PICTURE']['VALUE'], array('width'=>280, 'height'=>280), BX_RESIZE_IMAGE_PROPORTIONAL)['src'];  
