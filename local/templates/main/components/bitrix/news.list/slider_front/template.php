<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<?if(count($arResult["ITEMS"])):?>
	<div class="main-carousel js-main-carousel">

		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="main-carousel__slide">
				<div class="main-carousel__slide-bg main-carousel__slide-bg_desktop" style="background-image:url(<?=$arItem["DETAIL_PICTURE"]["SRC"]?>)"></div>
				<div class="main-carousel__slide-bg main-carousel__slide-bg_tablet" style="background-image:url(<?=$arItem['PICTURE_TABLET'] ? $arItem['PICTURE_TABLET'] : $arItem["DETAIL_PICTURE"]["SRC"]?>)"></div>
				<div class="main-carousel__slide-bg main-carousel__slide-bg_mobile" style="background-image:url(<?=$arItem['PICTURE_MOBILE'] ? $arItem['PICTURE_MOBILE'] : $arItem["DETAIL_PICTURE"]["SRC"]?>)"></div>
				<div class="container">
					<div class="main-carousel__inner">
					<div class="main-carousel__content"><span class="main-carousel__text main-carousel__text_small"><?=$arItem["PROPERTIES"]["SUBTITLE"]["VALUE"]?></span></div>
					<div class="main-carousel__content"><span class="main-carousel__text main-carousel__text_big"><?=$arItem["NAME"]?></span></div>
					<?if($arItem["DETAIL_TEXT"]):?>
						<div class="main-carousel__content"><span class="main-carousel__text"><?=$arItem["DETAIL_TEXT"];?></span></div>
					<?endif;?>
					<div class="main-carousel__buttons"><a class="btn btn_alpha" href="<?=$arItem["PROPERTIES"]["BUTTON_LINK"]["VALUE"]?>"><?=$arItem["PROPERTIES"]["BUTTON_NAME"]["VALUE"]?></a></div>
					</div>
				</div>
			</div>
		<?endforeach;?>

	</div>
<?endif;?>
