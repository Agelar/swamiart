<?php
use HB\helpers\LangHelpers;
foreach($arResult["ITEMS"] as $key => $arItem){
    $arResult["ITEMS"][$key] = LangHelpers::getTranslation($arItem);
    if($arItem['PROPERTIES']['PICTURE_TABLET']['VALUE']) $arResult["ITEMS"][$key]['PICTURE_TABLET'] = CFile::ResizeImageGet($arItem['PROPERTIES']['PICTURE_TABLET']['VALUE'], array('width'=>1024, 'height'=>1024), BX_RESIZE_IMAGE_PROPORTIONAL)['src'];  
    if($arItem['PROPERTIES']['PICTURE_MOBILE']['VALUE']) $arResult["ITEMS"][$key]['PICTURE_MOBILE'] = CFile::ResizeImageGet($arItem['PROPERTIES']['PICTURE_MOBILE']['VALUE'], array('width'=>768, 'height'=>768), BX_RESIZE_IMAGE_PROPORTIONAL)['src']; 
}

