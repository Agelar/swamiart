<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="section section_light js-section">
    <div class="section-head">
        <div class="container">
            <div class="section-head__inner">
                <span class="section-head__title">
                    <div class="gallery__buttons section-head__button">
                        <a class="btn" href="/works/"><?=GetMessage('FIND_YOUR_PICTURE');?></a>
                    </div>
                </span>
            </div>
        </div>
    </div>
    <div class="container">
        <?foreach ($arResult["ITEMS"] as $arItem) {?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="guide">
                <div class="guide__row">
                    <div class="guide__item guide__item_big">
                        <?if ($arItem["PROPERTIES"]["VIDEO"]["VALUE"]) {?>
                            <a class="note-video js-note-video" style="background-image:url('<?=$arItem["DETAIL_PICTURE"]["SRC"] ? $arItem["DETAIL_PICTURE"]["SRC"] : '//img.youtube.com/vi/' . str_replace(array('https://www.youtube.com/watch?v=', 'www.youtube.com/watch?v='), array('', ''), $arItem["PROPERTIES"]["VIDEO"]["VALUE"]) . '/maxresdefault.jpg'?>')" data-fancybox="data-fancybox" href="<?=$arItem["PROPERTIES"]["VIDEO"]["VALUE"]?>">
                                <span class="note-video__title"><?=$arItem["PROPERTIES"]["VIDEO_DESC"]["VALUE"]?></span>
                            </a>
                        <?} else {?>
                            <a class="note-video without-video" style="background-image:url('<?=$arItem["DETAIL_PICTURE"]["SRC"]?>')"  href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>"><span class="note-video__title"><?=$arItem["PROPERTIES"]["VIDEO_DESC"]["VALUE"]?></span></a>
                        <?}?>
                    </div>
                    <div class="guide__item">
                        <div class="note">
                        <div class="note__content">
                            <a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>">
                                <img class="note__img" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt=""/>
                            </a>
                            <a class="note__category" href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>"> <span><?=$arItem["NAME"]?></span></a>
                            <a class="note__title" href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>"><?=$arItem["DETAIL_TEXT"]?></a></div>
                        </div>
                    </div>
                </div>
            </div>
        <?}?>
    </div>
</div>
