<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $USER;
if($USER->IsAuthorized()){
	LocalRedirect("/personal/");
}
elseif(!isset($_REQUEST["USER_CHECKWORD"]) && !isset($_REQUEST["USER_LOGIN"])){
	LocalRedirect("/");
}?>

<div class="container">
	<div class="row">
		<div class="col-12">
			<form class="form authentication-box authentication-box--repair" method="post" action="<?=$arResult["AUTH_FORM"]?>" name="bform">
				<?if (strlen($arResult["BACKURL"]) > 0): ?>
					<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
				<? endif ?>
				<input type="hidden" name="AUTH_FORM" value="Y">
				<input type="hidden" name="TYPE" value="CHANGE_PWD">
				<div class="authentication-box__content">
					<div class="row">
						<div class="col-12">
							<h1 class="lk-head"><?=GetMessage("AUTH_CHANGE_PASSWORD")?></h1>
						</div>
						<div class="form__row">
							<span class="form__row-title"><?=GetMessage("AUTH_EMAIL")?></span>
							<div class="form__row-main">
								<div class="form__field">
									<input class="form__input" type="text" name="USER_LOGIN" placeholder="<?=GetMessage("AUTH_EMAIL")?>" required="" maxlength="50" value="<?=$arResult["LAST_LOGIN"]?>"/>
								</div>
							</div>
						</div>
						<div class="form__row">
							<span class="form__row-title"><?=GetMessage("AUTH_CHECKWORD")?></span>
							<div class="form__row-main">
								<div class="form__field">
									<input class="form__input" type="password" name="USER_CHECKWORD" placeholder="<?=GetMessage("AUTH_CHECKWORD")?>" required="" maxlength="50" value="<?=$arResult["USER_CHECKWORD"]?>" />
								</div>
							</div>
						</div>
						<div class="form__row">
							<span class="form__row-title"><?=GetMessage("AUTH_NEW_PASSWORD")?></span>
							<div class="form__row-main">
								<div class="form__field">
									<input class="form__input" type="password" name="USER_PASSWORD" placeholder="<?=GetMessage("AUTH_NEW_PASSWORD")?>" maxlength="50" value="<?=$arResult["USER_PASSWORD"]?>" class="bx-auth-input" autocomplete="off" />
								</div>
							</div>
						</div>
						<div class="form__row">
							<span class="form__row-title"><?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?></span>
							<div class="form__row-main">
								<div class="form__field">
									<input class="form__input" type="password" name="USER_CONFIRM_PASSWORD" placeholder="<?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?>" maxlength="50" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" class="bx-auth-input" autocomplete="off" />
								</div>
							</div>
						</div>
					</div>
					<div class="form__row">
						<div class="row row--aic">
							<div class="col-lg-4">
								<div class="form__action">
									<button type="submit" class="btn btn_green btn_form btn_form--top-margin" name="change_pwd"><?=GetMessage("AUTH_CHANGE")?></button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	document.bform.USER_PASSWORD.focus();
</script>