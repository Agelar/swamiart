<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */
$this->setFrameMode(true);
$countElements = count($arResult['ITEMS']);
?>

    <div class="section section_border section_gray">
        
        <?if ($countElements) {?>
            <div class="container">
                <div class="section-head section-head__catalog">
                    <div class="section-head__inner"><h1 class="section-head__title"><?=$APPLICATION->ShowTitle(false);?></h1></div>
                </div>
                <div class="section-tools section-tools_static">
                    <div class="section-tools__inner js-section-tools">
                        <div class="l-select"><span class="l-select__title"><?=GetMessage('SORT');?></span>
                            <select class="l-select__input js-l-select-input js-sort__catalog" name="sort">
                                <option value="PROPERTY_FINAL_RATING" data-desc="ASC"><?=GetMessage('RATING');?></option>
                                <option value="PROPERTY_EXPERT_RATING"><?=GetMessage('RATING_EXP');?></option>
                                <option value="CATALOG_PRICE_1" data-desc="ASC"><?=GetMessage('PRICE');?></option>
                                <option value="CREATED"><?=GetMessage('CREATED');?></option>
                                <option value="PROPERTY_SIZE_SORT"><?=GetMessage('SIZE');?></option>
                                <option value="" class="opt-checked" selected><?=GetMessage('RATING');?></option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        <?}?>
        
        <div class="container">
            <div class="gallery">
                <div class="gallery__inner js-gallery" id="js-catalog__ajax" data-page="1" data-element_count="<?=$countElements?>" data-element_page_count="<?=$arParams["PAGE_ELEMENT_COUNT"]?>" data-max_count="<?=$arParams["MAX_COUNT_WORK"]?>" data-h1="<?=$APPLICATION->GetTitle();?>" data-title="<?=$APPLICATION->GetTitle(false); ?>" data-desc="<?=$APPLICATION->GetPageProperty("description");?>">
                    <div class="gallery__gutter-sizer js-gallery-gutter-sizer"></div>
                    <?if ($_REQUEST['AJAX_CATALOG']) { $GLOBALS['APPLICATION']->RestartBuffer(); }?>
                        <?if ($countElements) {?>
                            <?foreach ($arResult['ITEMS'] as $arItem) {?>
                                <div class="gallery__item js-gallery-item" itemscope="" itemtype="http://schema.org/Product">
                                    <div class="gallery-card">
                                        <a class="gallery-card__img lazy" href="<?=str_replace('/favorites/', '/works/', $arItem['DETAIL_PAGE_URL'])?>">
                                            <img data-height="<?=$arItem['PREVIEW_PICTURE']['HEIGHT'] ? $arItem['PREVIEW_PICTURE']['HEIGHT'] : $arItem['DETAIL_PICTURE']['HEIGHT'] ?>" 
                                                 data-width="<?=$arItem['PREVIEW_PICTURE']['WIDTH'] ? $arItem['PREVIEW_PICTURE']['WIDTH'] : $arItem['DETAIL_PICTURE']['WIDTH'] ?>" 
                                                 data-src="<?=$arItem['PREVIEW_PICTURE']['SRC'] ? $arItem['PREVIEW_PICTURE']['SRC'] : $arItem['DETAIL_PICTURE']['SRC'] ?>" alt="" itemprop="image"/>
                                        </a>
                                        <div class="gallery-card__content">
                                            <div class="gallery-card__block">
                                                <div class="gallery-card__labels">
                                                    <?= $arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT'] ? '<span class="gallery-card__label gallery-card__label_green">'.GetMessage('DISCOUNT').' '.$arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT'].'%</span>' : '';?>
                                                    <?= $arItem['PROPERTIES']['NEW']['VALUE'] ? '<span class="gallery-card__label gallery-card__label_red">'.GetMessage('NEW').'</span>' : '';?>
                                                </div>
                                                <a class="gallery-card__title" href="<?=str_replace('/favorites/', '/works/', $arItem['DETAIL_PAGE_URL'])?>"><?=$arItem['NAME']?></a>
                                                <a class="gallery-card__author" href="<?= $arResult['AUTHORS'][$arItem['PROPERTIES']['AUTHOR']['VALUE']]['DETAIL_PAGE_URL'];?>">
                                                    <?= $arResult['AUTHORS'][$arItem['PROPERTIES']['AUTHOR']['VALUE']]['NAME'];?>
                                                </a>
                                                <span class="gallery-card__desc">
                                                    <?=GetMessage('PICTURE');?><?= $arItem['PROPERTIES']['SIZE']['VALUE'] ? ', '.$arItem['PROPERTIES']['SIZE']['VALUE'].' '.GetMessage('CM').'.' : '';?>
                                                </span>
                                                <meta itemprop="name" content="<?=$arItem['NAME']?>">
                                                <?
                                                $description = "";
                                                if ($arResult['AUTHORS'][$arItem['PROPERTIES']['AUTHOR']['VALUE']]['NAME'])
                                                    $description .= $arResult['AUTHORS'][$arItem['PROPERTIES']['AUTHOR']['VALUE']]['NAME'].",";
                                                $description .= GetMessage('PICTURE');
                                                if ($arItem['PROPERTIES']['SIZE']['VALUE'])
                                                    $description .= ", ".$arItem['PROPERTIES']['SIZE']['VALUE']." ".GetMessage('CM');
                                                ?>
                                                <meta itemprop="description" content="<?=$description?>">
                                            </div>
                                            <div class="gallery-card__block">
                                                <div class="gallery-card__tools">
                                                    <meta itemprop="sku" content="<?=$arItem['PROPERTIES']['VENDOR_CODE']['VALUE']?>">
                                                    <meta itemprop="brand" content="<?=$arResult['AUTHORS'][$arItem['PROPERTIES']['AUTHOR']['VALUE']]['NAME']?>">
                                                    <div class="gallery-card__tools-block" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                                                        <span class="gallery-card__price"><?=number_format($arItem['PRICES']['BASE']['DISCOUNT_VALUE'], 0, ',', ' ');?> <span class="rubl">₽</span></span>
                                                        <?= $arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT'] ? '<span class="gallery-card__oldprice">'.number_format($arItem['PRICES']['BASE']['VALUE'], 0, ',', ' ').'</span>' : '';?>
                                                        <meta itemprop="price" content="<?=number_format($arItem['PRICES']['BASE']['DISCOUNT_VALUE'], 0, ',', '')?>">
                                                        <meta itemprop="priceCurrency" content="RUB">
                                                        <?if($arItem["PROPERTIES"]["WORK_PURCHASED"]["VALUE_XML_ID"] !== "Y"){?>
                                                        <link itemprop="availability" href="http://schema.org/InStock">
                                                        <?}?>
                                                        <?
                                                        /*
                                                        <?$_date = date("Y-m-d", MakeTimeStamp($arItem["DATE_ACTIVE_FROM"], "DD.MM.YYYY HH:MI:SS") + 60*60*24*31)?>
                                                        <meta itemprop="priceValidUntil" content="<?=$_date?>">
                                                        */
                                                        ?>
                                                        <link itemprop="url" href="<?=str_replace('/favorites/', '/works/', $arItem['DETAIL_PAGE_URL'])?>">
                                                    </div>
                                                    <div class="gallery-card__tools-block">
                                                        <a class="gallery-card__tools-item js-gallery-card-tool js-catalog__favorites <?=$arParams['FAVORITES'][$arItem['ID']] ? 'is-active' : '';?>" href="" data-id="<?=$arItem['ID']?>">
                                                            <svg class="icon icon-favorite">
                                                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/svg/sprite.svg#icon-favorite"></use>
                                                            </svg>
                                                        </a>
                                                        <a class="gallery-card__tools-item js-gallery-card-tool js-catalog__cart <?=$arParams['CART_WORK'][$arItem['ID']] ? 'is-active' : '';?>" data-id="<?=$arItem['ID']?>" <?=$arParams['CART_WORK'][$arItem['ID']] ? 'data-cartid="'.$arParams['CART_WORK'][$arItem['ID']].'"' : '';?> href="">
                                                            <svg class="icon icon-cart">
                                                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/svg/sprite.svg#icon-cart"></use>
                                                            </svg>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <?if ($arParams['USER_REGION'] && $arParams['USER_REGION'] == $arItem['PROPERTIES']['REGION']['VALUE']) {?>
                                                <div class="gallery-card__labels gallery-card__labels_region">
                                                    <span class="gallery-card__label gallery-card__label_black"><?=GetMessage('CLOSE_TO_YOU')?></span>
                                                </div>
                                            <?}?>
                                        </div>
                                    </div>
                                </div>
                            <?}?>
                        <?} else {?>
                            <p><?=GetMessage('NO_PICTURES_FOUND')?></p>
                        <?}?>
                    <?if ($_REQUEST['AJAX_CATALOG']) { die(); }?>
                </div>
                <?= $countElements<$arParams["MAX_COUNT_WORK"] ? '<div class="gallery__buttons"><a class="btn js-see__all_catalog" href="#">'.GetMessage('SEE_ALL').'</a></div>' : '<div class="gallery__buttons"><a class="btn js-see__all_catalog" href="#" style="display:none;">'.GetMessage('SEE_ALL').'</a></div>';?>
              </div>
        </div>
        <div class="preloader" style="display:none">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    
<script>
    window.onpopstate = function(event) {
        location.href = location.pathname;
    };
</script>


