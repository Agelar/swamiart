<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use HB\helpers\AuthorHelpers;
use HB\helpers\LangHelpers;
/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

global $arrFilter;

$arAuthorsId = array();
foreach ($arResult["ITEMS"] as $key => &$arItem) {
    if ($arItem['PROPERTIES']['HIDE']['VALUE_XML_ID'] == 'Y' && $arrFilter &&
        !(in_array($arItem['PROPERTIES']['THEME_FOR_UNHIDE']['VALUE'], $arrFilter['=PROPERTY_' . WORK_PROP_THEME_ID]) && $arrFilter['=PROPERTY_' . WORK_PROP_THEME_ID])) {
        unset($arResult["ITEMS"][$key]);
        continue;
    }

    //перевод работ
    $arResult["ITEMS"][$key] = LangHelpers::getTranslation($arItem);
    $arAuthorsId[$arItem['PROPERTIES']['AUTHOR']['VALUE']][] = $arItem['PROPERTIES']['AUTHOR']['VALUE'];
    if (!$arItem['PREVIEW_PICTURE'] || $arItem['PREVIEW_PICTURE']['WIDTH'] > 550) {
        $arItem['PREVIEW_PICTURE']['SRC'] = CFile::ResizeImageGet($arItem['DETAIL_PICTURE'], array('width' => 550, 'height' => 1024), BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'];
    }
}

//информация об авторах работ
$arResult["AUTHORS"] = AuthorHelpers::getAuthors($arAuthorsId);
