<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */
$this->setFrameMode(true);
if (!empty($arResult['NAV_RESULT']))
{
	$navParams =  array(
		'NavPageCount' => $arResult['NAV_RESULT']->NavPageCount,
		'NavPageNomer' => $arResult['NAV_RESULT']->NavPageNomer,
		'NavNum' => $arResult['NAV_RESULT']->NavNum
	);
}
else
{
	$navParams = array(
		'NavPageCount' => 1,
		'NavPageNomer' => 1,
		'NavNum' => $this->randString()
	);
}
$showLazyLoad = false;

if ($arParams['PAGE_ELEMENT_COUNT'] > 0 && $navParams['NavPageCount'] > 1)
{
	$showLazyLoad = $arParams['LAZY_LOAD'] === 'Y' && $navParams['NavPageNomer'] != $navParams['NavPageCount'];
}
?>
<div class="container">
	<div class="gallery">
		<div class="gallery__inner js-gallery" id="js-catalog__ajax">
			<div class="gallery__gutter-sizer js-gallery-gutter-sizer"></div>
			<!-- items-container -->
			<?foreach($arResult['ITEMS'] as $arItem):?>
				<div class="gallery__item js-gallery-item <?if($_REQUEST["action"]=="showMore"):?> js-ajax-item<?endif;?>">
					<div class="gallery-card">
						<a class="gallery-card__img" href="<?=$arItem['DETAIL_PAGE_URL']?>">
							<img src="<?=$arItem['PREVIEW_PICTURE']['SRC'] ? $arItem['PREVIEW_PICTURE']['SRC'] : $arItem['DETAIL_PICTURE']['SRC'] ?>" alt=""/>
						</a>
						<div class="gallery-card__content">
							<div class="gallery-card__block">
								<div class="gallery-card__labels">
									<?= $arItem['ITEM_PRICES'][0]['PERCENT'] ? '<span class="gallery-card__label gallery-card__label_green">'.GetMessage('DISCOUNT').' '.$arItem['ITEM_PRICES'][0]['PERCENT'].'%</span>' : '';?>
									<?= $arItem['PROPERTIES']['NEW']['VALUE'] ? '<span class="gallery-card__label gallery-card__label_red">'.GetMessage('NEW').'</span>' : '';?>
								</div>
								<a class="gallery-card__title" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a>
								<span class="gallery-card__desc"><?=GetMessage("PICTURE")?><?= $arItem['PROPERTIES']['SIZE']['VALUE'] ? ', '.$arItem['PROPERTIES']['SIZE']['VALUE'].' '.GetMessage('CM').'.' : '';?></span>
							</div>
							<div class="gallery-card__block">
								<div class="gallery-card__tools">
									<div class="gallery-card__tools-block">
										<?if($arItem['ITEM_PRICES'][0]['PERCENT']):?>
											<span class="gallery-card__price"><?=number_format($arItem['ITEM_PRICES'][0]['RATIO_DISCOUNT'], 0, ',', ' ');?> <span class="rubl">₽</span></span>
										<?else:?>
											<span class="gallery-card__price"><?=number_format($arItem['ITEM_PRICES'][0]['PRICE'], 0, ',', ' ');?> <span class="rubl">₽</span></span>
										<?endif;?>
										<?= $arItem['ITEM_PRICES'][0]['PERCENT'] ? '<span class="gallery-card__oldprice">'.number_format($arItem['ITEM_PRICES'][0]['PRICE'], 0, ',', ' ').'</span>' : '';?>
									</div>
									<div class="gallery-card__tools-block">
										<a class="gallery-card__tools-item js-gallery-card-tool js-catalog__favorites <?=$arParams['FAVORITES'][$arItem['ID']] ? 'is-active' : '';?>" href="" data-id="<?=$arItem['ID']?>">
											<svg class="icon icon-favorite">
												<use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/svg/sprite.svg#icon-favorite"></use>
											</svg>
										</a>
										<a class="gallery-card__tools-item js-gallery-card-tool js-catalog__cart <?=$arParams['CART_WORK'][$arItem['ID']] ? 'is-active' : '';?>" data-id="<?=$arItem['ID']?>" <?=$arParams['CART_WORK'][$arItem['ID']] ? 'data-cartid="'.$arParams['CART_WORK'][$arItem['ID']].'"' : '';?> href="">
											<svg class="icon icon-cart">
											<use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/svg/sprite.svg#icon-cart"></use>
											</svg>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?endforeach;?>
			<!-- items-container -->
		</div>
		<div class="js-pagination">
			<!-- pagination-container -->
			<?if($showLazyLoad):?>
				<div class="gallery__buttons">
					<a class="btn" data-use="show-more-<?=$navParams['NavNum']?>" href="#"><?=GetMessage('SEE_ALL')?></a>
				</div>
			<?endif;?>
			<!-- pagination-container -->
		</div>
	</div>
</div>
<?
$signer = new \Bitrix\Main\Security\Sign\Signer;
$signedTemplate = $signer->sign($templateName, 'catalog.section');
$signedParams = $signer->sign(base64_encode(serialize($arResult['ORIGINAL_PARAMETERS'])), 'catalog.section');
?>
<script>
	var sectionShowMore = new CShowMore({
		siteId: '<?=CUtil::JSEscape($component->getSiteId())?>',
		template: '<?=CUtil::JSEscape($signedTemplate)?>',
		parameters: '<?=CUtil::JSEscape($signedParams)?>',
		navParams: <?=CUtil::PhpToJSObject($navParams)?>,
		componentPath: '<?=CUtil::JSEscape($componentPath)?>',
	});
</script>
