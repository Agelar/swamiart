var CShowMore = function(data){
    this.formPosting = false;
    this.siteId = data.siteId;
    this.template = data.template;
    this.parameters = data.parameters;
    this.navParams = data.navParams;
    this.componentPath = data.componentPath;
    $(document).on("click",'[data-use="show-more-' + this.navParams.NavNum + '"]',$.proxy(this.showMore,this));
}

CShowMore.prototype.showMore = function(e){
    e.preventDefault();
    if (this.navParams.NavPageNomer < this.navParams.NavPageCount)
    {
        var data = {};
        data['action'] = 'showMore';
        data['PAGEN_' + this.navParams.NavNum] = parseInt(this.navParams.NavPageNomer) + 1;

        if (!this.formPosting)
        {
            this.formPosting = true;
            this.sendRequest(data);
        }
    }
}
CShowMore.prototype.sendRequest = function(data){
    var defaultData = {
        siteId: this.siteId,
        template: this.template,
        parameters: this.parameters
    };

    BX.ajax({
        url: this.componentPath + '/ajax.php' + (document.location.href.indexOf('clear_cache=Y') !== -1 ? '?clear_cache=Y' : ''),
        method: 'POST',
        dataType: 'json',
        timeout: 60,
        data: BX.merge(defaultData, data),
        onsuccess: BX.delegate(function(result){
            if (!result || !result.JS)
                return;
            BX.ajax.processScripts(
                BX.processHTML(result.JS).SCRIPT,
                false,
                BX.delegate(function(){this.showAction(result, data);}, this)
            );
        }, this)
    });
}

CShowMore.prototype.showAction = function(result, data){
    this.formPosting = false;
    
    if (result)
    {
        this.navParams.NavPageNomer++;
        $("#js-catalog__ajax").append(result.items);
        $(".js-pagination").html(result.pagination);

        $('.js-gallery').masonry('appended',$(".js-ajax-item"));
        $(".js-ajax-item").removeClass("js-ajax-item");
    }
}