<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use HB\helpers\LangHelpers;
use Bitrix\Main\Loader;
use HB\helpers\AuthorHelpers;
Loader::IncludeModule("hb.site");
/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();
$arAuthorsId = array();
foreach($arResult["ITEMS"] as $key => $arItem){
    $arResult["ITEMS"][$key] = LangHelpers::getTranslation($arItem);
}
