<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Loader;
use HB\helpers\AuthorHelpers;
use HB\helpers\Helpers;
use HB\helpers\LangHelpers;
use HB\helpers\SearchHelpers;

Loader::IncludeModule("hb.site");
/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();
$arAuthorsId = array();
foreach ($arResult["ITEMS"] as $key => $arItem) {
    if ($arItem['PROPERTIES']['HIDE']['VALUE_XML_ID'] == 'Y') {
        unset($arResult["ITEMS"][$key]);
        continue;
    }

    $arResult["ITEMS"][$key] = LangHelpers::getTranslation($arItem);
    $arAuthorsId[$arItem['PROPERTIES']['AUTHOR']['VALUE']][] = $arItem['PROPERTIES']['AUTHOR']['VALUE'];
}

$arResult["AUTHORS"] = AuthorHelpers::getAuthors($arAuthorsId);

$sort = new Helpers($arParams["SORT_ARRAY"]);
usort($arResult["ITEMS"], array($sort, "sortByID"));

foreach ($arResult["ITEMS"] as $key => $arItem) {
    if ($arResult['AUTHORS'][$arItem['PROPERTIES']['AUTHOR']['VALUE']]) {
        $name = SearchHelpers::searchMathes($arResult['AUTHORS'][$arItem['PROPERTIES']['AUTHOR']['VALUE']]["NAME"], $arParams["SEARCH_QUERY"]);
        $arResult["ITEMS"][$key]["AUTHORS_FORMAT"] = array(
            "NAME" => $name,
            "DETAIL_PAGE_URL" => $arResult['AUTHORS'][$arItem['PROPERTIES']['AUTHOR']['VALUE']]["DETAIL_PAGE_URL"],
        );
    }
    $arResult["ITEMS"][$key]["NAME"] = SearchHelpers::searchMathes($arItem["NAME"], $arParams["SEARCH_QUERY"]);
}
