<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */
$countElements = count($arResult['ITEMS']);
?>
<?if ($countElements) {?>
    <div class="section js-section">
        <div class="section-head">
            <div class="container">
                <div class="section-head__inner"><span class="section-head__title"><?=GetMessage('NEW_WORK')?></span></div>
            </div>
        </div>
        <div class="section-tools section-tools_tablet_static">
           <div class="container">
                <div class="section-tools__inner js-section-tools">
                    <div class="l-select"><span class="l-select__title"><?=GetMessage('SORT');?></span>
                        <select class="l-select__input js-l-select-input js-sort__front" name="sort">
                            <option value="PROPERTY_FINAL_RATING" data-desc="ASC"><?=GetMessage('RATING');?></option>
                            <option value="PROPERTY_EXPERT_RATING"><?=GetMessage('RATING_EXP');?></option>
                            <option value="CATALOG_PRICE_1" data-desc="ASC"><?=GetMessage('PRICE');?></option>
                            <option value="CREATED"><?=GetMessage('CREATED');?></option>
                            <option value="PROPERTY_SIZE_SORT"><?=GetMessage('SIZE');?></option>
                            <option value="" class="opt-checked" selected><?=GetMessage('RATING');?></option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="gallery">
                <div class="gallery__inner js-gallery" id="js-catalog__ajax" data-page="1" data-element_count="<?=$countElements?>" data-element_page_count="<?=$countElements?>" data-max_count="<?=$arParams["MAX_COUNT_WORK"]?>">
                    <div class="gallery__gutter-sizer js-gallery-gutter-sizer"></div>
                    <?foreach ($arResult['ITEMS'] as $itemKey => $arItem) {
                        switch ($itemKey) {
                            case 2:?>
                                <div class="gallery__stamp js-gallery-item hidden-mobile-down hidden-desktop-up">
                                    <div class="gallery__stamp-inner"><span class="gallery__stamp-text"><?=GetMessage('SMART_CHOICE')?></span>
                                        <a class="gallery__stamp-href" href="<?=SITE_DIR?>works/"><?=GetMessage('PICK_UP')?><div class="arrow"></div></a>
                                    </div>
                                </div>
                                <?break;
                            case 3:?>
                                <div class="gallery__stamp js-gallery-item hidden-tablet-down">
                                    <div class="gallery__stamp-inner">
                                        <span class="gallery__stamp-text"><?=GetMessage('SMART_CHOICE')?></span>
                                        <a class="gallery__stamp-href" href="<?=SITE_DIR?>works/"><?=GetMessage('PICK_UP')?><div class="arrow"></div></a>
                                    </div>
                                </div>
                                <?break;
                        }?>
                        <div class="gallery__item js-gallery-item">
                            <div class="gallery-card">
                                <a class="gallery-card__img lazy" href="<?=$arItem['DETAIL_PAGE_URL']?>">
                                    <img data-height="<?=$arItem['PREVIEW_PICTURE']['HEIGHT'] ? $arItem['PREVIEW_PICTURE']['HEIGHT'] : $arItem['DETAIL_PICTURE']['HEIGHT'] ?>" 
                                         data-width="<?=$arItem['PREVIEW_PICTURE']['WIDTH'] ? $arItem['PREVIEW_PICTURE']['WIDTH'] : $arItem['DETAIL_PICTURE']['WIDTH'] ?>" 
                                         data-src="<?=$arItem['PREVIEW_PICTURE']['SRC'] ? $arItem['PREVIEW_PICTURE']['SRC'] : $arItem['DETAIL_PICTURE']['SRC'] ?>" alt=""/>
                                </a>
                                <div class="gallery-card__content">
                                    <div class="gallery-card__block">
                                        <div class="gallery-card__labels">
                                            <?= $arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT'] ? '<span class="gallery-card__label gallery-card__label_green">'.GetMessage('DISCOUNT').' '.$arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT'].'%</span>' : '';?>
                                            <?= $arItem['PROPERTIES']['NEW']['VALUE'] ? '<span class="gallery-card__label gallery-card__label_red">'.GetMessage('NEW').'</span>' : '';?>
                                        </div>
                                        <a class="gallery-card__title" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a>
                                        <a class="gallery-card__author" href="<?= $arResult['AUTHORS'][$arItem['PROPERTIES']['AUTHOR']['VALUE']]['DETAIL_PAGE_URL'];?>">
                                            <?= $arResult['AUTHORS'][$arItem['PROPERTIES']['AUTHOR']['VALUE']]['NAME'];?>
                                        </a>
                                        <span class="gallery-card__desc">
                                            <?=GetMessage('PICTURE');?><?= $arItem['PROPERTIES']['SIZE']['VALUE'] ? ', '.$arItem['PROPERTIES']['SIZE']['VALUE'].' '.GetMessage('CM').'.' : '';?>
                                        </span>
                                    </div>
                                    <div class="gallery-card__block">
                                        <div class="gallery-card__tools">
                                            <div class="gallery-card__tools-block">
                                                <span class="gallery-card__price"><?=number_format($arItem['PRICES']['BASE']['DISCOUNT_VALUE'], 0, ',', ' ');?> <span class="rubl">₽</span></span>
                                                <?= $arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT'] ? '<span class="gallery-card__oldprice">'.number_format($arItem['PRICES']['BASE']['VALUE'], 0, ',', ' ').'</span>' : '';?>
                                            </div>
                                            <div class="gallery-card__tools-block">
                                                <a class="gallery-card__tools-item js-gallery-card-tool js-catalog__favorites <?=$arParams['FAVORITES'][$arItem['ID']] ? 'is-active' : '';?>" href="" data-id="<?=$arItem['ID']?>">
                                                    <svg class="icon icon-favorite">
                                                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/svg/sprite.svg#icon-favorite"></use>
                                                    </svg>
                                                </a>
                                                <a class="gallery-card__tools-item js-gallery-card-tool js-catalog__cart <?=$arParams['CART_WORK'][$arItem['ID']] ? 'is-active' : '';?>" data-id="<?=$arItem['ID']?>" <?=$arParams['CART_WORK'][$arItem['ID']] ? 'data-cartid="'.$arParams['CART_WORK'][$arItem['ID']].'"' : '';?> href="">
                                                    <svg class="icon icon-cart">
                                                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/svg/sprite.svg#icon-cart"></use>
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <?if ($arParams['USER_REGION'] && $arParams['USER_REGION'] == $arItem['PROPERTIES']['REGION']['VALUE']) {?>
                                        <div class="gallery-card__labels gallery-card__labels_region">
                                            <span class="gallery-card__label gallery-card__label_black"><?=GetMessage('CLOSE_TO_YOU')?></span>
                                        </div>
                                    <?}?>
                                </div>
                            </div>
                        </div>
                    <?}?>              
                </div>
                <?= '<div class="gallery__buttons"><a class="btn" href="'.SITE_DIR.'works/">'.GetMessage('SEE_ALL').'</a></div>'?>
            </div>
        </div>
        <div class="preloader" style="display:none">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
<?}?>