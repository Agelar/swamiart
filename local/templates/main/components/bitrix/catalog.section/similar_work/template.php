<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */
$this->setFrameMode(true);
$countElements = count($arResult['ITEMS']);
?>
<?if($countElements):?>
	<div class="section js-section section_border section_gray">
        <div class="section-head">
          <div class="container">
            <div class="section-head__inner"><span class="section-head__title"><?=GetMessage('SIMILAR_WORK')?></span></div>
          </div>
        </div>
        <div class="section-tools section-tools_inner-page">
          <div class="container">
            <div class="section-tools__inner js-section-tools"></div>
          </div>
        </div>
        <div class="container">
          	<div class="similar-pictures js-similar-pictures">
				<?foreach($arResult['ITEMS'] as $arItem):?>
					<div class="similar-pictures__slide">
						
							<div class="gallery-card"><a class="gallery-card__img" href="<?=$arItem['DETAIL_PAGE_URL']?>"><img src="<?=$arItem['PREVIEW_PICTURE']['SRC'] ? $arItem['PREVIEW_PICTURE']['SRC'] : $arItem['DETAIL_PICTURE']['SRC'] ?>" alt=""/></a>
								<div class="gallery-card__content">
									<div class="gallery-card__block">
												<div class="gallery-card__labels">
													<?= $arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT'] ? '<span class="gallery-card__label gallery-card__label_green">'.GetMessage('DISCOUNT').' '.$arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT'].'%</span>' : '';?>
													<?= $arItem['PROPERTIES']['NEW']['VALUE'] ? '<span class="gallery-card__label gallery-card__label_red">'.GetMessage('NEW').'</span>' : '';?>
												</div><a class="gallery-card__title" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a><a class="gallery-card__author" href="<?= $arResult['AUTHORS'][$arItem['PROPERTIES']['AUTHOR']['VALUE']]['DETAIL_PAGE_URL'];?>"><?= $arResult['AUTHORS'][$arItem['PROPERTIES']['AUTHOR']['VALUE']]['NAME'];?></a><span class="gallery-card__desc"><?=GetMessage('PICTURE');?><?= $arItem['PROPERTIES']['SIZE']['VALUE'] ? ', '.$arItem['PROPERTIES']['SIZE']['VALUE'].' '.GetMessage('CM').'.' : '';?></span>
									</div>
									<div class="gallery-card__block">
												<div class="gallery-card__tools">
													<div class="gallery-card__tools-block">
														<span class="gallery-card__price"><?=number_format($arItem['PRICES']['BASE']['DISCOUNT_VALUE'], 0, ',', ' ');?> <span class="rubl">₽</span></span>
														<?= $arItem['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT'] ? '<span class="gallery-card__oldprice">'.number_format($arItem['PRICES']['BASE']['VALUE'], 0, ',', ' ').'</span>' : '';?>
													</div>
													<div class="gallery-card__tools-block"><a class="gallery-card__tools-item js-gallery-card-tool js-catalog__favorites <?=$arParams['FAVORITES'][$arItem['ID']] ? 'is-active' : '';?>" href="" data-id="<?=$arItem['ID']?>">
														<svg class="icon icon-favorite">
														<use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/svg/sprite.svg#icon-favorite"></use>
														</svg></a><a class="gallery-card__tools-item js-gallery-card-tool js-catalog__cart <?=$arParams['CART_WORK'][$arItem['ID']] ? 'is-active' : '';?>" data-id="<?=$arItem['ID']?>" <?=$arParams['CART_WORK'][$arItem['ID']] ? 'data-cartid="'.$arParams['CART_WORK'][$arItem['ID']].'"' : '';?> href="">
														<svg class="icon icon-cart">
														<use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/svg/sprite.svg#icon-cart"></use>
														</svg></a></div>
												</div>
									</div>
									<?if($arParams['USER_REGION'] && $arParams['USER_REGION']==$arItem['PROPERTIES']['REGION']['VALUE']):?>
														<div class="gallery-card__labels gallery-card__labels_region">
															<span class="gallery-card__label gallery-card__label_black"><?=GetMessage('CLOSE_TO_YOU')?></span>
														</div>
									<?endif;?>
								</div>
							</div>
						
					</div>
				<?endforeach;?>
            
            
			</div>
        </div>
	</div>
<?endif;?>