<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use HB\helpers\LangHelpers;
use HB\helpers\AuthorHelpers;
/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();


$arAuthorsId = array();
foreach($arResult["ITEMS"] as $key => &$arItem){
    //перевод работ
    $arResult["ITEMS"][$key] = LangHelpers::getTranslation($arItem);
    $arAuthorsId[$arItem['PROPERTIES']['AUTHOR']['VALUE']][] = $arItem['PROPERTIES']['AUTHOR']['VALUE'];
    if(!$arItem['PREVIEW_PICTURE'] || $arItem['PREVIEW_PICTURE']['WIDTH']>550){
        $arItem['PREVIEW_PICTURE']['SRC'] = CFile::ResizeImageGet($arItem['DETAIL_PICTURE'], array('width'=>550, 'height'=>1024), BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'];
    }
}
//информация об авторах работ
$arResult["AUTHORS"] = AuthorHelpers::getAuthors($arAuthorsId);

