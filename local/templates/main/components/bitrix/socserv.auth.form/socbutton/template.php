
<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); 

use HB\helpers\Helpers;
?>
<div class="popup__media">
	<span class="popup__table-heading"><?=GetMessage("HEADER_SOCSERV")?></span>
	<?foreach($arParams["~AUTH_SERVICES"] as $service):?>
		<a class="popup__media-btn" href="#" onclick="<?=$service["ONCLICK"]?>">
			<svg class="icon <?=$service["CLASS_ICON"]?>">
				<use xlink:href="<?=$service["SVG_ID"]?>"></use>
			</svg>
			<div class="popup__media-btn-text"><?=$service["NAME"]?></div>
		</a>
	<?endforeach;?>
	<!-- <span class="popup__table-heading  popup__table-heading--small"><?//=Helpers::getAuthTerms()?></span> -->
</div>