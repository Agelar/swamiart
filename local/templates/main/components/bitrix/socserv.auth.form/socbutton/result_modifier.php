<?
foreach($arParams["~AUTH_SERVICES"] as &$service){
    switch($service["ID"]){
        case "Facebook":
            $service["CLASS_ICON"] = "social-fb-w";
            $service["SVG_ID"] = "#social-fb";
            $service["NAME"] = "Facebook";
        break;
        case "VKontakte":
            $service["CLASS_ICON"] = "social-vk-w";
            $service["SVG_ID"] = "#social-vk";
            $service["NAME"] = "VKontakte";
        break;
        case "GoogleOAuth":
            $service["CLASS_ICON"] = "social-google-w";
            $service["SVG_ID"] = "#social-google";
            $service["NAME"] = "Google";
        break;
        case "Odnoklassniki":
            $service["CLASS_ICON"] = "social-ok-w";
            $service["SVG_ID"] = "#social-ok";
            $service["NAME"] = "Odnoklassniki";
        break;
    }
}
?>