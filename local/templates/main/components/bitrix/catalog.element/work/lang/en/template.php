<?
$MESS["BACK"] = "Back";
$MESS["HOME"] = "Home";
$MESS["CATALOG"] = "Catalog";
$MESS["TO_FAVORITES"] = "To&nbsp;favorites";
$MESS["IN_FAVORITES"] = "In&nbsp;favorites";
$MESS["LIKED"] = "Liked";
$MESS["DIGITAL_COPY"] = "Digital copy";
$MESS["NOT_SHOW"] = "Do not show the picture again";
$MESS["PAINTING_SIZE"] = "Painting size:";
$MESS["CM"] = "cm";
$MESS["STYLE"] = "Style:";
$MESS["ADDED_BY"] = "Go to cart";
$MESS["TO_ADD"] = "Add";
$MESS["IN_THE_BASKET"] = "to cart";
$MESS["DO_NOT_SHOW"] = "Do not show";
$MESS["DOWNLOAD_COPY"] = "Download copy";
$MESS["PARAM"] = "Parameters of the picture";
$MESS["DESC"] = "Description of the picture";
$MESS["KG"] = "kg";
$MESS["RETURN_PICTURE"] = "Return the picture for viewing";
$MESS["RETURN_PICTURE_MOBILE"] = "Show";
$MESS["VENDOR_CODE"] = "Vendor code";
$MESS["RATED_PICTURE"] = "You have already rated the picture";
$MESS["RATE_THIS_PICTURE"] = "Rate this picture";
?>