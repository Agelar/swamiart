<?
$MESS["BACK"] = "Назад";
$MESS["HOME"] = "Главная";
$MESS["CATALOG"] = "Каталог";
$MESS["TO_FAVORITES"] = "В&nbsp;избранное";
$MESS["IN_FAVORITES"] = "В&nbsp;избранном";
$MESS["LIKED"] = "Понравилось";
$MESS["DIGITAL_COPY"] = "Цифровая копия";
$MESS["NOT_SHOW"] = "Больше не показывать картину";
$MESS["PAINTING_SIZE"] = "Размер картины:";
$MESS["CM"] = "см";
$MESS["STYLE"] = "Стиль:";
$MESS["ADDED_BY"] = "Перейти в корзину";
$MESS["TO_ADD"] = "Добавить";
$MESS["IN_THE_BASKET"] = "в корзину";
$MESS["DO_NOT_SHOW"] = "Не показывать";
$MESS["DOWNLOAD_COPY"] = "Скачать копию";
$MESS["PARAM"] = "Параметры картины";
$MESS["DESC"] = "Описание картины";
$MESS["KG"] = "кг";
$MESS["RETURN_PICTURE"] = "Вернуть картину для просмотра";
$MESS["RETURN_PICTURE_MOBILE"] = "Показать";
$MESS["VENDOR_CODE"] = "Арт.";
$MESS["RATED_PICTURE"] = "Вы уже оценили картину";
$MESS["RATE_THIS_PICTURE"] = "Оцените картину";
?>