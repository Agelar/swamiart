<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

?>
<div class="section breadcrumbs breadcrumbs_important">
	<div class="container">
		<div class="breadcrumbs__inner"><a class="breadcrumbs__back" href="#" onclick="history.go(-1); return false;">
				<div class="breadcrumbs__back-icon">
					<svg class="icon arrow_smart-w">
						<use xlink:href="#arrow_smart"></use>
					</svg>
				</div>
				<div class="breadcrumbs__back-text"><?=GetMessage('BACK');?></div></a>
			<div class="breadcrumbs__history">
				<a class="breadcrumbs__history-link" data-type="home" href="<?=SITE_DIR?>"><?=GetMessage('HOME');?></a>
				<a class="breadcrumbs__history-link" data-type="catalog" href="<?=SITE_DIR?>works/"><?=GetMessage('CATALOG');?></a>
				<div class="breadcrumbs__history-current"><?=$arResult['NAME'];?></div>
			</div>
		</div>
	</div>
</div>
<div class="section">
	<div class="container" itemscope="" itemtype="http://schema.org/Product">
		<div class="picture js-picture">
		
			<div class="picture__content">
			<div class="picture__content__header">
				<span class="picture__title inlbl ml__title"><?=$arResult['AUTHOR']["NAME"] . " - "?></span><h1 class="picture__title inlbl" itemprop="name"><?=$arResult['NAME'];?></h1>
			</div>
				<div class="picture__row picture__row_justify hidden-mobile-down">
					<?if($arResult['PROPERTIES']['VENDOR_CODE']['VALUE']):?>
						<div><span class="picture__code"><?=GetMessage('VENDOR_CODE');?> <?=$arResult['PROPERTIES']['VENDOR_CODE']['VALUE']?></span></div>
					<?endif;?>
					<div><a class="link link_icon js-catalog__favorites <?=$arParams['FAVORITES'][$arResult['ID']] ? 'is-active' : '';?>" href="" data-id="<?=$arResult['ID']?>" href="#" data-in_favorites="<?=GetMessage('IN_FAVORITES')?>" data-to_favorites="<?=GetMessage('TO_FAVORITES')?>">
							<svg class="icon icon-favorite">
								<use xlink:href="#icon-favorite"></use>
							</svg><span class="js-favorites__text"><?=$arParams['FAVORITES'][$arResult['ID']] ? GetMessage('IN_FAVORITES') : GetMessage('TO_FAVORITES');?></span></a></div>
				</div>
				<div class="picture__row"><a class="picture__author" href="<?=$arResult["AUTHOR"]["DETAIL_PAGE_URL"]?>"><?=$arResult["AUTHOR"]["NAME"]?></a></div>
			</div>
			<div class="picture__aside">
				<?if($arResult["PICTURE"]) {?>
					<div class="picture__carousels">
						<div class="picture__carousel picture__carousel_main js-picture-carousel-main">
							<?foreach($arResult["PICTURE"] as $arImg):?>
								<div>
									<div class="picture__carousel-slide js-picture-carousel-slide"><img src="<?=$arImg['src']?>" alt="" itemprop="image"/></div>
								</div>
							<?endforeach;?>
							<?if($arResult["YOUTUBE_VIDEO"] != '') {
								foreach ($arResult["YOUTUBE_VIDEO"] as $video_url){?>
									<div>
										<a class="video_fancybox" data-fancybox-type="iframe" href="https://www.youtube.com/embed/<?=$video_url?>">
											<div class="picture__carousel-slide js-picture-carousel-slide">
												<img src="<?='//img.youtube.com/vi/'.$video_url.'/maxresdefault.jpg'?>" alt="" itemprop="image"/>
											</div>
										</a>  
									</div>                                    
								<?}
							}?>
							<?foreach ($arResult["INTERIORS"] as $interiorKey => $arInterior) {?>
								<div>
									<div class="picture__carousel-slide js-picture-carousel-slide">
										<img src="<?=$arInterior['PREVIEW_PICTURE']?>" alt="Интерьер #<?=($interiorKey+1)?>" itemprop="image"/>
										<div class="picture__carousel_above" 
											data-img-width="<?=$arInterior['PROPERTY_WIDTH_VALUE']?>" 
											data-img-top="<?=$arInterior['PROPERTY_MARGIN_TOP_VALUE']?>" 
											data-img-left="<?=$arInterior['PROPERTY_MARGIN_LEFT_VALUE']?>">
											<img src="<?=$arResult['PICTURE'][0]['src']?>" alt="<?=$arResult['NAME'].' Фото #1'?>"/>
										</div>
									</div>
								</div>
							<?}?>
						</div>
						<div class="picture__carousel picture__carousel_aside js-picture-carousel-aside">
							<?foreach($arResult["PICTURE"] as $arImg):?>
								<div>
									<div class="picture__carousel-slide js-picture-carousel-slide" style="background-image:url(<?=$arImg['src']?>)"></div>
								</div>
							<?endforeach;?>
							<?if($arResult["YOUTUBE_VIDEO"] != '') {
								foreach ($arResult["YOUTUBE_VIDEO"] as $video_url){?>
									<div>
										<div class="picture__carousel-slide js-picture-carousel-slide" style="background-image:url(<?='//img.youtube.com/vi/'.$video_url.'/maxresdefault.jpg'?>)"></div>
									</div>                                    
								<?}
							}?>
							<?foreach ($arResult["INTERIORS"] as $interiorKey => $arInterior) {?>
								<div>
									<div class="picture__carousel-slide js-picture-carousel-slide" style="background-image:url('<?=$arInterior['PREVIEW_PICTURE_SMALL']['src']?>')">
										<div class="picture__carousel_above" 
											data-img-width="<?=$arInterior['PROPERTY_WIDTH_VALUE']?>" 
											data-img-top="<?=$arInterior['PROPERTY_MARGIN_TOP_VALUE']?>" 
											data-img-left="<?=$arInterior['PROPERTY_MARGIN_LEFT_VALUE']?>">
											<img src="<?=$arResult["PICTURE"][0]['src']?>" alt="Интерьер #<?=($interiorKey+1)?>"/>
										</div>
									</div>
								</div>
							<?}?>
						</div>
						<div class="picture__zoom js-picture-zoom">
							<svg class="icon icon-zoom">
								<use xlink:href="#icon-zoom"></use>
							</svg>
						</div>
					</div>
				<?} elseif($arResult["YOUTUBE_VIDEO"]) {?>
					<div class="picture__carousels">
						<div class="picture__carousel picture__carousel_main js-picture-carousel-main">
							<?if($arResult["YOUTUBE_VIDEO"] != '') {
								foreach ($arResult["YOUTUBE_VIDEO"] as $video_url){?>
									<div>
										<a class="video_fancybox" data-fancybox-type="iframe" href="https://www.youtube.com/embed/<?=$video_url?>">
											<div class="picture__carousel-slide js-picture-carousel-slide">
												<img src="<?='//img.youtube.com/vi/'.$video_url.'/maxresdefault.jpg'?>" alt="" itemprop="image"/>
											</div>
										</a>  
									</div>                                    
								<?}
							}?>
						</div>
						<div class="picture__carousel picture__carousel_aside js-picture-carousel-aside">
							<?if($arResult["YOUTUBE_VIDEO"] != '') {
								foreach ($arResult["YOUTUBE_VIDEO"] as $video_url){?>
									<div>
										<div class="picture__carousel-slide js-picture-carousel-slide" style="background-image:url(<?='//img.youtube.com/vi/'.$video_url.'/maxresdefault.jpg'?>)"></div>
									</div>                                    
								<?}
							}?>
						</div>
						<div class="picture__zoom js-picture-zoom">
							<svg class="icon icon-zoom">
								<use xlink:href="#icon-zoom"></use>
							</svg>
						</div>
					</div>
				<?}?>
				<div class="picture__tools hidden-mobile-down">
					<div class="picture__tools-row">
						<?if($arResult["USER_EMAIL_CONFIRM"]):?>
							<a class="link link_icon js-work__like <?=$arParams['LIKES'][$arResult['ID']] ? 'is-active' : '';?>" href="" data-id="<?=$arResult['ID']?>" href="#">
								<svg class="icon icon-like">
								<use xlink:href="#icon-like"></use>
								</svg><span><?=GetMessage('LIKED');?> (<span class="js-work__count_likes"><?=($arResult['PROPERTIES']['COUNT_LIKES']['VALUE'] ? $arResult['PROPERTIES']['COUNT_LIKES']['VALUE'] : 0)?></span>)</span>
							</a>
						<?endif;?>
							<?if($arResult["DIGITAL_COPY"]):?>
								<?if($arParams["PAY_DIGITAL_COPY"] && $arParams["PAY_DIGITAL_COPY"][$arResult["ID"]]):?>
									<a class="link link_icon is-active" href="<?=SITE_DIR?>personal/download_digital_copy.php?work_id=<?=$arResult["ENCODE_ID"];?>&file_id=<?=$arResult["ENCODE_DIGITAL_COPY"];?>" target="_blank">
										<svg class="icon icon-download">
											<use xlink:href="#icon-download"></use>
										</svg><?=GetMessage('DIGITAL_COPY');?>
									</a>
								<?else:?>
									<?if($arResult['ORDER_DIGITAL_COPY']):?>
										<a class="link link_icon js-work_digital_copy_link" href="<?=SITE_DIR?>personal/order-list/?ID=<?=$arResult["ORDER_DIGITAL_COPY"]?>" target="_blank">
											<svg class="icon icon-download">
												<use xlink:href="#icon-download"></use>
											</svg><span><?=GetMessage('DIGITAL_COPY');?> <?= $arResult['DIGITAL_COPY_PRICE'] ? '('.$arResult['DIGITAL_COPY_PRICE'].' <span class="rubl">₽</span>)' : '' ?></span>
										</a>
									<?else:?>
										<a class="link link_icon js-work_digital_copy_link" href="<?=SITE_DIR?>personal/order-digital-copy/?work_id=<?=$arResult["ID"]?>">
											<svg class="icon icon-download">
												<use xlink:href="#icon-download"></use>
											</svg><span><?=GetMessage('DIGITAL_COPY');?> <?= $arResult['DIGITAL_COPY_PRICE'] ? '('.$arResult['DIGITAL_COPY_PRICE'].' <span class="rubl">₽</span>)' : '' ?></span>
										</a>
									<?endif;?>
								<?endif;?>
							<?endif;?>
					</div>
					<div class="picture__tools-row">
						<a class="link link_icon js-work__not_show <?=$arParams['NOT_SHOW'][$arResult['ID']] ? 'is-active' : '';?>" href="" data-id="<?=$arResult['ID']?>" href="#" data-not_show="<?=GetMessage('NOT_SHOW')?>" data-return_picture="<?=GetMessage('RETURN_PICTURE')?>">
							<svg class="icon icon-eye-close">
							<use xlink:href="#icon-eye-close"></use>
							</svg><span class="js-not_show_text"><?=$arParams['NOT_SHOW'][$arResult['ID']] ? GetMessage('RETURN_PICTURE') : GetMessage('NOT_SHOW');?></span>
						</a>
					</div>
				</div>
			</div>
			<div class="picture__content">
				<?if($arResult['PROPERTIES']['VENDOR_CODE']['VALUE']):?>
					<div class="picture__row picture__row_justify hidden-tablet-up">
						<div><span class="picture__code"><?=GetMessage('VENDOR_CODE');?> <?=$arResult['PROPERTIES']['VENDOR_CODE']['VALUE']?></span></div>
					</div>
				<?endif;?>
					<div class="picture__brief">
			<?if($arResult['PROPERTIES']['SIZE']['VALUE']):?>
			<span class="picture__brief-item"><?=GetMessage('PAINTING_SIZE');?> <span><?=$arResult['PROPERTIES']['SIZE']['VALUE']?> <?=GetMessage('CM');?></span></span>
		<?endif;?>
		<?if($arResult['PROPERTIES']['STYLE']['VALUE']):?>
			<span class="picture__brief-item"><?=GetMessage('STYLE');?> 
				<a href="<?=SITE_DIR?>works/pictures/filter/style-is-<?=$arResult["STYLE"][$arResult['PROPERTIES']['STYLE']['VALUE']]["CODE"]?>/" target="_blank">
					<?=$arResult["STYLE"][$arResult['PROPERTIES']['STYLE']['VALUE']]["NAME"]?>
				</a>
			</span>
		<?endif;?>
	</div>
				<div class="picture__row picture__row_justify">
					
					<meta itemprop="sku" content="<?=$arResult['PROPERTIES']['VENDOR_CODE']['VALUE']?>">
					<meta itemprop="brand" content="<?=$arResult["AUTHOR"]["NAME"]?>">
					<div class="fs0" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
						<div class="picture__price"><span><?=number_format($arResult['PRICES']['BASE']['DISCOUNT_VALUE'], 0, ',', ' ');?> <span class="rubl">₽</span></span><?= $arResult['PRICES']['BASE']['DISCOUNT_DIFF_PERCENT'] ? '<span>'.number_format($arResult['PRICES']['BASE']['VALUE'], 0, ',', ' ').'</span>' : '';?></div>
						<meta itemprop="price" content="<?=number_format($arResult['PRICES']['BASE']['DISCOUNT_VALUE'], 0, ',', '')?>">
						<meta itemprop="priceCurrency" content="RUB">
						<?if($arResult["PROPERTIES"]["WORK_PURCHASED"]["VALUE_XML_ID"] !== "Y"){?>
						<link itemprop="availability" href="http://schema.org/InStock">
						<?}?>
						<link itemprop="url" href="<?=str_replace('/favorites/', '/works/', $arResult['DETAIL_PAGE_URL'])?>">
					</div>
					<div>
						<a class="picture__btn  <?=$arParams['CART_WORK'][$arResult['ID']] ? 'is-active' : 'js-work__cart';?>" data-id="<?=$arResult['ID']?>" <?=$arParams['CART_WORK'][$arResult['ID']] ? 'data-cartid="'.$arParams['CART_WORK'][$arResult['ID']].'"' : '';?> <?=$arParams['CART_WORK'][$arResult['ID']] ? 'href="'.SITE_DIR.'personal/cart/"' : 'href="'.SITE_DIR.'personal/cart/"';?> data-added_by="<?=GetMessage('ADDED_BY');?>" data-to_add="<?=GetMessage('TO_ADD');?>" data-in_the_basket="<?=GetMessage('IN_THE_BASKET');?>">
							<?if($arParams['CART_WORK'][$arResult['ID']]):?>
							<?=GetMessage('ADDED_BY');?>
							<?else:?>
								<span class="hidden-tablet-down"><?=GetMessage('TO_ADD');?> </span><?=GetMessage('IN_THE_BASKET');?>
							<?endif;?>
						</a>
					</div>
				</div>
				<div class="picture__tools hidden-tablet-up">
					<?if($arResult["USER_IS_AUTH"]):?>
						<a class="link link_icon js-work__like <?=$arParams['LIKES'][$arResult['ID']] ? 'is-active' : '';?>" href="" data-id="<?=$arResult['ID']?>" href="#">
							<svg class="icon icon-like">
								<use xlink:href="#icon-like"></use>
							</svg><?=GetMessage('LIKED');?>
						</a>
					<?endif;?>
					<a class="link link_icon js-catalog__favorites <?=$arParams['FAVORITES'][$arResult['ID']] ? 'is-active' : '';?>" href="" data-id="<?=$arResult['ID']?>" href="#" data-in_favorites="<?=GetMessage('IN_FAVORITES')?>" data-to_favorites="<?=GetMessage('TO_FAVORITES')?>">
						<svg class="icon icon-favorite">
							<use xlink:href="#icon-favorite"></use>
						</svg><span class="js-favorites__text"><?=$arParams['FAVORITES'][$arResult['ID']] ? GetMessage('IN_FAVORITES') : GetMessage('TO_FAVORITES');?></span>
					</a>
					<a class="link link_icon js-work__not_show <?=$arParams['NOT_SHOW'][$arResult['ID']] ? 'is-active' : '';?>" href="" data-id="<?=$arResult['ID']?>" href="#" data-not_show="<?=GetMessage('DO_NOT_SHOW')?>" data-return_picture="<?=GetMessage('RETURN_PICTURE_MOBILE')?>">
						<svg class="icon icon-eye-close">
							<use xlink:href="#icon-eye-close"></use>
						</svg><span class="js-not_show_text"><?=$arParams['NOT_SHOW'][$arResult['ID']] ? GetMessage('RETURN_PICTURE_MOBILE') : GetMessage('DO_NOT_SHOW');?></span>
					</a>
					
						<?if($arResult["DIGITAL_COPY"]):?>
							<?if($arParams["PAY_DIGITAL_COPY"] && $arParams["PAY_DIGITAL_COPY"][$arResult["ID"]]):?>
								<a class="link link_icon" href="<?=SITE_DIR?>personal/download_digital_copy.php?work_id=<?=$arResult["ENCODE_ID"];?>&file_id=<?=$arResult["ENCODE_DIGITAL_COPY"];?>">
									<svg class="icon icon-download">
										<use xlink:href="#icon-download"></use>
									</svg><?=GetMessage('DOWNLOAD_COPY');?>
								</a>
							<?else:?>
								<?if($arResult['ORDER_DIGITAL_COPY']):?>
										<a class="link link_icon" href="<?=SITE_DIR?>personal/order-list/?ID=<?=$arResult["ORDER_DIGITAL_COPY"]?>" target="_blank">
											<svg class="icon icon-download">
												<use xlink:href="#icon-download"></use>
											</svg><span><?=GetMessage('DOWNLOAD_COPY');?> <?= $arResult['DIGITAL_COPY_PRICE'] ? '('.$arResult['DIGITAL_COPY_PRICE'].'&nbsp;<span class="rubl">₽</span>)' : '' ?></span>
										</a>
								<?else:?>
									<a class="link link_icon" href="<?=SITE_DIR?>personal/order-digital-copy/?work_id=<?=$arResult["ID"]?>">
										<svg class="icon icon-download">
											<use xlink:href="#icon-download"></use>
										</svg><span><?=GetMessage('DOWNLOAD_COPY');?> <?= $arResult['DIGITAL_COPY_PRICE'] ? '('.$arResult['DIGITAL_COPY_PRICE'].'&nbsp;<span class="rubl">₽</span>)' : '' ?></span>
									</a>
								<?endif;?>
							<?endif;?>
						<?endif;?>
					
					
					
				</div>
				<div class="picture__row picture__row_end">
				<!--noindex-->
					<div class="social">
						<a class="social__link js-social-link" href="<?=$arResult["SOCIAL"]["VK"];?>" target="_blank">
							<svg class="icon social-vk">
								<use xlink:href="#social-vk"></use>
							</svg>
						</a>
						<a class="social__link js-social-link" href="<?=$arResult["SOCIAL"]["FACEBOOK"];?>" target="_blank">
							<svg class="icon social-fb">
								<use xlink:href="#social-fb"></use>
							</svg>
						</a>
						<a class="social__link js-social-link" href="<?=$arResult["SOCIAL"]["OK"];?>" target="_blank">
							<svg class="icon social-ok">
								<use xlink:href="#social-ok"></use>
							</svg>
						</a>
						<a class="social__link js-social-link" href="<?=$arResult["SOCIAL"]["VIBER"];?>" target="_blank">
							<svg class="icon social-viber">
								<use xlink:href="#social-viber"></use>
							</svg>
						</a>
						<a class="social__link js-social-link" href="<?=$arResult["SOCIAL"]["WHATSAPP"];?>" target="_blank">
							<svg class="icon social-whatsapp">
								<use xlink:href="#social-whatsapp"></use>
							</svg>
						</a>
						<a class="social__link js-popup-share_mail" data-name="<?=$arResult["NAME"];?>" data-link="<?=$arResult["DETAIL_PAGE_URL"];?>" data-img="<?=$arResult["PICTURE"]['0']['src']?>" data-text='<?=$arResult["DETAIL_TEXT"];?>' href="#">
							<svg class="icon mail-filled">
								<use xlink:href="#mail-filled"></use>
							</svg>
						</a>
					</div>
					<!--/noindex-->
				</div>
				<div class="picture__accordeon"><span class="picture__accordeon-head js-picture-accordeon-head is-active"><?=GetMessage('PARAM');?></span>
					<div class="picture__accordeon-main js-picture-accordeon-main" style="display: block;">
						<table class="picture__options">
							<?foreach($arResult['PROPERTIES'] as $codeProp => $arProp):?>
								<?if(($arProp["PROPERTY_TYPE"]=='S' || $arProp["PROPERTY_TYPE"]=='N') && $arProp['VALUE'] && 
									stripos($codeProp, 'VENDOR_CODE')===FALSE &&
									stripos($codeProp, 'LOCATION_ID')===FALSE && 
									stripos($codeProp, 'RATING')===FALSE && 
									stripos($codeProp, 'SORT')===FALSE &&
									stripos($codeProp, 'REGION')===FALSE && 
									stripos($codeProp, 'COUNT')===FALSE && 
									stripos($codeProp, '_EN')===FALSE && 
									stripos($codeProp, '_ID')===FALSE && 
									stripos($codeProp, '_OLD') === false && 
									stripos($codeProp, 'VIDEO') === false && 
									stripos($codeProp, 'OLD_VALUES_JSON') === false &&
									stripos($codeProp, 'THEME_FOR_UNHIDE') === false):?>
									<tr>
										<td><?=$arProp['NAME']?></td>
										<td><?=$arProp['VALUE']?> <?=$codeProp=='WEIGHT' ? GetMessage('KG') : ''?></td>
									</tr>
								<?endif;?>
							<?endforeach;?>
							<?foreach($arResult['PROPERTIES'] as $codeProp => $arProp):?>
								<?if(($arProp["PROPERTY_TYPE"]=='G' || $arProp["PROPERTY_TYPE"]=='E') && $arProp['VALUE'] && $arProp['VALUE'] &&
									stripos($codeProp, 'SIZE_TYPE')===FALSE && 
									stripos($codeProp, 'AUTHOR')===FALSE  && 
									stripos($codeProp, '_OLD') === false && 
									stripos($codeProp, 'OLD_VALUES_JSON') === false &&
									stripos($codeProp, 'THEME_FOR_UNHIDE') === false):?>
									<tr>
										<td><?=$arProp['NAME']?></td>
										<?if(is_array($arProp['VALUE'])):?>
											<td>
												<? $i = 0; foreach($arProp['VALUE'] as $value):?>
													
													<?= $i ? ', ' . $arResult[$codeProp][$value]["NAME"] : $arResult[$codeProp][$value]["NAME"];?><? $i++; endforeach;?>
											</td>
										<?else:?>
											<td><?=$arResult[$codeProp][$arProp['VALUE']]["NAME"]?></td>
										<?endif;?>
									</tr>
								<?endif;?>
							<?endforeach;?>
							
						</table>
					</div>
					<?if($arResult["DETAIL_TEXT"]):?>
						<span class="picture__accordeon-head js-picture-accordeon-head"><?=GetMessage('DESC');?></span>
						<div class="picture__accordeon-main js-picture-accordeon-main">
							<p itemprop="description"><?=$arResult["DETAIL_TEXT"]?></p>
						</div>
					<?endif;?>
				</div>
			</div>
		</div>
	</div>
</div>
<?/*доработка для экспертов*/?>
<?if($arParams['IS_EXPERT'] && ($arParams['COUNT_EXPERT_WORK'] || $arResult['RATING_EXPERT'])):?>
	<div class="l-message">
			<div class="l-message__item js-l-message">
					<div class="container">
						<div class="l-message__inner">
							<div class="l-message__close js-l-message-close">  
								<svg class="icon icon-close">
								<use xlink:href="#icon-close"></use>
								</svg>
							</div>
							<div>
								<?if($arResult['RATING_EXPERT']):?>
									<div class="rating_expert_title"><?=GetMessage('RATED_PICTURE');?></div>
								<?else:?>
									<div class="rating_expert_title" data-text="<?=GetMessage('RATED_PICTURE');?>"><?=GetMessage('RATE_THIS_PICTURE');?></div>
								<?endif;?>
								<div id="reviewStars-input" class="<?=$arResult['RATING_EXPERT'] ? 'block_no_click' : '';?> ">
									<input id="star-0-<?=$arResult['ID'];?>" type="radio" name="EXPERT_RATING" value="10" class="js-experts_rating" data-id="<?=$arResult['ID']?>" <?= ($arResult['RATING_EXPERT']==9 || $arResult['RATING_EXPERT']==10) ? 'checked' : '' ?>/>
									<label title="10" for="star-0-<?=$arResult['ID'];?>"></label>
									<input id="star-1-<?=$arResult['ID'];?>" type="radio" name="EXPERT_RATING" value="8" class="js-experts_rating" data-id="<?=$arResult['ID']?>" <?= ($arResult['RATING_EXPERT']==7 || $arResult['RATING_EXPERT']==8) ? 'checked' : '' ?>/>
									<label title="8" for="star-1-<?=$arResult['ID'];?>"></label>
									<input id="star-2-<?=$arResult['ID'];?>" type="radio" name="EXPERT_RATING" value="6" class="js-experts_rating" data-id="<?=$arResult['ID']?>" <?= ($arResult['RATING_EXPERT']==5 || $arResult['RATING_EXPERT']==6) ? 'checked' : '' ?>/>
									<label title="6" for="star-2-<?=$arResult['ID'];?>"></label>
									<input id="star-3-<?=$arResult['ID'];?>" type="radio" name="EXPERT_RATING" value="4" class="js-experts_rating" data-id="<?=$arResult['ID']?>" <?= ($arResult['RATING_EXPERT']==3 || $arResult['RATING_EXPERT']==4) ? 'checked' : '' ?>/>
									<label title="4" for="star-4-<?=$arResult['ID'];?>"></label>
									<input id="star-5-<?=$arResult['ID'];?>" type="radio" name="EXPERT_RATING" value="2" class="js-experts_rating" data-id="<?=$arResult['ID']?>" <?= ($arResult['RATING_EXPERT']==1 || $arResult['RATING_EXPERT']==2) ? 'checked' : '' ?>/>
									<label title="2" for="star-5-<?=$arResult['ID'];?>"></label>
								</div>
							</div>
						</div>
					</div>
			</div>
	</div>
<?endif;?>
<?$templateData = $arResult;?>
