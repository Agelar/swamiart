<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use HB\helpers\WorkHelpers;
use HB\helpers\CatalogHelpers;
if (CModule::IncludeModule('iblock'))
{
    global $APPLICATION;
    //изображения для шеринга
    $APPLICATION->SetPageProperty("vk:image", "<meta property='vk:image' content='https://".$_SERVER['HTTP_HOST'].$arResult["DETAIL_PICTURE"]["SRC"]."' />");
    $APPLICATION->SetPageProperty("og:image", "<meta property='og:image' content='https://".$_SERVER['HTTP_HOST'].$arResult["DETAIL_PICTURE"]["SRC"]."' />");
    $APPLICATION->SetPageProperty("og:image:width", "<meta property='og:image:width' content='".$arResult['DETAIL_PICTURE']['WIDTH']."' />");
    $APPLICATION->SetPageProperty("og:image:height", "<meta property='og:image:height' content='".$arResult['DETAIL_PICTURE']['HEIGHT']."' />");
    $APPLICATION->SetPageProperty("og:title", "<meta property='og:title' content='".$arResult['OG_TITLE']."'  />");
    
    $APPLICATION->SetPageProperty("og:description", "<meta property='og:description' content='".($arResult['DETAIL_TEXT'] ? $arResult['DETAIL_TEXT'] : $arResult['OG_TITLE'] )."'/>");
    
    $arElMetaProp = CatalogHelpers::getSeoField($arResult["IBLOCK_ID"], $arResult["ID"], $templateData, ["NAME"]);

    if($arElMetaProp["ELEMENT_META_TITLE"]){
        $APPLICATION->SetPageProperty("title", html_entity_decode($arElMetaProp["ELEMENT_META_TITLE"]));
    }

    if($arElMetaProp["ELEMENT_META_DESCRIPTION"]){
        $APPLICATION->SetPageProperty("description", html_entity_decode($arElMetaProp["ELEMENT_META_DESCRIPTION"]));
    }

    if($arElMetaProp["ELEMENT_META_KEYWORDS"]){
        $APPLICATION->SetPageProperty("keywords", $arElMetaProp["ELEMENT_META_KEYWORDS"]);
    }

    if($arElMetaProp["ELEMENT_PAGE_TITLE"]){
        $APPLICATION->SetTitle($arElMetaProp["ELEMENT_PAGE_TITLE"]);
    }
}


//просмотры работы
if(!$_SESSION['COUNT_SHOW'][$arResult["ID"]]){
    $_SESSION['COUNT_SHOW'][$arResult["ID"]] = $arResult["ID"];
    WorkHelpers::userViewWork($arResult["ID"]);
}
