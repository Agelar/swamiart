<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use HB\helpers\LangHelpers;
use HB\helpers\AuthorHelpers;
use HB\helpers\Helpers;
use HB\helpers\WorkHelpers;
use HB\helpers\RatingHelpers;

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

//переводим arResult
$arResult = LangHelpers::getTranslation($arResult);

//переводим названия свойств
$arProperties = WorkHelpers::getWorkProperty();     
$arProperties = LangHelpers::getTranslationProperty($arProperties);
foreach($arResult["PROPERTIES"] as $code => $arProp){
    if($arProperties[$code]) $arResult["PROPERTIES"][$code]["NAME"] = $arProperties[$code];
}

//информация об авторе картины
$arResult["AUTHOR"] = AuthorHelpers::getAuthors($arResult['PROPERTIES']['AUTHOR']['VALUE'])[$arResult['PROPERTIES']['AUTHOR']['VALUE']];

//настройки сайта
$arSettings = Helpers::getSiteSettings();

//ссылки для шеринга
$nameWork = $arResult["NAME"];
if($arSettings['SHARING_NAME']['VALUE']) $nameWork = str_replace('#NAME#', $arResult["NAME"], $arSettings['SHARING_NAME']['VALUE']);
$arResult['OG_TITLE'] = $nameWork;
$textDetail = Helpers::cutStr($arResult["DETAIL_TEXT"], 140,"");
$arResult["SOCIAL"] = Helpers::getSocialLinks($nameWork, (is_array($textDetail)) ? $textDetail["BEFORE"] : $textDetail);

//стоимость цифровой копии
$arResult['DIGITAL_COPY_PRICE'] = number_format($arSettings['DIGITAL_COPY_PRICE']['VALUE'], 0, ',', '&nbsp;');

//собираем массив изображений
$arResult["PICTURE"] = array();
$arResult["PICTURE"][] = CFile::ResizeImageGet($arResult['DETAIL_PICTURE'], array('width'=>1024, 'height'=>1024), BX_RESIZE_IMAGE_PROPORTIONAL, true);
if($arResult["PROPERTIES"]["ADD_PICTURE"]["VALUE"]["0"]){
    foreach($arResult["PROPERTIES"]["ADD_PICTURE"]["VALUE"] as $imgId){
        $arResult["PICTURE"][] = CFile::ResizeImageGet($imgId, array('width'=>1024, 'height'=>1024), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    }
}  

//собираем массив видео
$arResult["YOUTUBE_VIDEO"] = array();
if($arResult["PROPERTIES"]["YOUTUBE_VIDEO"]["VALUE"]["0"]){
    foreach($arResult["PROPERTIES"]["YOUTUBE_VIDEO"]["VALUE"] as $video_url){
        $url = str_replace(array('https://www.youtube.com/watch?v=', 'www.youtube.com/watch?v=', 'https://www.youtube.com/embed/', 'www.youtube.com/embed/', 'https://youtu.be/'), array('', '', '', '', ''), $video_url);
        $arResult["YOUTUBE_VIDEO"][] = trim($url);
    }
}  

//файл цифровой копии
if($arResult["PROPERTIES"]["DIGITAL_COPY"]["VALUE"]){
    $arResult["DIGITAL_COPY"] = CFile::GetPath($arResult["PROPERTIES"]["DIGITAL_COPY"]["VALUE"]);
}

//информация о пользователе
global $USER;
$arResult["USER_IS_AUTH"] = $USER->IsAuthorized();
$arResult["USER_ID"] = $USER->GetID();
$arResult["USER_EMAIL_CONFIRM"] = $arParams["USER_EMAIL_CONFIRM"];

//кодируем ID элемента и ID цифровой копии для формирования ссылки на файл цифровой копии
if($arParams["PAY_DIGITAL_COPY"] && $arParams["PAY_DIGITAL_COPY"][$arResult["ID"]] && $arResult["USER_ID"] && $arResult['PROPERTIES']["DIGITAL_COPY"]['VALUE']){
    $arResult["ENCODE_ID"] = Helpers::encode($arResult["ID"], $arResult["USER_ID"]);
    $arResult["ENCODE_DIGITAL_COPY"] = Helpers::encode($arResult['PROPERTIES']["DIGITAL_COPY"]['VALUE'], $arResult["USER_ID"]);
}

//формируем сслыку на заказ цифровой копии, если заказ создан, но не оплачен
if(!$arParams["PAY_DIGITAL_COPY"][$arResult["ID"]] && $arResult["USER_ID"] && $arResult['PROPERTIES']["DIGITAL_COPY"]['VALUE']){
    $arResult['ORDER_DIGITAL_COPY'] = 0;
    if($arParams["ORDER_DIGITAL_COPY"][$arResult["ID"]]) $arResult['ORDER_DIGITAL_COPY'] = $arParams["ORDER_DIGITAL_COPY"][$arResult["ID"]];
}

//информация о справочниках (свойства типа привязка к элементам)
foreach($arResult['PROPERTIES'] as $codeProp => $arProp){
    if(($arProp["PROPERTY_TYPE"]=='E' || $arProp["PROPERTY_TYPE"]=='G') && $arProp['VALUE'] && stripos($codeProp, 'AUTHOR')===FALSE  && $arProp['VALUE']){
        if($arProp["PROPERTY_TYPE"] == 'E') {
            if($arProp["VALUE"]) $arResult[$codeProp] = Helpers::getElementList($arProp["VALUE"]);
        } else {
            if($arProp["VALUE"]) $arResult[$codeProp] = Helpers::getSectionById($arProp['LINK_IBLOCK_ID'], $arProp["VALUE"]);
        }
    }
}

//если пользователь эксперт, проверяем оценивал ли он картину
if($arParams['IS_EXPERT']){
    $arResult['RATING_EXPERT'] = RatingHelpers::getRatingWorkFromExpert($arResult["USER_ID"], $arResult["ID"]);
}

//для шеринга добавляем в arResult путь к детальной картинке, чтобы он был доступен в component_epilog
$cp = $this->__component;
if (is_object($cp))
{
    $cp->arResult['DETAIL_PICTURE'] = $arResult['DETAIL_PICTURE'];
    $cp->arResult['DETAIL_TEXT'] = $arResult['DETAIL_TEXT'];
    $cp->arResult['OG_TITLE'] = $arResult['OG_TITLE'];
    $cp->SetResultCacheKeys(array('DETAIL_PICTURE', 'DETAIL_TEXT', 'OG_TITLE'));
}

//добавляем слайды с интерьерами
$arResult["INTERIORS"] = Helpers::getInteriorList();
