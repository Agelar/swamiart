(function ($) {
    $.fn.sizeChanged = function (handleFunction) {
        var element = this;
        var lastWidth = element.width();
        var lastHeight = element.height();
        setInterval(function () {
            if (lastWidth === element.width() && lastHeight === element.height())
                return;
            if (typeof (handleFunction) == 'function') {
                handleFunction({ width: lastWidth, height: lastHeight },
                    { width: element.width(), height: element.height() });
                lastWidth = element.width();
                lastHeight = element.height();
            }
        }, 100);
        return element;
    };
}(jQuery));

//эксперт оценил работу
$(document).on('change', '.js-experts_rating', function () {
    var id = $(this).data('id');
    var rating = $(this).val();
    $.ajax({
        type: 'GET',
        url: '/local/ajax/ajax.php?AJAX=Y&EXPERTS_RATING=Y&WORK_ID=' + id + '&RATING=' + rating,

        dataType: 'json',
        success: function (data) {
            $('#reviewStars-input').addClass('block_no_click');
            $('.rating_expert_title').text($('.rating_expert_title').data('text'));

        }
    });
    return false;
});

var pictureCarouselInitImgProps = function () {
    $('body').find('.picture__carousel_above').each(function () {
        $(this).css('width', $(this).data('img-width') + '%');
        $(this).css('top', 'calc(' + $(this).data('img-top') + '% - ' + $(this).height() / 2 + 'px)');
        $(this).css('left', 'calc(' + $(this).data('img-left') + '% - ' + ($(this).width() / 2 + 2) + 'px)');
    });
}

$(document).ready(function () {  
    pictureCarouselInitImgProps();
});