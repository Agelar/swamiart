<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<div class="menu menu_top">
	<?
	foreach($arResult as $arItem):
		if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
			continue;
	?>
		<?if($arItem["SELECTED"]):?>
			<div class="menu__item"><a class="menu__link is-active" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></div>
		<?else:?>
			<div class="menu__item"><a class="menu__link" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></div>
		<?endif?>
		
	<?endforeach?>
	<div class="menu__item menu__item_dropdown"><a class="menu__link"><?=GetMessage('STILL');?>
							<svg class="icon icon-arrow">
								<use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/svg/sprite.svg#icon-arrow"></use>
							</svg></a>
							<?$APPLICATION->IncludeComponent(
								"bitrix:menu",
								"subtop_menu",
								Array(
									"ALLOW_MULTI_SELECT" => "N",
									"CHILD_MENU_TYPE" => "left",
									"DELAY" => "N",
									"MAX_LEVEL" => "1",
									"MENU_CACHE_GET_VARS" => array(""),
									"MENU_CACHE_TIME" => "3600",
									"MENU_CACHE_TYPE" => "A",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"ROOT_MENU_TYPE" => "subtop",
									"USE_EXT" => "N"
								)
							);?>
	</div>
</div>


<?endif?>