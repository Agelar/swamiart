<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?if (!empty($arResult)) {?>

	<div class="footer-block__content">
		<?
		foreach ($arResult as $arItem) {
			if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue;
			if ($arItem["SELECTED"]) {?>
				<div><a class="footer-block__link is-active" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></div>
			<?} elseif (empty($arItem["LINK"])) {?>
				<div><?=$arItem["TEXT"]?></div>
			<?} else {?>
				<div><a class="footer-block__link" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></div>
			<?}
		}
		?>
	</div>

<?}?>