<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>


	<?
	$key = 0;
	foreach($arResult as $arItem):
		if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
			continue;
	?>
		<?if($key%5==0 && $key!=0):?>
			<div class="menu__dropdown-separate"></div>
		<?endif;?>
		<?if($arItem["SELECTED"]):?>
			<div class="menu__dropdown-item"><a class="menu__dropdown-link is-active" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></div>
		<?else:?>
		<div class="menu__dropdown-item"><a class="menu__dropdown-link" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></div>
		<?endif?>
		
	<? $key++; endforeach?>
	

<?endif?>