<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
if (!empty($arResult)):?>

<div class="tool-user__dropdown">
	<div class="tool-user__dropdown-group">
		<?
		$key = 0;
		foreach($arResult as $arItem):
			if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
				continue;
		
		if($arItem["PARAMS"]["ACCSESS_GROUP"]){
			if(array_search($arItem["PARAMS"]["ACCSESS_GROUP"],CUser::GetUserGroup($USER->GetID())) === false){
				continue;
			}
		}
		?>
			<?=($key%3==0 && $key!=0) ? '</div><div class="tool-user__dropdown-group">' : '';?>
			<?if($arItem["SELECTED"]):?>
				<div class="tool-user__dropdown-item"><a class="tool-user__dropdown-link is-active" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></div>
			<?else:?>
				<div class="tool-user__dropdown-item"><a class="tool-user__dropdown-link" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></div>
			<?endif?>
			
		<?$key++; endforeach?>
	</div>
	<div class="tool-user__dropdown-group tool-user__dropdown-group_last">
											<div class="tool-user__dropdown-item"><a class="tool-user__dropdown-link" href="/?logout=yes"> 
												<svg class="icon icon-logout">
												<use xlink:href="#icon-logout"></use>
												</svg><span><?=GetMessage('GO_OUT');?></span></a></div>
	</div>
	
							
	
</div>


<?endif?>