<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use HB\userEntity\IblockAuthor;
use HB\helpers\Helpers;
?>
<?$photo = IblockAuthor::getPersonalPicture($USER->GetID(),array("width"=>"100", "height"=>"100", BX_RESIZE_IMAGE_PROPORTIONAL));?>
<div class="js-stick">
	<div class="lk__aside-inner">
		<div class="lk-user">
			<?if($photo):?>
				<div class="lk-user__img js-lk-user-img" style="background-image:url(<?=$photo?>)"><input class="js-lk-user-img-input" type="file"/></div>
			<?else:?>
				<div class="lk-user__img js-lk-user-img not-photo" style=""><input class="js-lk-user-img-input" type="file"/></div>
			<?endif;?>
			<span class="lk-user__hello"><?=GetMessage('HELLO')?></span><span class="lk-user__name"><?=Helpers::getUserName()?></span>
		</div>
		<?if (!empty($arResult["GROUPS"])):?>
			<div class="lk-menu">
				<?foreach($arResult["GROUPS"] as $arGroup):?>
					<?if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue;?>

					<div class="lk-menu__group">
						<?foreach($arGroup as $arItem):?>
							<a class="lk-menu__link <?if($arItem["PARAMS"]["ITEM_CLASS"]):?><?=$arItem["PARAMS"]["ITEM_CLASS"]?><?endif;?> <?if($arItem["SELECTED"]):?>is-active<?endif;?>" href="<?=$arItem["LINK"]?>">
								<?if($arItem["PARAMS"]["ICON"]):?>
									<svg class="icon <?=$arItem["PARAMS"]["CLASS"]?>">
										<use xlink:href="<?=$arItem["PARAMS"]["ICON"]?>"></use>
									</svg>
								<?endif;?>
								<?=$arItem["TEXT"]?>
							</a>
						<?endforeach;?>
					</div>
				<?endforeach;?>
			</div>
		<?endif;?>
	</div>
</div>