<?
foreach($arResult as $key => $arItem){
    if($arItem["PARAMS"]["ACCSESS_GROUP"]){
        if(array_search($arItem["PARAMS"]["ACCSESS_GROUP"],CUser::GetUserGroup($USER->GetID())) === false){
            continue;
        }
    }
    if($arItem["PARAMS"]["GROUP"]){
        $arResult["GROUPS"][$arItem["PARAMS"]["GROUP"]][] = $arItem;
    }else{
        $arResult["GROUPS"]["NO_GROUP"][] = $arItem;
    }
}
?>