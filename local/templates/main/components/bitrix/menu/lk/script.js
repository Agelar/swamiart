$(document).on("change",".js-lk-user-img-input",function(){
    $(".js-lk-user-img").removeClass("not-photo");
    var fd = new FormData;
    fd.append('PERSONAL_PHOTO', $(this).prop('files')[0]);
    $.ajax({
        url: '/local/ajax/changeUserAvatar.php',
        data: fd,
        processData: false,
        contentType: false,
        type: 'POST',
        dataType:'json',
        success: function (data) {
            if(!data.STATUS){
                alert(data.TEXT);
            }
        }
    });
});
$(document).on("click",".js-set-author-group",function(){
    $.ajax({
        url: "/local/ajax/changeGroupUser.php",
        dataType: "json",
        method: "POST",
        data:{
            "sessid": BX.bitrix_sessid(),
            "lang": BX.message('SITE_ID'),
        }
    }).done(function(data){
        if(data.HTML){
            var idPopup = Math.floor(Math.random() * (99999 - 11111 + 1)) + 11111;
            $(".popup .popup__window").append('<div class="popup__content popup__content--' + idPopup + '">' + data.HTML + "</div>");
            $('.popup__content').hide();
            $('.popup__content--' + idPopup).show();
            $(".popup").fadeIn(300);
            location.reload();
        }
        if(data.STATUS){
            $(".js-set-author-group").remove();
        }
    });
});