<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<div class="menu__dropdown">
	<div class="container">
		<div class="menu__dropdown-inner">
			<?
			foreach($arResult as $arItem):
				if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
					continue;
			?>
				<?if($arItem["SELECTED"]):?>
					<div class="menu__dropdown-item"><a class="menu__dropdown-link is-active" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></div>
				<?else:?>
					<div class="menu__dropdown-item"><a class="menu__dropdown-link" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></div>
				<?endif?>
				
			<?endforeach?>
		</div>
	</div>
</div>


<?endif?>