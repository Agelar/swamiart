<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!$_SESSION['reg_close']) {?>
<div class="unauth__reg js-unauth-reg">
	<a href="#">
		<span id="user__reg"><?=GetMessage("USER_REG");?></span>
		<div class="popup__close reg__close" onclick="$('.unauth__reg').fadeOut(300); <?$_SESSION['reg_close'] = 'Y';?>">
			<div class="popup__close-item"></div>
			<div class="popup__close-item"></div>
		</div>
	</a>
</div>
<?}?>