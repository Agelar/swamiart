<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<?
/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";
if(defined("MOB_SHOW")){
	$classMob = " breadcrumbs_important";
};
$strReturn = '';

$strReturn .= '<div class="section breadcrumbs'.$classMob.'">
					<div class="container">
						<div class="breadcrumbs__inner">
							<a class="breadcrumbs__back" href="#" onclick="history.go(-1); return false;">
								<div class="breadcrumbs__back-icon">
									<svg class="icon arrow_smart-w">
										<use xlink:href="#arrow_smart"></use>
									</svg>
								</div>
								<div class="breadcrumbs__back-text">'.GetMessage('BACK').'</div>
							</a>
							<div class="breadcrumbs__history" itemscope itemtype="http://schema.org/BreadcrumbList">';

$itemSize = count($arResult);

$i = 0;

for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	$arrow = ($index > 0? '<i class="fa fa-angle-right"></i>' : '');

	$i++;

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
		$strReturn .= '<span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem"><a class="breadcrumbs__history-link" itemprop="item" href="' . $arResult[$index]["LINK"] . '"><span itemprop="name">' . $title . '</span><meta itemprop="position" content="' . ($index+1) . '"></a></span>';
	}
	else
	{
		$strReturn .= '<span itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem"><div class="breadcrumbs__history-current" itemprop="item" content="'.(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http").'//'.$_SERVER['HTTP_HOST'].$APPLICATION->GetCurUri().'"><span itemprop="name">' . $title . '</span><meta itemprop="position" content="' . ($index+1) . '"></div></span>';
	}
}

$strReturn .= '</div></div></div></div>';

return $strReturn;
