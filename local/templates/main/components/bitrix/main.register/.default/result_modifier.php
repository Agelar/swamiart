<?
function fieldsSort($a, $b){
    $arMass = array(
        "NAME",
        "LAST_NAME",
        "EMAIL",
        "PASSWORD",
        "CONFIRM_PASSWORD",
        "LOGIN",
    );
    $index_a = array_search($a,$arMass);
    $index_b = array_search($b,$arMass);
    
    return ($index_a > $index_b) ? 1 : -1;
}

usort($arResult["SHOW_FIELDS"],'fieldsSort');
?>