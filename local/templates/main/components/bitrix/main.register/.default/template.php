<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
?>
<?
use HB\helpers\Helpers;

CModule::IncludeModule("hb.site");
if(!isset($arResult["VALUES"]["USER_ID"])):?>
	<form class="popup-form-reg popup__content-wrapper js-reg-form" method="post" action="<?=POST_FORM_ACTION_URI?>">
		<?if($arResult["BACKURL"] <> ''):?>
			<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
		<?endif;?>
		<input type="hidden" name="TYPE" value="REGISTRATION"/>
		<input type="hidden" name="register_submit_button" value="Y"/>
		<input type="hidden" class="js-conf-pass" name="REGISTER[CONFIRM_PASSWORD]" value="<?=$arResult["VALUES"]["CONFIRM_PASSWORD"]?>"/>
		<span class="popup__heading"><?=GetMessage("AUTH_REGISTER")?></span>
		<div class="popup__desc"><span class="popup__desc-text"><?=GetMessage("REGISTER_FREE_AND_TREE")?></span>
		<div class="popup__desc-switch">
			<div class="popup__desc-switch-text"><?=GetMessage("REGISTER_HAS_ACC")?></div>
			<a class="popup__desc-switch-link js-popup-auth" href="#"><?=GetMessage("REGISTER_REG_AUTH")?></a>
		</div>
		</div>
		<div class="popup__role">
			<label class="popup__role-label">
				<input class="radio" type="radio" name="REGISTER[GROUP_USER]" value="<?=BUYER_GROUP?>" checked="checked"/>
				<div class="popup__role-radio"></div>
				<div class="popup__role-text"><?=GetMessage("REGISTER_BUYER")?></div>
			</label>
			<label class="popup__role-label">
				<input class="radio" type="radio" name="REGISTER[GROUP_USER]" value="<?=AUTHOR_GROUP?>"/>
				<div class="popup__role-radio"></div>
				<div class="popup__role-text"><?=GetMessage("REGISTER_PAINTER")?></div>
			</label>
		</div>
		<div class="popup__table">
		<?
		CModule::IncludeModule("socialservices");
		$oAuthManager = new CSocServAuthManager();
		$arServices = $oAuthManager->GetActiveAuthServices($arResult);
		if(!isset($_REQUEST["auth_service_error"])){
			$oAuthManager->Authorize($_REQUEST["auth_service_id"]);
		}
		$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "socbutton", 
			array(
				"AUTH_SERVICES"=>$arServices,
				"AUTH_URL"=>"/",
				"POST"=>"",
				"SUFFIX"=>"form",
			), 
			$component, 
			array("HIDE_ICONS"=>"Y")
		);
		?>
		<div class="popup__mail">
			<span class="popup__table-heading hidden-mobile-down"><?=GetMessage("REGISTER_EMAIL_HELP")?></span>
			<div class="popup__mail-inputs">
			<div class="popup__names">
				<label class="label">
					<div class="popup__label"><?=GetMessage("REGISTER_FIELD_NAME")?></div>
					<input class="input input-text input-text-short" type="text" name="REGISTER[NAME]" value="<?=$arResult["VALUES"]["NAME"]?>" required="required"/>
				</label>
				<label class="label">
					<div class="popup__label"><?=GetMessage("REGISTER_FIELD_LAST_NAME")?></div>
					<input class="input input-text input-text-short" type="text" name="REGISTER[LAST_NAME]" value="<?=$arResult["VALUES"]["LAST_NAME"]?>" required="required"/>
				</label>
			</div>
			<label class="label">
				<div class="popup__label"><?=GetMessage("REGISTER_FIELD_EMAIL")?></div>
				<input class="input input-text js-email-to-login <?if($arResult["ERRORS"]):?>error<?endif;?>" type="email" name="REGISTER[EMAIL]" value="<?=$arResult["VALUES"]["EMAIL"]?>" required="required"/>
				<input type="hidden" name="REGISTER[LOGIN]" value="<?=$arResult["VALUES"]["LOGIN"]?>" class="js-login"/>
				<?if($arResult["ERRORS"]):?>
					<label id="REGISTER[EMAIL]-error" class="error" for="REGISTER[EMAIL]"><?=$arResult["ERRORS"][0] ? $arResult["ERRORS"][0] : GetMessage("REGISTER_HAS_USER")?></label>
				<?endif;?>
			</label>
			<label class="label">
				<div class="popup__label"><?=GetMessage("REGISTER_FIELD_PASSWORD")?></div>
				<input class="input input-text js-pass" type="password" minlength="4" name="REGISTER[PASSWORD]" value="<?=$arResult["VALUES"]["PASSWORD"]?>" required="required"/>
			</label>
			<label class="label label-inline">
				<input class="input input-checkbox" type="checkbox" name="agreement" required="required"/>
				<div class="input-checkbox-block">
				<svg class="icon icon-check-w">
					<use xlink:href="#icon-check"></use>
				</svg>
				</div>
				<div class="input-checkbox-text"><?=GetMessage("REGISTER_ICONFIRM")?> <a class="popup__mail-link" href="<?=Helpers::getTermsLink()?>" target="_blank"><?=GetMessage("REGISTER_USER_CONF")?></a></div>
			</label>
			</div>
			<button class="popup__mail-submit popup__mail-submit--auth" type="submit" value="submit"><?=GetMessage("REGISTER_REG_SUB")?></button>
		</div>
		</div>
	</form>
<?else:?>
	<form class="popup__content-wrapper" action="">
		<span class="popup__heading popup__heading--center"><?=GetMessage("SUCCSESS_REGISTATION")?></span>
		<span class="popup__subheading"><?=GetMessage("SUCCSESS_REGISTATION_CONF")?></span>
		<div class="popup__icon">
			<svg class="icon icon-check-w">
				<use xlink:href="#icon-check"></use>
			</svg>
		</div>
	</form>
	<script>
		location.reload();
	</script>
<?endif;?>