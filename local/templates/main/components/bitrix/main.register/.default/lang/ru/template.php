<?
$MESS["AUTH_REGISTER"] = "Регистрация";
$MESS["REGISTER_FREE_AND_TREE"] = "Это бесплатно и займет не более 3 минут";
$MESS["REGISTER_HAS_ACC"] = "Уже зарегистрированы?";
$MESS["REGISTER_REG_AUTH"] = "Авторизоваться";
$MESS["REGISTER_HAS_USER"] = "Такой пользователь уже существует";
$MESS["REGISTER_EMAIL_HELP"] = "С помощью E-mail";
$MESS["REGISTER_PAINTER"] = "Я художник";
$MESS["REGISTER_BUYER"] = "Я покупатель";
$MESS["REGISTER_ICONFIRM"] = "Я согласен с";
$MESS["REGISTER_USER_CONF"] = "пользовательским соглашением";
$MESS["REGISTER_FIELD_LAST_NAME"] = "Фамилия";
$MESS["REGISTER_FIELD_NAME"] = "Имя";
$MESS["REGISTER_FIELD_EMAIL"] = "E-mail";
$MESS["REGISTER_FIELD_PASSWORD"] = "Пароль";
$MESS["REGISTER_REG_SUB"] = "Зарегистрироваться";
$MESS["SUCCSESS_REGISTATION"] = "Успех!";
$MESS["SUCCSESS_REGISTATION_CONF"] = "Мы отправили на ваш e-mail ссылку для подтверждения регистрации";
?>