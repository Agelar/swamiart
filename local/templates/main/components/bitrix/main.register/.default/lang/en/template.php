<?
$MESS["AUTH_REGISTER"] = "Registration";
$MESS["REGISTER_FREE_AND_TREE"] = "It is free and takes less than 3 minutes";
$MESS["REGISTER_HAS_ACC"] = "Already registered?";
$MESS["REGISTER_REG_AUTH"] = "Login";
$MESS["REGISTER_HAS_USER"] = "Username already exists";
$MESS["REGISTER_EMAIL_HELP"] = "By E-mail";
$MESS["REGISTER_PAINTER"] = "I am an artist";
$MESS["REGISTER_BUYER"] = "I am a buyer";
$MESS["REGISTER_ICONFIRM"] = "I agree with";
$MESS["REGISTER_USER_CONF"] = "terms and conditions";
$MESS["REGISTER_FIELD_LAST_NAME"] = "Surname";
$MESS["REGISTER_FIELD_NAME"] = "Name";
$MESS["REGISTER_FIELD_EMAIL"] = "E-mail";
$MESS["REGISTER_FIELD_PASSWORD"] = "Password";
$MESS["REGISTER_REG_SUB"] = "Register";
$MESS["SUCCSESS_REGISTATION"] = "Success!";
$MESS["SUCCSESS_REGISTATION_CONF"] = "We've send you a confirmation email";
?>