$(document).on("change",".js-email-to-login",function(){
    $(".js-login").val($(this).val());
});
$(document).on("change",".js-pass",function(){
    $(".js-conf-pass").val($(this).val());
});
$(document).on('formSubmit',".js-reg-form",function(e){
    e.preventDefault();
    var form_action = $(this).attr('action');
    var form_backurl = $(this).find('input[name="backurl"]').val();

    $.ajax({
        type: "POST",
        url: '/local/ajax/auth.php',
        data: $(this).serialize() + "&site_id=" + BX.message('SITE_ID'),
        timeout: 3000,
        success: function(data) {
            $('.js-wrap-reg').html(data);
            $(document).trigger('formInit');
        }
    });

    return false;
});