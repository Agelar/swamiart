<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 */
?>
<script id="basket-total-template" type="text/html">
    <div class="cart-result" data-entity="basket-checkout-aligner">
        <div class="cart-result__main">
            <div class="cart-result__row">
                <span><?=Loc::getMessage('GOODS_WORTH')?></span>

                <span>{{{PRICE_WITHOUT_DISCOUNT_FORMATED}}} <span class="rubl">₽</span></span>
            </div>
            {{#DISCOUNT_PRICE_FORMATED}}
            <div class="cart-result__row"><a><?=Loc::getMessage('SBB_BASKET_ITEM_ECONOMY')?></a><span>—
                    {{{DISCOUNT_PRICE_FORMATED}}} <span class="rubl">₽</span></span></div>

            {{/DISCOUNT_PRICE_FORMATED}}
            <?if ($arParams['HIDE_COUPON'] !== 'Y') {?>
                {{#COUPON_LIST}}
                {{#ACTIVE}}
                <div class="cart-result__promo"><span>{{COUPON}} (<?=Loc::getMessage('SBB_COUPON')?> {{JS_CHECK_CODE}}
                        {{#DISCOUNT_NAME}}({{{DISCOUNT_NAME}}}){{/DISCOUNT_NAME}})</span><a
                        class="cart-result__promo-remove" data-entity="basket-coupon-delete"
                        data-coupon="{{COUPON}}"><?=Loc::getMessage('SBB_DELETE')?></a></div>
                {{/ACTIVE}}
                {{/COUPON_LIST}}
                <div class="cart-result__form">
                    <label class="cart-result__form-label"><?=Loc::getMessage('SBB_COUPON_ENTER')?>:</label>
                    <div class="cart-result__form-line">
                        <input class="js-coupone__input" type="text" required="required" />
                        <button class="js-coupone__update"
                            data-entity="basket-coupon-btn"><?=Loc::getMessage('TO_APPLY')?></button>
                    </div>
                </div>

                {{#COUPON_LIST}}
                {{#NON_ACTIVE}}
                <div class="cart-result__promo"><span>{{COUPON}} (<?=Loc::getMessage('SBB_COUPON')?> {{JS_CHECK_CODE}}
                        {{#DISCOUNT_NAME}}({{{DISCOUNT_NAME}}}){{/DISCOUNT_NAME}})</span><a
                        class="cart-result__promo-remove" data-entity="basket-coupon-delete"
                        data-coupon="{{COUPON}}"><?=Loc::getMessage('SBB_DELETE')?></a></div>
                {{/NON_ACTIVE}}
                {{/COUPON_LIST}}
            <?}?>
        </div>

        <div class="cart-result__bottom">
            <div class="cart-result__final">
                <span><?=Loc::getMessage('TOTAL_TO_PAY')?>:</span><span>
                <span data-entity="basket-total-price"> {{{PRICE_FORMATED}}}</span> 
                <span class="rubl">₽</span></span>
            </div>
            <div class="cart-result__buttons">
                <a class="btn btn_green" data-entity="basket-checkout-button">
                <?=Loc::getMessage('SBB_ORDER')?>
                <div class="arrow"></div></a>
            </div>
        </div>
    </div>
</script>