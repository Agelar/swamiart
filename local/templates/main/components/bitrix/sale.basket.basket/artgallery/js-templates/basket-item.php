<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $mobileColumns
 * @var array $arParams
 * @var string $templateFolder
 */

$usePriceInAdditionalColumn = in_array('PRICE', $arParams['COLUMNS_LIST']) && $arParams['PRICE_DISPLAY_MODE'] === 'Y';
$useSumColumn = in_array('SUM', $arParams['COLUMNS_LIST']);
$useActionColumn = in_array('DELETE', $arParams['COLUMNS_LIST']);

$restoreColSpan = 2 + $usePriceInAdditionalColumn + $useSumColumn + $useActionColumn;

$positionClassMap = array(
	'left' => 'basket-item-label-left',
	'center' => 'basket-item-label-center',
	'right' => 'basket-item-label-right',
	'bottom' => 'basket-item-label-bottom',
	'middle' => 'basket-item-label-middle',
	'top' => 'basket-item-label-top'
);

$discountPositionClass = '';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
	{
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$labelPositionClass = '';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
	{
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}
?>
<script id="basket-item-template" type="text/html">
	<div class="cart-item"
		id="basket-item-{{ID}}" data-entity="basket-item" data-id="{{ID}}">
		{{^SHOW_RESTORE}}
			
				
					<?
					if (in_array('PREVIEW_PICTURE', $arParams['COLUMNS_LIST']))
					{
						?>
						<div class="cart-item__img" style="background-image:url({{{IMAGE_URL}}}{{^IMAGE_URL}}<?=$templateFolder?>/images/no_photo.png{{/IMAGE_URL}}"></div>
						
						<?
					}
					?>
					<div class="cart-item__main">
						{{#PRODUCT}}
							<a class="cart-item__title" href="{{DETAIL_PAGE_URL}}">{{NAME}}</a>
						{{/PRODUCT}}
						{{#AUTHOR}}
							<a class="cart-item__author" href="{{DETAIL_PAGE_URL}}">{{NAME}}</a>
						{{/AUTHOR}}
						
						<div class="cart-item__info">
							<div class="cart-item__descs">
								{{#SIZE}}
									<div class="cart-item__desc"><span><?=Loc::getMessage('SIZE')?>: </span><span>{{SIZE}} <?=Loc::getMessage('CM')?>.</span></div>
								{{/SIZE}}
								{{#STYLE}}
									<div class="cart-item__desc"><span><?=Loc::getMessage('STYLE')?>: </span><a href="/works/pictures/filter/style-is-{{CODE}}/">{{NAME}}</a></div>
								{{/STYLE}}
							</div>
						</div>
						<?
						if ($usePriceInAdditionalColumn)
						{
							?>
							<div class="cart-item__prices">
								
								<span class="cart-item__price" id="basket-item-price-{{ID}}">{{{PRICE_FORMATED}}} <span class="rubl">₽</span></span>
								{{#SHOW_DISCOUNT_PRICE}}
									<span class="cart-item__oldprice">{{{FULL_PRICE_FORMATED}}}</span>
								{{/SHOW_DISCOUNT_PRICE}}
							</div>
							
							<?
						}
						?>
					</div>
					<?
						if (isset($mobileColumns['DELETE']))
						{
							?>
							
							<span class="cart-item__remove" data-entity="basket-item-delete"> <span><?=Loc::getMessage('SBB_DELETE')?></span>
								<svg class="icon icon-close">
								<use xlink:href="#icon-close"></use>
								</svg>
							</span>
							<?
						}
					?>
				
			
			
			
			
		{{/SHOW_RESTORE}}
	</div>
</script>