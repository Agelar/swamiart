<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use Bitrix\Main\Localization\Loc;
use HB\helpers\Helpers;
?>

<div class="bx-sbb-empty-cart-container">
	<div class="bx-sbb-empty-cart-image">
		<img src="" alt="">
	</div>
    <div class="bx-sbb-empty-cart-text"><?=Loc::getMessage("SBB_EMPTY_BASKET_TITLE")?></div>
    
    <?$last_order_ID = Helpers::getLastOrder();     
    if ($last_order_ID) {?>
        <div class="bx-sbb-empty-cart-desc recreate-last-order" data-order-id="<?=$last_order_ID?>">
            <?=Loc::getMessage(
                'SBB_PREV_ORDER_HINT',
                [
                    '#A1#' => '<a href="javascript:void(0);">',
                    '#A2#' => '</a>',
                ]
            )?>
        </div>
    <?}?>
        
    <?if (!empty($arParams['EMPTY_BASKET_HINT_PATH'])) {?>    
		<div class="bx-sbb-empty-cart-desc">
			<?=Loc::getMessage(
                'SBB_EMPTY_BASKET_HINT',
                [
                    '#A1#' => '<a href="' . $arParams['EMPTY_BASKET_HINT_PATH'] . '">',
                    '#A2#' => '</a>',
                ]
            )?>
		</div>
	<?}?>
</div>