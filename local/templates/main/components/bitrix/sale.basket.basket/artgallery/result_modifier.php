<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
use HB\helpers\AuthorHelpers;
use HB\helpers\Helpers;

$defaultParams = array(
    'TEMPLATE_THEME' => 'blue',
);
$arParams = array_merge($defaultParams, $arParams);
unset($defaultParams);

if ('' == $arParams['TEMPLATE_THEME']) {
    $arParams['TEMPLATE_THEME'] = 'blue';
}

foreach ($arResult['TOTAL_RENDER_DATA']['COUPON_LIST'] as $key => $arCoupon) {
    if (!$arCoupon['ACTIVE']) {
        $arResult['TOTAL_RENDER_DATA']['COUPON_LIST'][$key]['NON_ACTIVE'] = 'Y';

    }
    if ($arCoupon['ACTIVE'] && $arCoupon['STATUS_TEXT'] == 'применен') {
        $arResult['TOTAL_RENDER_DATA']['SALE_COUPON'] = 'Y';
    }

}
$arResult['TOTAL_RENDER_DATA']['PRICE_WITHOUT_DISCOUNT_FORMATED'] = str_replace(array(' руб.', ' rub.'), array('', ''), $arResult['TOTAL_RENDER_DATA']['PRICE_WITHOUT_DISCOUNT_FORMATED']);
$arResult['TOTAL_RENDER_DATA']['DISCOUNT_PRICE_FORMATED'] = str_replace(array(' руб.', ' rub.'), array('', ''), $arResult['TOTAL_RENDER_DATA']['DISCOUNT_PRICE_FORMATED']);
$arResult['TOTAL_RENDER_DATA']['PRICE_FORMATED'] = str_replace(array(' руб.', ' rub.'), array('', ''), $arResult['TOTAL_RENDER_DATA']['PRICE_FORMATED']);

$arResult['BASKET_MAIN_JSON'] = array();

foreach ($arResult['BASKET_ITEM_RENDER_DATA'] as $key => $aItem) {
    $arResult['BASKET_ITEM_RENDER_DATA'][$key]['PRODUCT'] = Helpers::getElementList($aItem['PRODUCT_ID'])[$aItem['PRODUCT_ID']];
    if ($arResult['BASKET_ITEM_RENDER_DATA'][$key]['PRODUCT']) {
        $arResult['BASKET_ITEM_RENDER_DATA'][$key]['PRICE_FORMATED'] = str_replace(array(' руб.', ' rub.'), array('', ''), $arResult['BASKET_ITEM_RENDER_DATA'][$key]['PRICE_FORMATED']);
        $arResult['BASKET_ITEM_RENDER_DATA'][$key]['FULL_PRICE_FORMATED'] = str_replace(array(' руб.', ' rub.'), array('', ''), $arResult['BASKET_ITEM_RENDER_DATA'][$key]['FULL_PRICE_FORMATED']);
        foreach ($aItem['COLUMN_LIST'] as $arColumn) {
            switch ($arColumn['CODE']) {
                case 'PROPERTY_AUTHOR_VALUE':
                    $arResult['BASKET_ITEM_RENDER_DATA'][$key]['AUTHOR'] = AuthorHelpers::getAuthors($arColumn['VALUE'])[$arColumn['VALUE']];
                    break;
                case 'PROPERTY_STYLE_VALUE':
                    $arResult['BASKET_ITEM_RENDER_DATA'][$key]['STYLE'] = Helpers::getElementList($arColumn['VALUE'])[$arColumn['VALUE']];
                    break;
                case 'PROPERTY_SIZE_VALUE':
                    $arResult['BASKET_ITEM_RENDER_DATA'][$key]['SIZE'] = $arColumn['VALUE'];
                    break;
            }
        }
        $arResult['BASKET_MAIN_JSON']['PRODUCT'][$aItem['PRODUCT_ID']]['PRODUCT']['NAME'] = $arResult['BASKET_ITEM_RENDER_DATA'][$key]['PRODUCT']['NAME'];
        $arResult['BASKET_MAIN_JSON']['PRODUCT'][$aItem['PRODUCT_ID']]['PRODUCT']['ID'] = $arResult['BASKET_ITEM_RENDER_DATA'][$key]['PRODUCT']['ID'];
        $arResult['BASKET_MAIN_JSON']['PRODUCT'][$aItem['PRODUCT_ID']]['AUTHOR'] = $arResult['BASKET_ITEM_RENDER_DATA'][$key]['AUTHOR'];
        $arResult['BASKET_MAIN_JSON']['PRODUCT'][$aItem['PRODUCT_ID']]['STYLE'] = $arResult['BASKET_ITEM_RENDER_DATA'][$key]['STYLE'];
        $arResult['BASKET_MAIN_JSON']['PRODUCT'][$aItem['PRODUCT_ID']]['SIZE'] = $arResult['BASKET_ITEM_RENDER_DATA'][$key]['SIZE'];
    } else {
        unset($arResult['BASKET_ITEM_RENDER_DATA'][$key]);
    }
}

$arResult['BASKET_MAIN_JSON'] = str_replace("'", "", json_encode($arResult['BASKET_MAIN_JSON']));
