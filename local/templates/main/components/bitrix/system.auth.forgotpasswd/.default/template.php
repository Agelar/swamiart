<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($APPLICATION->arAuthResult["TYPE"]!="OK"):?>
	<form class="popup__content-wrapper popup__content-wrapper--restore js-fogot-form" method="post" action="<?=$arResult["AUTH_URL"]?>">
		<input type="hidden" name="AUTH_FORM" value="Y">
		<input type="hidden" name="TYPE" value="SEND_PWD">
		<span class="popup__heading popup__heading--center"><?=GetMessage("HEADER_FOGOT")?></span>
		<label class="label">
			<div class="popup__label"><?=GetMessage("EMAIL_FOGOT")?></div>
			<input class="input input-text <?if($APPLICATION->arAuthResult["TYPE"] == "ERROR"):?>error<?endif;?>" type="email" name="USER_LOGIN" value="<?=$_REQUEST["USER_LOGIN"]?>" required="required"/>
			<?if($APPLICATION->arAuthResult["TYPE"] == "ERROR"):?>
				<label id="USER_LOGIN-error" class="error" for="USER_LOGIN"><?=GetMessage("USER_NOT_FOUND")?></label>
			<?endif;?>
		</label>
		<button type="submit" class="popup__mail-submit popup__mail-submit--auth"><?=GetMessage("HEADER_FOGOT")?></button>
		<div class="popup__mail-restore">
			<span class="popup__mail-restore-text"><?=GetMessage("QUESTION_FOGOT")?></span>
			<a class="popup__mail-restore-link js-popup-auth" href="#"><?=GetMessage("AUTH_FOGOT")?></a>
		</div>
	</form>
<?else:?>
	<form class="popup__content-wrapper" action=""><span class="popup__heading popup__heading--center"><?=GetMessage("SUCCSESS_FOGOT")?></span><span class="popup__subheading"><?=GetMessage("SUCCSESS_FOGOT_CONF")?></span>
		<div class="popup__icon">
			<svg class="icon icon-check-w">
				<use xlink:href="#icon-check"></use>
			</svg>
		</div>
	</form>
<?endif;?>