<?php
use HB\helpers\Helpers;
use HB\helpers\LangHelpers;
use HB\helpers\WorkHelpers;
use HB\helpers\AuthorHelpers;
foreach($arResult['ITEMS'] as $key => $arItem){
    $arResult["ITEMS"][$key] = LangHelpers::getTranslation($arItem);
    $arResult['ITEMS'][$key]["PRICE"] = CCatalogProduct::GetOptimalPrice($arItem['ID'], 1);

    //информация о справочниках (свойства типа привязка к элементам)
    foreach($arItem['PROPERTIES'] as $codeProp => $arProp){
        if(stripos($codeProp, 'MATERIAL')!==FALSE  && $arProp['VALUE']){
            if($arProp["VALUE"]) $arResult['ITEMS'][$key][$codeProp] = Helpers::getElementList($arProp["VALUE"]);
            break;
        }
    }
}

//получаем список авторов для фильтра
global $arrFilterWorks;
$arAuthorsId = WorkHelpers::getAuthorIdByFilterWork($arrFilterWorks);
$arAuthors = AuthorHelpers::getAuthors($arAuthorsId);

$arResult['AUTHOR_NAME'] = array();
foreach($arAuthors as $authorId => $arAuthor){
	$firstLetter = mb_substr($arAuthor['NAME'],0,1,'UTF-8');
	$arResult['AUTHOR_NAME'][$firstLetter][$authorId] = $arAuthor;
}
ksort($arResult['AUTHOR_NAME']);