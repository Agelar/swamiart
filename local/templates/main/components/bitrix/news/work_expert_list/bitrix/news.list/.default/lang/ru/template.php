<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["MY_WORKS"] = "Оцените работы";
$MESS["IN_SECTION"] = "В этом разделе вы можете размещать новые работы, <br/> а также редактировать существующие";
$MESS["ADD_WORK"] = "Добавить работу";
$MESS["SORT"] = "Сортировать по";
$MESS["RATING"] = "Рейтингу";
$MESS["RATING_EXP"] = "Рейтингу эксперта";
$MESS["PRICE"] = "Цене";
$MESS["CREATED"] = "Дате публикации";
$MESS["SIZE"] = "Размеру";
$MESS["NAME_SORT"] = "Названию";
$MESS["WORK"] = "Работа";
$MESS["PUBLISHED"] = "опубликована";
$MESS["NOT_PUBLISHED"] = "не опубликована";
$MESS["MODERATION"] = "Модерация";
$MESS["PASSED"] = "пройдена";
$MESS["IN_THE_PROCESS"] = "в процессе";
$MESS["DELETE"] = "Удалить";
$MESS["EDIT"] = "Редактировать";
$MESS["SOLD"] = "Продана";

$MESS["WORKS_TODAY"] = "работ осталось оценить сегодня";
$MESS["RATE_PICTURE"] = "Оцените картину";
$MESS["TODAY_MAXIMUM"] = "Нет картин для оценки";
$MESS["MATERIAL"] = "Материал";
$MESS["WEB_MATERIAL"] = "Материал полотна";
?>