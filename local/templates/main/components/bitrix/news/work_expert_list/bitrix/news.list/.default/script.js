//эксперт оценил работу
$(document).on('change', '.js-experts_rating', function(){
    var id = $(this).data('id');
    var rating = $(this).val();
    var element = $(this);
    $.ajax({
        type: 'GET',
        url: '/local/ajax/ajax.php?AJAX=Y&EXPERTS_RATING=Y&WORK_ID=' + id + '&RATING=' + rating,
        
        dataType: 'json',
        success: function(data){
            //var authorId = $('.js-author_filter_work_expert').val();
            var count = parseInt($('.count_expert_work').text());
            $('.count_expert_work').text(count-1);
            $('.alert__success_rating_expert').show();
            $(element).parents('.cart-item_work').fadeOut();
            setTimeout(
                function(){
                    $('.alert__success_rating_expert').hide();
                    $.ajax({
                        type: 'GET',
                        url: location.pathname + '?AJAX=Y',
                        dataType: 'html',
                        success: function(data){
                            $('#lk-works__ajax').html(data);
                            $('.js-select2').each(function(index, elem) {
                                var placeholderText = $(this).attr('data-placeholder');
                                $(this).selectize({
                                    highlight: true,
                                    maxItems: 1,
                                    closeAfterSelect: true,
                                    hideSelected: false,
                                    onDropdownClose: function($dropdown){
                                        $($dropdown).find('.selected').not('.active').removeClass('selected');
                                    },
                                    onChange: function() {
                                        if(this.$input.is('[required]')) {
                                            this.$input.valid();
                                        }
                                    }
                                });
                            });
                        }
                
                    });
                }, 2000
            );
        }
    });
    return false;
});

//фильтрация по автору
$(document).on('change', '.js-author_filter_work_expert', function(){
    var authorId = $(this).val();
    
    $.ajax({
        type: 'GET',
        url: location.pathname + '?AJAX=Y&AUTHOR_ID=' + authorId,
        dataType: 'html',
        success: function(data){
            $('#lk-works__ajax').html(data);
            $('.js-select2').each(function(index, elem) {
                var placeholderText = $(this).attr('data-placeholder');
                $(this).selectize({
                    highlight: true,
                    maxItems: 1,
                    closeAfterSelect: true,
                    hideSelected: false,
                    onDropdownClose: function($dropdown){
                        $($dropdown).find('.selected').not('.active').removeClass('selected');
                    },
                    onChange: function() {
                        if(this.$input.is('[required]')) {
                            this.$input.valid();
                        }
                    }
                });
            });
        }
  
    });
    return false;
});

$.fancybox.defaults.touch = false;
$.fancybox.defaults.mobile.clickContent = 'close';
$.fancybox.defaults.mobile.clickSlide = 'close';

$(document).on('click', '.cart-item__img_expert_list, .cart-item__title', function() {

    var $slides = $('.cart-item__img_expert_list img');
    var gallery = [];
    $slides.each(function() {
        var img = bgToHref($(this).attr('src'));
        gallery.push({
            src: img
        });
    });

    var index = 1;
    $.fancybox.open(gallery, {
        // toolbar: false,
        buttons: ['close'],
        onInit: function(instance, current) {
            instance.jumpTo(index, 0);
        }
    });
});

function bgToHref(val) {
    var href = val.replace('url', '');
    href = href.replace(/[\(\)\'\"]/ig, '');
    return href;
}
