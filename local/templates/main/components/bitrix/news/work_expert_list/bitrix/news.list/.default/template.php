<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="lk__main-inner"><span class="lk-head"><?=GetMessage('MY_WORKS');?></span>	
                <div class="lk-tools">
					<div class="lk-tools__column">
						<span class="count_expert_work"><?=$arParams['COUNT_DAY'];?></span> <?=GetMessage('WORKS_TODAY')?>
                  	</div>
				</div>

				<div class="alert alert__success alert__success_rating_expert">Оценка картины принята</div>
				
					<div id="lk-works__ajax" data-page="<?=$_REQUEST['PAGEN_1'] ? $_REQUEST['PAGEN_1'] : 1;?>">
						<?if($_REQUEST['AJAX']){ $GLOBALS['APPLICATION']->RestartBuffer();  }?>
							<div class="lk-works lk-works_expert_list">
								<?if(count($arResult["ITEMS"])):?>
									<? $count = 0; foreach($arResult["ITEMS"] as $arItem):?>
										<?
										$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
										$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
										?>
										
											<div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="cart-item cart-item_work cart-item_work_<?=$arItem['ID']?> <?= $count>=$arParams['COUNT_DAY'] ? 'block_no_click' : '';?>">
												<div class="cart-item__title">
													<?=$arItem["NAME"]?>.
													<?if($arItem['MATERIAL']):?>
														<? $i=0; foreach($arItem['MATERIAL'] as $arMaterial):?><?=$i==0 ? ' '.GetMessage('MATERIAL').': ' . $arMaterial['NAME'] : ', ' . $arMaterial['NAME'];?><? $i++; endforeach;?>.
													<?endif;?>
													<?=$arItem['PROPERTIES']['WEB_MATERIAL']['VALUE'] ? ' '.GetMessage('WEB_MATERIAL').': ' . $arItem['PROPERTIES']['WEB_MATERIAL']['VALUE'] . '.' : '';?>
												</div>
												<div class="cart-item__img cart-item__img_expert_list" ><img src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>"></div>
												<div class="cart-item__main">
													<div class="cart-item__info">
														
														<?if($arItem['PRICE']):?>	
															<div class="cart-item__prices">
																<span class="cart-item__price"><?=number_format($arItem['PRICE']['DISCOUNT_PRICE'], 0, ',', ' ')?> <span class="rubl">₽</span></span>
																<?if($arItem['PRICE']['DISCOUNT']):?>
																	<span class="cart-item__oldprice"><?=number_format($arItem['PRICE']['PRICE']['PRICE'], 0, ',', ' ')?></span>
																<?endif;?>
															</div>
														<?endif;?>
													</div>
												</div>
												<div class="cart-item__tools js-cart-item-tools">
													
													<div class="form__row-main">
														<div class="form__field">
															<span class="form__row-title"><?=GetMessage('RATE_PICTURE')?></span>
															<?php 
																	$keyExpertRating = array_search($USER->GetID(), $arItem['PROPERTIES']['EXPERTS_RATING']['VALUE']);
																	$rating = 0;
																	if($keyExpertRating!==FALSE) 
																		$rating = $arItem['PROPERTIES']['EXPERTS_RATING']['DESCRIPTION'][$keyExpertRating];
															?>
															<div id="reviewStars-input">
																<input id="star-0-<?=$arItem['ID'];?>" type="radio" name="EXPERT_RATING" value="10" class="js-experts_rating" data-id="<?=$arItem['ID']?>"/>
																<label title="10" for="star-0-<?=$arItem['ID'];?>"></label>

																<input id="star-1-<?=$arItem['ID'];?>" type="radio" name="EXPERT_RATING" value="8" class="js-experts_rating" data-id="<?=$arItem['ID']?>"/>
																<label title="8" for="star-1-<?=$arItem['ID'];?>"></label>

																<input id="star-2-<?=$arItem['ID'];?>" type="radio" name="EXPERT_RATING" value="6" class="js-experts_rating" data-id="<?=$arItem['ID']?>"/>
																<label title="6" for="star-2-<?=$arItem['ID'];?>"></label>

																<input id="star-3-<?=$arItem['ID'];?>" type="radio" name="EXPERT_RATING" value="4" class="js-experts_rating" data-id="<?=$arItem['ID']?>"/>
																<label title="4" for="star-4-<?=$arItem['ID'];?>"></label>

																<input id="star-5-<?=$arItem['ID'];?>" type="radio" name="EXPERT_RATING" value="2" class="js-experts_rating" data-id="<?=$arItem['ID']?>"/>
																<label title="2" for="star-5-<?=$arItem['ID'];?>"></label>
															</div>
															
														</div>
													</div>
												</div>
												
											</div>
										
										
									<? $count++; endforeach;?>
								<?else:?>

									<p class="empty_work_list"><?=GetMessage('TODAY_MAXIMUM');?></p>
								<?endif;?>
							</div>
							<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
								<br /><?=$arResult["NAV_STRING"]?>
							<?endif;?>

						<?if($_REQUEST['AJAX']){ die();}?>
					</div>
				
                
</div>

