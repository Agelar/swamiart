<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["MY_WORKS"] = "Rate work";
$MESS["IN_SECTION"] = "In this section you can post new works, <br/> and edit existing ones";
$MESS["ADD_WORK"] = "Add work";
$MESS["SORT"] = "Sort by";
$MESS["RATING"] = "Rating";
$MESS["RATING_EXP"] = "Expert Rating";
$MESS["PRICE"] = "Price";
$MESS["CREATED"] = "Date of publication";
$MESS["SIZE"] = "Size";
$MESS["NAME_SORT"] = "Name";
$MESS["WORK"] = "Work";
$MESS["PUBLISHED"] = "published";
$MESS["NOT_PUBLISHED"] = "not published";
$MESS["MODERATION"] = "Moderation";
$MESS["PASSED"] = "passed";
$MESS["IN_THE_PROCESS"] = "in the process";
$MESS["DELETE"] = "Delete";
$MESS["EDIT"] = "Edit";
$MESS["SOLD"] = "Sold";

$MESS["WORKS_TODAY"] = "works left to evaluate today";
$MESS["RATE_PICTURE"] = "Rate this picture";
$MESS["TODAY_MAXIMUM"] = "No pictures to evaluate";
$MESS["MATERIAL"] = "Material";
$MESS["WEB_MATERIAL"] = "Web material";
?>
