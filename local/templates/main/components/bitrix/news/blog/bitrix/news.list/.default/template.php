<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use HB\helpers\LangHelpers;

$arSettings = HB\helpers\Helpers::getSiteSettings();
?>
<div class="section grid">
	<div class="container">
		<div class="grid__inner" itemscope itemtype="http://schema.org/Blog">
			<meta itemprop="description" content="<?=GetMessage('BL_DESCRIPTION_BEFORE')?> <?=CSite::GetByID(SITE_ID)->GetNext()["NAME"]?><?=GetMessage('BL_DESCRIPTION_AFTER')?" ".GetMessage('BL_DESCRIPTION_AFTER'):""?>">
			<?foreach($arResult["ITEMS"] as $arItem):?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				$arItem = LangHelpers::getTranslation($arItem);
				?>
				<div class="article-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>" itemprop="blogPosts" itemscope itemtype="http://schema.org/BlogPosting">
					<?$_date = date("Y-m-d", strtotime($arItem["DISPLAY_ACTIVE_FROM"]))?>
					<meta itemprop="datePublished" content="<?=$_date?>">
					<meta itemprop="dateModified" content="<?=$_date?>">
					<meta itemprop="name" content="<?=$arItem["NAME"]?>">
					<meta itemprop="headline" content="<?=$arItem["NAME"]?>">
					<?if($arItem["PROPERTIES"]["AUTHOR"]["FORMAT"]){?>
					<meta itemprop="author" content="<?=$arItem["PROPERTIES"]["AUTHOR"]["FORMAT"]["NAME"]?>">
					<?}else{?>
					<meta itemprop="author" content="<?=CSite::GetByID(SITE_ID)->GetNext()["NAME"]?>">
					<?}?>
					<div style="display:none" itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
						<meta itemprop="name" content="<?=CSite::GetByID(SITE_ID)->GetNext()["NAME"]?>">
						<div itemprop="logo image" itemscope itemtype="https://schema.org/ImageObject">
							<img itemprop="url contentUrl" src="//<?=$_SERVER['HTTP_HOST']?>/favicon.ico" alt="logo" />
							<meta itemprop="width" content="6" />
							<meta itemprop="height" content="6" />
						</div>
						<span style="display: none;" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
							<?
							// Вытаскиваем город из адреса
							$adress = $arSettings['ADRESS']['VALUE'];
							$city = "";
							foreach (explode(',', $adress) as $adressPart) {
								if(strpos($adressPart, "г.") !== FALSE)
									$city = trim($adressPart);
							}
							$adress = preg_replace(['/\s+/', '/\,+/', '/\.+/'], [' ', ',', '.'], 
										str_replace($city, "", $adress)
									);
							$adress = trim(trim(trim($adress, "."), ","));
							?>
							<span itemprop="addressLocality"><?=$city?></span>, 
							<span itemprop="streetAddress"><?=$adress?></span>
							<span itemprop="postalCode"><?=$arSettings['POSTAL_CODE']['VALUE']?></span>
						</span>
						<meta itemprop="telephone" content="<?=$arSettings['PHONE']['VALUE']?>" />
					</div>

					<img src="<?=$arItem["PREVIEW_PICTURE"]["RESIZE"]["src"]?>" itemprop="image" style="display:none" alt="">
					<a class="article-item__img" href="<?=$arItem["DETAIL_PAGE_URL"]?>" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["RESIZE"]["src"]?>)"></a>
					<a href="<?=$arResult["SECTION"][$arItem["IBLOCK_SECTION_ID"]]["LINK"]?>" class="article-item__category"><?=$arResult["SECTION"][$arItem["IBLOCK_SECTION_ID"]]["NAME"]?>
						<span class="article-item__category-date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span></a>
					<h3 class="article-item__heading"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h3>
					<div class="article-item__text" itemprop="description"><?=$arItem["PREVIEW_TEXT"]?></div>
				</div>
			<?endforeach;?>	
		</div>
		<?=$arResult["NAV_STRING"]?>
	</div>
</div>
