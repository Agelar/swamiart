<?
use HB\helpers\LangHelpers;

$arIDs = array();
foreach($arResult["ITEMS"] as $arItem){
    if($arItem["IBLOCK_SECTION_ID"]){
        $arIDs[] = $arItem["IBLOCK_SECTION_ID"];
    }
}
$db = CIBlockSection::GetList(array(), array("ACTIVE" => "Y", "IBLOCK_ID" => $arParams["IBLOCK_ID"], "ID" => $arIDs), false, array("ID","NAME","SECTION_PAGE_URL","UF_NAME_EN"));
while($res = $db->getNext()){
    $res = LangHelpers::getTranslationSection($res);
    $arResult["SECTION"][$res["ID"]] =array(
        "NAME" => $res["NAME"],
        "LINK" => $res["SECTION_PAGE_URL"],
    );
}
?>