<?
use HB\helpers\LangHelpers;
use HB\helpers\BlogHelpers;
if($arResult["IBLOCK_SECTION_ID"]){
    $section = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $arResult["IBLOCK_ID"], "ID" =>$arResult["IBLOCK_SECTION_ID"]), false, array("SECTION_PAGE_URL","NAME","UF_NAME_EN"))->GetNext();
    $section = LangHelpers::getTranslationSection($section);
    $APPLICATION->AddChainItem($section["NAME"], $section["SECTION_PAGE_URL"]);
}
$APPLICATION->AddChainItem($arResult["NAME"], $arResult["DETAIL_PAGE_URL"]);

$ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($arResult["IBLOCK_ID"], $arResult["ID"]);
$arElMetaProp = $ipropValues->getValues();

if($arElMetaProp["ELEMENT_META_TITLE"]){
    $arElMetaProp["ELEMENT_META_TITLE"] = explode("|",$arElMetaProp["ELEMENT_META_TITLE"]);
    $APPLICATION->SetPageProperty("title", trim($arElMetaProp["ELEMENT_META_TITLE"][($isEngVersion) ? "1" : "0"]));
}else{
    $APPLICATION->SetPageProperty("title", $arResult["NAME"]);
}

if($arElMetaProp["ELEMENT_META_DESCRIPTION"]){
    $arElMetaProp["ELEMENT_META_DESCRIPTION"] = explode("|",$arElMetaProp["ELEMENT_META_DESCRIPTION"]);
    $APPLICATION->SetPageProperty("description", $arElMetaProp["ELEMENT_META_DESCRIPTION"][($isEngVersion) ? "1" : "0"]);
}

if($arElMetaProp["ELEMENT_META_KEYWORDS"]){
    $arElMetaProp["ELEMENT_META_KEYWORDS"] = explode("|",$arElMetaProp["ELEMENT_META_KEYWORDS"]);
    $APPLICATION->SetPageProperty("keywords", $arElMetaProp["ELEMENT_META_KEYWORDS"][($isEngVersion) ? "1" : "0"]);
}

if(!$_SESSION['COUNT_SHOW_BLOG'][$arResult["ID"]]){
    $_SESSION['COUNT_SHOW_BLOG'][$arResult["ID"]] = $arResult["ID"];
    BlogHelpers::userViewBlog($arResult["ID"]);
}?>