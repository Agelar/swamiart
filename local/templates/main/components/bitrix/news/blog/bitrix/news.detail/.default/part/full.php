<?
use HB\helpers\LangHelpers;

$imgNum = intval($match[1][$num]);
if($imgNum > 0):?>
    <?if($arResult["PROPERTIES"]["IMAGES"]["VALUE"][$imgNum - 1]):?>
        <div class="blog-detail__image blog-detail__image--full">
            <img class="blog-detail__image-pic" src="<?=CFile::GetPath($arResult["PROPERTIES"]["IMAGES"]["VALUE"][$imgNum - 1])?>" alt="">
            <?if($arResult["PROPERTIES"]["IMAGES"]["DESCRIPTION"][$imgNum - 1]):?>
                <?
                    $decriptionImg = explode("|",$arResult["PROPERTIES"]["IMAGES"]["DESCRIPTION"][$imgNum - 1]);
                    $decriptionImg = LangHelpers::isEnVersion() ? $decriptionImg[1] : $decriptionImg[0]
                ?>
                <?if($decriptionImg):?>
                    <div class="blog-detail__image-desc"><?=$decriptionImg?></div>
                <?endif;?>
            <?endif;?>
        </div>
    <?endif;?>
<?endif;?>