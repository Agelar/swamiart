<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use HB\userEntity\Notification;

$arSettings = HB\helpers\Helpers::getSiteSettings();
?>
<div class="section blog-detail">
	<div class="container" itemscope itemtype="http://schema.org/BlogPosting">
		<link itemprop="mainEntityOfPage" itemscope href="<?=$APPLICATION->GetCurPage(true)?>"/>
		<?$_date = date("Y-m-d", strtotime($arResult["DISPLAY_ACTIVE_FROM"]))?>
		<meta itemprop="datePublished" content="<?=$_date?>">
		<meta itemprop="dateModified" content="<?=$_date?>">
		<?if($arResult["PROPERTIES"]["AUTHOR"]["FORMAT"]){?>
		<meta itemprop="author" content="<?=$arResult["PROPERTIES"]["AUTHOR"]["FORMAT"]["NAME"]?>">
		<?}else{?>
		<meta itemprop="author" content="<?=CSite::GetByID(SITE_ID)->GetNext()["NAME"]?>">
		<?}?>
		<div style="display:none" itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
			<meta itemprop="name" content="<?=CSite::GetByID(SITE_ID)->GetNext()["NAME"]?>">
			<div itemprop="logo image" itemscope itemtype="https://schema.org/ImageObject">
				<img itemprop="url contentUrl" src="//<?=$_SERVER['HTTP_HOST']?>/favicon.ico" alt="logo" />
				<meta itemprop="width" content="6" />
				<meta itemprop="height" content="6" />
			</div>
			<span style="display: none;" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
				<?
				// Вытаскиваем город из адреса
				$adress = $arSettings['ADRESS']['VALUE'];
				$city = "";
				foreach (explode(',', $adress) as $adressPart) {
					if(strpos($adressPart, "г.") !== FALSE)
						$city = trim($adressPart);
				}
				$adress = preg_replace(['/\s+/', '/\,+/', '/\.+/'], [' ', ',', '.'], 
							str_replace($city, "", $adress)
						);
				$adress = trim(trim(trim($adress, "."), ","));
				?>
				<span itemprop="addressLocality"><?=$city?></span>, 
				<span itemprop="streetAddress"><?=$adress?></span>
				<span itemprop="postalCode"><?=$arSettings['POSTAL_CODE']['VALUE']?></span>
			</span>
			<meta itemprop="telephone" content="<?=$arSettings['PHONE']['VALUE']?>" />
		</div>

		<div class="blog-detail__inner" itemprop="articleBody"><span class="blog-detail__category"><?=$arResult["SECTION_INFO"]["NAME"]?></span>
			<h1 class="blog-detail__heading" itemprop="headline"><?=$arResult["NAME"]?></h1>
			<div class="blog-detail__desc">
				<span class="blog-detail__desc-date <?if(empty($arResult["PROPERTIES"]["AUTHOR"]["FORMAT"])):?>blog-detail__desc-date--center<?endif;?>"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
				<?if($arResult["PROPERTIES"]["AUTHOR"]["FORMAT"]):?>
					<div class="blog-detail__desc-avatar">
						<img class="blog-detail__desc-avatar-img" src="<?=$arResult["PROPERTIES"]["AUTHOR"]["FORMAT"]["DETAIL_PICTURE"]?>"
						alt="" />
					</div>
					<span class="blog-detail__desc-name"><?=$arResult["PROPERTIES"]["AUTHOR"]["FORMAT"]["NAME"]?></span>
				<?endif;?>
			</div>
			<h2 class="blog-detail__subheading"><?=$arResult["PROPERTIES"]["SUBTITLE"]["VALUE"]?></h2>
			<div class="blog-detail__content">
				<?if($arResult["DETAIL_PICTURE"]["SRC"]):?>
					<div class="blog-detail__image blog-detail__image--full">
						<span itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
							<img itemprop="image url" class="blog-detail__image-pic" src="<?=CFile::GetPath($arResult["DETAIL_PICTURE"]["ID"])?>" alt="">
							<meta itemprop="width" content="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>">
							<meta itemprop="height" content="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>">
						</span>
					</div>
				<?endif;?>
				<?=$arResult["DETAIL_TEXT"]?>
			</div>
			<div class="blog-detail__actions">
				<div class="blog-detail__actions-block">
					<div class="blog-detail__actions-info">
						<div class="blog-detail__actions-info-date"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></div>
						<div class="blog-detail__actions-info-avatar"><img class="blog-detail__actions-info-avatar-img" src="<?=$arResult["PROPERTIES"]["AUTHOR"]["FORMAT"]["DETAIL_PICTURE"]?>"
							 alt="" /></div>
						<div class="blog-detail__actions-info-name"><?=$arResult["PROPERTIES"]["AUTHOR"]["FORMAT"]["NAME"]?></div>
					</div>
					<div class="blog-detail__actions-share">
						<div class="author__share-text"><?=GetMessage("SHARE")?></div>
						<!--noindex-->
						<div class="author__share-social">
							<a rel="nofollow" class="author__share-link js-social-link" href="<?=$arResult["SOCIAL"]["VK"]?>" target="_blank">
								<div class="author__share-link-icon">
									<svg class="icon social-vk-w">
										<use xlink:href="#social-vk"></use>
									</svg>
								</div>
							</a>
							<a rel="nofollow" class="author__share-link js-social-link" href="<?=$arResult["SOCIAL"]["FACEBOOK"]?>" target="_blank">
								<div class="author__share-link-icon">
									<svg class="icon social-fb-w">
										<use xlink:href="#social-fb"></use>
									</svg>
								</div>
							</a>
							<a rel="nofollow" class="author__share-link js-social-link" href="<?=$arResult["SOCIAL"]["OK"]?>" target="_blank">
								<div class="author__share-link-icon">
									<svg class="icon social-ok-w">
										<use xlink:href="#social-ok"></use>
									</svg>
								</div>
							</a>	
							<a rel="nofollow" class="author__share-link js-social-link" href="<?=$arResult["SOCIAL"]["VIBER"];?>" target="_blank">
								<div class="author__share-link-icon">
									<svg class="icon social-viber">
										<use xlink:href="#social-viber"></use>
									</svg>
								</div>
							</a>
							<a rel="nofollow" class="author__share-link js-social-link" href="<?=$arResult["SOCIAL"]["WHATSAPP"];?>" target="_blank">
								<div class="author__share-link-icon">
									<svg class="icon social-whatsapp">
										<use xlink:href="#social-whatsapp"></use>
									</svg>
								</div>
							</a>
							<a rel="nofollow" class="author__share-link js-popup-share_mail" data-name="<?=$arResult["NAME"]?>" data-link="<?=$APPLICATION->GetCurPage(false)?>" href="#">
								<div class="author__share-link-icon">
									<svg class="icon mail-filled-w">
										<use xlink:href="#mail-filled"></use>
									</svg>
								</div>
							</a>
						</div>
						<!--/noindex-->
					</div>
				</div>
				<div class="blog-detail__actions-block">
					<div class="blog-detail__actions-tags">
						<?if($arResult["PROPERTIES"]["THEME"]["VALUE"]):?>
							<div class="blog-detail__actions-tags-text"><?=GetMessage("THEME")?>:</div>
							<div class="blog-detail__actions-tags-list">
								<?foreach($arResult["PROPERTIES"]["THEME"]["VALUE"] as $idTheme):?>
									<a class="blog-detail__actions-tags-item" href="<?=$arResult["PROPERTIES"]["THEME"]["FORMAT"][$idTheme]["DETAIL_PAGE_URL"]?>"><?=$arResult["PROPERTIES"]["THEME"]["FORMAT"][$idTheme]["NAME"]?></a>
								<?endforeach;?>
							</div>
						<?endif;?>
					</div>
					#SUBSRIBE_BTN#
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	BX.message({
		"SUBSCRIBE": "<?=GetMessage("SUBSCRIBE")?>",
		"UNSUBSCRIBE": "<?=GetMessage("UNSUBSCRIBE")?>",
	})
</script>
<?
global $arrFilter;
$arrFilter = array("ID" => $arResult["POPULAR"]);
$APPLICATION->IncludeComponent("bitrix:news.list", "similarBlog", Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
		"DISPLAY_DATE" => "N",	// Выводить дату элемента
		"DISPLAY_NAME" => "Y",	// Выводить название элемента
		"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
		"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"FIELD_CODE" => array(	// Поля
			0 => "NAME",
			1 => "PREVIEW_TEXT",
			2 => "PREVIEW_PICTURE",
			3 => "",
		),
		"FILTER_NAME" => "arrFilter",	// Фильтр
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],	// Код информационного блока
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],	// Тип информационного блока (используется только для проверки)
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
		"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
		"NEWS_COUNT" => "6",	// Количество новостей на странице
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"PAGER_TITLE" => "Новости",	// Название категорий
		"PARENT_SECTION" => "",	// ID раздела
		"PARENT_SECTION_CODE" => "",	// Код раздела
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"PROPERTY_CODE" => array(	// Свойства
			0 => "ICON",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
		"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SHOW_404" => "N",	// Показ специальной страницы
		"SORT_BY1" => "PROPERTY_COUNT_SHOW",	// Поле для первой сортировки новостей
		"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
		"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
		"SORT_ORDER2" => "DESC",	// Направление для второй сортировки новостей
		"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
	),
	false
);?>