<?
use HB\helpers\LangHelpers;

$imgNum = intval($match[1][$num]);
if($imgNum > 0):?>
    <?if($arResult["PROPERTIES"]["IMAGES"]["VALUE"][$imgNum - 1]):?>
        <div class="blog-detail__image blog-detail__image--left">
            <img class="blog-detail__image-pic" src="<?=CFile::GetPath($arResult["PROPERTIES"]["IMAGES"]["VALUE"][$imgNum - 1])?>" alt="">
        </div>
    <?endif;?>
<?endif;?>