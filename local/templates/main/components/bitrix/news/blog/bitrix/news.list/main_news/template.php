<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use HB\helpers\LangHelpers;
?>
<div itemscope itemtype="http://schema.org/Blog">
	<meta itemprop="description" content="<?=GetMessage('BL_DESCRIPTION_BEFORE')?> <?=CSite::GetByID(SITE_ID)->GetNext()["NAME"]?><?=GetMessage('BL_DESCRIPTION_AFTER')?" ".GetMessage('BL_DESCRIPTION_AFTER'):""?>">
<?foreach($arResult["ITEMS"] as $arItem):?>
<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	$arItem = LangHelpers::getTranslation($arItem);
	?>
	<div class="blog__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>" itemprop="blogPosts" itemscope itemtype="http://schema.org/BlogPosting">
		<?$_date = date("Y-m-d", strtotime($arItem["DISPLAY_ACTIVE_FROM"]))?>
		<meta itemprop="datePublished" content="<?=$_date?>">
		<meta itemprop="dateModified" content="<?=$_date?>">
		<meta itemprop="name" content="<?=$arItem["NAME"]?>">
		<meta itemprop="headline" content="<?=$arItem["NAME"]?>">
		
		<?if($arItem["PROPERTIES"]["AUTHOR"]["FORMAT"]){?>
		<meta itemprop="author" content="<?=$arItem["PROPERTIES"]["AUTHOR"]["FORMAT"]["NAME"]?>">
		<?}else{?>
		<meta itemprop="author" content="<?=CSite::GetByID(SITE_ID)->GetNext()["NAME"]?>">
		<?}?>
		<div style="display:none" itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
			<meta itemprop="name" content="<?=CSite::GetByID(SITE_ID)->GetNext()["NAME"]?>">
			<div itemprop="logo image" itemscope itemtype="https://schema.org/ImageObject">
				<img itemprop="url contentUrl" src="//<?=$_SERVER['HTTP_HOST']?>/favicon.ico" alt="logo" />
				<meta itemprop="width" content="6" />
				<meta itemprop="height" content="6" />
			</div>
			<span style="display: none;" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
				<span itemprop="addressLocality">г. Москва</span>, 
				<span itemprop="streetAddress">ул. Михневская, д.8, офис 785</span>
				<span itemprop="postalCode">15547</span>
			</span>
			<meta itemprop="telephone" content="" />
		</div>

		<a class="blog__item-img" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
			<img class="blog__item-img-pic" itemprop="image" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="" />
		</a>
		<div class="blog__item-content">
			<a class="blog__item-category" href="<?=$arResult["SECTION"][$arItem["IBLOCK_SECTION_ID"]]["LINK"]?>"><?=$arResult["SECTION"][$arItem["IBLOCK_SECTION_ID"]]["NAME"]?></a>
			<h2 class="blog__item-heading">
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
			</h2>
			<div class="blog__item-text" itemprop="description"><?=$arItem["PREVIEW_TEXT"]?></div>
				<a class="blog__item-link" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("LEARN_MORE")?> <div class="arrow"></div></a>
		</div>
	</div>
<?endforeach;?>
</div>