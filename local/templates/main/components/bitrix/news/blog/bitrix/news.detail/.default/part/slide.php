<?
use HB\helpers\LangHelpers;

$imgNums = explode("-", $match[1][$num]);
if($imgNums):?>
    <div class="blog-detail__slider">
        <?foreach($imgNums as $slideNum):?>
            <?if(intval($slideNum) > 0):?>
                <div class="blog-detail__slider-slide">
                    <div class="blog-detail__slider-bg" style="background-image: url('<?=CFile::GetPath($arResult["PROPERTIES"]["IMAGES"]["VALUE"][$slideNum - 1])?>')"></div>
                    <?
                        $decriptionImg = explode("|",$arResult["PROPERTIES"]["IMAGES"]["DESCRIPTION"][$slideNum - 1]);
                        $decriptionImg = LangHelpers::isEnVersion() ? $decriptionImg[1] : $decriptionImg[0]
                    ?>
                    <?if($decriptionImg):?>
                        <div class="blog-detail__slider-desc"><?=$decriptionImg?></div>
                    <?endif;?>
                </div>
            <?endif;?>
        <?endforeach;?>
    </div>
<?endif;?>