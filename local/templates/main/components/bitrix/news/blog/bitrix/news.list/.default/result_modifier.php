<?
use HB\helpers\LangHelpers;

$arIDs = array();
foreach($arResult["ITEMS"] as $key => $arItem){
    if($arItem["IBLOCK_SECTION_ID"]){
        $arIDs[] = $arItem["IBLOCK_SECTION_ID"];
    }
    if($arItem["PREVIEW_PICTURE"]["SRC"]){
        $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["RESIZE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array("width"=>"385","height"=>"270", BX_RESIZE_IMAGE_PROPORTIONAL));
    }
}
$db = CIBlockSection::GetList(array(), array("ACTIVE" => "Y", "IBLOCK_ID" => $arParams["IBLOCK_ID"], "ID" => $arIDs), false, array("ID","NAME","SECTION_PAGE_URL","UF_NAME_EN"));
while($res = $db->getNext()){
    $res = LangHelpers::getTranslationSection($res);
    $arResult["SECTION"][$res["ID"]] =array(
        "NAME" => $res["NAME"],
        "LINK" => $res["SECTION_PAGE_URL"],
    );
}
?>