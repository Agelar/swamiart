<?
use HB\userEntity\Notification;
use HB\helpers\LangHelpers;
use HB\helpers\Helpers;

if($arResult["PROPERTIES"]["AUTHOR"]["VALUE"]){
    $db = CIBlockElement::GetList(array(), array("ACTIVE" => "Y", "IBLOCK_ID" => $arResult["PROPERTIES"]["AUTHOR"]["LINK_IBLOCK_ID"],"=ID" => $arResult["PROPERTIES"]["AUTHOR"]["VALUE"]), false, false, array("NAME", "PROPERTY_NAME_EN", "DETAIL_PICTURE"));
    if($res = $db->fetch()){
        if($res["DETAIL_PICTURE"]){
            $res["DETAIL_PICTURE"] = CFile::ResizeImageGet($res["DETAIL_PICTURE"], array("width"=>"50","height"=>"50", BX_RESIZE_IMAGE_PROPORTIONAL))['src'];
        }
        $arResult["PROPERTIES"]["AUTHOR"]["FORMAT"] = LangHelpers::getTranslation($res);
    }
}
if($arResult["PROPERTIES"]["THEME"]["VALUE"]){
    $db = CIBlockElement::GetList(array(), array("ACTIVE" => "Y", "IBLOCK_ID" => $arResult["PROPERTIES"]["THEME"]["LINK_IBLOCK_ID"],"=ID" => $arResult["PROPERTIES"]["THEME"]["VALUE"]), false, false, array("ID","NAME","PROPERTY_NAME_EN","CODE","DETAIL_PAGE_URL"));
    while($res = $db->getNext()){
        $arResult["PROPERTIES"]["THEME"]["FORMAT"][$res["ID"]] = LangHelpers::getTranslation($res);
    }    
}

$arResult["SOCIAL"] = Helpers::getSocialLinks($arResult["NAME"],strip_tags($arResult["PREVIEW_TEXT"]));

$arResult = LangHelpers::getTranslation($arResult);
if($arResult["DETAIL_TEXT"]){
    $arPattern = array(
        "full" => "/#ELEMENT_FULL_(.*)_IMG#/",
        "right" => "/#ELEMENT_RIGHT_(.*)_IMG#/",
        "slide" => "/#ELEMENT_SLIDE_(.*)_IMG#/",
        "left" => "/#ELEMENT_LEFT_(.*)_IMG#/",
    );
    foreach($arPattern as $key => $pattern){
        $match = false;
        preg_match_all($pattern,$arResult["DETAIL_TEXT"],$match);
        if($match){
            foreach($match[0] as $num => $element){
                if(!empty($element)){
                    ob_start();
                        include("part/". $key . ".php");
                        $arContents["/".$element."/"] = ob_get_contents();
                    ob_end_clean();
                }
 
            }
        } 
    } 
    if($arContents){
        $arResult["DETAIL_TEXT"] = preg_replace(array_keys($arContents),array_values($arContents),$arResult["DETAIL_TEXT"]);
    }
}

$arrFilter = array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y","!ID" => array($arResult["ID"]));
$db = CIBlockElement::GetList(
    array("PROPERTY_COUNT_SHOW" => "desc"), 
    array_merge($arrFilter, array("SECTION_ID" => $arResult["IBLOCK_SECTION_ID"])),
    false, 
    array("nTopCount" => 6), 
    array("ID")
);
$arResult["POPULAR"] = array();
while($res = $db->fetch()){
    $arResult["POPULAR"][] = $res["ID"];
}

$count = $db->SelectedRowsCount();
if($count < 6){
    $arrFilter["!ID"] = array_merge($arrFilter["!ID"],$arResult["POPULAR"]);
    $db = CIBlockElement::GetList(
        array("PROPERTY_COUNT_SHOW" => "desc"), 
        $arrFilter,
        false, 
        array("nTopCount" => 6 - $count), 
        array("ID")
    ); 
    
    while($res = $db->fetch()){
        $arResult["POPULAR"][] = $res["ID"];
    }
}

if($arResult["IBLOCK_SECTION_ID"]){
    $section = CIBlockSection::GetList(array(), array("IBLOCK_ID" => $arResult["IBLOCK_ID"], "ID" =>$arResult["IBLOCK_SECTION_ID"]), false, array("SECTION_PAGE_URL","NAME","UF_NAME_EN"))->GetNext();
    $arResult["SECTION_INFO"] = LangHelpers::getTranslationSection($section);
}
?>