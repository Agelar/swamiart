<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="lk__main-inner"><span class="lk-head"><?=GetMessage('MY_WORKS');?></span>
                <div class="lk-desc"><span><?=GetMessage('IN_SECTION');?></span></div>
                <div class="lk-tools">
				  <div class="lk-tools__column"><a class="btn btn_plus btn_green" href="<?=SITE_DIR?>personal/adding-work/"><?=GetMessage('ADD_WORK');?></a></div>
				  	<?if(count($arResult["ITEMS"])):?>
						<div class="lk-tools__column">
							<div class="l-select"><span class="l-select__title"><?=GetMessage('SORT');?></span>
							<select class="l-select__input js-l-select-input js-sort__work_list" name="sort">
									<option value="PROPERTY_FINAL_RATING" <?=$_REQUEST['SORT']=='PROPERTY_FINAL_RATING' ? 'selected' : '';?>><?=GetMessage('RATING');?></option>
									<option value="PROPERTY_EXPERT_RATING" <?=$_REQUEST['SORT']=='PROPERTY_EXPERT_RATING' ? 'selected' : '';?>><?=GetMessage('RATING_EXP');?></option>
									<option value="CATALOG_PRICE_1" <?=$_REQUEST['SORT']=='CATALOG_PRICE_1' ? 'selected' : '';?>><?=GetMessage('PRICE');?></option>
									<option value="CREATED" <?=$_REQUEST['SORT']=='CREATED' ? 'selected' : '';?>><?=GetMessage('CREATED');?></option>
									<option value="PROPERTY_SIZE_SORT" <?=$_REQUEST['SORT']=='PROPERTY_SIZE_SORT' ? 'selected' : '';?>><?=GetMessage('SIZE');?></option>
									<option value="NAME" <?=$_REQUEST['SORT']=='NAME' ? 'selected' : '';?>><?=GetMessage('NAME_SORT');?></option>
							</select>
							</div>
						</div>
					<?endif;?>
				</div>
				<div id="lk-works__ajax" data-page="<?=$_REQUEST['PAGEN_1'] ? $_REQUEST['PAGEN_1'] : 1;?>">
					<?if($_REQUEST['AJAX']){ $GLOBALS['APPLICATION']->RestartBuffer();  } //print_r($_REQUEST);?>
						<div class="lk-works">
							<?foreach($arResult["ITEMS"] as $arItem):?>
								<?
								$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
								$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
								?>
								<?if(!$arItem['PROPERTIES']['WORK_PURCHASED']['VALUE']):?>
									<div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="cart-item cart-item_work cart-item_work_<?=$arItem['ID']?>">
										<a class="cart-item__img" href="<?=str_replace(array('/personal/work-list/', '/'.$arItem["ID"].'/'), array('/works/', '/'.$arItem["CODE"].'/'), $arItem["DETAIL_PAGE_URL"]);?>" style="background-image:url(<?=$arItem["DETAIL_PICTURE"]["SRC"]?>"></a>
										<div class="cart-item__main"><a class="cart-item__title" href="<?=str_replace(array('/personal/work-list/', '/'.$arItem["ID"].'/'), array('/works/', '/'.$arItem["CODE"].'/'), $arItem["DETAIL_PAGE_URL"]);?>"><?=$arItem["NAME"]?></a>
										<div class="cart-item__info">
											<div>
												<?if ($arItem['ACTIVE'] == 'Y') {?>
													<span class="cart-item__status"><?=GetMessage('WORK');?> <span class="status-good"><?=GetMessage('PUBLISHED');?></span></span>
												<?} elseif ($arItem['PROPERTIES']['MODERATION']['VALUE_ENUM_ID'] == WORK_PROP_MODERATION_UNCOMPLETED) {?>
													<span class="cart-item__status"><?=GetMessage('WORK');?> <span class="status-warning"><?=GetMessage('NOT_PUBLISHED');?></span></span>
												<?} else{ ?>
													<span class="cart-item__status"><?=GetMessage('WORK');?> <span class="status-good"><?=GetMessage('IN_PROCESS');?></span></span>
												<?}?>
												<? switch ($arItem['PROPERTIES']['MODERATION']['VALUE_ENUM_ID']) {
													case WORK_PROP_MODERATION_COMPLETED:
														?><span class="cart-item__status"><?=GetMessage('MODERATION');?> <span class="status-good"><?=GetMessage('PASSED');?></span></span><?
														break;
													case WORK_PROP_MODERATION_PROCESS:
														?><span class="cart-item__status"><?=GetMessage('MODERATION');?> <span class="status-warning"><?=GetMessage('IN_THE_PROCESS');?></span></span><?
														break;
													case WORK_PROP_MODERATION_UNCOMPLETED:
														?><span class="cart-item__status"><?=GetMessage('MODERATION');?> <span class="status-error"><?=GetMessage('NOT_PASSED');?></span></span><?
														break;
												}?>
											</div>
											<?if($arItem['PRICE']):?>	
												<div class="cart-item__prices">
													<span class="cart-item__price"><?=number_format($arItem['PRICE']['DISCOUNT_PRICE'], 0, ',', ' ')?> <span class="rubl">₽</span></span>
													<?if($arItem['PRICE']['DISCOUNT']):?>
														<span class="cart-item__oldprice"><?=number_format($arItem['PRICE']['PRICE']['PRICE'], 0, ',', ' ')?></span>
													<?endif;?>
												</div>
											<?endif;?>
										</div>
										</div>
										<div class="cart-item__tools js-cart-item-tools">
										<div class="cart-item__tools-head js-cart-item-tools-head">
											<svg class="icon icon-dots">
											<use xlink:href="#icon-dots"></use>
											</svg>
										</div>
										<div class="cart-item__tools-main js-cart-item-tools-main"><span class="cart-item__tool cart-item__tool_dot js-work__delete" data-id="<?=$arItem['ID']?>" data-name="<?=$arItem["NAME"]?>"><span><?=GetMessage('DELETE');?></span>
											<svg class="icon icon-close">
												<use xlink:href="#icon-close"></use>
											</svg></span><span class="cart-item__tool"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage('EDIT');?></a>
											<svg class="icon icon-edit">
												<use xlink:href="#icon-edit"></use>
											</svg></span></div>
										</div>
									</div>
								<?else:?>
									<div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="cart-item cart-item_work cart-item_work_<?=$arItem['ID']?>">
										<a class="cart-item__img" href="<?=str_replace(array('/personal/work-list/', '/'.$arItem["ID"].'/'), array('/works/', '/'.$arItem["CODE"].'/'), $arItem["DETAIL_PAGE_URL"]);?>" style="background-image:url(<?=$arItem["DETAIL_PICTURE"]["SRC"]?>"></a>
										<div class="cart-item__main"><a class="cart-item__title" href="<?=str_replace(array('/personal/work-list/', '/'.$arItem["ID"].'/'), array('/works/', '/'.$arItem["CODE"].'/'), $arItem["DETAIL_PAGE_URL"]);?>"><?=$arItem["NAME"]?></a>
											<div class="cart-item__info">
												<div>
													<span class="cart-item__status"><?=GetMessage('WORK');?> <span class="status-good"><?=GetMessage('SOLD');?></span></span>

													
												</div>
												<?if($arItem['PRICE']):?>	
													<div class="cart-item__prices">
														<span class="cart-item__price"><?=number_format($arItem['PRICE']['DISCOUNT_PRICE'], 0, ',', ' ')?> <span class="rubl">₽</span></span>
														<?if($arItem['PRICE']['DISCOUNT']):?>
															<span class="cart-item__oldprice"><?=number_format($arItem['PRICE']['PRICE']['PRICE'], 0, ',', ' ')?></span>
														<?endif;?>
													</div>
												<?endif;?>
											</div>
										</div>
										<div class="cart-item__tools js-cart-item-tools">
											<div class="cart-item__tools-head js-cart-item-tools-head">
												<svg class="icon icon-dots">
												<use xlink:href="#icon-dots"></use>
												</svg>
											</div>
										</div>
										
									</div>
								<?endif;?>
							<?endforeach;?>
						
						</div>
						<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
							<br /><?=$arResult["NAV_STRING"]?>
						<?endif;?>
					<?if($_REQUEST['AJAX']){ die();}?>
				</div>
                
</div>

