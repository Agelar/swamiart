<?php
use HB\helpers\LangHelpers;
foreach($arResult['ITEMS'] as $key => $arItem){
    $arResult["ITEMS"][$key] = LangHelpers::getTranslation($arItem);
    $arResult['ITEMS'][$key]["PRICE"] = CCatalogProduct::GetOptimalPrice($arItem['ID'], 1);
}