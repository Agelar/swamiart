<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["MY_WORKS"] = "My works";
$MESS["IN_SECTION"] = "In this section you can post new works, <br/> and edit existing ones";
$MESS["ADD_WORK"] = "Add work";
$MESS["SORT"] = "Sort by";
$MESS["RATING"] = "Rating";
$MESS["RATING_EXP"] = "Expert Rating";
$MESS["PRICE"] = "Price";
$MESS["CREATED"] = "Date of publication";
$MESS["SIZE"] = "Size";
$MESS["NAME_SORT"] = "Name";
$MESS["WORK"] = "Work";
$MESS["PUBLISHED"] = "published";
$MESS["NOT_PUBLISHED"] = "not published";
$MESS["IN_PROCESS"] = "work is processed";
$MESS["MODERATION"] = "Moderation";
$MESS["PASSED"] = "passed";
$MESS["IN_THE_PROCESS"] = "in the process";
$MESS["NOT_PASSED"] = "canceled";
$MESS["DELETE"] = "Delete";
$MESS["EDIT"] = "Edit";
$MESS["SOLD"] = "Sold";
?>
