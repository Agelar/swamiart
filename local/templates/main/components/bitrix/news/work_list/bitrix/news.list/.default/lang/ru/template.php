<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["MY_WORKS"] = "Мои работы";
$MESS["IN_SECTION"] = "В этом разделе вы можете размещать новые работы, <br/> а также редактировать существующие";
$MESS["ADD_WORK"] = "Добавить работу";
$MESS["SORT"] = "Сортировать по";
$MESS["RATING"] = "Рейтингу";
$MESS["RATING_EXP"] = "Рейтингу эксперта";
$MESS["PRICE"] = "Цене";
$MESS["CREATED"] = "Дате публикации";
$MESS["SIZE"] = "Размеру";
$MESS["NAME_SORT"] = "Названию";
$MESS["WORK"] = "Работа";
$MESS["PUBLISHED"] = "опубликована";
$MESS["NOT_PUBLISHED"] = "не опубликована";
$MESS["IN_PROCESS"] = "обрабатывается";
$MESS["MODERATION"] = "Модерация";
$MESS["PASSED"] = "пройдена";
$MESS["IN_THE_PROCESS"] = "в процессе";
$MESS["NOT_PASSED"] = "отклонена";
$MESS["DELETE"] = "Удалить";
$MESS["EDIT"] = "Редактировать";
$MESS["SOLD"] = "Продана";
?>