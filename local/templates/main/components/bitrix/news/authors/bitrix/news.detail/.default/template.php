<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use HB\helpers\Helpers;
use HB\helpers\WorkHelpers;
?>
<div class="section author">
	<div class="container">
		<div class="author__picture">
			<div class="author__picture-bg"></div>
			<div class="author__picture-parallax">
				<?if($arResult["AUTHOR_PICTURES"]):?>
					<img class="author__picture-parallax-img" src="<?=$arResult["AUTHOR_PICTURES"]?>" alt="picture" data-parallax-img="data-parallax-img" data-speed="0.2" />
				<?endif;?>
			</div>
			<div class="author__avatar" <?if($arResult["DETAIL_PICTURE"]["RESIZE"]["src"]) echo "style='background-image:url(" . $arResult["DETAIL_PICTURE"]["RESIZE"]["src"] . ")'"?>>
			</div>
		</div>
		<h1 class="author__name"><?=$arResult["NAME"]?><?if($arResult["STYLE"]):?><?=GetMessage("GENERAL_STYLE")?> <?=$arResult["STYLE"]?><?endif;?></h1>
		<div class="author__stats-container">
			<div class="author__stats-first"></div>
			<div class="author__stats">
				<div class="author__stats-wrapper">
					<div class="author__stats-cell">
						<div class="author__stats-val"><?=number_format($arParams["SHOW_ALL"], 0, "", " ")?></div>
						<div class="author__stats-desc"><?=GetMessage(Helpers::endingWord($arParams["SHOW_ALL"], array("ONE_SHOW", "TWO_SHOW", "FIVE_SHOW")))?></div>
					</div>
					<div class="author__stats-cell">
						<div class="author__stats-val"><?=number_format($arParams["COUNT_PICTURE"], 0, "", " ")?></div>
						<div class="author__stats-desc"><?=GetMessage(Helpers::endingWord($arParams["COUNT_PICTURE"], array("ONE_PAINT", "TWO_PAINT", "FIVE_PAINT")))?></div>
					</div>
					<?if($arResult["PROPERTIES"]["TOWN"]["VALUE"]):?>
						<div class="author__stats-cell">
							<div class="author__stats-val"><?=$arResult["PROPERTIES"]["TOWN"]["VALUE"]?></div>
							<div class="author__stats-desc"><?=GetMessage("CITY")?></div>
						</div>
					<?endif;?>
					<?if($arResult["PROPERTIES"]["STYLE"]["VALUE_FORMAT"]):?>
						<div class="author__stats-cell">
							<div class="author__stats-val"><?=$arResult["PROPERTIES"]["STYLE"]["VALUE_FORMAT"]?></div>
							<div class="author__stats-desc"><?=GetMessage("STYLE")?></div>
						</div>
					<?endif;?>
				</div>
			</div>
			<div class="author__stats-last"></div>
		</div>
		<div class="author__bio">
			<?if(is_array($arResult["DETAIL_TEXT"])):?>
				<span class="author__bio-shown"><?=$arResult["DETAIL_TEXT"]["BEFORE"]?></span>
				<span class="author__bio-hidden">
					<?=$arResult["DETAIL_TEXT"]["AFTER"]?>
				</span>
				<div class="author__bio-more">
					<div class="author__bio-more-text"><?=GetMessage("MORE")?></div>
					<div class="author__bio-more-icon">
						<div class="arrow"></div>
					</div>
				</div>
			<?else:?>
				<span class="author__bio-shown not-after"><?=$arResult["DETAIL_TEXT"]?></span>
			<?endif;?>
		</div>
		<div class="author__share">
			<div class="author__share-text"><?=GetMessage("SHARE")?></div>
			<div class="author__share-social">
				<a class="author__share-link js-social-link" href="<?=$arResult["SOCIAL"]["VK"]?>" target="_blank">
					<div class="author__share-link-icon">
						<svg class="icon social-vk-w">
							<use xlink:href="#social-vk"></use>
						</svg>
					</div>
				</a>
				<a class="author__share-link js-social-link" href="<?=$arResult["SOCIAL"]["FACEBOOK"]?>" target="_blank">
					<div class="author__share-link-icon">
						<svg class="icon social-fb-w">
							<use xlink:href="#social-fb"></use>
						</svg>
					</div>
				</a>
				<a class="author__share-link js-social-link" href="<?=$arResult["SOCIAL"]["OK"]?>" target="_blank">
					<div class="author__share-link-icon">
						<svg class="icon social-ok-w">
							<use xlink:href="#social-ok"></use>
						</svg>
					</div>
				</a>
				<a class="author__share-link js-social-link" href="<?=$arResult["SOCIAL"]["VIBER"];?>" target="_blank">
					<div class="author__share-link-icon">
						<svg class="icon social-viber">
							<use xlink:href="#social-viber"></use>
						</svg>
					</div>
                  </a>
                  <a class="author__share-link js-social-link" href="<?=$arResult["SOCIAL"]["WHATSAPP"];?>" target="_blank">
				  	<div class="author__share-link-icon">
                    	<svg class="icon social-whatsapp">
                      		<use xlink:href="#social-whatsapp"></use>
                    	</svg>
					</div>
                  </a>
				<a class="author__share-link js-popup-share_mail" data-name="<?=$arResult["NAME"]?>" data-link="<?=$APPLICATION->GetCurPage(false)?>" href="#">
					<div class="author__share-link-icon">
						<svg class="icon mail-filled-w">
							<use xlink:href="#mail-filled"></use>
						</svg>
					</div>
				</a>
			</div>
			<?if($arParams["NOTIFICATION"] == "Y"):?>
				<a class="author__share-btn js-subscribe unsubscribe" href="#" data-lang="<?=SITE_ID?>" data-id="<?=$arResult["ID"]?>"><?=GetMessage("UNSUBSCRIBE")?></a>
			<?else:?>
				<a class="author__share-btn <?if($USER->isAuthorized()):?>js-subscribe<?else:?>js-popup-auth<?endif;?>" href="#" data-lang="<?=SITE_ID?>" data-id="<?=$arResult["ID"]?>"><?=GetMessage("SUBSCRIBE")?></a>
			<?endif;?>
		</div>
	</div>
</div>
<div class="section author__gallery">
	<div class="section-head">
		<div class="container">
			<div class="section-head__inner"><span class="section-head__title"><?=GetMessage("WORKS_AUTHOR")?></span></div>
		</div>
	</div>
		<?
		$arFavorites = WorkHelpers::getFavoritesUserWork();
		$arCart = WorkHelpers::getCartWork();
		global $arrFilter;
		$arrFilter = array("PROPERTY_AUTHOR" => $arResult["ID"],"PROPERTY_MODERATION_VALUE" => "Пройдена", "PROPERTY_WORK_PURCHASED" => false);?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:catalog.section", 
			"work_authors", 
			array(
				"IBLOCK_TYPE" => "catalogs",
				"IBLOCK_ID" => "7",
				"ELEMENT_SORT_FIELD" => "id",
				"ELEMENT_SORT_ORDER" => "desc",
				"BROWSER_TITLE" => "-",
				"SET_LAST_MODIFIED" => "N",
				"INCLUDE_SUBSECTIONS" => "Y",
				"FILTER_NAME" => "arrFilter",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => 3600,
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "N",
				"SET_TITLE" => "N",
				"SET_STATUS_404" => "N",
				"SHOW_404" => "N",
				"DISPLAY_COMPARE" => "N",
				"PAGE_ELEMENT_COUNT" => "10",
				"LINE_ELEMENT_COUNT" => "3",
				"PRICE_CODE" => array(
					0 => "BASE",
				),
				"USE_PRICE_COUNT" => "N",
				"PRICE_VAT_INCLUDE" => "N",
				"USE_PRODUCT_QUANTITY" => "N",
				"ADD_PROPERTIES_TO_BASKET" => "N",
				"PARTIAL_PRODUCT_PROPERTIES" => "N",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"PAGER_TEMPLATE" => ".default",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
			
				"PAGER_SHOW_ALL" => "N",
				"PAGER_BASE_LINK_ENABLE" => "N",
				"LAZY_LOAD" => "N",
				"LOAD_ON_SCROLL" => "N",
				"USE_MAIN_ELEMENT_SECTION" => "N",
				"CONVERT_CURRENCY" => "N",
				"HIDE_NOT_AVAILABLE" => "Y",
				"HIDE_NOT_AVAILABLE_OFFERS" => "Y",
				"LABEL_PROP" => array(
				),
				"ADD_PICT_PROP" => "-",
				"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'0','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
				"ENLARGE_PRODUCT" => "STRICT",
				"SHOW_SLIDER" => "N",
				"PRODUCT_SUBSCRIPTION" => "N",
				"SHOW_DISCOUNT_PERCENT" => "N",
				"SHOW_OLD_PRICE" => "Y",
				"SHOW_PRICE_COUNT" => "1",
				"PRICE_VAT_INCLUDE" =>"1",
				"SHOW_MAX_QUANTITY" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"ADD_TO_BASKET_ACTION" => "ADD",
				"SHOW_CLOSE_POPUP" => "N",
				"USE_COMPARE_LIST" => "Y",
				"COMPATIBLE_MODE" => "N",
				"DISABLE_INIT_JS_IN_COMPONENT" => "N",
				"COMPONENT_TEMPLATE" => "work",
				"SECTION_USER_FIELDS" => array(
					0 => "",
					1 => "",
				),
				"PRODUCT_BLOCKS_ORDER"=> array(
					"price",
				),

				"SHOW_ALL_WO_SECTION" => "Y",
				"CUSTOM_FILTER" => "",
				"RCM_TYPE" => "personal",
				"SHOW_FROM_SECTION" => "N",
				"SEF_MODE" => "N",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"SET_BROWSER_TITLE" => "N",
				"SET_META_KEYWORDS" => "N",
				"SET_META_DESCRIPTION" => "N",
				"LAZY_LOAD" => "Y",
				"FAVORITES" => $arFavorites,
				"CART_WORK" => $arCart,
			),
			false
		);?>
</div>
<?$templateData = $arResult;?>
<script>
BX.message({
	SUBSCRIBE: '<?=GetMessageJS('SUBSCRIBE')?>',
	UNSUBSCRIBE: '<?=GetMessageJS('UNSUBSCRIBE')?>',
});
</script>