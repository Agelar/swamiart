<?
use Bitrix\Main\Loader;
use HB\helpers\LangHelpers;
use HB\helpers\Helpers;
Loader::IncludeModule("hb.site");
$arResult = LangHelpers::getTranslation($arResult);

$pictureBg = Helpers::getSiteSetting("AUTHOR_PAGE_PICTURE");
if($pictureBg){
    $arResult["AUTHOR_PICTURES"] = CFile::getPath($pictureBg);
}

if($arResult["DETAIL_PICTURE"]["ID"]){
    $arResult["DETAIL_PICTURE"]["RESIZE"] = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"]["ID"], array("width"=>"1000","height"=>"1000", BX_RESIZE_IMAGE_PROPORTIONAL));
}

if($arResult["PROPERTIES"]["STYLE"]["VALUE"]){
    $style = CIBlockElement::GetList(array(), array("ACTIVE" => "Y", "ID" => $arResult["PROPERTIES"]["STYLE"]["VALUE"], "IBLOCK_ID" => $arResult["PROPERTIES"]["STYLE"]["LINK_IBLOCK_ID"]), false, false, array("NAME","PROPERTY_NAME_EN"))->fetch();
    $arResult["PROPERTIES"]["STYLE"]["VALUE_FORMAT"] = LangHelpers::getTranslation($style)["NAME"];
    $arResult["STYLE"] = LangHelpers::getTranslation($style)["NAME"];
}

$arResult["SOCIAL"] = Helpers::getSocialLinks($arResult["NAME"],$arResult["DETAIL_TEXT"]);

if($arResult["DETAIL_TEXT"]){
    $arResult["DETAIL_TEXT"] = Helpers::cutStr($arResult["DETAIL_TEXT"], 140,"");
}
?>