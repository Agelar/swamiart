<?
 $MESS["ONE_SHOW"] = "Просмотр";
 $MESS["TWO_SHOW"] = "Просмотра";
 $MESS["FIVE_SHOW"] = "Просмотров";
 $MESS["ONE_PAINT"] = "Картина";
 $MESS["TWO_PAINT"] = "Картины";
 $MESS["FIVE_PAINT"] = "Картин";
 $MESS["CITY"] = "Город";
 $MESS["STYLE"] = "Стиль";
 $MESS["MORE"] = "Подробнее";
 $MESS["SHARE"] = "Поделиться";
 $MESS["SUBSCRIBE"] = "Подписаться";
 $MESS["UNSUBSCRIBE"] = "Отписаться";
 $MESS["WORKS_AUTHOR"] = "Работы автора";
 $MESS["GENERAL_STYLE"] = ". Основной стиль";

?>