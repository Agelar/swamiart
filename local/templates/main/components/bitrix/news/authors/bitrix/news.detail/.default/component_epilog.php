<?
use HB\helpers\LangHelpers;
use HB\helpers\CatalogHelpers;
$APPLICATION->AddChainItem($arResult["NAME"], $arResult["DETAIL_PAGE_URL"]);

$arElMetaProp = CatalogHelpers::getSeoField($arResult["IBLOCK_ID"], $arResult["ID"], $templateData,["NAME"]);
    if($arElMetaProp["ELEMENT_META_TITLE"]){
        $APPLICATION->SetPageProperty("title", $arElMetaProp["ELEMENT_META_TITLE"]);
    }

    if($arElMetaProp["ELEMENT_META_DESCRIPTION"]){
        $APPLICATION->SetPageProperty("description", $arElMetaProp["ELEMENT_META_DESCRIPTION"]);
    }

    if($arElMetaProp["ELEMENT_META_KEYWORDS"]){
        $APPLICATION->SetPageProperty("keywords", $arElMetaProp["ELEMENT_META_KEYWORDS"]);
    }

    if($arElMetaProp["ELEMENT_PAGE_TITLE"]){
        $APPLICATION->SetTitle($arElMetaProp["ELEMENT_PAGE_TITLE"]);
    }