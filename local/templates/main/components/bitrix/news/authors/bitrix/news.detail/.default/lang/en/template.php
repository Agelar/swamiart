<?
 $MESS["ONE_SHOW"] = "View";
 $MESS["TWO_SHOW"] = "View";
 $MESS["FIVE_SHOW"] = "Views";
 $MESS["ONE_PAINT"] = "Painting";
 $MESS["TWO_PAINT"] = "Paintings";
 $MESS["FIVE_PAINT"] = "Paintings";
 $MESS["CITY"] = "City";
 $MESS["STYLE"] = "Pencil";
 $MESS["MORE"] = "More";
 $MESS["SHARE"] = "Share";
 $MESS["SUBSCRIBE"] = "Subscribe";
 $MESS["UNSUBSCRIBE"] = "Unsubscribe";
 $MESS["WORKS_AUTHOR"] = "Author's works";
 $MESS["GENERAL_STYLE"] = ". Basic style";
?>