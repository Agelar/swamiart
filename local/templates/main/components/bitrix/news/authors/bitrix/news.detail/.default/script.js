$(document).on("click",".js-subscribe",function(e){
    e.preventDefault();
    var userID = $(this).data("id");
    var self = $(this);
    var type = self.hasClass("unsubscribe") ? "unsubscribe" : "subscribe";
    $.ajax({
        url: "/local/ajax/subscribe_on_author.php",
        dataType: "json",
        method: "POST",
        data:{
            "sessid": BX.bitrix_sessid(),
            "authorID": userID,
            "action" : type,
            "lang" : BX.message("SITE_ID"),
        }
    }).done(function(data){
        if(data.HTML){
            var idPopup = Math.floor(Math.random() * (99999 - 11111 + 1)) + 11111;
            $(".popup .popup__window").append('<div class="popup__content popup__content--' + idPopup + '">' + data.HTML + "</div>");
            $('.popup__content').hide();
            $('.popup__content--' + idPopup).show();
            $(".popup").fadeIn(300);
            setTimeout(
                function(){
                    $('.popup__close').trigger('click');
                },
                2000
            );
        }
        if(data.STATUS){
            if(self.hasClass("unsubscribe")){
                self.text(BX.message('SUBSCRIBE')).removeClass("unsubscribe");
            }else{
                self.text(BX.message('UNSUBSCRIBE')).addClass("unsubscribe");
            }
        }
    });
});