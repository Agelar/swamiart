<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?global $USER;?>
<?if($USER->isAuthorized()):?>
	<script>
		$('.popup').hide();
		window.location.reload();
	</script>
<?endif;?>
<form class="popup-form-auth popup__content-wrapper js-auth-form" method="post" action="<?=$arResult["AUTH_URL"]?>">
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="AUTH" />
	<?if (strlen($arResult["BACKURL"]) > 0):?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
	<?endif?>
	<?foreach ($arResult["POST"] as $key => $value):?>
		<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
	<?endforeach?>

	<span class="popup__heading popup__heading--bottom"><?=GetMessage("AUTH_HEADER")?></span>
	<div class="popup__desc hidden-mobile-down">
		<span class="popup__desc-text"></span>
		<div class="popup__desc-switch">
			<div class="popup__desc-switch-text"><?=GetMessage("AUTH_NOT_ACC")?></div>
			<a class="popup__desc-switch-link js-popup-reg" href="#reg"><?=GetMessage("AUTH_REG")?></a>
		</div>
	</div>
	<div class="popup__table">
	<?if($arResult["AUTH_SERVICES"]):?>
		<?
		$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "socbutton", 
			array(
				"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
				"AUTH_URL"=>$arResult["AUTH_URL"],
				"POST"=>$arResult["POST"],
				"SUFFIX"=>"form",
			), 
			$component, 
			array("HIDE_ICONS"=>"Y")
		);
		?>
	<?endif?>
	<div class="popup__mail">
		<div class="popup__mail-inputs">
			
		<label class="label">
			<div class="popup__label"><?=GetMessage("AUTH_EMAIL")?></div>
			<input class="input input-text <?if($arParams["FIND_ERROR"]["LOGIN"]):?>error<?endif;?>" type="email" name="USER_LOGIN" value="<?=$_REQUEST["USER_LOGIN"]?>" required="required"/>
			<?if($arParams["FIND_ERROR"]["LOGIN"]):?>
				<label id="USER_LOGIN-error" class="error" for="USER_LOGIN"><?=GetMessage("USER_NOT_FOUND")?></label>
			<?endif;?>
		</label>
		<label class="label">
			<div class="popup__label"><?=GetMessage("AUTH_PASSWORD")?></div>
			<input class="input input-text <?if($arParams["FIND_ERROR"]["PASSWORD"]):?>error<?endif;?>" type="password" name="USER_PASSWORD" required="required"/>
			<?if($arParams["FIND_ERROR"]["PASSWORD"]):?>
				<label id="USER_PASSWORD-error" class="error" for="USER_PASSWORD"><?=GetMessage("USER_PASS_NOT")?></label>
			<?endif;?>
		</label>
		</div>
		<button class="popup__mail-submit popup__mail-submit--auth" type="submit" value="" name="Login"><?=GetMessage("AUTH_BTN_SUBMIT")?></button>
		<div class="popup__mail-restore">
		<a class="popup__desc-switch-link js-popup-reg hidden-tablet-up" href="#"><?=GetMessage("AUTH_REG")?></a>
			<span class="popup__mail-restore-text hidden-mobile-down"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></span>
			<a class="popup__mail-restore-link js-popup-restore" href="#fogot"><?=GetMessage("AUTH_FORGOT_PASSWORD_GET")?></a>
		</div>
	</div>
	</div>
</form>