<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="container access-denied">
    <div class="blog-detail__subheading"><?=GetMessage("ACCSESS_DENIED")?>
        <a class="js-popup-auth" href="#auth"><?=GetMessage("ACCSESS_DENIED_LINK")?></a><?=GetMessage("ACCSESS_DENIED1")?>.
    </div>
</div>