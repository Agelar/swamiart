<?
$MESS["AUTH_HEADER"] = "Авторизация";
$MESS["AUTH_BTN_SUBMIT"] = "Авторизоваться";
$MESS["AUTH_NOT_ACC"] = "Еще нет аккаунта?";
$MESS["AUTH_REG"] = "Зарегистрироваться";
$MESS["AUTH_EMAIL"] = "E-mail";
$MESS["AUTH_PLEASE_AUTH"] = "Пожалуйста, авторизуйтесь:";
$MESS["AUTH_LOGIN"] = "Логин:";
$MESS["AUTH_PASSWORD"] = "Пароль";
$MESS["USER_NOT_FOUND"] = "Пользователь с таким e-mail не найден";
$MESS["USER_PASS_NOT"] = "Неверный email или пароль";
$MESS["AUTH_REMEMBER_ME"] = "Запомнить меня";
$MESS["AUTH_AUTHORIZE"] = "Вход";
$MESS["AUTH_REGISTER"] = "Зарегистрироваться";
$MESS["AUTH_FIRST_ONE"] = "Если вы впервые на сайте, заполните, пожалуйста, регистрационную форму.";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "Забыли пароль?";
$MESS["AUTH_FORGOT_PASSWORD_GET"] = "Восстановить пароль";
$MESS["AUTH_CAPTCHA_PROMT"] = "Введите слово на картинке";
$MESS["AUTH_TITLE"] = "Войти на сайт";
$MESS["AUTH_SECURE_NOTE"] = "Перед отправкой формы авторизации пароль будет зашифрован в браузере. Это позволит избежать передачи пароля в открытом виде.";
$MESS["AUTH_NONSECURE_NOTE"] = "Пароль будет отправлен в открытом виде. Включите JavaScript в браузере, чтобы зашифровать пароль перед отправкой.";
$MESS["ACCSESS_DENIED"] = "Для доступа к данным необходимо ";
$MESS["ACCSESS_DENIED_LINK"] = "авторизоваться";
?>