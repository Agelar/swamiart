<?
$MESS["AUTH_HEADER"] = "Login";
$MESS["AUTH_NOT_ACC"] = "No account?";
$MESS["AUTH_REG"] = "Register";
$MESS["AUTH_EMAIL"] = "E-mail";
$MESS["AUTH_PLEASE_AUTH"] = "Please authorize:";
$MESS["AUTH_LOGIN"] = "Login:";
$MESS["AUTH_PASSWORD"] = "Password:";
$MESS["AUTH_REMEMBER_ME"] = "Remember me";
$MESS["USER_NOT_FOUND"] = "User with this e-mail not found";
$MESS["USER_PASS_NOT"] = "Invalid email or password";
$MESS["AUTH_AUTHORIZE"] = "Authorize";
$MESS["AUTH_REGISTER"] = "Register";
$MESS["AUTH_FIRST_ONE"] = "If you are a first-time visitor on the site, please fill in the registration form.";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "Forgot your password?";
$MESS["AUTH_CAPTCHA_PROMT"] = "Type text from image";
$MESS["AUTH_TITLE"] = "Log In";
$MESS["AUTH_SECURE_NOTE"] = "The password will be encrypted before it is sent. This will prevent the password from appearing in open form over data transmission channels.";
$MESS["AUTH_NONSECURE_NOTE"] = "The password will be sent in open form. Enable JavaScript in your web browser to enable password encryption."; 
$MESS["ACCSESS_DENIED"] = "You must ";
$MESS["ACCSESS_DENIED_LINK"] = "login";
$MESS["ACCSESS_DENIED1"] = " to access the data";
?>