$(document).on('formSubmit',".js-auth-form",function(e){
    e.preventDefault();
    var form_action = $(this).attr('action');
    var form_backurl = $(this).find('input[name="backurl"]').val();

    $.ajax({
        type: "POST",
        url: '/local/ajax/auth.php',
        data: $(this).serialize(),
        timeout: 3000,
        success: function(data) {
            $('.js-wrap-auth').html(data);
            $(document).trigger('formInit');
        }
    });

    return false;
});