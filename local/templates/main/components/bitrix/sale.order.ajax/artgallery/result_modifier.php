<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */

$component = $this->__component;
$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);

global $USER;
if($USER->IsAuthorized()){
    $arSelect = array("NAME", "LAST_NAME", "PERSONAL_PHONE", "EMAIL", "UF_CITY", "UF_STREET", "UF_HOUSE",  "UF_APARTAMENT");
    $arResult['USER'] = CUser::GetList(($by="id"), ($order="desc"), array("ID" => $USER->GetID()),array("SELECT" => $arSelect, "FIELDS" =>$arSelect))->fetch();
}