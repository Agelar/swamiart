<?
$MESS ['FILTER'] = "Filter";
$MESS ['EXPAND'] = "Expand";
$MESS ['COLLAPSE'] = "Collapse";
$MESS ['PRICE'] = "Price";
$MESS ['START_TYPING_NAME'] = "Start typing the author's first or last name";
$MESS ['DEL_FILTER'] = "Reset";
$MESS["CT_BCSF_FILTER_COUNT"] = "Selected: #ELEMENT_COUNT#";
$MESS['REGION'] = 'Painting in your area';
$MESS['SHOW_PICTURES'] = 'Show pictures';
?>