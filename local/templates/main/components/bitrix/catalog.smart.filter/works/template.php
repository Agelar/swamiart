<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="filter-wrapper js-filter-wrapper">
    <div class="filter filter_fixed js-filter js-filter-fixed">
            <div class="filter__head js-filter-head <?if($_REQUEST['bxajaxid']):?>is-active<?endif;?>">
            <div class="container">
                <div class="filter__head-inner">
                <svg class="icon icon-filter">
                    <use xlink:href="#icon-filter"></use>
                </svg><span><?=GetMessage('FILTER');?></span>
                </div>
            </div>
            </div>
            <div class="filter__main js-filter-main" <?if($_REQUEST['bxajaxid']):?>style="display:block;"<?endif;?>>
                <div class="filter__buttons js-filter-buttons">
                    <div class="container">
                        <div class="filter__buttons-inner">
                            <button class="btn btn_icon btn_arrow filter__toggle js-filter-toggle <?=$arResult['FILTER_ACTIVE']=='Y' ? 'is-active' : ''?>"><span><?=GetMessage('EXPAND');?></span><span><?=GetMessage('COLLAPSE');?></span></button>
                        </div>
                    </div>
                </div>
                <form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">
                    <?foreach($arResult["HIDDEN"] as $arItem):?>
                        <input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
                    <?endforeach;?>
                    <div class="container">
                        <div class="filter__inner">
                            <div class="filter__row">
                                <? $activeItem = false; foreach($arResult["ITEMS"] as $key=>$arItem)//prices
                                {
                                    if($arItem["DISPLAY_EXPANDED"]=='Y' && !isset($arItem["PRICE"]) && $arItem['CODE']!='SIZE_TYPE' && $arItem['CODE']!='ORIENTATION'){
                                        $activeItem = true;
                                    }
                                    if(empty($arItem["VALUES"]))
                                        continue;

                                    if (
                                        $arItem["DISPLAY_TYPE"] == "A"
                                        && (
                                            $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
                                        )
                                    )
                                        continue;
                                    
                                    $key = $arItem["ENCODED_ID"];
                                    if(isset($arItem["PRICE"])):
                                        
                                        if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
                                            continue;

                                        $step_num = 4;
                                        $step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / $step_num;
                                        $prices = array();
                                        if (Bitrix\Main\Loader::includeModule("currency"))
                                        {
                                            for ($i = 0; $i < $step_num; $i++)
                                            {
                                                $prices[$i] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MIN"]["VALUE"] + $step*$i, $arItem["VALUES"]["MIN"]["CURRENCY"], false);
                                            }
                                            $prices[$step_num] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MAX"]["VALUE"], $arItem["VALUES"]["MAX"]["CURRENCY"], false);
                                        }
                                        else
                                        {
                                            $precision = $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0;
                                            for ($i = 0; $i < $step_num; $i++)
                                            {
                                                $prices[$i] = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step*$i, $precision, ".", "");
                                            }
                                            $prices[$step_num] = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
                                        }?>
                                        
                                        <div class="filter__item filter__item_top bx-filter-parameters-box"><span class="bx-filter-container-modef"></span><span class="filter__item-title"><?=GetMessage('PRICE');?></span>
                                            <div class="filter__item-main">
                                                <div class="l-rangeslider js-l-rangeslider">
                                                    <div class="l-rangeslider__inputs">
                                                        <input 
                                                            class="l-rangeslider__input js-l-rangeslider-from" 
                                                            type="text"
                                                            name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
                                                            id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
                                                            value="<?=$arItem["VALUES"]["MIN"]["HTML_VALUE"] ? $arItem["VALUES"]["MIN"]["HTML_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ;?>"
                                                            size="5"
                                                            onclick="smartFilter.keyup(this)"
                                                            onchange="smartFilter.keyup(this)"
                                                        />
                                                        <div class="l-rangeslider__separate"></div>
                                                        <input 
                                                            class="l-rangeslider__input js-l-rangeslider-to" 
                                                            type="text"
                                                            name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
                                                            id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
                                                            value="<?=$arItem["VALUES"]["MAX"]["HTML_VALUE"] ? $arItem["VALUES"]["MAX"]["HTML_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"]?>"
                                                            size="5"
                                                            onclick="smartFilter.keyup(this)"
                                                            onchange="smartFilter.keyup(this)"
                                                        />
                                                    </div>
                                                    <div class="l-rangeslider__main">
                                                        <input class="js-l-rangeslider-main" type="text" data-min="<?=$arItem["VALUES"]["MIN"]["VALUE"] ;?>" data-max="<?=$arItem["VALUES"]["MAX"]["VALUE"]?>" data-from="<?=$arItem["VALUES"]["MIN"]["HTML_VALUE"] ? $arItem["VALUES"]["MIN"]["HTML_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ;?>" data-to="<?=$arItem["VALUES"]["MAX"]["HTML_VALUE"] ? $arItem["VALUES"]["MAX"]["HTML_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"]?>" data-step="1000" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?
                                        $arJsParams = array(
                                            "leftSlider" => 'left_slider_'.$key,
                                            "rightSlider" => 'right_slider_'.$key,
                                            "tracker" => "drag_tracker_".$key,
                                            "trackerWrap" => "drag_track_".$key,
                                            "minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
                                            "maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
                                            "minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
                                            "maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
                                            "curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
                                            "curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
                                            "fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ,
                                            "fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
                                            "precision" => $precision,
                                            "colorUnavailableActive" => 'colorUnavailableActive_'.$key,
                                            "colorAvailableActive" => 'colorAvailableActive_'.$key,
                                            "colorAvailableInactive" => 'colorAvailableInactive_'.$key,
                                        );
                                        ?>
                                        <script type="text/javascript">
                                            BX.ready(function(){
                                                window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
                                            });
                                        </script>
                                        
                                    <?break; endif;
                                    

                                }
                                //фильтры по размеру и ориентации
                                foreach($arResult["ITEMS"] as $key=>$arItem){?>
                                        <?if(
                                            empty($arItem["VALUES"])
                                            || isset($arItem["PRICE"])
                                            || ($arItem['CODE']!='SIZE_TYPE'
                                            && $arItem['CODE']!='ORIENTATION' && $arItem['CODE']!='REGION') 
                                        )
                                            continue;?>
                                    <?if($arItem['CODE']=='SIZE_TYPE'): //фильтр по размеру?>
                                        <? $filterSize = ''; $num = 0; 
                                        $flag = true;
                                        $from = 0;
                                        $to = -1;
                                        $count = 0;
                                        $filterSizeBuf = '';
                                        $check_num = 0;
                                        $check_num2 = 0;
                                        $check_num3 = 0;
                                        $check_num4 = 0;
                                        
                                        foreach($arItem["VALUES"] as $val => $ar){ 

                                            if(!$ar["CHECKED"] && !$ar["DISABLED"] && $flag){
                                                $check_num++;
                                            }

                                            if(!$ar["CHECKED"] && !$ar["DISABLED"] && !$flag){
                                                $check_num3++;
                                                $check_num4++;
                                            }

                                            if($ar["CHECKED"] && !$ar["DISABLED"]){
                                                $flag = false;
                                                $check_num2++;
                                                $check_num4 = 0;
                                            }

                                            if(!$ar["DISABLED"]){
                                                if($num==0) $filterSize .= $ar["VALUE"]; else $filterSize .= ',' . $ar["VALUE"];  
                                                $num++;
                                            }
                                            
                                            if($count==0) $filterSizeBuf .= $ar["VALUE"]; else $filterSizeBuf .= ',' . $ar["VALUE"];  
                                            
                                        }
                                        
                                        $from = $check_num2 ? $check_num : 0;
                                        $to = $check_num + $check_num2  + $check_num3 - $check_num4 - 1;
                                        if($to==-1) $to = $num;
                                        if($to==-1){
                                            $to = $count;
                                            $filterSize = $filterSizeBuf;
                                        }
                                        if($to==0){
                                            $filterSize = $filterSize . ',' . $filterSize;
                                            $to = 1;
                                        }?>
                                        
                                        <div class="filter__item filter__item-size filter__item_compact bx-filter-parameters-box"><span class="bx-filter-container-modef"></span><span class="filter__item-title"><?=$arItem['NAME'];?></span>
                                            <div class="filter__item-main">
                                                <div class="l-rangeslider js-l-rangeslider">
                                                    <div class="l-rangeslider__inputs"></div>
                                                    <div class="l-rangeslider__main">
                                                        <input class="js-l-rangeslider-main js-filter__size" type="text" data-from="<?=$from?>" data-to="<?=$to;?>" data-values="<?=$filterSize;?>"/>
                                                    </div>
                                                </div>
                                                <div class="l-options l-options-size" style="display: none;">
                                                    <?foreach($arItem["VALUES"] as $val => $ar):?>
                                                        <div class="l-options__item">
                                                            <input
                                                                data-size="<?=$ar["VALUE"];?>"
                                                                type="checkbox"
                                                                value="<? echo $ar["HTML_VALUE"] ?>"
                                                                name="<? echo $ar["CONTROL_NAME"] ?>"
                                                                id="<? echo $ar["CONTROL_ID"] ?>"
                                                                <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                                                onchange="smartFilter.click(this)"
                                                            />
                                                            <label  <? echo $ar["DISABLED"] ? 'disabled': 'for="'.$ar["CONTROL_ID"].'"' ?>><?=$ar["VALUE"];?></label>
                                                        </div>
                                                    <?endforeach;?>
                                                </div>
                                            </div>
                                        </div>
                                    <?elseif($arItem['CODE']=='ORIENTATION'): //фильтр по ориентации?>
                                        <div class="filter__item bx-filter-parameters-box <?=$arResult['IS_REGION'] ? 'filter__item-orientation_region_block' : '';?>">
                                            <?if($arResult['IS_REGION']):?><div class="filter__item-orientation_region_block_content"><?endif;?>
                                                <?if($arResult['IS_REGION']):?><div class="filter__item-orientation_block_content"><?endif;?><span class="bx-filter-container-modef"></span><span class="filter__item-title"><?=$arItem['NAME'];?></span>
                                                    <div class="filter__item-main">
                                                        <div class="l-orientation">
                                                            <?foreach($arItem["VALUES"] as $val => $ar):?>
                                                                <div class="l-orientation__option l-orientation__option_<? echo $ar["URL_ID"] ?>">
                                                                    <input
                                                                        type="checkbox"
                                                                        value="<? echo $ar["HTML_VALUE"] ?>"
                                                                        name="<? echo $ar["CONTROL_NAME"] ?>"
                                                                        id="<? echo $ar["CONTROL_ID"] ?>"
                                                                        <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                                                        onchange="smartFilter.click(this)"
                                                                    />
                                                                    <label <? echo $ar["DISABLED"] ? 'disabled': 'for="'.$ar["CONTROL_ID"].'"' ?>></label>
                                                                </div>
                                                            <?endforeach;?>
                                                        </div>
                                                    </div>
                                                </div>
                                    <?elseif($arItem['CODE']=='REGION'): //фильтр по региону?>
                                                <div class="filter__item-orientation_block_content"><span class="bx-filter-container-modef"></span>
                                                    <div class="filter__item-main">
                                                        <?foreach($arItem["VALUES"] as $val => $ar):?>
                                                            <label class="form__check">
                                                                <input
                                                                    type="checkbox"
                                                                    value="<? echo $ar["HTML_VALUE"] ?>"
                                                                    name="<? echo $ar["CONTROL_NAME"] ?>"
                                                                    id="<? echo $ar["CONTROL_ID"] ?>"
                                                                    <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                                                    onchange="smartFilter.click(this)"
                                                                /><span><?=GetMessage('REGION');?></span>
                                                            </label>
                                                        <?endforeach;?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?endif;
                                }?>
                                <div class="filter__item filter__item_btn">
                                    <a href="<?= $arParams["FILTER_DELETE"] ? $arParams["FILTER_DELETE"] : SITE_DIR . "works/";?>" class="btn btn_icon btn-delete__filter"><?=GetMessage("DEL_FILTER")?>
                                        <svg class="icon icon-reset">
                                            <use xlink:href="#icon-reset"></use>
                                        </svg>
                                    </a>
                                    <div class="btn btn_icon btn_arrow filter__toggle js-filter-toggle hidden-tablet-up <?=$arResult['FILTER_ACTIVE']=='Y' ? 'is-active' : ''?>"><span><?=GetMessage('EXPAND');?></span><span><?=GetMessage('COLLAPSE');?></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="filter__tabs js-filter-tabs" <?=$arResult['FILTER_ACTIVE']=='Y' ? 'style="display: block;"' : ''?>>
                        <div class="filter__tabs-head">
                            <div class="container">
                                <div class="filter__tabs-head-inner">
                                    <? $displayItem = false; 
                                    foreach($arResult["ITEMS"] as $key=>$arItem):?>
                                        <?if(
                                            (empty($arItem["VALUES"])
                                            || isset($arItem["PRICE"])
                                            || $arItem['CODE']=='SIZE_TYPE'
                                            || $arItem['CODE']=='ORIENTATION'
                                            || $arItem['CODE']=='REGION') ||
                                            ($arItem['CODE'] == "GENRE_SUB" || $arItem['CODE'] == "THEME_SUB" || $arItem['CODE'] == "MATERIAL_SUB" || $arItem['CODE'] == "STYLE_SUB")
                                        )
                                            continue;

                                        if (
                                            $arItem["DISPLAY_TYPE"] == "A"
                                            && (
                                                $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
                                            )
                                        )
                                            continue;?>
                                        <span class="filter__tabs-head-item js-filter-tabs-head-item <?if((!$activeItem && $arItem['CODE']=='AUTHOR') || ($arItem["DISPLAY_EXPANDED"]=='Y' && !$displayItem)){ $displayItem = true; print 'is-active';}?>"><?=$arItem['NAME'];?></span>
                                    <?endforeach;?>
                                </div>
                            </div>
                        </div>
                        <div class="filter__tabs-main">
                            <? $displayItem = false; 
                            foreach($arResult["ITEMS"] as $key=>$arItem):?>
                                <?if(
                                    (empty($arItem["VALUES"])
                                    || isset($arItem["PRICE"])
                                    || $arItem['CODE']=='SIZE_TYPE'
                                    || $arItem['CODE']=='ORIENTATION'
                                    || $arItem['CODE']=='REGION') ||
                                    ($arItem['CODE'] == "GENRE_SUB" || $arItem['CODE'] == "THEME_SUB" || $arItem['CODE'] == "MATERIAL_SUB" || $arItem['CODE'] == "STYLE_SUB")
                                )
                                    continue;

                                if (
                                    $arItem["DISPLAY_TYPE"] == "A"
                                    && (
                                        $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
                                    )
                                )
                                    continue;?>
                            
                                <?if($arItem['CODE']=='AUTHOR'): //фильтр по автору?>
                                    <span class="filter__tabs-head-item js-filter-tabs-head-item"><?=$arItem['NAME']?></span>
                                    <div class="filter__tabs-main-item js-filter-tabs-main-item bx-filter-parameters-box" <?if(!$activeItem || ($arItem["DISPLAY_EXPANDED"]=='Y' && !$displayItem)){ $displayItem = true; print 'style="display: block;"';}?>>
                                        <span class="bx-filter-container-modef"></span>
                                        <div class="container">
                                            <div class="l-autocomplete js-l-autocomplete js-filter-author">
                                                <div class="l-autocomplete__row"><span class="l-autocomplete__title"><?=GetMessage('START_TYPING_NAME');?></span>
                                                    <div class="l-autocomplete__select">
                                                    <select class="js-l-autocomplete-select js-filter-author__select" multiple>
                                                        <?foreach($arResult['AUTHOR_NAME'] as $letter => $arAuthors):?>
                                                            <optgroup label="<?=$letter;?>">
                                                                <?foreach($arAuthors as $authorId => $arAuthor):?>
                                                                    <option 
                                                                        data-id="<?=$arAuthor['CONTROL_ID']?>"
                                                                        value="<?=$arAuthor['CONTROL_ID']?>"
                                                                        <? echo $arAuthor["CHECKED"]? 'selected="selected"': '' ?>
                                                                        <? echo $arAuthor["DISABLED"]? 'disabled': '' ?>
                                                                    ><?=$arAuthor['VALUE']?>
                                                                    </option>
                                                                <?endforeach;?>
                                                            </optgroup>
                                                        <?endforeach;?>
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="l-autocomplete__row">
                                                    <div class="l-autocomplete__options js-l-autocomplete-options"></div>
                                                </div>
                                            </div>
                                            <div class="l-options l-options-authors" style="display: none;">
                                                <?foreach($arItem["VALUES"] as $val => $ar):?>
                                                    <div class="l-options__item">
                                                        <input
                                                            type="checkbox"
                                                            value="<? echo $ar["HTML_VALUE"] ?>"
                                                            name="<? echo $ar["CONTROL_NAME"] ?>"
                                                            id="<? echo $ar["CONTROL_ID"] ?>"
                                                            <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                                            onclick="smartFilter.click(this)"
                                                        />
                                                        <label <? echo $ar["DISABLED"] ? 'disabled': 'for="'.$ar["CONTROL_ID"].'"' ?>><?=$ar["VALUE"];?></label>
                                                    </div>
                                                <?endforeach;?>
                                            </div>
                                        </div>
                                    </div>
                                <?else:?>
                                    <span class="filter__tabs-head-item js-filter-tabs-head-item"><?=$arItem['NAME']?></span>
                                    <div class="filter__tabs-main-item js-filter-tabs-main-item bx-filter-parameters-box" <?if($arItem["DISPLAY_EXPANDED"]=='Y' && !$displayItem){ $displayItem = true; print 'style="display: block;"';}?>>
                                        <span class="bx-filter-container-modef"></span>
                                        <div class="container">
                                            <div class="l-options">
                                                <?foreach($arItem["VALUES"] as $val => $ar):?>													
                                                    <div class="l-options__item">
                                                        <?
                                                        $name =  ($arItem['DISPLAY_TYPE'] = 'F') ? strtolower(str_replace('.', '', $ar['UPPER'])) : $ar["VALUE"];
                                                        if($ar["CHECKED"]) {
                                                            $section_id = 0;
                                                            switch ($arItem['ID']) {
                                                                case WORK_PROP_GENRE_ID: // жанр
                                                                    $section_id = 5;
                                                                    break;
                                                                case WORK_PROP_THEME_ID: // тема
                                                                    $section_id = 1;
                                                                    break;
                                                                case WORK_PROP_MATERIAL_ID: // материал
                                                                    $section_id = 4;
                                                                    break;
                                                                case WORK_PROP_STYLE_ID: // стиль
                                                                    $section_id = 2;
                                                                    break;
                                                            }	

                                                            $elementIDs = HB\helpers\FilterHelpers::getElementIDsBySectionID($val, $section_id);
                                                        }
                                                        ?>
                                                        <input
                                                            type="radio"
                                                            value="<? echo $ar["HTML_VALUE_ALT"] ?>"
                                                            name="<? echo $ar['CONTROL_NAME_ALT'] ?>"
                                                            id="<? echo $ar["CONTROL_ID"] ?>"
                                                            <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                                            data-section-id="<?=$val?>"
                                                            data-prop-id="<?=$arItem['ID']?>"
                                                            onchange="smartFilter.click(this)"
                                                        />
                                                        <label <? echo $ar["CHECKED"]? 'class="checked"': '' ?> <? echo $ar["DISABLED"] ? 'disabled': 'for="'.$ar["CONTROL_ID"].'"' ?>><?=$name;?></label>
                                                    </div>
                                                <?endforeach;?>
                                            </div>
                                        </div>
                                        <?
                                        if($_REQUEST['subfilter_ajax'] == 'Y') {
                                            $elementIDs = $_REQUEST['elementIDs'];
                                        }



                                        if($arResult["ITEMS"][173] || $arResult["ITEMS"][170] || $arResult["ITEMS"][171] || $arResult["ITEMS"][172]) {
                                            $sub_section_id = "";
                                            switch ($arItem['CODE']) {
                                                case "GENRE": 
                                                    $sub_section_id = WORK_PROP_GENRE_SUB_ID;
                                                    break;
                                                case "THEME":
                                                    $sub_section_id = WORK_PROP_THEME_SUB_ID;
                                                    break;
                                                case "MATERIAL": 
                                                    $sub_section_id = WORK_PROP_MATERIAL_SUB_ID;
                                                    break;
                                                case "STYLE": 
                                                    $sub_section_id = WORK_PROP_STYLE_SUB_ID;
                                                    break;
                                            }
                                        }                                        
                                        $checked = false;
                                        foreach($arResult["ITEMS"][$sub_section_id]["VALUES"] as $val => $ar) {
                                            if(in_array($val, $elementIDs)) {
                                                $checked = true;
                                            }
                                        }
                                        ?>
                                        <div class="filter__sub-tabs <?=($checked ? '' : 'disabled');?>" data-item-id="<?=$arItem['ID']?>">
                                            <?if($arItem['CODE'] == "GENRE" || $arItem['CODE'] == "THEME" || $arItem['CODE'] == "MATERIAL" || $arItem['CODE'] == "STYLE") {?>                                               
                                                <span class="filter__tabs_hr"></span>
                                                <div class="filter__tabs-main-sub-item">
                                                    <span class="bx-filter-container-modef"></span>
                                                    <div class="container">
                                                        <div class="l-options">
                                                            <?foreach($arResult["ITEMS"][$sub_section_id]["VALUES"] as $val => $ar):?>
                                                                <?if(in_array($val, $elementIDs)) {?>
                                                                    <div class="l-options__item">
                                                                        <?$name2 = ($arResult["ITEMS"][$sub_section_id]['DISPLAY_TYPE'] = 'F') ? strtolower(str_replace('.', '', $ar['UPPER'])) : $ar["VALUE"];?>
                                                                        <input
                                                                            type="radio"
                                                                            value="<? echo $ar["HTML_VALUE_ALT"] ?>"
                                                                            name="<? echo $ar['CONTROL_NAME_ALT'] ?>"
                                                                            id="<? echo $ar["CONTROL_ID"] ?>"
                                                                            <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                                                            onchange="smartFilter.click(this)"
                                                                        />
                                                                        <label <? echo $ar["CHECKED"]? 'class="checked"': '' ?> <? echo $ar["DISABLED"] ? 'disabled': 'for="'.$ar["CONTROL_ID"].'"' ?>><?=$name2;?></label>
                                                                    </div>
                                                                <?}?>
                                                            <?endforeach;?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?}?>
                                        </div>
                                    </div>
                                <?endif;?>
                            <?endforeach;?>
                        </div>
                        <div class="filter__hide"><span class="filter__hide-inner js-filter-hide"><?=GetMessage("SHOW_PICTURES")?></span></div>
                    </div>
                    <div class="bx-filter-parameters-box-container" style="display:none;">
                        <input
                            class="btn btn-themes"
                            type="submit"
                            id="set_filter"
                            name="set_filter"
                            value="<?=GetMessage("CT_BCSF_SET_FILTER")?>"
                        />
                        <input
                            class="btn btn-link"
                            type="submit"
                            id="del_filter"
                            name="del_filter"
                            value="<?=GetMessage("CT_BCSF_DEL_FILTER")?>"
                        />
                        <div class="bx-filter-popup-result <?if ($arParams["FILTER_VIEW_MODE"] == "VERTICAL") echo $arParams["POPUP_POSITION"]?>" id="modef" <?if(!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"';?> style="display: inline-block;">
                            <?echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));?>
                            <span class="arrow"></span>
                            <br/>
                            <a href="<?echo $arResult["FILTER_URL"]?>" target=""><?echo GetMessage("CT_BCSF_FILTER_SHOW")?></a>
                        </div>
                    </div>
                </form>
            </div>
    </div>
</div>
<div class="up-button__filter">
    <button type="button" class="slick-arrow slick-prev" style="">
        <svg class="icon icon-arrow"><use xlink:href="#icon-arrow"></use></svg>
    </button>
</div>
<script type="text/javascript">
    var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);

    $( document ).ready(function() {
        $('body').on('click', '.l-options__item label', function(e) {
            e.preventDefault();
            var label = $(this);
            var radio = $(this).parent().children('input');
            if(radio.is(':checked')) {
                if(radio.parents('.filter__sub-tabs').length == 0) {
                    $('body').find('div[class^="filter__sub-tabs"]').not('.disabled').find('input:checked').each(function() {
                        $(this).removeAttr('checked');
                    });
                    radio.parents('.filter__tabs-main-item').children('.filter__sub-tabs').addClass('disabled');
                } 
                radio.removeAttr('checked');
                label.removeClass('checked');
            } else {
                var checkedRadio = label.parents('.l-options').find('input:checked');
                var checkedLabel = label.parents('.l-options').find('label.checked');
                checkedRadio.each(function() {
                    $(this).removeAttr('checked');
                });
                checkedLabel.each(function() {
                    $(this).removeClass('checked');
                });
                radio.attr('checked', 'checked');
                label.addClass('checked');
                if(radio.parents('.filter__sub-tabs').length == 0) {
                    $('body').find('div[class^="filter__sub-tabs"]').not('.disabled').find('input:checked').each(function() {
                        $(this).removeAttr('checked');
                        $(this).parent().children('label').removeClass('checked');
                    });
                    label.parents('.filter__tabs-main-item').children('.filter__sub-tabs').addClass('disabled');
                }
                var id = radio.data('section-id') || 0;
                var propID = radio.data('prop-id') || 0;
                if(id*propID > 0) {
                    $.ajax({ // получение списка ID элементов-подразделов выбранного раздела
                        type: 'GET',
                        url: '/local/ajax/get_sub_filter_ajax.php?id=' + id + '&propID=' + propID,
                        dataType: 'json',
                        success: function(result){
                            if(result) {
                                $.ajax({ // получение списка ID элементов-подразделов выбранного раздела
                                    type: 'POST',
                                    url: '/works/',
                                    data: {'subfilter_ajax': 'Y', 'elementIDs': result},
                                    dataType: 'html',
                                    success: function(subFilter){
                                        if(subFilter.length > 0 && $(subFilter).find('.filter__sub-tabs[data-item-id="'+propID+'"]').find('input').length > 0) {
                                            radio.parents().find('.filter__sub-tabs').html($(subFilter).find('.filter__sub-tabs[data-item-id="'+propID+'"]').html());
                                            radio.parents('.filter__tabs-main-item').children('.filter__sub-tabs').removeClass('disabled');
                                        } else {
                                            radio.parents().find('.filter__sub-tabs').html();
                                            radio.parents('.filter__tabs-main-item').children('.filter__sub-tabs').addClass('disabled');
                                        }                                       
                                    }
                                });
                            }
                        }
                    });
                }
            }
            radio.trigger('change');
        });
    });
</script>


