<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use HB\helpers\Helpers;
use HB\helpers\WorkHelpers;
use HB\helpers\LangHelpers;

if (isset($arParams["TEMPLATE_THEME"]) && !empty($arParams["TEMPLATE_THEME"]))
{
	$arAvailableThemes = array();
	$dir = trim(preg_replace("'[\\\\/]+'", "/", dirname(__FILE__)."/themes/"));
	if (is_dir($dir) && $directory = opendir($dir))
	{
		while (($file = readdir($directory)) !== false)
		{
			if ($file != "." && $file != ".." && is_dir($dir.$file))
				$arAvailableThemes[] = $file;
		}
		closedir($directory);
	}

	if ($arParams["TEMPLATE_THEME"] == "site")
	{
		$solution = COption::GetOptionString("main", "wizard_solution", "", SITE_ID);
		if ($solution == "eshop")
		{
			$templateId = COption::GetOptionString("main", "wizard_template_id", "eshop_bootstrap", SITE_ID);
			$templateId = (preg_match("/^eshop_adapt/", $templateId)) ? "eshop_adapt" : $templateId;
			$theme = COption::GetOptionString("main", "wizard_".$templateId."_theme_id", "blue", SITE_ID);
			$arParams["TEMPLATE_THEME"] = (in_array($theme, $arAvailableThemes)) ? $theme : "blue";
		}
	}
	else
	{
		$arParams["TEMPLATE_THEME"] = (in_array($arParams["TEMPLATE_THEME"], $arAvailableThemes)) ? $arParams["TEMPLATE_THEME"] : "blue";
	}
}
else
{
	$arParams["TEMPLATE_THEME"] = "blue";
}

$arParams["FILTER_VIEW_MODE"] = (isset($arParams["FILTER_VIEW_MODE"]) && toUpper($arParams["FILTER_VIEW_MODE"]) == "HORIZONTAL") ? "HORIZONTAL" : "VERTICAL";
$arParams["POPUP_POSITION"] = (isset($arParams["POPUP_POSITION"]) && in_array($arParams["POPUP_POSITION"], array("left", "right"))) ? $arParams["POPUP_POSITION"] : "left";


$arProperties = WorkHelpers::getWorkProperty();
        
$arProperties = LangHelpers::getTranslationProperty($arProperties);

foreach($arResult['ITEMS'] as $id => &$arItem){
	if($arProperties[$arItem['CODE']]){
		$arResult['ITEMS'][$id]['NAME'] = $arProperties[$arItem['CODE']];
		if($arItem['VALUES']){
			$arPropInfo = Helpers::getProperty($id, $arItem['IBLOCK_ID']);
			$arHandBook = Helpers::getHandbookList($arPropInfo['LINK_IBLOCK_ID']);
			foreach($arItem['VALUES'] as $valueId => &$arValue){
				$arValue['VALUE'] = $arHandBook[$valueId]['NAME'];
			}
		}
	}
}


$arResult['AUTHOR_NAME'] = array();

foreach($arResult['ITEMS']['18']['VALUES'] as $authorId => $arAuthor){
	$firstLetter = mb_substr($arAuthor['VALUE'],0,1,'UTF-8');
	$arResult['AUTHOR_NAME'][$firstLetter][$authorId] = $arAuthor;
}

ksort($arResult['AUTHOR_NAME']);
