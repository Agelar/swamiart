<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?foreach ($arResult["SITES"] as $key => $arSite):?>

	<?if ($arSite["CURRENT"] == "Y"):?>
		
	<?else:?>
		<div class="menu__dropdown-item hidden-tablet-up"><a class="menu__dropdown-link" href="https://<?=$arSite["DOMAINS"]["0"]?><?=$APPLICATION->GetCurPage();?>"><?=GetMessage('VERSION');?></a></div>
		
	<?endif?>

<?endforeach;?>