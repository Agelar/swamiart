<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $APPLICATION;?>

<?foreach ($arResult["SITES"] as $key => $arSite):?>
	<?if ($arSite["CURRENT"] == "Y"):?>
		
	<?else:?>
		<div class="tools__item hidden-mobile-down">
			<!--noindex-->
			<a rel="nofollow" class="tools__link" href="https://<?=$arSite["DOMAINS"]["0"]?><?=$APPLICATION->GetCurPage();?>"><?=strtoupper($arSite["LANG"]);?></a>
			<!--/noindex-->
		</div>
		
	<?endif?>

<?endforeach;?>