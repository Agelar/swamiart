<?php
$MESS['ABOUT'] = "SwamiArt";
$MESS['BUYERS'] = "Buyers";
$MESS['CATALOG'] = "Painter";
$MESS['CONTACTS'] = "Contacts";
$MESS['SOCIAL'] = "Follow us in social networks:";
$MESS['PAYMENT'] = "Payment systems";
$MESS['DEVELOP'] = "Site development";
$MESS['DEVELOP_MOBILE'] = "Site created";

$MESS['SEND_MAIL'] = "Send to e-mail";
$MESS['ENTER_MAIL'] = "Enter your E-mail";
$MESS['TO_SEND'] = "To send";
$MESS['SUCCESS'] = "Success!";
$MESS['SENT_LINK_MAIL'] = "We sent a link to this work to your e-mail address";
$MESS['WORK_REMOVAL'] = "Work removal";
$MESS['DELETE'] = "Delete";
$MESS['WORK_SUCCESS_DELETED'] = "Job successfully deleted";
$MESS['DELETE_WORK'] = "Are you sure you want to delete the job";
$MESS['LOGGED_IN'] = "You are already logged in";
$MESS['PERSONAL_AREA'] = "personal area";
$MESS['GO_TO'] = "Go to ";