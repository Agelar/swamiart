<?php
$MESS['ABOUT'] = "SwamiArt";
$MESS['BUYERS'] = "Покупателям";
$MESS['CATALOG'] = "Художникам";
$MESS['CONTACTS'] = "Контакты";
$MESS['SOCIAL'] = "Мы в социальных сетях:";
$MESS['PAYMENT'] = "Платежные системы";
$MESS['DEVELOP'] = "Разработка сайта";
$MESS['DEVELOP_MOBILE'] = "Сайт создан";

$MESS['SEND_MAIL'] = "Отправить на e-mail";
$MESS['ENTER_MAIL'] = "Введите свой E-mail";
$MESS['TO_SEND'] = "Отправить";
$MESS['SUCCESS'] = "Успех!";
$MESS['SENT_LINK_MAIL'] = "Мы отправили на ваш e-mail ссылку на данную работу";
$MESS['WORK_REMOVAL'] = "Удаление работы";
$MESS['DELETE'] = "Удалить";
$MESS['WORK_SUCCESS_DELETED'] = "Работа успешно удалена";
$MESS['DELETE_WORK'] = "Вы действительно хотите удалить работу";
$MESS['LOGGED_IN'] = "Вы уже авторизованы на сайте";
$MESS['PERSONAL_AREA'] = "личный кабинет";
$MESS['GO_TO'] = "Перейти в ";