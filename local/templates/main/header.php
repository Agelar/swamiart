<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Page\Asset;
use HB\helpers\Helpers;

global $USER;
CModule::IncludeModule("hb.site");

$arSettings = Helpers::getSiteSettings();
?>
<!DOCTYPE html>
<html>
	<head>
		<?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/libs.min.js");?>
		<?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/common.js");?>
		<?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/js/dev.js");?>
		<?$APPLICATION->ShowHead();?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<meta http-equiv="X-UA-Compatible" content="ie=edge"/>
		<?$APPLICATION->ShowProperty("og:type", "<meta property='og:type' content='website' />"); echo PHP_EOL . '    ';?>
		<?$APPLICATION->ShowProperty("og:url", "<meta property='og:url' content='https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']."' />"); echo PHP_EOL . '    ';?>
		<?$APPLICATION->ShowProperty("og:image", "<meta property='og:image' content='https://".$_SERVER['SERVER_NAME']."/upload/iblock/b2d/1.png' />"); echo PHP_EOL . '    ';?>
		<?$APPLICATION->ShowProperty("vk:image", "<meta property='vk:image' content='https://".$_SERVER['SERVER_NAME']."/upload/iblock/b2d/1.png' />"); echo PHP_EOL . '    ';?>
		<?$APPLICATION->ShowProperty("og:image:width", "<meta property='og:image:width' content='1200' />"); echo PHP_EOL . '    ';?>
		<?$APPLICATION->ShowProperty("og:image:height", "<meta property='og:image:height' content='630' />"); echo PHP_EOL . '    ';?>
		<?$APPLICATION->ShowProperty("og:description", "<meta property='og:description' content=''/>"); echo PHP_EOL;?>
		<meta property="og:title" content="<?=$APPLICATION->ShowTitle();?>"/>
		<title><?$APPLICATION->ShowTitle();?></title>
		<?php CJSCore::Init(array('ajax'));?>
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" /> 	
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/libs.min.css"/>
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/main.min.css?ver=2"/>
		<?$requestUrl = $_SERVER['REQUEST_URI'];
		if (stripos($requestUrl, 'price-base-from') !== FALSE) {
            $requestUrl = preg_replace("/price-base-from.*?\//", '',$requestUrl);
        }
		?>
		<link rel="canonical" href="<?="http" . (empty($_SERVER['HTTPS'])? '': 's') . "://" . $_SERVER['SERVER_NAME'] . $requestUrl?>">

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137561690-1"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-137561690-1');
		</script>
		<?=$arSettings['HEADER_COUNTERS']['~VALUE']['TEXT']?>
	</head>
	<body>
		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>
		<div class="svg-sprite"><\?xml version="1.0" encoding="utf-8"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><symbol viewBox="0 0 0.74 0.74" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd" id="arrow_size_down" xmlns="http://www.w3.org/2000/svg"><path d="M.2.63L.74.08.66 0 .11.54v-.2H0V.74H.4V.63z"/></symbol><symbol viewBox="0 0 11.36 3.15" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd" id="arrow_smart" xmlns="http://www.w3.org/2000/svg"><path d="M0 1.24h11.36v.65H0z"/><path d="M9.41 3.15h.66v-.31h.33v-.31h.33v-.31h.32V.94h-.32V.62h-.33V.31h-.33V0h-.66v.62h.32v.32h.35v1.28h-.35v.31h-.32v.31z"/></symbol><symbol viewBox="0 0 10 22" id="arrow_wide" xmlns="http://www.w3.org/2000/svg"><image data-name="Layer 60 copy 13" width="10" height="22" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAWCAYAAAD5Jg1dAAAAUUlEQVQoke2SUQoAIAhDV3RXz+RpjQRjgUH07T63p0OwmRlYIuKGqjb2Ox5VYIH/4Lj9XyjyvTGMDFryD88g1mrzjbdaznZ1BrN3XM3BMQhgAg6aI4sttsF/AAAAAElFTkSuQmCC"/></symbol><symbol viewBox="0 0 21000.1 16102.74" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd" id="check_smooth" xmlns="http://www.w3.org/2000/svg"><path class="adfil0" d="M291.45 7722.6l1128.1-1128.1c322.32-322.32 805.78-322.32 1128.1 0l80.58 80.58 4431.81 4754.12c161.16 161.16 402.9 161.16 564.05 0L18421.59 228.81c329.33-281.39 880.82-327.86 1208.67-.01l1128.1 1128.1c322.32 322.32 322.32 805.78 0 1128.1L7865.83 15861c-161.16 161.16-322.32 241.74-564.05 241.74-241.74 0-402.9-80.58-564.05-241.74L452.62 9091.8c-475.82-565.54-649.03-881.37-161.18-1369.2z"/></symbol><symbol viewBox="0 0 9.99 18" id="icon-arrow" xmlns="http://www.w3.org/2000/svg"><path d="M1.12 17.82L9.8 9.45a.62.62 0 0 0 0-.9.69.69 0 0 0-.93 0L.19 16.92a.63.63 0 0 0 0 .9.69.69 0 0 0 .93 0z" fill-rule="evenodd"/><path d="M9.8 9.45a.62.62 0 0 0 0-.9L1.12.19a.67.67 0 0 0-.93 0 .62.62 0 0 0 0 .89l8.68 8.37a.67.67 0 0 0 .93 0z" fill-rule="evenodd"/></symbol><symbol id="icon-cart" viewBox="0 0 17.1 21.3" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><style>.afst0{stroke-width:2;stroke-miterlimit:10}.afst1{fill:none}</style><path class="afst0" d="M16 20.3H1v-15l15.1.1z"/><path fill="none" class="afst0 afst1" d="M5.1 5.4c0-5.7 6.7-5.9 6.7 0"/></symbol><symbol viewBox="0 0 13.99 10.99" id="icon-check" xmlns="http://www.w3.org/2000/svg"><path d="M0 6l2-2 5 5-2 2z" fill-rule="evenodd"/><path d="M3 9l9-9 2 2-9 9z" fill-rule="evenodd"/></symbol><symbol viewBox="0 0 24.96 24.96" id="icon-close" xmlns="http://www.w3.org/2000/svg"><path d="M22.88 0L25 2.08 2.08 25 0 22.88z" fill-rule="evenodd"/><path d="M2.08 0L25 22.88 22.88 25 0 2.08z" fill-rule="evenodd"/></symbol><symbol viewBox="0 0 16 4" id="icon-dots" xmlns="http://www.w3.org/2000/svg"><circle cx="2" cy="2" r="2"/><circle cx="8" cy="2" r="2"/><circle cx="14" cy="2" r="2"/></symbol><symbol viewBox="0 0 1.72 1.8" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd" id="icon-download" xmlns="http://www.w3.org/2000/svg"><path d="M.97.95V0H.8v.95L.58.73.46.85l.31.31.12.12.12-.12.31-.31L1.2.73.98.95zm.76.85V1.39h-.17v.24H.18v-.24H.01V1.8H1.73z"/></symbol><symbol viewBox="0 0 512 512" id="icon-edit" xmlns="http://www.w3.org/2000/svg"><path d="M352.459 220c0-11.046-8.954-20-20-20h-206c-11.046 0-20 8.954-20 20s8.954 20 20 20h206c11.046 0 20-8.954 20-20zM126.459 280c-11.046 0-20 8.954-20 20s8.954 20 20 20H251.57c11.046 0 20-8.954 20-20s-8.954-20-20-20H126.459z"/><path d="M173.459 472H106.57c-22.056 0-40-17.944-40-40V80c0-22.056 17.944-40 40-40h245.889c22.056 0 40 17.944 40 40v123c0 11.046 8.954 20 20 20s20-8.954 20-20V80c0-44.112-35.888-80-80-80H106.57c-44.112 0-80 35.888-80 80v352c0 44.112 35.888 80 80 80h66.889c11.046 0 20-8.954 20-20s-8.954-20-20-20z"/><path d="M467.884 289.572c-23.394-23.394-61.458-23.395-84.837-.016l-109.803 109.56a20.005 20.005 0 0 0-5.01 8.345l-23.913 78.725a20 20 0 0 0 24.476 25.087l80.725-22.361a19.993 19.993 0 0 0 8.79-5.119l109.573-109.367c23.394-23.394 23.394-61.458-.001-84.854zM333.776 451.768l-40.612 11.25 11.885-39.129 74.089-73.925 28.29 28.29-73.652 73.514zM439.615 346.13l-3.875 3.867-28.285-28.285 3.862-3.854c7.798-7.798 20.486-7.798 28.284 0 7.798 7.798 7.798 20.486.014 28.272zM332.459 120h-206c-11.046 0-20 8.954-20 20s8.954 20 20 20h206c11.046 0 20-8.954 20-20s-8.954-20-20-20z"/></symbol><symbol viewBox="0 0 512 394.59" id="icon-eye-close" xmlns="http://www.w3.org/2000/svg"><path d="M508.74 188.84C504.17 182.58 395.19 35.63 256 35.63S7.82 182.58 3.25 188.83a16.91 16.91 0 0 0 0 19.93C7.82 215 116.81 362 256 362s248.17-147 252.74-153.24a16.87 16.87 0 0 0 0-19.92zM256 328.2c-102.53 0-191.33-97.53-217.62-129.41C64.63 166.87 153.25 69.39 256 69.39c102.52 0 191.32 97.52 217.61 129.42C447.36 230.72 358.75 328.2 256 328.2z"/><path d="M256 97.52A101.28 101.28 0 1 0 357.27 198.8 101.4 101.4 0 0 0 256 97.52zm0 168.79a67.52 67.52 0 1 1 67.51-67.51A67.59 67.59 0 0 1 256 266.31z"/><path d="M425.2-.003l28.094 28.094-366.5 366.5L58.7 366.498z"/></symbol><symbol id="icon-favorite" viewBox="0 0 21.6 19.1" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><style>.amst0{stroke-width:2;stroke-miterlimit:10}</style><path class="amst0" d="M10.8 3.7c5.5-6.8 12.6 1.4 8.7 5.7l-8.8 8.8L2 9.4C-1.5 5 5.3-3.2 10.8 3.7z"/></symbol><symbol viewBox="0 0 438.533 438.533" id="icon-file" xmlns="http://www.w3.org/2000/svg"><path d="M382.58 108.493l-89.078-89.081C283.981 9.895 271.415 3.706 255.81.854v145.324h145.326c-2.855-15.612-9.045-28.172-18.556-37.685z"/><path d="M246.676 182.72c-7.617 0-14.089-2.663-19.417-7.993-5.33-5.327-7.992-11.799-7.992-19.414V0H63.953C56.341 0 49.869 2.663 44.54 7.993c-5.33 5.327-7.994 11.799-7.994 19.414v383.719c0 7.617 2.664 14.089 7.994 19.417 5.33 5.325 11.801 7.991 19.414 7.991h310.633c7.611 0 14.079-2.666 19.407-7.991 5.328-5.332 7.994-11.8 7.994-19.417V182.72H246.676z"/></symbol><symbol viewBox="0 0 18 21" id="icon-filter" xmlns="http://www.w3.org/2000/svg"><path d="M0 2.5h18v2H0zM0 9.5h18v2H0zM0 16.5h18v2H0z" fill="#201600" fill-rule="evenodd"/><path d="M13 14.5a3 3 0 1 1-3 3 3 3 0 0 1 3-3zM9 7.5a3 3 0 1 1-3 3 3 3 0 0 1 3-3zM5 .5a3 3 0 1 1-3 3 3 3 0 0 1 3-3z" fill="#fff" stroke="#201600" stroke-miterlimit="10" fill-rule="evenodd"/></symbol><symbol viewBox="0 0 100 94.99" id="icon-like" xmlns="http://www.w3.org/2000/svg"><path d="M36.35 95l.14-.13.12-.13a2 2 0 0 1 .11-.26v-6.21c-.08-6.53-.08-13.16-.08-19.57 0-5.35 0-12.41.08-19.57 0-.51 0-3.72.08-5.9l.09-3.88H8.71a8.25 8.25 0 0 0-8 4.85c-.14.29-.25.57-.35.81l-.11.25L0 46v43l.5.88c.08.13.16.3.25.48a10.83 10.83 0 0 0 1.29 2A7.7 7.7 0 0 0 8 95h28.35zm-7.27-48v2.08C29 56.3 29 63.35 29 68.7c0 6.14 0 12.49.07 18.77H7.76a4.33 4.33 0 0 1-.23-.41V47.42c.15-.32.22-.47 1.08-.47h20.47V47z"/><path d="M40.67 92.2a16.73 16.73 0 0 1-9.17-2.6l-1.78-1.13v-2.11c.07-10 .1-20.84.1-34.22V41.63l1-1.09L32 39a50.08 50.08 0 0 0 3.34-4.48c1.88-3 3.7-6.2 5.46-9.26l2.26-3.92a1.67 1.67 0 0 0 .23-1c-.09-2.13-.13-4.28-.17-6.35 0-1.22 0-2.49-.08-3.73a9 9 0 0 1 5.13-8.55A14.61 14.61 0 0 1 61 1.27a9.77 9.77 0 0 1 5.52 5.84l.93 2.42c.84 2.2 1.7 4.47 2.49 6.75a23.26 23.26 0 0 1 .52 12.33c-.27 1.38-.5 2.76-.74 4.19h17.86a16 16 0 0 1 5.17.83 10.59 10.59 0 0 1 6.86 7.9 1.61 1.61 0 0 0 .13.42l.1.31.16.58v4.54l-.4.53a14.13 14.13 0 0 1-2 4.59 13.74 13.74 0 0 1 .51 3 13.21 13.21 0 0 1-2.37 7.88 11.71 11.71 0 0 1 .45 2.5 14.06 14.06 0 0 1-2.34 8.54 15.58 15.58 0 0 1 .27 4.72 13.4 13.4 0 0 1-9.51 12 23.81 23.81 0 0 1-7.41 1H40.67zm-3.34-8.13a9.87 9.87 0 0 0 3.41.55c8.93-.05 18 0 26.79 0h9.66a16.58 16.58 0 0 0 5-.61 5.83 5.83 0 0 0 4.3-5.37 7.2 7.2 0 0 0-.3-3.13 4.74 4.74 0 0 1 .81-4.46 6.88 6.88 0 0 0 1.62-4.67 9.48 9.48 0 0 0-.48-1.85 4.9 4.9 0 0 1 .86-4.7 6.23 6.23 0 0 0 1.55-4.07 7.82 7.82 0 0 0-.29-1.44c-.11-.43-.23-.91-.33-1.43a4.54 4.54 0 0 1 1-4.08 6.56 6.56 0 0 0 1.43-3.28V44c0-.17-.1-.35-.15-.55-.47-1.84-1.29-2.36-2-2.62a8.32 8.32 0 0 0-2.71-.4H66.22a4.24 4.24 0 0 1-4.67-3.76 4.11 4.11 0 0 1 .13-1.6l.42-2.55c.28-1.74.57-3.54.92-5.33a15.76 15.76 0 0 0-.25-8.4c-.74-2.17-1.59-4.38-2.4-6.51l-.94-2.48a2.27 2.27 0 0 0-1.36-1.52 7 7 0 0 0-6.38.15 1.52 1.52 0 0 0-1.07 1.62c0 1.27.06 2.55.08 3.78 0 2 .08 4.14.17 6.19a9.37 9.37 0 0 1-1.25 5.14L47.38 29c-1.79 3.12-3.65 6.35-5.59 9.48A55.7 55.7 0 0 1 38 43.75l-.55.68v7.72c-.04 12.38-.06 22.57-.12 31.92z"/></symbol><symbol viewBox="0 0 24.33 18.11" id="icon-logout" xmlns="http://www.w3.org/2000/svg"><path d="M16.27 9.06H1.44m4.83-5L.73 9.79M6.31 14L.71 8.33m10.54 3.75v5h12.08L23.3 1h-12v5.05" fill="none" stroke-width="2"/></symbol><symbol viewBox="0 0 5.91 4.53" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" clip-rule="evenodd" id="icon-mail" xmlns="http://www.w3.org/2000/svg"><path d="M5.91.29v4.24H0V0h5.91v.29zm-.58 3.65V.58H.59v3.36h4.74z"/><path d="M5.78.54L3.11 2.57a.3.3 0 0 1-.37-.01L.11.52A.289.289 0 0 1 .06.11C.12.04.2 0 .29 0h5.33c.16 0 .29.13.29.29 0 .1-.05.19-.13.24zM2.93 1.98L4.75.59H1.14l1.79 1.39z"/></symbol><symbol viewBox="0 0 512 512" id="icon-map" xmlns="http://www.w3.org/2000/svg"><path d="M256 0C153.755 0 70.573 83.182 70.573 185.426c0 126.888 165.939 313.167 173.004 321.035 6.636 7.391 18.222 7.378 24.846 0 7.065-7.868 173.004-194.147 173.004-321.035C441.425 83.182 358.244 0 256 0zm0 469.729c-55.847-66.338-152.035-197.217-152.035-284.301 0-83.834 68.202-152.036 152.035-152.036s152.035 68.202 152.035 152.035C408.034 272.515 311.861 403.37 256 469.729z"/><path d="M256 92.134c-51.442 0-93.292 41.851-93.292 93.293S204.559 278.72 256 278.72s93.291-41.851 93.291-93.293S307.441 92.134 256 92.134zm0 153.194c-33.03 0-59.9-26.871-59.9-59.901s26.871-59.901 59.9-59.901 59.9 26.871 59.9 59.901-26.871 59.901-59.9 59.901z"/></symbol><symbol viewBox="0 0 3.56 4.65" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd" fill="none" stroke-width=".46" stroke-miterlimit="22.926" id="icon-phone" xmlns="http://www.w3.org/2000/svg"><path d="M.23.23h3.1v4.19H.23zM1.36 3.41h.85"/></symbol><symbol viewBox="0 0 16 19.01" id="icon-play" xmlns="http://www.w3.org/2000/svg"><path d="M16 9.5L0 19V0z" fill="#201600"/></symbol><symbol viewBox="0 0 26 26" id="icon-plus" xmlns="http://www.w3.org/2000/svg"><path d="M11.16 0h3.68v26h-3.68z" fill-rule="evenodd"/><path d="M0 11.16h26v3.68H0z" fill-rule="evenodd"/></symbol><symbol viewBox="0 0 7.4 8.23" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd" id="icon-reset" xmlns="http://www.w3.org/2000/svg"><path d="M2.5 2.15c.2 0 .36.16.36.36 0 .2-.16.36-.36.36H.28c-.02 0-.04-.01-.06-.02l-.04-.02-.02-.01a.376.376 0 0 1-.17-.3V.36c0-.2.16-.36.36-.36.2 0 .36.16.36.36v1.12C1.44.77 2.43.36 3.46.36 5.63.36 7.4 2.12 7.4 4.3c0 2.17-1.76 3.94-3.94 3.94-1.41 0-2.71-.75-3.41-1.97a.329.329 0 0 1-.04-.17c0-.2.16-.36.36-.36.12 0 .24.06.3.17.57 1 1.64 1.61 2.79 1.61 1.78 0 3.22-1.44 3.22-3.22 0-1.78-1.44-3.22-3.22-3.22-.91 0-1.79.39-2.4 1.07H2.5z"/></symbol><symbol viewBox="0 0 3.6 3.6" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd" id="icon-search" xmlns="http://www.w3.org/2000/svg"><path d="M2.32 2.6l1 1 .28-.28-1-1z"/><path d="M2.64.45c.3.3.45.7.45 1.09 0 .4-.15.79-.45 1.09a1.545 1.545 0 0 1-2.18 0 1.545 1.545 0 0 1 0-2.18c.3-.3.7-.45 1.09-.45.4 0 .79.15 1.09.45zm.06 1.09c0-.29-.11-.59-.34-.81C2.13.5 1.84.39 1.55.39 1.26.39.96.5.74.73.52.95.4 1.25.4 1.54c0 .29.11.59.34.81.22.22.52.34.81.34.29 0 .59-.11.81-.34.23-.22.34-.52.34-.81z"/></symbol><symbol viewBox="0 0 486.3 486.3" id="icon-upload" xmlns="http://www.w3.org/2000/svg"><path d="M395.5 135.8c-5.2-30.9-20.5-59.1-43.9-80.5-26-23.8-59.8-36.9-95-36.9-27.2 0-53.7 7.8-76.4 22.5-18.9 12.2-34.6 28.7-45.7 48.1-4.8-.9-9.8-1.4-14.8-1.4-42.5 0-77.1 34.6-77.1 77.1 0 5.5.6 10.8 1.6 16C16.7 200.7 0 232.9 0 267.2c0 27.7 10.3 54.6 29.1 75.9 19.3 21.8 44.8 34.7 72 36.2h86.8c7.5 0 13.5-6 13.5-13.5s-6-13.5-13.5-13.5h-85.6C61.4 349.8 27 310.9 27 267.1c0-28.3 15.2-54.7 39.7-69 5.7-3.3 8.1-10.2 5.9-16.4-2-5.4-3-11.1-3-17.2 0-27.6 22.5-50.1 50.1-50.1 5.9 0 11.7 1 17.1 3 6.6 2.4 13.9-.6 16.9-6.9 18.7-39.7 59.1-65.3 103-65.3 59 0 107.7 44.2 113.3 102.8.6 6.1 5.2 11 11.2 12 44.5 7.6 78.1 48.7 78.1 95.6 0 49.7-39.1 92.9-87.3 96.6h-73.7c-7.5 0-13.5 6-13.5 13.5s6 13.5 13.5 13.5h75.2c30.5-2.2 59-16.2 80.2-39.6 21.1-23.2 32.6-53 32.6-84-.1-56.1-38.4-106-90.8-119.8z"/><path d="M324.2 280c5.3-5.3 5.3-13.8 0-19.1l-71.5-71.5c-2.5-2.5-6-4-9.5-4s-7 1.4-9.5 4l-71.5 71.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4s6.9-1.3 9.5-4l48.5-48.5v222.9c0 7.5 6 13.5 13.5 13.5s13.5-6 13.5-13.5V231.5l48.5 48.5c5.2 5.3 13.7 5.3 19 0z"/></symbol><symbol viewBox="0 0 4.99 5.15" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" clip-rule="evenodd" id="icon-user" xmlns="http://www.w3.org/2000/svg"><path d="M4.26 3.32c.49.49.73 1.13.73 1.76v.07h-.52v-.07c0-.51-.19-1.01-.58-1.4a1.96 1.96 0 0 0-1.4-.58c-.51 0-1.01.19-1.4.58-.39.39-.58.89-.58 1.4v.07h-.52v-.07c0-.64.24-1.28.73-1.76.49-.49 1.13-.73 1.76-.73.64 0 1.28.24 1.76.73z"/><path d="M3.05 1.86a.78.78 0 0 0 .23-.55c0-.2-.08-.4-.23-.55A.78.78 0 0 0 2.5.53c-.2 0-.4.08-.55.23a.78.78 0 0 0-.23.55c0 .2.08.4.23.55.15.15.35.23.55.23.2 0 .4-.08.55-.23zm.75-.55c0 .33-.13.67-.38.92s-.59.38-.92.38c-.33 0-.67-.13-.92-.38s-.38-.59-.38-.92c0-.33.13-.67.38-.92s.59-.38.92-.38c.33 0 .67.13.92.38s.38.59.38.92z"/></symbol><symbol viewBox="0 0 29.26 30.26" id="icon-zoom" xmlns="http://www.w3.org/2000/svg"><path d="M3.5 28.34l9.43-9.61-1.39-1.4-9.62 9.44v-3.5H0v6.99h6.99v-1.92H3.5z" opacity=".95"/><path d="M3.5 27.34l9.43-9.61-1.39-1.4-9.62 9.44v-3.5H0v6.99h6.99v-1.92H3.5z"/><path d="M25.77 2.92l-9.44 9.62 1.4 1.39 9.61-9.43v3.49h1.92V1h-6.99v1.92h3.5z" opacity=".95"/><path d="M25.77 1.92l-9.44 9.62 1.4 1.39 9.61-9.43v3.49h1.92V0h-6.99v1.92h3.5z"/></symbol><symbol viewBox="0 0 550.795 550.795" id="mail-filled" xmlns="http://www.w3.org/2000/svg"><path d="M501.613 491.782c12.381 0 23.109-4.088 32.229-12.16L377.793 323.567a2878.213 2878.213 0 0 0-10.801 7.767c-11.678 8.604-21.156 15.318-28.434 20.129-7.277 4.822-16.959 9.737-29.045 14.755-12.094 5.024-23.361 7.528-33.813 7.528h-.612c-10.453 0-21.72-2.503-33.813-7.528-12.093-5.018-21.775-9.933-29.045-14.755-7.277-4.811-16.75-11.524-28.434-20.129a1351.934 1351.934 0 0 0-10.771-7.809L16.946 479.622c9.119 8.072 19.854 12.16 32.234 12.16h452.433zM31.047 225.299C19.37 217.514 9.015 208.598 0 198.555V435.98l137.541-137.541c-27.516-19.21-62.969-43.562-106.494-73.14zM520.059 225.299c-41.865 28.336-77.447 52.73-106.75 73.195l137.486 137.492V198.555c-8.815 9.841-19.059 18.751-30.736 26.744z"/><path d="M501.613 59.013H49.181c-15.784 0-27.919 5.33-36.42 15.979C4.253 85.646.006 98.97.006 114.949c0 12.907 5.636 26.892 16.903 41.959 11.267 15.061 23.256 26.891 35.961 35.496 6.965 4.921 27.969 19.523 63.012 43.801a20367.497 20367.497 0 0 1 49.505 34.395 8855.122 8855.122 0 0 1 31.022 21.701c.985.691 2.534 1.799 4.59 3.269 2.215 1.591 5.018 3.61 8.476 6.107 6.659 4.816 12.191 8.709 16.597 11.683 4.4 2.975 9.731 6.298 15.985 9.988 6.249 3.685 12.143 6.456 17.675 8.299 5.533 1.842 10.655 2.766 15.367 2.766h.612c4.711 0 9.834-.924 15.368-2.766 5.531-1.843 11.42-4.608 17.674-8.299 6.248-3.69 11.572-7.02 15.986-9.988 4.406-2.974 9.938-6.866 16.598-11.683 3.451-2.497 6.254-4.517 8.469-6.102a643.575 643.575 0 0 1 4.596-3.274c6.684-4.651 17.1-11.892 31.104-21.616 25.482-17.705 63.01-43.764 112.742-78.281 14.957-10.447 27.453-23.054 37.496-37.803 10.025-14.749 15.051-30.22 15.051-46.408 0-13.525-4.873-25.098-14.598-34.737-9.736-9.627-21.265-14.443-34.584-14.443z"/></symbol><symbol viewBox="0 0 96.124 96.123" id="social-fb" xmlns="http://www.w3.org/2000/svg"><path d="M72.089.02L59.624 0C45.62 0 36.57 9.285 36.57 23.656v10.907H24.037a1.96 1.96 0 0 0-1.96 1.961v15.803a1.96 1.96 0 0 0 1.96 1.96H36.57v39.876a1.96 1.96 0 0 0 1.96 1.96h16.352a1.96 1.96 0 0 0 1.96-1.96V54.287h14.654a1.96 1.96 0 0 0 1.96-1.96l.006-15.803a1.963 1.963 0 0 0-1.961-1.961H56.842v-9.246c0-4.444 1.059-6.7 6.848-6.7l8.397-.003a1.96 1.96 0 0 0 1.959-1.96V1.98A1.96 1.96 0 0 0 72.089.02z"/></symbol><symbol viewBox="0 0 512 512" id="social-google" xmlns="http://www.w3.org/2000/svg"><path d="M160 224v64h90.528c-13.216 37.248-48.8 64-90.528 64-52.928 0-96-43.072-96-96s43.072-96 96-96c22.944 0 45.024 8.224 62.176 23.168l42.048-48.256C235.424 109.824 198.432 96 160 96 71.776 96 0 167.776 0 256s71.776 160 160 160 160-71.776 160-160v-32H160z"/></symbol><symbol viewBox="0 0 26 26" id="social-ins" xmlns="http://www.w3.org/2000/svg"><path d="M20 7c-.551 0-1-.449-1-1V4c0-.551.449-1 1-1h2c.551 0 1 .449 1 1v2c0 .551-.449 1-1 1h-2zM13 9.188c-.726 0-1.396.213-1.973.563.18-.056.367-.093.564-.093a1.933 1.933 0 1 1-1.933 1.934c0-.199.039-.386.094-.565A3.77 3.77 0 0 0 9.188 13a3.81 3.81 0 0 0 3.813 3.813A3.812 3.812 0 1 0 13 9.188z"/><path d="M13 7a6 6 0 1 1 0 12 6 6 0 0 1 0-12m0-2c-4.411 0-8 3.589-8 8s3.589 8 8 8 8-3.589 8-8-3.589-8-8-8z"/><path d="M21.125 0H4.875A4.874 4.874 0 0 0 0 4.875v16.25A4.874 4.874 0 0 0 4.875 26h16.25A4.874 4.874 0 0 0 26 21.125V4.875A4.874 4.874 0 0 0 21.125 0zM24 9h-6.537A5.97 5.97 0 0 1 19 13a6 6 0 0 1-12 0c0-1.539.584-2.938 1.537-4H2V4.875A2.879 2.879 0 0 1 4.875 2h16.25A2.878 2.878 0 0 1 24 4.875V9z"/></symbol><symbol viewBox="0 0 95.481 95.481" id="social-ok" xmlns="http://www.w3.org/2000/svg"><path d="M43.041 67.254c-7.402-.772-14.076-2.595-19.79-7.064-.709-.556-1.441-1.092-2.088-1.713-2.501-2.402-2.753-5.153-.774-7.988 1.693-2.426 4.535-3.075 7.489-1.682.572.27 1.117.607 1.639.969 10.649 7.317 25.278 7.519 35.967.329 1.059-.812 2.191-1.474 3.503-1.812 2.551-.655 4.93.282 6.299 2.514 1.564 2.549 1.544 5.037-.383 7.016-2.956 3.034-6.511 5.229-10.461 6.761-3.735 1.448-7.826 2.177-11.875 2.661.611.665.899.992 1.281 1.376 5.498 5.524 11.02 11.025 16.5 16.566 1.867 1.888 2.257 4.229 1.229 6.425-1.124 2.4-3.64 3.979-6.107 3.81-1.563-.108-2.782-.886-3.865-1.977-4.149-4.175-8.376-8.273-12.441-12.527-1.183-1.237-1.752-1.003-2.796.071-4.174 4.297-8.416 8.528-12.683 12.735-1.916 1.889-4.196 2.229-6.418 1.15-2.362-1.145-3.865-3.556-3.749-5.979.08-1.639.886-2.891 2.011-4.014 5.441-5.433 10.867-10.88 16.295-16.322.359-.362.694-.746 1.217-1.305z"/><path d="M47.55 48.329c-13.205-.045-24.033-10.992-23.956-24.218C23.67 10.739 34.505-.037 47.84 0c13.362.036 24.087 10.967 24.02 24.478-.068 13.199-10.971 23.897-24.31 23.851zm12.001-24.186c-.023-6.567-5.253-11.795-11.807-11.801-6.609-.007-11.886 5.316-11.835 11.943.049 6.542 5.324 11.733 11.896 11.709 6.552-.023 11.768-5.285 11.746-11.851z"/></symbol><symbol viewBox="0 0 612 612" id="social-twitter" xmlns="http://www.w3.org/2000/svg"><path d="M612 116.258a250.714 250.714 0 0 1-72.088 19.772c25.929-15.527 45.777-40.155 55.184-69.411-24.322 14.379-51.169 24.82-79.775 30.48-22.907-24.437-55.49-39.658-91.63-39.658-69.334 0-125.551 56.217-125.551 125.513 0 9.828 1.109 19.427 3.251 28.606-104.326-5.24-196.835-55.223-258.75-131.174-10.823 18.51-16.98 40.078-16.98 63.101 0 43.559 22.181 81.993 55.835 104.479a125.556 125.556 0 0 1-56.867-15.756v1.568c0 60.806 43.291 111.554 100.693 123.104-10.517 2.83-21.607 4.398-33.08 4.398-8.107 0-15.947-.803-23.634-2.333 15.985 49.907 62.336 86.199 117.253 87.194-42.947 33.654-97.099 53.655-155.916 53.655-10.134 0-20.116-.612-29.944-1.721 55.567 35.681 121.536 56.485 192.438 56.485 230.948 0 357.188-191.291 357.188-357.188l-.421-16.253c24.666-17.593 46.005-39.697 62.794-64.861z"/></symbol><symbol viewBox="0 0 512.019 512.019" id="social-viber" xmlns="http://www.w3.org/2000/svg"><path d="M255.994 41.372c5.88 4.412 11.025 4.89 26.402 5.76 45.056 2.586 73.626 12.211 105.404 35.541 38.903 28.459 63.309 68.156 72.559 117.99 2.381 12.902 3.558 23.851 4.36 40.354.486 11.068.87 15.36 3.388 20.207 3.55 6.613 10.428 11.008 18.406 11.75.785.068 1.57.102 2.347.102 7.1 0 13.688-3.004 17.894-8.235 5.589-6.81 5.581-13.158 5.035-26.829-1.604-42.342-10.863-80.384-27.52-113.067-11.878-23.253-23.219-38.485-43.358-58.231-21.478-20.881-38.033-32.563-63.309-44.638C350.825 9.295 331.881 5.25 304.139 1.863c-5.598-.64-16.137-1.476-23.45-1.715-13.935-.461-19.379-.085-25.813 5.632-4.932 4.343-7.62 10.94-7.398 18.116.221 7.125 3.31 13.465 8.516 17.476z"/><path d="M396.436 224.422c0 11.989.99 23.245 12.808 29.065 3.243 1.681 7.467 2.526 11.588 2.526 3.661 0 7.228-.674 9.95-2.039 4.779-2.458 9.6-7.646 11.486-12.339 2.398-6.05 1.306-27.742.623-33.86-3.2-27.332-12.698-53.137-27.46-74.615-26.743-38.852-66.816-62.345-119.083-69.828-17.724-2.611-30.891-3.533-38.639 8.294-6.084 8.986-5.308 20.446 1.877 28.476 5.726 6.494 13.926 7.962 21.18 8.542 20.77 1.638 44.032 8.439 59.281 17.323l.026.008c17.741 10.274 32.427 25.208 42.487 43.196 8.816 15.733 13.876 35.872 13.876 55.251z"/><path d="M350.977 151.133c-11.674-10.692-25.353-17.391-43.017-21.077-4.045-.853-10.906-1.596-15.172-1.835-7.356-.452-10.385-.375-15.616 2.167-6.323 3.174-10.752 9.062-12.169 16.137-1.408 7.031.393 14.106 5.001 19.482 5.325 6.084 10.778 7.603 21.777 8.909 18.381 2.202 28.732 8.388 34.586 20.659 2.961 6.263 4.642 13.124 5.615 22.989 1.007 9.242 2.816 15.172 9.395 20.036 4.139 3.123 9.378 4.668 14.566 4.668 5.675 0 11.298-1.852 15.403-5.513l.051-.034c8.482-7.671 8.841-17.186 6.545-32.034-3.558-23.783-11.861-40.611-26.965-54.554z"/><path d="M511.965 412.822c-.418-9.293-3.61-17.604-9.566-24.781-5.751-6.784-42.487-35.831-58.88-47.053-12.749-8.678-32.896-20.898-42.317-25.668-13.918-6.921-27.913-8.132-41.685-3.567-3.012 1.007-5.163 1.903-7.185 2.961-6.972 3.661-12.092 9.429-22.042 21.666-10.658 12.868-12.877 15.369-12.851 15.369-1.007.606-2.611 1.459-4.557 2.347-7.927 3.61-20.924.64-42.061-9.66-17.835-8.627-33.852-19.703-51.942-35.9-15.411-13.73-30.908-31.539-39.509-45.355-11.947-19.166-19.174-41.344-19.26-51.644 0-4.011 1.758-9.771 3.661-12.945l.836-.862a54.899 54.899 0 0 1 2.039-1.886c2.97-2.671 7.27-6.246 11.75-9.788 19.661-15.488 25.89-21.76 29.218-33.408 3.217-10.769.393-24.03-8.892-41.719-13.833-26.394-57.694-82.569-75.998-94.413-3.012-1.886-7.97-4.429-11.477-5.606-7.509-2.654-18.295-3.558-25.003-2.133-9.284 1.903-19.473 7.748-41.958 24.081-12.321 9.02-31.316 26.658-36.471 35.089-12.16 20.42-9.062 38.135 1.963 67.806 10.505 28.314 23.979 56.815 39.996 84.642l.836 1.408a615.617 615.617 0 0 0 9.993 16.7l.896 1.408c3.439 5.47 6.921 10.931 10.564 16.367l5.743 8.354c2.142 3.123 4.275 6.238 6.485 9.336a711.918 711.918 0 0 0 13.466 18.176c49.399 64.324 106.308 115.968 173.969 157.858l2.773 1.741c9.822 6.016 20.275 12.032 31.061 17.869l4.215 2.27c11.162 5.939 21.768 11.264 32.427 16.256l3.319 1.51c12.587 5.828 24.098 10.837 35.174 15.3 13.406 5.41 19.61 7.066 29.03 7.066 1.673 0 3.456-.051 5.402-.145 6.076-.29 11.537-1.195 16.708-2.773 7.262-2.202 14.532-5.897 22.852-11.64a162.513 162.513 0 0 0 4.463-3.166c1.843-1.357 3.806-3.021 5.794-4.736 9.481-8.201 20.361-20.113 29.124-31.863 4.676-6.272 10.889-15.377 13.986-23.194 2.901-7.313 4.215-14.609 3.908-21.666v-.009z"/></symbol><symbol viewBox="0 0 511.962 511.962" id="social-vk" xmlns="http://www.w3.org/2000/svg"><path d="M507.399 370.471c-1.376-2.304-9.888-20.8-50.848-58.816-42.88-39.808-37.12-33.344 14.528-102.176 31.456-41.92 44.032-67.52 40.096-78.464-3.744-10.432-26.88-7.68-26.88-7.68l-76.928.448s-5.696-.768-9.952 1.76c-4.128 2.496-6.784 8.256-6.784 8.256s-12.192 32.448-28.448 60.032c-34.272 58.208-48 61.28-53.6 57.664-13.024-8.416-9.76-33.856-9.76-51.904 0-56.416 8.544-79.936-16.672-86.016-8.384-2.016-14.528-3.36-35.936-3.584-27.456-.288-50.72.096-63.872 6.528-8.768 4.288-15.52 13.856-11.392 14.4 5.088.672 16.608 3.104 22.72 11.424 7.904 10.72 7.616 34.848 7.616 34.848s4.544 66.4-10.592 74.656c-10.4 5.664-24.64-5.888-55.2-58.72-15.648-27.04-27.488-56.96-27.488-56.96s-2.272-5.568-6.336-8.544c-4.928-3.616-11.84-4.768-11.84-4.768l-73.152.448s-10.976.32-15.008 5.088c-3.584 4.256-.288 13.024-.288 13.024s57.28 133.984 122.112 201.536c59.488 61.92 127.008 57.856 127.008 57.856h30.592s9.248-1.024 13.952-6.112c4.352-4.672 4.192-13.44 4.192-13.44s-.608-41.056 18.464-47.104c18.784-5.952 42.912 39.68 68.48 57.248 19.328 13.28 34.016 10.368 34.016 10.368l68.384-.96s35.776-2.208 18.816-30.336z"/></symbol><symbol viewBox="0 0 308 308" id="social-whatsapp" xmlns="http://www.w3.org/2000/svg"><path d="M227.904 176.981c-.6-.288-23.054-11.345-27.044-12.781-1.629-.585-3.374-1.156-5.23-1.156-3.032 0-5.579 1.511-7.563 4.479-2.243 3.334-9.033 11.271-11.131 13.642-.274.313-.648.687-.872.687-.201 0-3.676-1.431-4.728-1.888-24.087-10.463-42.37-35.624-44.877-39.867-.358-.61-.373-.887-.376-.887.088-.323.898-1.135 1.316-1.554 1.223-1.21 2.548-2.805 3.83-4.348a140.77 140.77 0 0 1 1.812-2.153c1.86-2.164 2.688-3.844 3.648-5.79l.503-1.011c2.344-4.657.342-8.587-.305-9.856-.531-1.062-10.012-23.944-11.02-26.348-2.424-5.801-5.627-8.502-10.078-8.502-.413 0 0 0-1.732.073-2.109.089-13.594 1.601-18.672 4.802C90 87.918 80.89 98.74 80.89 117.772c0 17.129 10.87 33.302 15.537 39.453.116.155.329.47.638.922 17.873 26.102 40.154 45.446 62.741 54.469 21.745 8.686 32.042 9.69 37.896 9.69h.001c2.46 0 4.429-.193 6.166-.364l1.102-.105c7.512-.666 24.02-9.22 27.775-19.655 2.958-8.219 3.738-17.199 1.77-20.458-1.348-2.216-3.671-3.331-6.612-4.743z"/><path d="M156.734 0C73.318 0 5.454 67.354 5.454 150.143c0 26.777 7.166 52.988 20.741 75.928L.212 302.716a3.998 3.998 0 0 0 4.999 5.096l79.92-25.396c21.87 11.685 46.588 17.853 71.604 17.853C240.143 300.27 308 232.923 308 150.143 308 67.354 240.143 0 156.734 0zm0 268.994c-23.539 0-46.338-6.797-65.936-19.657a3.996 3.996 0 0 0-3.406-.467l-40.035 12.726 12.924-38.129a4.002 4.002 0 0 0-.561-3.647c-14.924-20.392-22.813-44.485-22.813-69.677 0-65.543 53.754-118.867 119.826-118.867 66.064 0 119.812 53.324 119.812 118.867.001 65.535-53.746 118.851-119.811 118.851z"/></symbol><symbol  viewBox="0 0 310.05 310.05" id="social-pinterest" xmlns="http://www.w3.org/2000/svg"><path id="XMLID_799_" d="M245.265,31.772C223.923,11.284,194.388,0,162.101,0c-49.32,0-79.654,20.217-96.416,37.176 c-20.658,20.9-32.504,48.651-32.504,76.139c0,34.513,14.436,61.003,38.611,70.858c1.623,0.665,3.256,1,4.857,1 c5.1,0,9.141-3.337,10.541-8.69c0.816-3.071,2.707-10.647,3.529-13.936c1.76-6.495,0.338-9.619-3.5-14.142 c-6.992-8.273-10.248-18.056-10.248-30.788c0-37.818,28.16-78.011,80.352-78.011c41.412,0,67.137,23.537,67.137,61.425 c0,23.909-5.15,46.051-14.504,62.35c-6.5,11.325-17.93,24.825-35.477,24.825c-7.588,0-14.404-3.117-18.705-8.551 c-4.063-5.137-5.402-11.773-3.768-18.689c1.846-7.814,4.363-15.965,6.799-23.845c4.443-14.392,8.643-27.985,8.643-38.83 c0-18.55-11.404-31.014-28.375-31.014c-21.568,0-38.465,21.906-38.465,49.871c0,13.715,3.645,23.973,5.295,27.912 c-2.717,11.512-18.865,79.953-21.928,92.859c-1.771,7.534-12.44,67.039,5.219,71.784c19.841,5.331,37.576-52.623,39.381-59.172 c1.463-5.326,6.582-25.465,9.719-37.845c9.578,9.226,25,15.463,40.006,15.463c28.289,0,53.73-12.73,71.637-35.843 c17.367-22.418,26.932-53.664,26.932-87.978C276.869,77.502,265.349,51.056,245.265,31.772z"/></symbol><symbol  viewBox="0 0 512 512" id="social-youtube" xmlns="http://www.w3.org/2000/svg"><path d="M490.24,113.92c-13.888-24.704-28.96-29.248-59.648-30.976C399.936,80.864,322.848,80,256.064,80 c-66.912,0-144.032,0.864-174.656,2.912c-30.624,1.76-45.728,6.272-59.744,31.008C7.36,138.592,0,181.088,0,255.904 C0,255.968,0,256,0,256c0,0.064,0,0.096,0,0.096v0.064c0,74.496,7.36,117.312,21.664,141.728 c14.016,24.704,29.088,29.184,59.712,31.264C112.032,430.944,189.152,432,256.064,432c66.784,0,143.872-1.056,174.56-2.816 c30.688-2.08,45.76-6.56,59.648-31.264C504.704,373.504,512,330.688,512,256.192c0,0,0-0.096,0-0.16c0,0,0-0.064,0-0.096 C512,181.088,504.704,138.592,490.24,113.92z M192,352V160l160,96L192,352z"/></symbol></svg></div>
		<div class="wrapper <?=($APPLICATION->GetCurPage(false) !== SITE_DIR) ? 'wrapper_inner' : '';?>">
			<header class="header">
				<div class="container">
					<div class="header__inner">
						<div>
							<div class="menu-touch js-menu-touch">
								<div class="menu-touch__burger js-menu-touch-burger">
									<div class="menu-touch__burger-line"></div>
									<div class="menu-touch__burger-line"></div>
									<div class="menu-touch__burger-line"></div>
								</div>
								<div class="menu-touch__dropdown menu__dropdown">
									<div class="container">
										<div class="menu__dropdown-inner">
											<?$APPLICATION->IncludeComponent(
													"bitrix:menu",
													"topmobile_menu",
													Array(
														"ALLOW_MULTI_SELECT" => "N",
														"CHILD_MENU_TYPE" => "left",
														"DELAY" => "N",
														"MAX_LEVEL" => "1",
														"MENU_CACHE_GET_VARS" => array(""),
														"MENU_CACHE_TIME" => "3600",
														"MENU_CACHE_TYPE" => "A",
														"MENU_CACHE_USE_GROUPS" => "Y",
														"ROOT_MENU_TYPE" => "topmobile",
														"USE_EXT" => "N"
													)
											);?>
											<?$APPLICATION->IncludeComponent(
												"bitrix:main.site.selector",
												"select_lang_mobile",
												Array(
													"CACHE_TIME" => "3600",
													"CACHE_TYPE" => "A",
													"SITE_LIST" => array(0=>"en",1=>"s1",)
												)
											);?>
											<div class="menu__dropdown-item">
												<div class="menu__dropdown-link location location_mobile">
													<div class="location__link js-link">
														<div class="location__wrap js-popup-location"> 
														<svg class="icon icon-map">
															<use xlink:href="#icon-map"></use>
														</svg>
														<?if($city = HB\helpers\LocationHelpers::getUserCity(LANGUAGE_ID)):?>
															<span class="location__text"><?=$city?></span>
														<?elseif($arCity = HB\helpers\LocationHelpers::getCityByIp()):?>
															<span class="location__text"><?=$arCity['city'];?></span>
														<?else:?>
															<span class="location__text"><?=GetMessage('CHOOSE_CITY');?></span>
														<?endif;?>
														</div>
														<div class="location__city city js-city">
															<div class="city__content"><?=GetMessage('YOUR_CITY');?>: <?=$arCity['city'];?></div>
															<div class="city__close js-city-close"></div>
															<div class="city__btn-wrap">
																<div class="city__btn js-location-close-select button_city_yes"><?=GetMessage('YES');?></div>
																<div class="city__btn js-location-open-select"><?=GetMessage('NO');?></div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="menu__dropdown-copyright"><span><?=$arSettings["COPYRIGHT"]["VALUE"]?><?=date('Y');?></span></div>
										</div>
									</div>
								</div>
							</div><a class="logo hidden-mobile-down" href="<?=SITE_DIR?>"><span>Swami</span><span>Art</span></a>
							<div class="menu__item location js-location-mobile">
								<div class="location__link js-link">
									<div class="location__wrap js-popup-location"> 
										<svg class="icon icon-map">
											<use xlink:href="#icon-map"></use>
										</svg>
										<?if($city):?>
											<span class="location__text"><?=$city?></span>
										<?elseif($arCity):?>
											<span class="location__text"><?=$arCity['city'];?></span>
										<?else:?>
											<span class="location__text"><?=GetMessage('CHOOSE_CITY');?></span>
										<?endif;?>
									</div>
									<div class="location__city city js-city">
										<div class="city__content"><?=GetMessage('YOUR_CITY');?>: <?=$arCity['city'];?></div>
										<div class="city__close js-city-close"></div>
										<div class="city__btn-wrap">
											<div class="city__btn js-location-close-select button_city_yes" data-city="<?=$arCity['city'];?>" data-region="<?=$arCity['region'];?>"><?=GetMessage('YES');?></div>
											<div class="city__btn js-location-open-select"><?=GetMessage('NO');?></div>
										</div>
									</div>
								</div>
							</div><a class="logo hidden-tablet-up" href="/"><span>S</span><span>A</span></a>
						</div>
						<?$APPLICATION->IncludeComponent(
								"bitrix:menu",
								"main_menu",
								Array(
									"ALLOW_MULTI_SELECT" => "N",
									"CHILD_MENU_TYPE" => "left",
									"DELAY" => "N",
									"MAX_LEVEL" => "1",
									"MENU_CACHE_GET_VARS" => array(""),
									"MENU_CACHE_TIME" => "3600",
									"MENU_CACHE_TYPE" => "A",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"ROOT_MENU_TYPE" => "top",
									"USE_EXT" => "N"
								)
						);?>
						<div class="tools">
							<?if($USER->IsAuthorized()):?>
								<div class="tools__item tool-user js-tool-user"><a class="tools__link tool-user__link js-tool-user-link" href="<?=SITE_DIR;?>personal/">
									<div class="tool-user__icon" style="background-image:url(<?=HB\helpers\Helpers::getAvatar();?>)">
										<svg class="icon icon-user">
										<use xlink:href="#icon-user"></use>
										</svg>
									</div>
										<span><?=HB\helpers\Helpers::getUserName()?></span>
									</a>
										<?$APPLICATION->IncludeComponent(
											"bitrix:menu", 
											"personal_menu", 
											array(
												"ALLOW_MULTI_SELECT" => "N",
												"CHILD_MENU_TYPE" => "left",
												"DELAY" => "N",
												"MAX_LEVEL" => "1",
												"MENU_CACHE_GET_VARS" => array(
												),
												"MENU_CACHE_TIME" => "3600",
												"MENU_CACHE_TYPE" => "A",
												"MENU_CACHE_USE_GROUPS" => "Y",
												"ROOT_MENU_TYPE" => "personal",
												"USE_EXT" => "N",
												"COMPONENT_TEMPLATE" => "personal_menu"
											),
											false
										);?>
								</div>
							<?else:?>
								<div class="tools__item tool-user js-popup-auth"><a class="tools__link tool-user__link" href="#">
									<div class="tool-user__icon">
										<svg class="icon icon-user">
										<use xlink:href="#icon-user"></use>
										</svg>
									</div></a>
									<?$APPLICATION->IncludeComponent(
											"bitrix:menu", 
											"reg_menu", 
											array(
												"MENU_CACHE_GET_VARS" => array(
												),
												"MENU_CACHE_TIME" => "3600",
												"MENU_CACHE_TYPE" => "A",
												"MENU_CACHE_USE_GROUPS" => "Y",
												"ROOT_MENU_TYPE" => "personal",
												"COMPONENT_TEMPLATE" => "reg_menu"
											),
											false
										);?>
								</div>
							<?endif;?>
								<?php $countFavorites = HB\helpers\WorkHelpers::getCountFavoritesUserWork();?>
								<div class="tools__item tools__item_favorite" data-count="<?=$countFavorites;?>"><a class="tools__link tools__link_tip" href="<?=SITE_DIR?>favorites/">
									<svg class="icon icon-favorite">
										<use xlink:href="#icon-favorite"></use>
									</svg>
									<span class="tools__link-tip"><?=$countFavorites<=99 ? $countFavorites : '99+';?></span></a>
								</div>
								<?php $countCart = HB\helpers\WorkHelpers::getCountCartWork();?>
								<div class="tools__item tools__item_cart" data-count="<?=$countCart;?>"><a class="tools__link tools__link_tip" href="<?=SITE_DIR?>personal/cart/">
									<svg class="icon icon-cart">
										<use xlink:href="#icon-cart"></use>
									</svg><span class="tools__link-tip"><?=$countCart<=99 ? $countCart : '99+';?></span></a>
								</div>
							
							
							<div class="tools__item tool-search js-tool-search">
							<a class="tools__link tool-search__link js-tool-search-link" href="#">
								<svg class="icon icon-search">
									<use xlink:href="#icon-search"></use>
								</svg></a>
								<form class="tool-search__form" method="GET" action="<?=SITE_DIR?>search/">
								<div class="container">
									<div class="tool-search__field">
									<input class="tool-search__input" name ="q" type="text" placeholder="<?=GetMessage("SERACH");?>"/>
									<button class="tool-search__reset" type="reset">
										<svg class="icon icon-close">
										<use xlink:href="#icon-close"></use>
										</svg>
									</button>
									<button class="tool-search__submit" type="submit">
										<svg class="icon icon-search">
										<use xlink:href="#icon-search"></use>
										</svg>
									</button>
									</div>
								</div>
								</form>
							</div>
							<?$APPLICATION->IncludeComponent(
								"bitrix:main.site.selector",
								"select_lang",
								Array(
									"CACHE_TIME" => "3600",
									"CACHE_TYPE" => "A",
									"SITE_LIST" => array(0=>"en",1=>"s1",)
								)
							);?>
						</div>
					</div>
				</div>
			</header>
			<div class="l-message">
				<?if(!$_COOKIE['cookie_close']):?>
					<div class="l-message__item js-l-message">
							<div class="container">
								<div class="l-message__inner">
									<div class="l-message__close l-message__close_btn js-l-message-close js-l-message-close-cookie">  <span class="btn btn_small"><?=GetMessage('TO_ACCEPT');?></span></div>
									<?=$arSettings['COOKIE_MESSAGE']['~VALUE']['TEXT'];?>
									
								</div>
							</div>
					</div>				
				<?endif;?>
				<?if($USER->isAuthorized()):?>
					<?if(!HB\userEntity\Confirm::checkEmailConfirm($USER->GetID())):?>
						<?if($_SESSION["CONFIRM_EMAIL"] != "Y" && !$_COOKIE['cookie_confirm_email_close']):?>
							<div class="l-message__item js-l-message">
								<div class="container">
									<div class="l-message__inner">
										<div class="l-message__close js-l-message-close js-cookie-confirm-email">  
											<svg class="icon icon-close">
											<use xlink:href="#icon-close"></use>
											</svg>
										</div>
										<div>
											<?=$arSettings['EMAIL_MESSAGE']['~VALUE']['TEXT'];?>
										</div>
									</div>
								</div>
							</div>
						<?endif;?>
					<?endif;?>
				<?endif;?>
				
				<?if ($USER->isAuthorized()) {?>
					<?$authorModerationStatuses = HB\helpers\Helpers::getAuthorModerationStatuses($arSettings);
					if (!empty(authorModerationStatuses)) {
						foreach ($authorModerationStatuses as $statusKey => $statusValue) {
							if (!$_COOKIE[$statusKey] && $statusValue['STATUS_TEXT']) {?>
								<div class="l-message__item js-l-message">
									<div class="container">
										<div class="l-message__inner">
											<div class="l-message__close l-message__close_btn js-l-message-close js-l-message-close-statuses-cookie" data-id="<?=$statusKey;?>">  
												<span class="btn btn_small"><?=GetMessage('TO_ACCEPT');?></span>
											</div>
											<p><?=$statusValue['STATUS_TEXT'];?></p>
											<p><?=$statusValue['COMMENT'] ? $statusValue['COMMENT'] : '';?></p>
										</div>
									</div>
								</div>
							<?}?>
						<?}?>				
					<?}?>
				<?}?>
			</div>		
			<?$APPLICATION->IncludeComponent("HB:location.determination","",Array());?>
			<?$APPLICATION->IncludeComponent(
				"bitrix:breadcrumb",
				"",
				Array(
					"PATH" => "",
					"SITE_ID" => SITE_ID,
					"START_FROM" => "0"
				)
			);?>
