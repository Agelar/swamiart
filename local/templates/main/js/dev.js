var globalSort =  'PROPERTY_FINAL_RATING';
var globalDesc =  'DESC';
//сортировка на главной странице
$(document).on('change', '.js-sort__front', function(){
    var elementCount = parseInt($('#js-catalog__ajax').attr('data-element_count'));
    var pageNum = 1;
    var sort = $(this).val();
    var desc = 'DESC';
    
    globalSort = sort;
    if($(this).find('option:selected').data('desc')) desc = $(this).find('option:selected').data('desc');
    globalDesc = desc;
    $.ajax({
        type: 'GET',
        url: '/local/ajax/catalog_ajax_front.php?PAGEN_1=' + pageNum + '&ELEMENT_COUNT=' + elementCount + '&SORT=' + sort + '&DESC=' + desc,
        dataType: 'html',
        success: function(data){
            var stamp = '<div class="gallery__gutter-sizer js-gallery-gutter-sizer"></div>';
            $('#js-catalog__ajax').html('');
            $('#js-catalog__ajax').html(stamp + data);

            $gallery = $('.js-gallery');
            $gallery.masonry('destroy'); // destroy

            function galleryInit() {
                $gallery = $('.js-gallery').masonry({
                    // columnWidth: 280,
                    itemSelector: '.js-gallery-item',
                    gutter: '.js-gallery-gutter-sizer',
                    horizontalOrder: true
                    
                });
                $gallery.imagesLoaded().progress(function() {
                    $gallery.masonry('layout');
                });
            }
            $(document).on('gallery:reinit', '.js-gallery', function() {
                galleryCardImgInit();
                galleryInit();
                galleryLazyload();
            });
            $('.js-gallery').trigger('gallery:reinit');
        }
    });
    $(this).find('option').data('desc', 'DESC');
    $(this).find('option:selected').data('desc', 'ASC');
    if(desc=='ASC') $(this).find('option:selected').data('desc', 'DESC');
    
    $(this).find('option:selected').prop('selected', false);
    $(this).find('.opt-checked').prop('selected', true);
    return false;
});

//сортировка в каталоге
$(document).on('change', '.js-sort__catalog', function(){
    var elementCount = parseInt($('#js-catalog__ajax').attr('data-element_count'));
    var pageNum = 1;
    var sort = $(this).val();
    globalSort = sort;
    var desc = 'DESC';
    
    if($(this).find('option:selected').data('desc')) desc = $(this).find('option:selected').data('desc');
    globalDesc = desc;
    $.ajax({
        type: 'GET',
        url: location.pathname + '?AJAX_CATALOG=Y&PAGEN_1=' + pageNum + '&ELEMENT_COUNT=' + elementCount + '&SORT=' + sort + '&DESC=' + desc,
        dataType: 'html',
        beforeSend: function(){
            $('body').find('.preloader').addClass('load').show();
        },
        complete: function() {
            $('body').find('.preloader').removeClass('load').hide();
        },
        success: function(data){
            var stamp = '<div class="gallery__gutter-sizer js-gallery-gutter-sizer"></div>';
            $('#js-catalog__ajax').html('');
            $gallery = $('.js-gallery');
            $gallery.masonry('destroy'); // destroy
            $('#js-catalog__ajax').html(stamp + data);            
        
            $(document).on('gallery:reinit', '.js-gallery', function() {
                    galleryCardImgInit();
                    galleryInit();
                    galleryLazyload();
            });
            $('.js-gallery').trigger('gallery:reinit');
        }
  
    });
    $(this).find('option').data('desc', 'DESC');
    $(this).find('option:selected').data('desc', 'ASC');
    if(desc=='ASC') $(this).find('option:selected').data('desc', 'DESC');
    
    $(this).find('option:selected').prop('selected', false);
    $(this).find('.opt-checked').prop('selected', true);
    return false;
});


//сортировка в моих работах
$(document).on('change', '.js-sort__work_list', function(){
    var authorId = $('.js-author_filter_work_expert').val();
    var pageNum = $('#lk-works__ajax').data('page');
    var sort = $(this).val();
    $.ajax({
        type: 'GET',
        url: location.pathname + '?AJAX=Y&AUTHOR_ID=' + authorId + '&PAGEN_1=' + pageNum + '&SORT=' + sort,
        dataType: 'html',
        success: function(data){
            $('#lk-works__ajax').html(data);
            $('.js-select2').each(function(index, elem) {
                var placeholderText = $(this).attr('data-placeholder');
                $(this).selectize({
                    highlight: true,
                    maxItems: 1,
                    closeAfterSelect: true,
                    hideSelected: false,
                    onDropdownClose: function($dropdown){
                        $($dropdown).find('.selected').not('.active').removeClass('selected');
                    },
                    onChange: function() {
                        if(this.$input.is('[required]')) {
                            this.$input.valid();
                        }
                    }
                });
            });
        }
  
    });
    return false;
});

//пагинация в моих работах
$(document).on('click', '#lk-works__ajax .pagination_lk a', function(){
    var url = $(this).attr('href');
    $.ajax({
        type: 'GET',
        url: url + '&AJAX=Y',
        dataType: 'html',
        success: function(data){
            $('#lk-works__ajax').html(data);
            $('.js-select2').each(function(index, elem) {
                var placeholderText = $(this).attr('data-placeholder');
                $(this).selectize({
                    highlight: true,
                    maxItems: 1,
                    closeAfterSelect: true,
                    hideSelected: false,
                    onDropdownClose: function($dropdown){
                        $($dropdown).find('.selected').not('.active').removeClass('selected');
                    },
                    onChange: function() {
                        if(this.$input.is('[required]')) {
                            this.$input.valid();
                        }
                    }
                });
            });
        }
  
    });
    return false;
});


//смотреть все в каталоге
$(document).on('click', '.js-see__all_catalog', function(){
    var elementPageCount = parseInt($('#js-catalog__ajax').attr('data-element_page_count'));
    var elementCount = parseInt($('#js-catalog__ajax').attr('data-element_count'));
    var maxCount = parseInt($('#js-catalog__ajax').attr('data-max_count'));
   
    var pageNum = parseInt($('#js-catalog__ajax').attr('data-page')) + 1;
    var sort = globalSort;
    var desc = globalDesc;

    if(window.pageYOffset != 0) {
        document.cookie = 'PAGEN_CATALOG_SCROLL='+window.pageYOffset+'; path=/; expires=' + new Date(new Date().getTime() + 60 * 1000).toUTCString();
    }
    if(pageNum != 0) {
        document.cookie = 'PAGEN_HISTORY='+pageNum+'; path=/; expires=' + new Date(new Date().getTime() + 60 * 1000).toUTCString();
    }
    $.ajax({
        type: 'GET',
        url: location.pathname + '?AJAX_CATALOG=Y&PAGEN_1=' + pageNum + '&ELEMENT_COUNT=' + elementPageCount + '&SORT=' + sort + '&DESC=' + desc,
        dataType: 'html',
        beforeSend: function(){
            $('body').find('.preloader').addClass('load').show();
        },
        complete: function() {
            $('body').find('.preloader').removeClass('load').hide();
        },
        success: function(data){
            var $items = $(data);
            
            var contentItems = $('<div class="content_items">' + data + '</div>');
            
            elementCount += contentItems.children('.gallery__item').length;
            if(maxCount<=elementCount) $('.js-see__all_catalog').hide();
                   
            galleryCardImgInit();
            $gallery = $('.js-gallery');
            $gallery.append( $items ).masonry( 'appended', $items );
            $gallery.imagesLoaded().progress(function() {
                $gallery.masonry('layout');
            });
            galleryLazyload();
            
            $('.js-gallery').trigger('gallery:reinit');
            
            $('#js-catalog__ajax').attr('data-page', pageNum);
            $('#js-catalog__ajax').attr('data-element_count', elementCount);
        }
    });
    return false;
});

function scrollCatalogByCookie(pagen, pagenMax, scrollY = 0) {
    var elementPageCount = parseInt($('#js-catalog__ajax').attr('data-element_page_count'));
    var elementCount = parseInt($('#js-catalog__ajax').attr('data-element_count'));
    var maxCount = parseInt($('#js-catalog__ajax').attr('data-max_count'));
   
    var sort = globalSort;
    var desc = globalDesc;

    if($('.gallery').length > 0 && elementPageCount !== NaN && elementCount !== NaN && pagen <= pagenMax) {
        $.ajax({
            type: 'GET',
            url: location.pathname + '?AJAX_CATALOG=Y&PAGEN_1=' + pagen + '&ELEMENT_COUNT=' + elementPageCount + '&SORT=' + sort + '&DESC=' + desc,
            dataType: 'html',
            beforeSend: function(){
                $('body').find('.preloader').addClass('load').show();
            },
            complete: function() {
                $('body').find('.preloader').removeClass('load').hide();
            },
            success: function(data){
                var $items = $(data);
                
                var contentItems = $('<div class="content_items">' + data + '</div>');
                
                elementCount += contentItems.children('.gallery__item').length;
                if(maxCount<=elementCount) $('.js-see__all_catalog').hide();
                    
                galleryCardImgInit();
                $gallery = $('.js-gallery');
                $gallery.append( $items ).masonry( 'appended', $items );
                $gallery.imagesLoaded().progress(function() {
                    $gallery.masonry('layout');
                });
                galleryLazyload();
                
                $('.js-gallery').trigger('gallery:reinit');
                
                $('#js-catalog__ajax').attr('data-page', pagen);
                $('#js-catalog__ajax').attr('data-element_count', elementCount);

                if(scrollY > 0) {
                    window.scrollTo(0, scrollY);
                    galleryLazyload();
                }

                if(getCookie('PAGEN_HISTORY')) {
                    scrollCatalogByCookie(pagen+1, getCookie('PAGEN_HISTORY'), getCookie('PAGEN_CATALOG_SCROLL'));
                    galleryLazyload();
                }

                if(pagen == pagenMax) {
                    document.cookie = 'PAGEN_CATALOG_SCROLL=; path=/; expires=0';
                    document.cookie = 'PAGEN_HISTORY=; path=/; expires=0';
                }
            }
        });
    }
}


//добавление в корзину
$(document).on('click', '.js-catalog__cart', function(){
    var this_element = $(this);
    var id = parseInt($(this).attr('data-id'));
    var method = 'ADD';
    var cartId = 0;
    if($(this).hasClass('is-active')){
        method = 'DELETE';
        cartId = parseInt($(this).attr('data-cartid'));
    }
    $.ajax({
        type: 'GET',
        url: '/local/ajax/ajax.php?AJAX=Y&CART=Y&PRODUCT_ID=' + id + '&METHOD=' + method + '&CART_ID=' + cartId,
        
        dataType: 'json',
        success: function(data){
            
            this_element.attr('data-cartid', data.SUCCESS);
            if(data.SUCCESS){
                if(method == 'ADD'){
                    var countCart = parseInt($('.tools__item_cart').data('count')) + 1;
                    $('.tools__item_cart').data('count', countCart);
                    if(countCart<=99) $('.tools__item_cart .tools__link-tip').text(countCart);
                    else $('.tools__item_cart .tools__link-tip').text('99+');
                }
                else{
                    var countCart = parseInt($('.tools__item_cart').data('count')) - 1;
                    $('.tools__item_cart').data('count', countCart);
                    if(countCart<=99) $('.tools__item_cart .tools__link-tip').text(countCart);
                    else $('.tools__item_cart .tools__link-tip').text('99+');
                }
            }
        }
    });
    return false;
});


//добавление в корзину (карточка товара)
$(document).on('click', '.js-work__cart', function(){
    var this_element = $(this);
    var id = parseInt($(this).attr('data-id'));
    var method = 'ADD';
    var cartId = 0;
    if($(this).hasClass('is-active')){
        method = 'DELETE';
        cartId = parseInt($(this).attr('data-cartid'));
    }
    $.ajax({
        type: 'GET',
        url: '/local/ajax/ajax.php?AJAX=Y&CART=Y&PRODUCT_ID=' + id + '&METHOD=' + method + '&CART_ID=' + cartId,
        
        dataType: 'json',
        success: function(data){
            
            this_element.attr('data-cartid', data.SUCCESS);
            if(data.SUCCESS){
                if(method == 'ADD'){
                    var countCart = parseInt($('.tools__item_cart').data('count')) + 1;
                    $('.tools__item_cart').data('count', countCart);
                    if(countCart<=99) $('.tools__item_cart .tools__link-tip').text(countCart);
                    else $('.tools__item_cart .tools__link-tip').text('99+');
                    this_element.html(this_element.data('added_by'));
                    if(!this_element.hasClass('is-active')) this_element.addClass('is-active');
                    this_element.removeClass('js-work__cart');
                }
                else{
                    var countCart = parseInt($('.tools__item_cart').data('count')) - 1;
                    $('.tools__item_cart').data('count', countCart);
                    if(countCart<=99) $('.tools__item_cart .tools__link-tip').text(countCart);
                    else $('.tools__item_cart .tools__link-tip').text('99+');
                    this_element.html('<span class="hidden-tablet-down">'+this_element.data('to_add')+' </span>'+this_element.data('in_the_basket'));
                    if(this_element.hasClass('is-active')) this_element.removeClass('is-active');
                }
            }
        }
    });
    return false;
});

//добавление в избранное
$(document).on('click', '.js-catalog__favorites', function(){
    var this_element = $(this);
    var id = parseInt($(this).attr('data-id'));
    var method = 'ADD';
    var cartId = 0;
    if($(this).hasClass('is-active')){
        method = 'DELETE';
    }
    $.ajax({
        type: 'GET',
        url: '/local/ajax/ajax.php?AJAX=Y&FAVORITES=Y&PRODUCT_ID=' + id + '&METHOD=' + method,
        
        dataType: 'json',
        success: function(data){
            if(data.SUCCESS){
                if(method == 'ADD'){
                    var countFav = parseInt($('.tools__item_favorite').data('count')) + 1;
                    $('.tools__item_favorite').data('count', countFav);
                    if(countFav<=99) $('.tools__item_favorite .tools__link-tip').text(countFav);
                    else $('.tools__item_favorite .tools__link-tip').text('99+');
                    if(!this_element.hasClass('is-active')) this_element.addClass('is-active');
                    if(this_element.data('in_favorites')) $('.js-favorites__text').text(this_element.data('in_favorites'));
                }
                else{
                    var countFav = parseInt($('.tools__item_favorite').data('count')) - 1;
                    $('.tools__item_favorite').data('count', countFav);
                    if(countFav<=99) $('.tools__item_favorite .tools__link-tip').text(countFav);
                    else $('.tools__item_favorite .tools__link-tip').text('99+');
                    if(this_element.hasClass('is-active')) this_element.removeClass('is-active');
                    if(this_element.data('to_favorites')) $('.js-favorites__text').text(this_element.data('to_favorites'));
                }
            }
            
        }
    });
    return false;
});



//лайк
$(document).on('click', '.js-work__like', function(){
    var this_element = $(this);
    var id = parseInt($(this).attr('data-id'));
    var method = 'ADD';
    var cartId = 0;
    if($(this).hasClass('is-active')){
        method = 'DELETE';
    }
    $.ajax({
        type: 'GET',
        url: '/local/ajax/ajax.php?AJAX=Y&LIKES=Y&PRODUCT_ID=' + id + '&METHOD=' + method,
        
        dataType: 'json',
        success: function(data){
            if(data.SUCCESS){
                if(method == 'ADD'){
                    var countFav = parseInt($('.js-work__count_likes').text()) + 1;
                    $('.js-work__count_likes').text(countFav);
                    if(!this_element.hasClass('is-active')) this_element.addClass('is-active');
                }
                else{
                    var countFav = parseInt($('.js-work__count_likes').text()) - 1;
                    $('.js-work__count_likes').text(countFav);
                    if(this_element.hasClass('is-active')) this_element.removeClass('is-active');
                }
            }
            
        }
    });
    return false;
});


//больше не показывать картину
$(document).on('click', '.js-work__not_show', function(){
    var this_element = $(this);
    var id = parseInt($(this).attr('data-id'));
    var method = 'ADD';
    var cartId = 0;
    if($(this).hasClass('is-active')){
        method = 'DELETE';
    }
    $.ajax({
        type: 'GET',
        url: '/local/ajax/ajax.php?AJAX=Y&NOT_SHOW=Y&PRODUCT_ID=' + id + '&METHOD=' + method,
        
        dataType: 'json',
        success: function(data){
            if(data.SUCCESS){
                if(method == 'ADD'){
                    if(!this_element.hasClass('is-active')) this_element.addClass('is-active');
                    $('.js-not_show_text').text(this_element.data('return_picture'));
                }
                else{
                    if(this_element.hasClass('is-active')) this_element.removeClass('is-active');
                    $('.js-not_show_text').text(this_element.data('not_show'));
                }
            }
            
        }
    });
    return false;
});


//фильтр по авторам
$(document).on('change', '.js-filter-author__select',
    function(){
        var authors = $(this).val();
        

        $('.l-options-authors input').each(
            function(i, element){
                if($.inArray($(element).attr('id'), authors)==-1 && $(element).prop('checked')){
                    
                    $(element).trigger('click');
                }
            }
        );

        $(authors).each(
            function(i, element){
                if(!$('#' + element).prop('checked')){
                   
                    $('#' + element).trigger('click');
                }
            }
        );
        
    }
);

//фильтр по размеру
$(document).on('click', '.js-filter__size',
    function(){
        var arSize = $(this).data('values').split(',');
       
        var size = $(this).val().split(';');
        
        var flag = false;
        $(arSize).each(
            function(i, element){
                if(element==size['0']) flag = true;
                if(flag){
                    if(!$('.l-options-size [data-size="'+element+'"]').prop('checked')) $('.l-options-size [data-size="'+element+'"]').click();
                }
                else{
                    if($('.l-options-size [data-size="'+element+'"]').prop('checked')) $('.l-options-size [data-size="'+element+'"]').click();
                }
                if(element==size['1']) flag = false;
            }
        );
    }
);

//поделиться по email на странице товара
$('.popup__content-wrapper--share_mail').on('submit', function (e) {
    var this_form = $(this);
    var mail = this_form.find('[name="email"]').val();
    var name = $('.js-popup-share_mail').data('name');
    var link = $('.js-popup-share_mail').data('link');
    var img = $('.js-popup-share_mail').data('img');
    var text = encodeURIComponent($('.js-popup-share_mail').data('text'));
    $.ajax({
        type: 'GET',
        url: '/local/ajax/ajax.php?AJAX=Y&SHARE_MAIL=Y&MAIL=' + mail + '&NAME=' + name + '&LINK=' + link + '&IMG=' + img + '&TEXT=' + text,
        
        dataType: 'json',
        success: function(data){
            if(data.SUCCESS){
                $('.popup__content-wrapper--share_mail').html($('.popup__content-wrapper--share_mail_success').html());
                setTimeout(
                    function(){
                        $('.popup__close').trigger('click');
                    },
                    2000
                );
            }
            
        }
    });
});


//выбор города в оформлении заказа
$(document).on('input', "#soa-property-5", function(){
    $('#soa-property-5').parent('.form__field').addClass('is-loader');
});

$("#soa-property-5").autocomplete({
    source : function(request, response) {
        $.ajax({
            url : "https://api.cdek.ru/city/getListByTerm/jsonp.php?callback=?",
            dataType : "json",
            data : {
                q : function() {
                    return $("#soa-property-5").val()
                },
                name_startsWith : function() {
                    return $("#soa-property-5").val()
                }
            },
            success : function(data) {
                $('#soa-property-5').parent('.form__field').removeClass('is-loader');
                response($.map(data.geonames, function(item) {
                    return {
                        label : item.name,
                        value : item.name,
                        id : item.id
                    }
                }));
            }
        });
    },
    minLength : 1,
    select : function(event, ui) {
        setTimeout(
            function(){
                var cityName = ui.item.value;
                var cityId = ui.item.id;
                if(cityName && cityId){
                    $.ajax({
                        type: 'GET',
                        url: '/local/ajax/update_id_city_user.php?AJAX=Y&CITY_NAME='+cityName+'&CITY_ID=' + cityId,
                        
                        dataType: 'json',
                        success: function(data){
                            if(data.SUCCESS){
                                BX.Sale.OrderAjaxComponent.selectDelivery(event);
                            }
                            
                        }
                    });
                }
            }, 200
        );
    }
});

$(document).on('input', "#city", function(){
    $('#city').parent('.form__field').addClass('is-loader');
});


//выбор города при добавлении картины
$("#city").autocomplete({
    source : function(request, response) {
        $.ajax({
            url : "https://api.cdek.ru/city/getListByTerm/jsonp.php?callback=?",
            dataType : "json",
            data : {
                q : function() {
                    return $("#city").val()
                },
                name_startsWith : function() {
                    return $("#city").val()
                }
            },
            success : function(data) {
                $('#city').parent('.form__field').removeClass('is-loader');
                response($.map(data.geonames, function(item) {
                    return {
                        label : item.name,
                        value : item.name,
                        id : item.id,
                        region : item.regionName
                    }
                }));
            }
        });
    },
    minLength : 1,
    select : function(event, ui) {
        console.log(ui.item);
        $("#city").val(ui.item.value);
        $("#location_id_hidden").val(ui.item.id);
        $("#region_hidden").val(ui.item.region);
        
    }
});




/*$(document).on('click', '.btn_cart_order_right', function(){
    $('.btn_cart_order_save').trigger('click');
});*/

$(document).on("click",".js-subscribe-news",function(e){
    e.preventDefault();
    var self = $(this);
    var type = self.hasClass("unsubscribe") ? "unsubscribe" : "subscribe";
    $.ajax({
        url: "/local/ajax/subscribe_on_news.php",
        dataType: "json",
        method: "POST",
        data:{
            "sessid": BX.bitrix_sessid(),
            "action" : type,
            "lang": $(this).data("lang"),
        }
    }).done(function(data){
        if(data.HTML){
            var idPopup = Math.floor(Math.random() * (99999 - 11111 + 1)) + 11111;
            $(".popup .popup__window").append('<div class="popup__content popup__content--' + idPopup + '">' + data.HTML + "</div>");
            $('.popup__content').hide();
            $('.popup__content--' + idPopup).show();
            $(".popup").fadeIn(300);
        }
        if(data.STATUS){
            if(self.hasClass("unsubscribe")){
                self.text(BX.message('SUBSCRIBE')).removeClass("unsubscribe");
            }else{
                self.text(BX.message('UNSUBSCRIBE')).addClass("unsubscribe");
            }
        }
    });
});

$(document).on("click",".js-popup-share_mail",function(e){
    e.preventDefault();
    $(document).trigger('showPopup', ['share_mail']);
});

//подтверждение удаления работы (мои работы)
$(document).on("click",".js-work__delete",function(){
    var idWork = $(this).data('id');
    var nameWork = $(this).data('name');
    $('#work_delete_id').data('id', idWork);
    $('.work-delete__name').text(nameWork);
    $(document).trigger('showPopup', ['delete_confirm']);
    setTimeout(function () {
        $('.popup__close').click();
    }, 5000); // время в мс
        
});

//удаление работы (мои работы)
$('.popup__content-wrapper--delete_confirm').on('submit', function (e) {
    var this_form = $(this);
    
    var idWork =  $('#work_delete_id').data('id');
    
    $.ajax({
        type: 'GET',
        url: '/local/ajax/ajax.php?AJAX=Y&DELETE_WORK=Y&WORK_ID=' + idWork,
        
        dataType: 'json',
        success: function(data){
            if(data.SUCCESS){
                
                $('.popup__content-wrapper--delete_confirm').html($('.popup__content-wrapper--delete_confirm_success').html());
                var pageNum = $('#lk-works__ajax').data('page');
                var sort = $('.js-sort__work_list').val();
                $.ajax({
                    type: 'GET',
                    url: location.pathname + '?AJAX=Y&PAGEN_1=' + pageNum + '&SORT=' + sort,
                    dataType: 'html',
                    success: function(data){
                        $('#lk-works__ajax').html(data);
                    }
            
                });
                
            }
            
        }
    });
    return false;
});


$(document).on("click",".js-confirm-email",function(e){
    e.preventDefault();
    var self = $(this);
    $.ajax({
        url: "/local/ajax/send_email_confirm.php",
        dataType: "json",
        data: {
            lang: BX.message("SITE_ID")
        }
    }).done(function(data){
        if(data.MESSAGE){
            self.closest("div").text(data.MESSAGE);
            setTimeout(function(){
                $(".js-close-confirm_email").click();
            },2000);
        }
    });
});

//использование cokie
$(document).on('click', '.js-l-message-close-cookie', function(){
    setCookie('cookie_close', 'y', {}, 60*60*24*7);
});

$(document).on('click', '.js-l-message-close-statuses-cookie', function(){
    if(getCookie($(this).data('id')) == undefined) {
        setCookie($(this).data('id'), 'y', {}, 60*60*4);
    }
});


//сохранение в cookie статуса закрытия окна уведомления о подтверждении почты
$(document).on('click', '.js-cookie-confirm-email', function(){
    setCookie('cookie_confirm_email_close', 'y', {}, 60*60*4);
});

function getCookie(name) {
    if (name == '') return '';
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0)
            return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return '';
}

function setCookie(name, value, options, time) {
    options = options || {};
    options.expires = time;
    var expires = options.expires;
    options.path = '/';

    if (typeof expires == "number" && expires) {
      var d = new Date();
      d.setTime(d.getTime() + expires * 1000);
      expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
      options.expires = expires.toUTCString();
    }
  
    value = encodeURIComponent(value);
  
    var updatedCookie = name + "=" + value;
  
    for (var propName in options) {
      updatedCookie += "; " + propName;
      var propValue = options[propName];
      if (propValue !== true) {
        updatedCookie += "=" + propValue;
      }
    }
  
    document.cookie = updatedCookie;
}
$(document).on("click",".js-social-link",function(e){
    e.preventDefault();
    var href = $(this).attr("href");
    var width = "550";
    var height = "420";
    window.open(href, "", "scrollbars=1,resizable=1,menubar=0,toolbar=0,status=0,left=" + (screen.width - width) / 2 + ",top=" + (screen.height - height) / 2 + ",width=" + width + ",height=" + height);
});

//описание платежных систем
$(document).on('change', '[name="PAY_SYSTEM_ID"]', function(){
    var text = $(this).next('.block-radio__main').find('.block-radio__desc').text();
    $('.pay_item-description').text(text);
});

//кнопка наверх
$('.up-button__block').click(
    function(){
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        
        return false;
    }
);

$(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
        $('.up-button__block').fadeIn();
    } else {
        $('.up-button__block').fadeOut();
    }
});

$(document).ready(
    function(){
        if ($(window).scrollTop() > 100) {
            $('.up-button__block').fadeIn();
        } else {
            $('.up-button__block').fadeOut();
        }  
    }
);

/*выбор города*/
$('.tool-location__link').click(
    function(){
        $(document).trigger('showPopup', ['city__select']);
        return false;
    }
);
