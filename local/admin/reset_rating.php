<?require_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php";
$APPLICATION->SetTitle("Сброс рейтинга");
ini_set("display_errors", true);
ignore_user_abort(1);
set_time_limit(0);

\Bitrix\Main\Loader::IncludeModule("hb.site");
use HB\helpers\RatingHelpers;

IncludeModuleLangFile(__FILE__);
$aTabs = array(array("DIV" => "edit1", "TAB" => "Сброс рейтинга", "ICON" => "sale", "TITLE" => "Введите ID картины", "SORT" => 1));
$tabControl = new CAdminTabControl("tabControl", $aTabs);
require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php';

if ($_REQUEST["reset"] && $_REQUEST['work_id']) {
    $reset_result = RatingHelpers::resetWorkRating($_REQUEST['work_id']);
    if($reset_result['STATUS'] == true) {
        echo "<h3>Рейтинг успешно сброшен.</h3>";
    } else {
        echo "<h3>Ошибка во время сброса рейтинга</h3></br>";
        echo $reset_result['ERROR'];
    }
    echo "<br>";
}
?>


<form method="post" action="<?echo $APPLICATION->GetCurPage() ?>" enctype="multipart/form-data" name="post_form" id="post_form">
    <?
    echo bitrix_sessid_post();
    $tabControl->Begin();
    $tabControl->BeginNextTab();
    ?>
    <tr>
        <label for="work_id"><b>ID картины: </b></label>
    </tr>
	<tr>
        <input type="number" id="work_id" name="work_id" size="50">
    </tr>
	<?$tabControl->buttons();?>
    <input type="submit"
        name="reset"
        value="Сбросить"
        title="Сбросить"
        class="adm-btn-save"
    />
    <?$tabControl->end();?>
</form>
<?require $_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/epilog_admin.php";?>