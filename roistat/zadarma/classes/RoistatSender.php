<?php
/**
 * Created by PhpStorm.
 * User: Вячеслав
 * Date: 18.04.2019
 * Time: 15:01
 */

class RoistatSender{
    CONST INTEGRATION_KEY = "MTkxODg5OjEyNzU3MDplOGY5YzkyNjA4NjBjZDY1YTUwNTgzZDlmMWQ0ZTFkZg==";
    CONST DEBUG_URL = "http://webhook.site/91082f4e-09bf-454c-bc37-9cb35ba45c9e";
    CONST ROISTAT_KEY = "3e832077599b0402767d595c23125b10";
    CONST ROISTAT_PROJECT = "127570";

    /**
     * RoistatSender constructor.
     */
    public function __construct()
    {

    }


    /**
     * Обработка данных с колтрекинга
     * @param $data
     */
    public static function processCalltracking($data,$managerId){

        $visit = isset($data['visit_id'])?(string)$data['visit_id']:null;
        if(empty($visit)){
            $visit = isset($data['marker'])?(string)$data['marker']:null;
        }
        $roistatData = array(
            'roistat_visit' => $visit,
            'form'          => 'Звонок',
            'title'         => "Звонок от {$data['caller']}",
            'name'          => 'Неизвестный контакт',
            'email'         => '',
            'phone'         => $data['caller'],
            'comment'       => '',
            'fields'=>array(
                'UF_CRM_1561714199'=>'{city}', //Город
                'STAGE_ID'=>'NEW', //статус
                'ASSIGNED_BY_ID'=>$managerId, //статус
            )
        );

        self::sendToRoistat($roistatData);

        //self::addCalltrackingRecord($data_to_history);
    }

    /**
     * Отправка данных в историю звонков Ройстат
     *
     * @param $data
     */
    private static function addCalltrackingRecord($data){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://cloud.roistat.com/api/v1/project/phone-call?key=".self::ROISTAT_KEY."&project=".self::ROISTAT_PROJECT,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
    }

    /**
     * Отправка данных в проксилид Ройстат
     * @param $data
     */
    public static function sendToRoistat($data){
        $roistatData = array(
            'roistat' => $data['roistat_visit'],
            'key'     => self::INTEGRATION_KEY, // Ключ для интеграции с CRM, указывается в настройках интеграции с CRM.
            'title'   => isset($data['title'])?$data['title']:"Заявка с '{$data['form']}'", // Название сделки
            'comment' => $data['comment'], // Комментарий к сделке
            'name'    => $data['name'], // Имя клиента
            'email'   => $data['email'], // Email клиента
            'phone'   => $data['phone'], // Номер телефона клиента
            'fields'  => $data['fields']
        );

        file_get_contents("https://cloud.roistat.com/api/proxy/1.0/leads/add?" . http_build_query($roistatData));
    }

    /**
     * Отправка строки для дебага
     * @param $debug_str
     */
    public static function sendDebug($debug_str){

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, self::DEBUG_URL);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $debug_str);
        curl_exec($curl);
        curl_close($curl);

    }

}
